package utils.database;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class OracleDataSource extends DataSourceFactory {
    private final static Logger logger = Logger.getLogger(OracleDataSource.class);
    private static DataSource dataSource;

    public static OracleDataSource instance = null;

    public OracleDataSource() {
        dataSource = DataSourceFactory.getOracleDataSource();
    }


    public Connection getConnection() {
        dataSource = DataSourceFactory.getOracleDataSource();
        Connection con;
        try {
            logger.info("Start to connect to DB.");
            con = dataSource.getConnection();
        } catch (Exception ex) {
            throw new RuntimeException("could not connect to the ORACLE database - " + ex.getMessage());
        }
        return con;
    }

    public String getStringValue(Connection con, String query, String columnLabel) {
        Statement statement;
        String res = null;
        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                res = rs.getString(columnLabel);
            }
            return res;
        } catch (SQLException e) {
            throw new RuntimeException("could not find your result:  " + e.getMessage());
        }
    }

    public int getIntValue(Connection con, String query, String columnLabel) {
        Statement statement;
        int res = 0;
        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                res = rs.getInt(columnLabel);
            }
            return res;
        } catch (SQLException e) {
            throw new RuntimeException("could not find your result:  " + e.getMessage());
        }
    }


    /**
     * This method is to execute query in Oracle database
     * @param inputFile your sql file name
     */
    public void executeSqlScript(File inputFile) {
        Connection conn = getConnection();
        StringBuffer sb = new StringBuffer();
        String delimiter = ";";
        Scanner scanner;
        try {
            scanner = new Scanner(inputFile).useDelimiter(delimiter);
            Statement currentStatement = null;
            while (scanner.hasNext()) {
                String rawStatement = scanner.next() + delimiter;
                sb.append(rawStatement);

            }
            sb.deleteCharAt(sb.length() - 1);
            //logger.info("Query send to DB is \n\n" + sb.toString());//only for debugging.
            try {
                currentStatement = conn.createStatement();
                currentStatement.execute(sb.toString());
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                // Release resources
                if (currentStatement != null) {
                    try {
                        currentStatement.close();
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            scanner.close();
        }
        catch (FileNotFoundException e) {
            e.getMessage();
        }
    }

    public static OracleDataSource getInstance() {
        if(instance == null) {
            instance = new OracleDataSource();
        }
        return instance;
    }

}
