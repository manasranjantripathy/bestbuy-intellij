package utils.database;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;

public class MysqlDataSource extends DataSourceFactory {
    private final static Logger logger = Logger.getLogger(MysqlDataSource.class);
    private static DataSource dataSource;

    public static MysqlDataSource instance = null;

    MysqlDataSource() {
        dataSource = DataSourceFactory.getMySQLDataSource();
    }

    public Connection getConnection() {
        Connection con;
        try {
            con = dataSource.getConnection();
        } catch (Exception ex) {
            throw new RuntimeException("could not connect to the MYSQL database - " + ex.getMessage());
        }
        return con;
    }

    public String getStringValue(Connection con, String query, String columnLabel) {
        Statement statement;
        String res = null;
        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                res = rs.getString(columnLabel);
            }
            return res;
        } catch (SQLException e) {
            throw new RuntimeException("could not find your result:  " + e.getMessage());
        }
    }

    public int getIntValue(Connection con, String query, String columnLabel) {
        Statement statement;
        int res = 0;
        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                res = rs.getInt(columnLabel);
            }
            return res;
        } catch (SQLException e) {
            throw new RuntimeException("could not find your result:  " + e.getMessage());
        }
    }

    public static MysqlDataSource getInstance() {
        if(instance == null) {
            instance = new MysqlDataSource();
        }
        return instance;
    }

}

