package utils.database;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class DataSourceFactory {

    protected static DataSource createDataSource(String driver, String url, String username, String password) {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        return ds;
    }

    protected static DataSource getMySQLDataSource() {
        Properties props = new Properties();
        BasicDataSource mysqlDS = null;
        FileInputStream fis;
        try {
            fis = new FileInputStream("ecomm-ui-automation/ecomm_api/src/main/resources/db.properties");
            props.load(fis);
            mysqlDS = new BasicDataSource();
            mysqlDS.setDriverClassName(props.getProperty("MYSQL_DB_DRIVER_CLASS"));
            mysqlDS.setUrl(props.getProperty("MYSQL_DB_URL"));
            mysqlDS.setUsername(props.getProperty("MYSQL_DB_USERNAME"));
            mysqlDS.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mysqlDS;
    }

    //Check in the future: properties file in different module, are not loading.
   /* protected static DataSource getOracleDataSource() {
        Properties props = new Properties();
        BasicDataSource oracleDS = null;
        InputStream fis;
        try {
            fis = new FileInputStream("ecomm_automation/src/test/resources/db.properties");
            props.load(fis);
            oracleDS = new BasicDataSource();
            oracleDS.setDriverClassName(props.getProperty("ORACLE_DB_DRIVER_CLASS"));
            oracleDS.setUrl(props.getProperty("ORACLE_DB_URL"));
            oracleDS.setUsername(props.getProperty("ORACLE_DB_USERNAME"));
            oracleDS.setPassword(props.getProperty("ORACLE_DB_PASSWORD"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return oracleDS;
    }*/

    protected static DataSource getOracleDataSource() {
        BasicDataSource oracleDS = new BasicDataSource();
        oracleDS.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        oracleDS.setUrl("jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = ecommtcls01-scan.ca.bestbuy.com)(PORT = 50000))(CONNECT_DATA = (SERVICE_NAME = dbs_cwbt03)))");
        oracleDS.setUsername("fsvr");
        oracleDS.setPassword("fsvr99");
        return oracleDS;
    }

}