package utils;

/**
 * Created by sucho on 8/16/2017.
 */
public class TestUser {
    public static String postalCodeBC = "V5K 0A1";
    public static String postalCodeQC = "G1G 1G1";
    public static String postalCodeON = "M1N 1M1";
    public static String postalCodeMB = "R2R 1R1";
    public static String postalCodePaypal = "M5A 1E1";
    public static String firstName = "Charlie";
    public static String lastName = "Brown";
    public static String firstNamePaypal = "EnglishTester";
    public static String lastNamePaypal = "EnflishTester";
    public static String suiteName = "123";
    public static String fullName = firstName + " " + lastName;
    public static String streetAddress = "ATS - 8800 GlenLyon Parkway";
    public static String city = "Burnaby";
    public static String province = "British Columbia";
    public static String visaUSProvince = "US";
    public static String provinceVisaCheckout = "BC";
    public static String provinceYukon = "Yukon";
    public static String country = "Canada";
    public static String cityProvincePostalCode = "Burnaby,  British Columbia,  V5K 0A1";
    public static String fullAddress = "ATS - 8800 GlenLyon Parkway, Burnaby BC V5K0A1 Canada";
    public static String phone0 = "604";
    public static String phone1 = "111";
    public static String phone2 = "1111";
    public static String phone3 = "123456789";
    public static String fullPhone1 = "(604)111-1111Extension123456789";
    public static String otherPhone0 = "604";
    public static String otherPhone1 = "222";
    public static String otherPhone2 = "2222";
    public static String otherPhone3 = "123456789";
    public static String fullPhone2 = "(604)222-2222Extension123456789";
    public static String visaCheckoutCustomerPhone = "7789999999";
    public static String email = "atsdonotusemanually1@yahoo.ca"; //email = "atsdonotusemanually1@yahoo.ca";
    public static String visaUsername =  "atsdonotusemanually2@yahoo.com";
    public static String VisaCheckoutVBVUsername = "bby_visacheckout@domain.com";
    public static String email2 = "bbytester2@yahoo.ca";
    public static String email3 = "mccptest1@yahoo.com";
    public static String password = "bestbuy2013";
    public static String password2 = "bestbuy2016";
    public static String password3 = "mccp@1234";
    public static String visaCheckoutCustomerPassword = "mypassword2013";
    public static String newEmail;
    public static String wishListItem;
    public static String creditCard1;
    public static String creditCard2;
    public static String inactivatedEmail = "deactivatedaccount1@yahoo.com";
    public static String defaultGuestUser = "aaa@domain.com";
    public static String productTitle;
    public static String productTitle2;
    public static String productTotal;
    public static String subTotal;
    public static String orderNumber;
    public static String countryUS = "United States";
    public static String defaultUSCity = "New York";
    public static String defaultUSPostalCode = "10001";
    public static String defaultUSProvince = "New York";
    public static Double subTotalPrice;
    public static String lastFourDigitVisaCardNumber = "0002";
    public static String subCategory;
    public static String currentPageNumber;

    public TestUser(){

    }

}