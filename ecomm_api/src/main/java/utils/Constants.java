package utils;

public class Constants {

    static String testData;

    static String[] regressionTestData;

    public static void getConstants(String data) {

        testData = data;
    }

    public static void getConstants(String[] data) {

        regressionTestData = data;
    }

    public enum Sku {
        backOrderSkuKeyword("Logitech iPad"),
        backOrderSku("10248114"),
        bgEhfPspIspuSku("10204251"),
        bgAutoFree("10236214"),
        bundleSku("B0010073"),
        bgEhfSku("10204251"),
        bgLessThan20DollarsNoEhfSku("10175595"),
        bundleComponentSku1("10253079"),
        bundleComponentSku2("10253081"),
        multiLineSku1("10273377"),
        multiLineSku2("10131815"),
        multiLineSku3("10084333"),
        multiLineSku4("10087739"),
        multiLineSku5("10204251"),
        multiLineSku6("10105723"),
        multiLineSku7("10089195"),
        multiLineSku8("10074835"),
        multiLineSku9("10017601"),
        multiLineSku10("10027825"),
        multiLineSku11("10070796"),
        bundleComponentNonEhfSku("10275589"),
        defaultSku("10275322"),
        esrbSkuGame("10196287"),
        expiredSku("10268123"),
        comingSoonSku("10165255"),
        noBgFrenchSku("10275263"),
        noRateNoReviewSku("10273179"),
        prodSku("10246561"),
        plpTitleProductsTab("Search Products for"),
        plpTitleArticlesAndBlogsTab("Search Articles & Blogs for"),
        plpTitleForumsTab("Search Forums for"),
        plpTitleHelpTopicsTab("Search Help Topics for"),
        simpleDiscountSku("10190746"),
        smoketestSku("10275322"), //10204251
        preOrderSkuKeyword("[ATS]-Sharp Aquos 70\" 4K Ultra HD 120Hz Smart 3D LED TV (LC70UD1U)"),
        preOrderSku("10268313"),
        plpAvailableSkuKeyword("Wolverine (Blu-ray) (2009)"),
        plpAvailableSku("M2169202"),
        plpSoldOutInNearbyStoresSku("10238215"),
        pim18Sku("10214484"),
        limitedQuantitiesInStockSkuKeyword("Canon EOS 70D 20.2MP Digital"),
        limitedQuantitiesInStockSku("10246849"),
        wgEhfIspuSku("10244030"),
        wgSku("10244682"),
        vdSku("10027825"),
        disku("10247707"),
        wgEhfSku2("10211452"),
        wgEhfSku3("10243928"),
        wgEhfSku4("10275322"),
        wgEhfSku5("10317672"),
        wgEhfSku6("10303360"),
        warrantysku("10275263"),
        marketplacesku("10560477"),
        diMultipleImagesSku("10246520"),
        searchKeyword("iPod"),
        keywordCan("can"),
        serviceSku("10246922"),
        cusRevSku("10202313"),
        householdLimitSku("10229780"),
        shippingLevelDiscountSku("10183283"),
        cameraSku("10246850"),
        countdownLimitZonalWgSku("10195674"),
        wgEhfPspNonIspuNonConstituentSku("10199149"),
        webExclusiveSku("10204251"),
        bgSku("10273762"),
        preOrderSkuWithAgeVerification("M2220034");

        private final String sku;

/*Sku(final String sku) {
            this.sku = sku;
        }*/

        @Override
        public String toString() {
            return sku;
        }

        Sku(String data) {
            // Calling the static method on the outer type
            this.sku = data;
            Constants.getConstants(data);
        }
    }

    public enum Payment {
        AMEX_CREDITCARD_TYPE("American Express"),
        AMEX_CREDITCARD_TYPE2("amex"),
        AMEX_CREDITCARD_NUMBER("373953192351004"),
        AMEX_EXPIRYDATE_MONTH("02"),
        AMEX_EXPIRYDATE_YEAR("2025"),
        AMEX_EXPIRYDATE_MONTH2("02"),
        AMEX_EXPIRYDATE_YEAR2("2024"),
        AMEX_CID("1234"),
        MC_CREDITCARD_TYPE("MasterCard"),
        MC_CREDITCARD_TYPE2("mc"),
        MC_CREDITCARD_NUMBER("5204730000002449"),
        MC_EXPIRYDATE_MONTH("02"),
        MC_EXPIRYDATE_YEAR("2025"),
        MC_CID("123"),
        PLCC_CREDITCARD_TYPE("Best Buy or Future Shop"),
        PLCC_CREDITCARD_NUMBER("4530949000007020"),
        PLCC_EXPIRYDATE_MONTH("02"),
        PLCC_EXPIRYDATE_YEAR("2025"),
        PLCC_CID("123"),
        VISA_CREDITCARD_TYPE("Visa"),
        VISA_CREDITCARD_NUMBER("4111111111111111"),
        VISA_EXPIRYDATE_MONTH("02"),
        VISA_EXPIRYDATE_YEAR("2025"),
        DEFAULT_EXPIRYDATE_VISACHECKOUT("1024"),
        VISA_CID("123"),
        BBYCC_CARD_TYPE("bbycc"), //PLCC
        BBYCC_CARD_NUMBER("4530949000351014"),
        BBYCC_EXPIRYDATE_MONTH("02"),
        BBYCC_EXPIRYDATE_YEAR("2025"),
        BBYCC_CID("123"),
        VISA2_CREDITCARD_TYPE("Visa"),
        VISA2_CREDITCARD_NUMBER("4501105720109"),
        VISA2_EXPIRYDATE_MONTH("03"),
        VISA2_EXPIRYDATE_YEAR("2025"),
        VISA2_CID("123"),
        PAYPAL_USEREMAIL("bbytester1@yahoo.ca"),
        PAYPAL_US_USEREMAIL("bbytesterUS@yahoo.ca"),
        PAYPAL_PASSWORD("bestbuy2013"),
        GIFTCARD1_NUMBER("7777085272216926"), //$100
        GIFTCARD1_CID("3652"),
        GIFTCARD2_NUMBER("7777085272228767"), //$10
        GIFTCARD2_CID("4593"),
        GIFTCARD3_NUMBER("7777085272092300"), //$300
        GIFTCARD3_CID("9938"),
        GIFTCARD4_NUMBER("7777085272183046"), //$10000
        GIFTCARD4_CID("2872"),
        DEFAULT_REWARD_ZONE_ID("1244009474137"),
        PROMOTION_CODE1("ATS_PromoCode"),
        PROMOTION_CODE2("QA_Promo"),
        PROMOTION_CODE_VISA_CHECKOUT("ATS_PromoCode"),
        NULL("");

        private final String payment;

        Payment(final String data) {
            // Calling the static method on the outer type
            this.payment = data;
            Constants.getConstants(data);
        }

       /* MethodOfPayment(final String payment) {
            this.payment = payment;
        }*/

        @Override
        public String toString() {
            return payment;
        }
    }

    public enum AddressInformation {
        BC_PROVINCE("BC"),
        AB_PROVINCE("AB"),
        NT_PROVINCE("NT"),
        NU_PROVINCE("NU"),
        YT_PROVINCE("YT"),
        NB_PROVINCE("NB"),
        NL_PROVINCE("NL"),
        NS_PROVINCE("NS"),
        ON_PROVINCE("ON"),
        PE_PROVINCE("PE"),
        MB_PROVINCE("MB"),
        SK_PROVINCE("SK"),
        QC_PROVINCE("QC"),
        AB_POSTALCODE("T2M 2M2"),
        NT_POSTALCODE("X0E 0T0"),
        NU_POSTALCODE("X0A 0L0"),
        YT_POSTALCODE("Y0B 1G0"),
        NB_POSTALCODE("E1E 1E1"),
        NL_POSTALCODE("A1A 1A1"),
        NS_POSTALCODE("B3K 1M3"),
        PE_POSTALCODE("C1C 1C1"),
        SK_POSTALCODE("S7H 4Z8"),
        BC_POSTALCODE("V5K 0A1"),
        QC_POSTALCODE("G1G 1G1"),
        ON_POSTALCODE("M1N 1M1"),
        MB_POSTALCODE("R2R 1R1"),
        FIRST_NAME("Charlie"),
        LAST_NAME("Brown"),
        SUITE_NAME("123"),
        FULL_NAME(FIRST_NAME + " " + LAST_NAME),
        STREET_ADDRESS("ATS - 8800 GlenLyon Parkway"),
        CITY("Burnaby"),
        BC_PROVINCE_EN("British Columbia"),
        Yokun_PROVINCE_EN("Yukon"),
        BC_PROVINCE_FR("Colombie-Britannique"),
        FULL_ADDRESS("ATS - 8800 GlenLyon Parkway, Burnaby BC V5K0A1 Canada"),
        PHONE0("604"),
        PHONE1("111"),
        PHONE2("1111"),
        PHONE3("123456789"),
        OTHER_PHONE0("604"),
        OTHER_PHONE1("222"),
        OTHER_PHONE2("2222"),
        OTHER_PHONE3("123456789"),
        EMAIL("atsdonotusemanually1@yahoo.ca"), //"atsdonotusemanually2@yahoo.com"
        PASSWORMD("bestbuy2013");


        private final String shippingInfo;

/*Sku(final String sku) {
            this.sku = sku;
        }*/

        @Override
        public String toString() {
            return shippingInfo;
        }

        AddressInformation(String data) {
            // Calling the static method on the outer type
            this.shippingInfo = data;
            Constants.getConstants(data);
        }
    }

    public enum TestEnvironment {
        STOREFRONT_TEST01("https://test1-bbyca.ca.bestbuy.com"),
        STOREFRONT_TEST01_EN("https://test1-bbyca.ca.bestbuy.com/en-CA/home.aspx"),
        STOREFRONT_TEST01_FR("https://test1-bbyca.ca.bestbuy.com/fr-CA/home.aspx"),

        STOREFRONT_TEST02("https://test2-bbyca.ca.bestbuy.com"),
        STOREFRONT_TEST02_EN("https://test2-bbyca.ca.bestbuy.com/en-CA/home.aspx"),
        STOREFRONT_TEST02_FR("https://test2-bbyca.ca.bestbuy.com/fr-CA/home.aspx"),

        STOREFRONT_TEST03("https://test3-bbyca.ca.bestbuy.com"),
        STOREFRONT_TEST03_EN("https://test3-bbyca.ca.bestbuy.com/en-CA/home.aspx"),
        STOREFRONT_TEST03_FR("https://test3-bbyca.ca.bestbuy.com/fr-CA/home.aspx"),

        STOREFRONT_TEST04("https://test4-bbyca.ca.bestbuy.com"),
        STOREFRONT_TEST04_EN("https://test4-bbyca.ca.bestbuy.com/en-CA/home.aspx"),
        STOREFRONT_TEST04_FR("https://test4-bbyca.ca.bestbuy.com/fr-CA/home.aspx"),

        STOREFRONT_TEST05("https://test5-bbyca.ca.bestbuy.com"),
        STOREFRONT_TEST05_EN("https://test5-bbyca.ca.bestbuy.com/en-CA/home.aspx"),
        STOREFRONT_TEST05_FR("https://test5-bbyca.ca.bestbuy.com/fr-CA/home.aspx"),


        CSR_TEST01("https://bca_s_automation_aut:Spe9Y3Ute7t4h9JrcuesfPJycU9CcW@test1-bbyca-csradmin.ca.bestbuy.com"),
        CSR_TEST02("https://bca_s_automation_aut:Spe9Y3Ute7t4h9JrcuesfPJycU9CcW@test2-bbyca-csradmin.ca.bestbuy.com"),
        CSR_TEST03("https://bca_s_automation_aut:Spe9Y3Ute7t4h9JrcuesfPJycU9CcW@test3-bbyca-csradmin.ca.bestbuy.com"),
        CSR_TEST04("https://bca_s_automation_aut:Spe9Y3Ute7t4h9JrcuesfPJycU9CcW@test4-bbyca-csradmin.ca.bestbuy.com"),
        CSR_TEST05("https://bca_s_automation_aut:Spe9Y3Ute7t4h9JrcuesfPJycU9CcW@test5-bbyca-csradmin.ca.bestbuy.com"),

        MOBILE_TEST01_EN("https://test1-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_TEST01_FR("https://test1-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_TEST02_EN("https://test2-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_TEST02_FR("https://test2-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_TEST03_EN("https://test3-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_TEST03_FR("https://test3-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_TEST04_EN("https://test4-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_TEST04_FR("https://test4-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_TEST05_EN("https://test5-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_TEST05_FR("https://test5-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_DI("https://di-ecomm-webapp.dev-ocp.ca.bestbuy.com"),
        MOBILE_DI_EN("https://di-ecomm-webapp.dev-ocp.ca.bestbuy.com/en-CA"),
        MOBILE_DI_FR("https://di-ecomm-webapp.dev-ocp.ca.bestbuy.com/fr-CA"),

        MOBILE_STG("https://beta-stg.bestbuy.ca"),
        MOBILE_STG_EN("https://beta-stg.bestbuy.ca/en-CA"),
        MOBILE_STG_FR("https://beta-stg.bestbuy.ca/fr-CA");

        private final String testEnv;

        @Override
        public String toString() {
            return testEnv;
        }

        TestEnvironment(String data) {
            // Calling the static method on the outer type
            this.testEnv = data;
            Constants.getConstants(data);
        }
    }

    public enum TestPlatform {
        STOREFRONT("storefront"),
        CSR("csr"),
        MOBILE("mobile"),
        TABLET_600_800("tablet_600_800"),
        SMALL_TABLET_768_1024("small_tablet_768_1024"),
        MEDIUM_TABLET_1024_1366("medium_tablet_1024_1366"),
        LARGE_1366_768("large_1366_768"),
        XLARGE_1920_1080("xlarge_1920_1080");

        private final String testPlatform;

        @Override
        public String toString() {
            return testPlatform;
        }

        TestPlatform(String data) {
            // Calling the static method on the outer type
            this.testPlatform = data;
            Constants.getConstants(data);
        }

    }

    public enum TestLanguage {
        EN("en"),
        ENGLISH("english"),
        FR("fr"),
        FRENCH("french");

        private final String testLanguage;

        @Override
        public String toString() {
            return testLanguage;
        }

        TestLanguage(String data) {
            // Calling the static method on the outer type
            this.testLanguage = data;
            Constants.getConstants(data);
        }

    }

    public enum Property {
        TEST_PLATFORM("testPlatform"),
        TEST_ENVIRONMENT("testEnvironment"),
        TEST_LANGUAGE("testLanguage");

        private final String property;

        @Override
        public String toString() {
            return property;
        }

        Property(String data) {
            // Calling the static method on the outer type
            this.property = data;
            Constants.getConstants(data);
        }

    }

    public enum TestDataValidation {
        CLIENT_ID("atspartner2@yahoo.com"),
        CLIENT_SECRET("bestbuy2013"),
        TOKEN_CONTENT_TYPE("application/x-www-form-urlencoded"),
        MEDIA_TYPE("application/vnd.bestbuy.v1+json"),
        LANGUAGE_TYPE("En"),
        BASE_URI("https://test3-bbyca-ssl-api.ca.bestbuy.com"),
        TOKEN_URI("/api/token"),
        CART_URL("/api/cart"),
        TOKEN_TYPE("token_type"),
        EXPIRES_IN("expires_in");

        private final String TestDataValidation;

        @Override
        public String toString() {
            return TestDataValidation;
        }

        TestDataValidation(String data) {
            // Calling the static method on the outer type
            this.TestDataValidation = data;
            Constants.getConstants(data);
        }
    }

    // This is no longer used as we are reading sku data from the excel spreadsheet.
    public enum RegressionTestData {
        ONLINE_SKUs(new String[]{"10188038","10271010","10268313","10197291","10203542","10222570","10228264",
                "10193049","10225115","10254740","10105613","10194635","M2193564","M2192547","M2098821","M2203131","M2008578", "M2208258","B0010074","11394485","11394487","B0010037","11393112","11393135","B0003173","10294110","10294115","10197489","10193047",
                "10193066","10205067", "10214484","10216330","10240506", "10074835", "10027825","10253079","10253081","10275589","10190746","10199149","10246922","10236214","10261690","10261691", "10261700","10203509","10178626",
                "10206477","10202572","10238215","11380180","10265497","10246688","11393289","10246586","10248114"}),
        RESERVE_IN_STORE_SKUs(new String[]{"10268123","10246561","10169436", "10148981","10201557","10193352",
                "10202118","10273179","10262566","10204333","10202313","10246561","10109541","10202313",
                "10239889","10273110","10265499","10276182",}),
        ONLINE_AND_RESERVE_IN_STORE_SKUs(new String[]{"B0010056","10275322","10246849","10204251","10244682","10273762",
                "10166641","10229786","10176113","10246505","M2164905","10222429", "10169880","M2165666","10244030","B0010048","M1269251","B0010073","10273377","10131815","10084333","10087739","10105723","10017601","10183283","10087739","10195674","M2198409","10175595", "10243928","10210926","10204253","10268312","M2220034","M2207201","10196287","10252450","10229780","10246850","10207570","10241461" ,"10219625","10265208","10089195" ,"M2057651","M2169202","B0010046","11393280"});

        private String[] regressionData;

        public String[] getArray() {
            return regressionData;
        }

        RegressionTestData(String[] arrays) {
            // Calling the static method on the outer type
            this.regressionData = arrays;
            Constants.getConstants(arrays);
        }
    }

    public enum Messages {
        ENGLISH("English"),
        FRENCH("Français"),
        SEARCH_HEADING("Results for "),
        SEARCH_HEADING_FR("Résultats de la recherche "),
        ERR_MSG_MISSING_CC_FIRST_NAME("First name contains one or more invalid characters. Only letters and certain special characters (e.g., apostrophe, hyphen, period, space) are accepted. Please check for accuracy and try again. (0012)"),
        ERR_MSG_MISSING_CC_LAST_NAME("Last name contains one or more invalid characters. Only letters and certain special characters (e.g., apostrophe, hyphen, period, space) are accepted. Please check for accuracy and try again. (0013)"),
        ERR_MSG_MISSING_CC_STREET_ADDRESS("Please enter an address. (0020)"),
        ERR_MSG_MISSING_CC_CITY("Cities can only contain letters, numbers and certain special characters. Please check for accuracy and try again. (0021)"),
        ERR_MSG_MISSING_CC_PROVINCE("Please select a Province. (0022)"),
        ERR_MSG_MISSING_CC_POSTAL_CODE("Your postal code seems to be entered incorrectly. Postal codes can only contain letters, numbers and spaces. Please check for accuracy and try again. (0014)"),
        ERR_MSG_MISSING_CC_PHONE("All three fields for Phone are required. Please check for accuracy and try again. (0027)"),
        ERR_MSG_PROVINCE_NOT_MATCHING_PC("We were unable to match the Province you selected with the Postal Code you entered. Please check for accuracy and try again. (0023)"),
        ERR_MSG_POSTAL_CODE_NOT_RECOGNIZED("We did not recognize the Postal Code you entered. Please check for accuracy and try again. (0024)"),
        CONFIRM_MSG_ADDED_CC("Thank You. A new Credit Card has been added."),
        MORE_ABOUT_CUSTOMER_REVIEWS("More about Customer Reviews"),
        ORIGINAL_POSTED_ON_BESTBUY("Originally posted on Bestbuy.com"),
        ERR_MSG_SIMPLE_PASSWORD("The password you’ve chosen is in a blacklist of simple passwords; please choose something more complex."),
        CONFIRM_MSG_ADDED_ADDRESS("Thank you. Changes have been made to your Address Book."),
        ADDRESS_TIPS_URL("are-there-any-tips-to-ensure-fast-delivery-of-my-order-to-the-correct-address"),
        ADDRESS_TIPS_HEADER("Are there any tips to ensure fast delivery of my order to the correct address?"),
        ERR_MSG_REACHED_MAX_ADDRESS("You currently have the maximum number of allowed addresses saved. Please either delete one address or edit an existing address. (0139)"),
        ERR_MSG_REACHED_MAX_CC("You cannot add another credit card because you already have the maximum number of cards saved to your profile. If you wish to add another credit card, you must first delete one or more credit cards from your profile, then try again. (0157)"),
        ERR_MSG_REACHED_MAX_CC_CHECKOUT("This credit card cannot be saved to your profile, because you have already entered the maximum number of credit cards. If you would still like to use this credit card, please de-select the"),
        TEXT_PRIVACY_REQUEST("This is for testing purpose"),
        PROVINCE_PRIVACY_INQUIRY("BC - British Columbia"),
        PROVINCE_NULL_PRIVACY_INQUIRY("-- Select --"),
        CONF_MSG_GUEST_CREATE_ACCOUNT("Your Account Has Been Created!"),
        ERR_MSG_MISSING_EMAIL("Please enter your email address using the following format: yourname@domain.com. (0033)"),
        CONFIRM_MSG_PRIVACY_INQUIRY("Thank you for your inquiry. You can expect to hear from our Privacy Manager by phone, mail or email within 30 days."),
        ERR_MSG_EMAIL_FORMAT("Please enter your email address using the following format: yourname@domain.com. (0033)"),
        ERR_MSG_DUPLICATE_EMAIL_CREATING("We’re sorry, we cannot create an account using that email address. Please try another email address. (0906)"),
        ERR_MSG_MIS_MATCH_EMAIL("Please make sure both your email entries match. (0035)"),
        ERR_MSG_MIS_MATCH("Please make sure both your new password entries match. (0036)"),
        PRE_ORDER("Pre-Order"),
        ONLINE_AVAILABILITY_OF_EXPIRED_SKU("Not Available: This item is no longer available for purchase."),
        CANNOT_RESERVE("Cannot Reserve"),
        ENGLISH_TEXT("English"),
        FRENCH_TEXT("Français"),
        PROMO_CODE_AVAILIBILITY("promo"),
        LOCAL_STORE_SECTION_WHEN_CANNOT_ADD_TO_CART_OR_RESERVE_BBY("Only available online."),
        COMING_SOON("Coming Soon: This item is not yet available for purchase."),
        ERR_MSG_OLD_PASSWORD("Please make sure both your new password entries match. (0036)"),
        ERR_MSG_SAME_PASSWORD("Your new password should not be the same as your old password. (0923)"),
        ERR_MSG_MISSING_PASSWORD("Your Password must be at least 6 characters long and must contain at least one digit and one uppercase or lowercase letter. Please try again. (0007)"),
        ERR_MSG_MISSING_RETYPE_PASSWORD("The Retype Password entry must be at least 6 characters long and must contain at least one digit and one uppercase or lowercase letter. Please try again. (0037)"),
        ERR_MSG_INVALID_PASSWORD_OR_EMAIL("We were unable to find a match for the e-mail address and/or password you entered. Please check for accuracy and try again. (0031)"),
        ERR_MSG_ACCOUNT_DEACTIVATED("This account has been inactivated. Please call Customer Care at 1-866-BEST BUY (1-866-2378-289) if you have questions. (0032)"),
        ERR_MSG_FORGET_MY_PASSWORD("Please enter your email address using the following format: yourname@domain.com. (0033)"),
        ALERT_MSG_REMOVED_PSP("was removed from your Cart because you removed"),
        ERR_MSG_SKU_HOUSEHOLD_LIMIT("0135: Sorry, only 1 Apple iPad mini 7.9\" 16GB With Wi-Fi - White can be purchased per order. Please update the quantity. If you’d like to order more, call Best Buy for Business at 1-877-423-3429."),
        MASKED_VISA2_NUMBER("************0109"),
        MASKED_MASTERCARD_NUMBER("************2449"),
        MASKED_VISA_NUMBER("************1111"),
        MASKED_GIFT_CARD1("********** 6926"),
        PROMOTIONAL_SAVINGS_IN_THE_CART("Promotional Savings:"),
        TOTAL_SAVINGS_IN_THE_CART("Total Savings"),
        SHIPPING_COST_TEXT("Shipping Total"),
        SHIPPING_COST_VALUE("Free"),
        SHIPPING_COST_TEXT_ORDERDETAILS("Shipping"),
        SHIPPING_COST_VALUE_ORDERDETAILS("No charge"),
        ORDER_SAVINGS_IN_THE_CART("Order Level Discount :"),
        ENVIRONMENTAL_HANDLING_FEES("Environmental Handling Fees:"),
        EHF_IN_ORDER_DETAILS("Surcharge"),
        PSP_ORDER_DETAILS("Protection Service Plan"),
        ISPU_MISSING_EMAIL("Please enter an email address. (0125)"),
        ISPU_MISSING_FIRST_NAME("First name contains one or more invalid characters. Only letters and certain special characters (e.g., apostrophe, hyphen, period, space) are accepted. Please check for accuracy and try again. (0012)"),
        ISPU_MISSING_LAST_NAME("Last name contains one or more invalid characters. Only letters and certain special characters (e.g., apostrophe, hyphen, period, space) are accepted. Please check for accuracy and try again. (0013)"),
        ISPU_MISSING_PHONE("All three fields for Phone are required. Please check for accuracy and try again. (0027)"),
        PAYMENT_CHANGE_CONFIRMATION("Your payment information has been changed."),
        SHIPPING_CHANGE_CONFIRMATION("Your shipping information has been changed."),
        CONF_MSG_ORDER_CANCELED("Your order has been cancelled."),
        VBV_UNSUCCESSFUL_AUTH("An error occurred sending the visa data to our system. Please use another card or try again later. (0174)"),
        MASTERCARD_UNSUCCESSFUL_AUTH("We've encountered an issue with the SecureCode you entered. Please enter it again or try a different payment method. (0176)"),
        ERR_MSG_PAYPAL_US_SHIPPING_ADDRESS("Unfortunately, we only ship in Canada"),
        CONF_MSG_REVIEW_SUBMITTED("Review submitted"),
        PRODUCT_COMPARE_ERR_MSG("Please select at least 2 items to compare");

        private final String messages;

        Messages(final String data) {
            // Calling the static method on the outer type
            this.messages = data;
            Constants.getConstants(data);
        }

        @Override
        public String toString() {
            return messages;
        }

    }

}