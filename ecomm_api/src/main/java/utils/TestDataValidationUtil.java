package utils;

import com.jayway.jsonpath.JsonPath;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import utils.Constants.TestDataValidation;
import utils.database.OracleDataSource;
import utils.excel.ExcelReader;
import utils.excel.SkuObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestDataValidationUtil {

    private final static Logger logger = Logger.getLogger(TestDataValidationUtil.class);

    private final static String ONLINE_INVENTORY = "inventory-scripts/OnlineInventory.sql";
    private final static String STORE_INVENTORY = "inventory-scripts/StoreInventory.sql";
    private final static String VENDOR_INVENTORY = "inventory-scripts/VendorInventory.sql";

    private final static String SKU_AVAILABILITY = "inventory-scripts/SkusAvailability.xlsx";

    private static TestDataValidationUtil instance = null;
    private static ExcelReader excel = ExcelReader.getInstance();

    private static Map<String, SkuObject> skuList = new HashMap<>();
    private static Map<String, SkuObject> failures = new HashMap<>();

    private static File[] files;
    private static String excelFile;


    public TestDataValidationUtil() {
        loadFiles();
    }

    private void loadFiles() {
        try {
            String scriptPath1 = this.getClass().getClassLoader().getResource(ONLINE_INVENTORY).getFile();
            String scriptPath2 = this.getClass().getClassLoader().getResource(STORE_INVENTORY).getFile();
            String scriptPath3 = this.getClass().getClassLoader().getResource(VENDOR_INVENTORY).getFile();
            String filePath = this.getClass().getClassLoader().getResource(SKU_AVAILABILITY).getFile();
            files = new File[]{new File(scriptPath1), new File(scriptPath2), new File(scriptPath3)};
            excelFile = filePath;
        } catch (Exception e) {
            throw new RuntimeException("Could not get files." + e.getMessage());
        }
    }

    private String getExcelFilePath() {
        return excelFile;
    }

    /**
     * This method is to check sku availability.
     *
     * @param sku
     * @return ClientResponse
     */
    public static ClientResponse validateSku(String sku, String postcode) {
        String locations = "242%7C705%7C952%7C973%7C227%7C13%7C212%7C994%7C216%7C941%7C228%7C958%7C241" +
                "%7C961%7C226&maxlos=3";
        String accessToken = Util.getAuthenticationToken();
        Client client = Client.create();
        WebResource webResource = client.resource
                (TestDataValidation.BASE_URI + String.format
                        ("/availability/products?callback=apiAvailability&skus=%s&postalCode=%s&locations=%s", sku,
                                postcode, locations));

        ClientResponse clientResponse = webResource
                .accept(TestDataValidation.MEDIA_TYPE.toString())
                .acceptLanguage(TestDataValidation.LANGUAGE_TYPE.toString())
                .header("Authorization", "bearer" + " " + accessToken)
                .get(ClientResponse.class);
        return clientResponse;

    }


    public static String getResponseBody(ClientResponse response) {
        String result = "";
        try {
            response.bufferEntity();
            String entity = response.getEntity(String.class);
            response.getEntityInputStream().reset();
            result = StringUtils.substringBetween(entity, "(", ")");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    public static String logAssertionError(final ClientResponse response) {
        try {
            response.bufferEntity();
            StringBuilder error = new StringBuilder();
            error.append("\n")
                    .append(response.getStatus())
                    .append(" ")
                    .append(response.getStatusInfo().toString())
                    .append("\n")
                    .append(response.getEntity(String.class))
                    .append("\n");
            response.getEntityInputStream().reset();
            return error.toString();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static boolean verifyPickupStatus(String responseBody, String sku) {
        String value;
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(responseBody);
            value = JsonPath.read(jsonObject, "$.availabilities[0].pickup.status");
            if (value.equals("InStock")) {
                logger.info(String.format("Sku of %s is available for reserving in store", sku));
                return true;
            } else {
                logger.error(String.format("Sku of %s is not available for reserving in store", sku));
                failures.put(sku, setFailedReason(sku, String.format("It is not available for reserving in store", sku)));
                return false;
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static boolean verifyShippingStatus(String responseBody, String sku) {
        String value;
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(responseBody);
            value = JsonPath.read(jsonObject, "$.availabilities[0].shipping.status");
            switch (value) {
                case "InStock":
                    logger.info(String.format("Sku of %s is available for %s", sku, value));
                    return true;
                case "BackOrder":
                    logger.info(String.format("Sku of %s is available for %s", sku, value));
                    return true;
                case "PreOrder":
                    logger.info(String.format("Sku of %s is available for %s", sku, value));
                    return true;
                case "InStockOnlineOnly":
                    logger.info(String.format("Sku of %s is available for %s", sku, value));
                    return true;
                default:
                    logger.error(String.format("Sku of %s is not available because it's %s", sku, value));
                    failures.put(sku, setFailedReason(sku, String.format("It is not available because it's " +
                            "%s", value)));
                    return false;
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static SkuObject setFailedReason(String sku, String reason) {
        Map<String, SkuObject> map = getTestData();
        SkuObject skuObject = new SkuObject();
        map.forEach((key, value) -> {

            if ((map.get(key).getSku()).equals(sku)) {
                skuObject.setScenario(map.get(key).getScenario());
                skuObject.setFailedReason(reason);
            }
        });
        return skuObject;
    }


    public void setTestDataFromExcel() {
        String path = getExcelFilePath();
        XSSFSheet sheet = excel.setExcelSheet(path, 0);
        logger.info("Excel file has loaded from path of " + path);
        int n = 0, m = 0, l = 0;
        int totalRows = sheet.getLastRowNum();
        XSSFRow row;
        row = sheet.getRow(totalRows);
        int totalCols = row.getLastCellNum();
        logger.info(String.format("Total rows is %s, total columns is %s ", totalRows, totalCols));
        for (int i = 0; i < totalRows; i++) {
            String sku = excel.getCellData(sheet, i + 1, 3).trim();
            if (!(excel.getCellData(sheet, i + 1, 4)).isEmpty() && excel.getCellData(sheet, i + 1, 4) != "") {
                SkuObject online = new SkuObject();
                online.setSku(sku);
                online.setScenario(excel.getCellData(sheet, i + 1, 1).trim());
                online.setIsOnlineOnly(true);
                skuList.put(sku, online);
                n += 1;
            }
            if (!(excel.getCellData(sheet, i + 1, 5)).isEmpty() && excel.getCellData(sheet, i + 1, 5) != "") {
                SkuObject inStore = new SkuObject();
                inStore.setSku(sku);
                inStore.setScenario(excel.getCellData(sheet, i + 1, 1).trim());
                inStore.setInstoreStatus(true);
                skuList.put(sku, inStore);
                m += 1;
            }
            if (!(excel.getCellData(sheet, i + 1, 6)).isEmpty() && excel.getCellData(sheet, i + 1, 6) != "") {
                SkuObject onlineAndInStore = new SkuObject();
                onlineAndInStore.setSku(sku);
                onlineAndInStore.setScenario(excel.getCellData(sheet, i + 1, 1).trim());
                onlineAndInStore.setOnlineAndInstoreStatus(true);
                skuList.put(sku, onlineAndInStore);
                l += 1;
            }

        }
        logger.info(String.format("%s skus for online, %s skus for in store, %s skus for online& in store.", n, m, l));
    }

    public void createDataInventoryInOracle() {
        OracleDataSource oracleDataSource = OracleDataSource.getInstance();
        for (File f : files) {
            logger.info("Load file " + f + "\n");
            oracleDataSource.executeSqlScript(f);
            Util.wait(3);
            logger.info(f.getName() + " has executed in DB.");
        }
        Util.wait(1800);
    }

    public static Map getTestData() {
        return skuList;
    }

    public static Map getFailures() {
        return failures;
    }



}