package utils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.xml.ws.http.HTTPException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Due to ZAPI license issue this feature is not supported anymore
 *
 * Created by sucho on 7/25/2017.
 */
public class JiraUpdater {
    private Properties properties;
    private String jiraURL;
    private String username;
    private String password;
    private static final String PASSED = "passed";
    private static final String FAILED = "failed";

    private final static Logger logger = Logger.getLogger(JiraUpdater.class);

    public JiraUpdater(){
        properties = new Properties();
        InputStream input = null;
        try {
            input = getClass().getClassLoader().getResourceAsStream("config.properties");

            if (input == null) {
                logger.error("Sorry, unable to find 'config.properties' file");
                return;
            }
            //load a properties file from class path
            properties.load(input);

            //get the property value and print it out
            jiraURL = properties.getProperty("jiraprod");
            username = properties.getProperty("jira_username");
            password = properties.getProperty("jira_password");

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    public void resetLabels(final String jiraID){
        try {
            JSONArray labelsArr = getLabels(jiraID);

            // check if labels contains 'passed' or 'failed'
            for (int i = 0; i < labelsArr.size(); i++) {
                Object obj = labelsArr.get(i);
                if (String.valueOf(obj).equalsIgnoreCase(PASSED)) {
                    removeLabels(jiraID, PASSED);
                }
                if (String.valueOf(obj).equalsIgnoreCase(FAILED)) {
                    removeLabels(jiraID, FAILED);
                }
            }
        }
        catch (Exception e){
            logger.debug(e.getMessage(), e);
        }
    }


    public void updateLabels(final String jiraID, final String result){
        try {
            // double check if labels contains 'passed' or 'failed'
            resetLabels(jiraID);

            // add label
            addLabels(jiraID, result);
        }
        catch(Exception e){
            logger.warn(e.getMessage(), e);
        }
    }

    private JSONArray getLabels(final String jiraID) throws Exception {
        // construct uri
        String uri = String.format("%s/rest/api/2/issue/%s", jiraURL, jiraID);

        Client client = Client.create();
        WebResource webResource = client.resource(uri);

        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

        if (response.getStatus() != 200) {
            logger.debug("Failed : HTTP error code : " + response.getStatus());
            throw new HTTPException(response.getStatus());
        }
        else{
            // parse json string and get jiraIssueID
            String jsonString = response.getEntity(String.class);
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
            jsonObject = (JSONObject)jsonObject.get("fields");
            JSONArray labels = (JSONArray)jsonObject.get("labels");

            return labels;
        }
    }

    private void addLabels(final String jiraID, final String label){
        // update labels based on the 'result'
        JSONObject update = new JSONObject();
        JSONObject labels = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject add = new JSONObject();

        add.put("add", label);
        jsonArray.add(add);
        labels.put("labels", jsonArray);
        update.put("update", labels);

        setLabels(jiraID, update);
    }

    private void removeLabels(final String jiraID, final String label){
        // update labels based on the 'result'
        JSONObject update = new JSONObject();
        JSONObject labels = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject add = new JSONObject();

        add.put("remove", label);
        jsonArray.add(add);
        labels.put("labels", jsonArray);
        update.put("update", labels);

        setLabels(jiraID, update);
    }

    private void setLabels(final String jiraID, final JSONObject jsonObject){
        String uri = String.format("%s/rest/api/2/issue/%s", jiraURL, jiraID);

        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(username, password));
        WebResource webResource = client.resource(uri);

        webResource.header("Content-Type", "application/json").put(ClientResponse.class, jsonObject.toJSONString());
    }

    // Raymond, I am commenting out below code for just in case we have ZAPI license.
    // we need to re-add jersey client 2.x version in the pom.xml file to get this working.

 /*   public void updateResult(String jiraID, String result){
        try{
            String jiraIssueID = getJiraIssueID(jiraID);
            String zephyrExecutionID = getExecutionID(jiraIssueID);
            updateTestResult(zephyrExecutionID, result);
        }
        catch (Exception e){
            // log error to inform what happened, but should not stop running test.
            logger.warn(e.getMessage(), e);
        }
    }

    private String getJiraIssueID(final String jiraID) throws Exception {
        // construct uri
        StringBuilder uri = new StringBuilder();
        uri.append(jiraURL).append("/rest/api/2").append("/search?jql=").append("issue=").append(jiraID);

        // send http get request with basic authentication
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        ClientConfig clientConfig = new ClientConfig().register(feature);
        Client client = ClientBuilder.newClient(clientConfig);
        Response response = client.target(uri.toString()).request(MediaType.APPLICATION_JSON_TYPE).get();

        // consume http response
        if(response.getStatus() != 200){
            throw new Exception(uri.toString() + " connection problem occurred.");
        }
        else{
            // parse json string and get jiraIssueID
            String jsonString = response.readEntity(String.class);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonString);
            JsonNode issues = jsonNode.get("issues");

            // TODO not sure why issues.get("id") does not work but iterator.next().get("id") works...
            Iterator<JsonNode> iterator = issues.iterator();
            String issueID = iterator.next().get("id").asText();

            return issueID;
        }
    }

    private String getExecutionID(final String jiraIssueID) throws Exception {
        // construct uri
        StringBuilder uri = new StringBuilder();
        uri.append(jiraURL).append("/rest/zapi/latest").append("/execution?issueId=").append(jiraIssueID);

        // send http get request with basic authentication
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        ClientConfig clientConfig = new ClientConfig().register(feature);
        Client client = ClientBuilder.newClient(clientConfig);
        Response response = client.target(uri.toString()).request(MediaType.APPLICATION_JSON_TYPE).get();

        // consume http response
        if(response.getStatus() != 200){
            throw new Exception(uri.toString() + " connection problem occurred.");
        }
        else {
            // parse json string and get executionID
            String jsonString = response.readEntity(String.class);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonString);
            JsonNode issues = jsonNode.get("executions");

            Iterator<JsonNode> iterator = issues.iterator();
            String executionID = iterator.next().get("id").asText();

            return executionID;
        }

    }

    private void updateTestResult(final String zephyrExecutionID, final String result){
        // construct uri
        StringBuilder uri = new StringBuilder();
        uri.append(jiraURL).append("/rest/zapi/latest/execution/").append(zephyrExecutionID).append("/execute");

        // prepare entity for http put request
        Entity payload = Entity.json("{  \"status\": \"-1\"}"); // UNEXECUTED, The test has not yet been executed.
        if(result.equalsIgnoreCase(PASSED)){
            payload = Entity.json("{  \"status\": \"1\"}");
        }
        else if(result.equalsIgnoreCase(FAILED)){
            payload = Entity.json("{  \"status\": \"2\"}");
        }
        else if(result.equalsIgnoreCase(WIP)){
            payload = Entity.json("{  \"status\": \"3\"}");
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        ClientConfig clientConfig = new ClientConfig().register(feature);
        Client client = ClientBuilder.newClient(clientConfig);

        client.target(uri.toString()).request(MediaType.APPLICATION_JSON).put(payload);

        // Left for debugging
        //Response response = client.target(uri.toString()).request(MediaType.APPLICATION_JSON).put(payload);

        *//*
        System.out.println("status: " + response.getStatus());
        System.out.println("headers: " + response.getHeaders());
        System.out.println("body:" + response.readEntity(String.class));
        *//*
    }

    private void getListOfCycle(final String projectId) throws Exception {
        // construct uri
        StringBuilder uri = new StringBuilder();
        uri.append(jiraURL).append("/rest/zapi/latest/cycle?").append("projectId=").append(projectId);

        // send http get request with basic authentication
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        ClientConfig clientConfig = new ClientConfig().register(feature);
        Client client = ClientBuilder.newClient(clientConfig);

        client.target(uri.toString()).request(MediaType.APPLICATION_JSON_TYPE).get();

        // Left for debugging
        //Response response = client.target(uri.toString()).request(MediaType.APPLICATION_JSON_TYPE).get();

        *//*
        System.out.println("status: " + response.getStatus());
        System.out.println("headers: " + response.getHeaders());
        System.out.println("body:" + response.readEntity(String.class));
        *//*
    }
*/

}