package utils.email;

import utils.TestDataValidationUtil;
import utils.excel.SkuObject;

import java.util.Map;


public class EmailMessage {

    private static int counter = 0;

    private EmailObject emailObject;

    public EmailMessage() {
        createMailObject();
    }

    private void createMailObject() {
        emailObject = new EmailObject();
        emailObject.setFrom("QA-Automation@bestbuy.com");
        emailObject.setTo("jontsang@bestbuycanada.ca");
        emailObject.setCc("QA-Automation@bestbuy.com;QA-Automation-Offshore@bestbuy.com");
        emailObject.setSubject("Test Failures from test data validation test for automation");
        emailObject.setBody(htmlMessage());
    }

    private static String htmlMessage() {
        Map<String, SkuObject> map = TestDataValidationUtil.getFailures();
        StringBuilder email = new StringBuilder();
        email.append("<html><body>")
                .append("<p>Hi, Johnathan,</p>")
                .append("<p>Please help us to resolve following failures from test data availability " +
                        "validation test.</p>")
                .append("<p>Thank you.</p>")
                .append("<table style='border:1px solid black; table, th, td {\n" +
                        "    border: 1px solid black;\n" +
                        "    border-collapse: collapse;\n" +
                        "}'>")
                .append("<tr " + "bgcolor=\"#33CC99\"><th>Index<th>Sku<th>Scenario<th>FailedReason</tr>");
        map.forEach((key, value) -> {
            getCount();
            email.append("<tr bgcolor=\"#D3D3D3\"><td>" + counter + "<td>" + key + "<td>" + map.get(key)
                    .getScenario() + "<td>" + map.get(key).getFailedReason());
        });

        email.append("</table></body></html>")
                .append("<p>Automation Team</p>");
        return email.toString();
    }


    private static int getCount() {
        return counter++;
    }

    public EmailObject getEmailObject() {
        return emailObject;
    }

}