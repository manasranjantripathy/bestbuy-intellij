package utils.email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailSender {
    private final static Logger logger = Logger.getLogger(EmailSender.class);
    @Autowired
    private JavaMailSender mailSender;

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendMessage(EmailObject emailObject) {
        this.mailSender.send(createEmailMessage(emailObject));
    }

    private MimeMessage createEmailMessage(EmailObject emailObject) {
        MimeMessage message = null;
        try {
            message = mailSender.createMimeMessage();
            message.setSubject(emailObject.getSubject());
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(emailObject.getFrom());

            String[] toList = emailObject.getTo().split(";|,");
            helper.setTo(toList);

            String ccAddress = emailObject.getCc();
            if (ccAddress != null && !ccAddress.trim().isEmpty()) {
                String[] ccList = ccAddress.split(";|,");
                helper.setCc(ccList);
            }

            helper.setText(emailObject.getBody(), true);


        } catch (MessagingException e) {
            logger.error("MailObject is invalid, please check.");
        }
        return message;
    }

}