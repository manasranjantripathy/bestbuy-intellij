package utils.environments.windows;

import org.openqa.selenium.chrome.ChromeDriverService;
import utils.environments.ChromeDriverServiceFactory;
import utils.environments.EnvironmentChecks;

import java.io.File;


public class ChromeDriverServiceFactoryWindows implements ChromeDriverServiceFactory {

    private EnvironmentChecks environmentChecks;

    public ChromeDriverServiceFactoryWindows(EnvironmentChecks environmentChecks) {
        this.environmentChecks = environmentChecks;
    }

    @Override
    public ChromeDriverService makeChromeDriverService() {
        return new ChromeDriverService.Builder()
                .usingDriverExecutable(new File("..\\ecomm_api\\src\\main\\resources\\chromedriver.exe"))
                .usingAnyFreePort()
                .build();
    }

    @Override
    public boolean isEnvironmentSuitable() {
        return environmentChecks.isRunningOn("win");
    }
}