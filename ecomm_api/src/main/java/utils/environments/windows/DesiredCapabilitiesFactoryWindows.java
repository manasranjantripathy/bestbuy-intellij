package utils.environments.windows;

import org.openqa.selenium.remote.DesiredCapabilities;
import utils.environments.DesiredCapabilitiesFactory;
import utils.environments.EnvironmentChecks;


public class DesiredCapabilitiesFactoryWindows implements DesiredCapabilitiesFactory {

    private EnvironmentChecks environmentChecks;

    public DesiredCapabilitiesFactoryWindows(EnvironmentChecks environmentChecks) {
        this.environmentChecks = environmentChecks;
    }

    @Override
    public DesiredCapabilities makeDesiredCapabilities() {
        return DesiredCapabilities.chrome();
    }

    @Override
    public boolean isEnvironmentSuitable() {
        return environmentChecks.isRunningOn("win");
    }

}