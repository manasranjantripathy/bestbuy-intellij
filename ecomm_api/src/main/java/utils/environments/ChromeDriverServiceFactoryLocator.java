package utils.environments;

import java.util.List;
import java.util.function.Predicate;


public class ChromeDriverServiceFactoryLocator {

    private List<ChromeDriverServiceFactory> factories;


    public ChromeDriverServiceFactoryLocator(List<ChromeDriverServiceFactory> factories) {
        this.factories = factories;
    }

    public ChromeDriverServiceFactory getChromeDriverServiceFactory() {
        Predicate<EnvironmentSuitable> predicate = es -> es.isEnvironmentSuitable();
        return factories.stream().filter(predicate).findFirst().get();
    }

}