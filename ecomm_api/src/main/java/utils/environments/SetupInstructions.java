package utils.environments;

public class SetupInstructions {

    private static final String ENVIRONMENT_SETUP_INSTRUCTIONS = "" + '\n'
            + "We tried to find suitable Chrome and ChromeDriver for your environment... and failed." + '\n'
            + "Please make sure all the components are in place or add a new runtime environment to the code." + '\n'
            + "" + '\n'
            + "WINDOWS" + '\n'
            + "Chrome Web Browser installed at the default location" + '\n'
            + "ChromeDriver executable installed at the 'src\\main\\resources\\chromedriver.exe' project location" + '\n'
            + "" + '\n'
            + "MAC OS X with Fortress folder" + '\n'
            + "Chrome Web Browser installed at '~/Fortress/opt/Google Chrome.app' folder" + '\n'
            + "ChromeDriver executable installed at '~/Fortress/opt/cromedriver' " + '\n'
            + "" + '\n'
            + "As of Aug. 2017 we expect Chrome version 60 and ChromeDriver vesrion 2.31 at the locations above." + '\n'
            + "" + '\n';

    public void print() {
        System.err.println(ENVIRONMENT_SETUP_INSTRUCTIONS);
    }

}