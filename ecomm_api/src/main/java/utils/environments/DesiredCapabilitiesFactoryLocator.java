package utils.environments;

import java.util.List;
import java.util.function.Predicate;


public class DesiredCapabilitiesFactoryLocator {

    private List<DesiredCapabilitiesFactory> factories;

    public DesiredCapabilitiesFactoryLocator(List<DesiredCapabilitiesFactory> factories) {
        this.factories = factories;
    }

    public DesiredCapabilitiesFactory getDesiredCapabilitiesFactory() {
        Predicate<EnvironmentSuitable> predicate = es -> es.isEnvironmentSuitable();
        return factories.stream().filter(predicate).findFirst().get();
    }

}