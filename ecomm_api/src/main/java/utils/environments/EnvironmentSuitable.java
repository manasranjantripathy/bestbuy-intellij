package utils.environments;


public interface EnvironmentSuitable {

    boolean isEnvironmentSuitable();

}