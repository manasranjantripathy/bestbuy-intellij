package utils.environments;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;


public class EnvironmentChecks {

    private static final String OS = System.getProperty("os.name");

    public boolean isRunningOn(String osName) {
        return OS != null && OS.toLowerCase().indexOf(osName) >= 0;
    }

    public boolean hasPath(String path) {
        FileSystem fileSystem = FileSystems.getDefault();
        Path p = fileSystem.getPath(path);
        return  Files.exists(p);
    }

}