package utils.environments;

import org.openqa.selenium.remote.DesiredCapabilities;


public interface DesiredCapabilitiesFactory extends EnvironmentSuitable {

    DesiredCapabilities makeDesiredCapabilities();

}