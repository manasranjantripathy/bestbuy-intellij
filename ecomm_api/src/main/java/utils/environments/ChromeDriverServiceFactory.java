package utils.environments;

import org.openqa.selenium.chrome.ChromeDriverService;


public interface ChromeDriverServiceFactory extends EnvironmentSuitable {

    ChromeDriverService makeChromeDriverService();

}