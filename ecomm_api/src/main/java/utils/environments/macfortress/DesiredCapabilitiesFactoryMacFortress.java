package utils.environments.macfortress;

import org.openqa.selenium.remote.DesiredCapabilities;
import utils.environments.DesiredCapabilitiesFactory;
import utils.environments.EnvironmentChecks;


public class DesiredCapabilitiesFactoryMacFortress implements DesiredCapabilitiesFactory {

    public static final String USER_HOME = System.getProperty("user.home");
    public static final String FORTRESS_CHROME_PATH = USER_HOME + "/Fortress/opt/Google Chrome.app/Contents/MacOS/Google Chrome";
    private EnvironmentChecks environmentChecks;

    public DesiredCapabilitiesFactoryMacFortress(EnvironmentChecks environmentChecks) {
        this.environmentChecks = environmentChecks;
    }

    @Override
    public DesiredCapabilities makeDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities().chrome();
        desiredCapabilities.setCapability("chrome.binary", FORTRESS_CHROME_PATH);
        return desiredCapabilities;
    }

    @Override
    public boolean isEnvironmentSuitable() {
        return environmentChecks.isRunningOn("mac") && environmentChecks.hasPath(FORTRESS_CHROME_PATH);
    }

}