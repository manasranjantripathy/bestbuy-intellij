package utils.environments.macfortress;

import org.openqa.selenium.chrome.ChromeDriverService;
import utils.environments.ChromeDriverServiceFactory;
import utils.environments.EnvironmentChecks;

import java.io.File;


public class ChromeDriverServiceFactoryMacFortress implements ChromeDriverServiceFactory {

    public static final String USER_HOME = System.getProperty("user.home");
    public static final String FORTRESS_CHROMEDRIVER_PATH = USER_HOME + "/Fortress/opt/chromedriver";
    private EnvironmentChecks environmentChecks;


    public ChromeDriverServiceFactoryMacFortress(EnvironmentChecks environmentChecks) {
        this.environmentChecks = environmentChecks;
    }

    @Override
    public ChromeDriverService makeChromeDriverService() {
        return new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(FORTRESS_CHROMEDRIVER_PATH))
                .usingAnyFreePort()
                .withVerbose(false)
                .build();
    }

    @Override
    public boolean isEnvironmentSuitable() {
        return environmentChecks.isRunningOn("mac") && environmentChecks.hasPath(FORTRESS_CHROMEDRIVER_PATH);
    }
}