package utils.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
    private final static Logger logger = Logger.getLogger(ExcelReader.class);
    private static XSSFWorkbook excelWBook;
    private static XSSFCell cell;
    private static ExcelReader instance = null;

    public XSSFSheet setExcelSheet(String path, int index) {
        XSSFSheet excelSheet = null;
        try {
            FileInputStream ExcelFile = new FileInputStream(path);
            excelWBook = new XSSFWorkbook(ExcelFile);
            excelSheet = excelWBook.getSheetAt(index);
            logger.info(String.format("The excel sheet %s has loaded.", excelSheet.getSheetName()));
        } catch (FileNotFoundException e) {
            logger.error("Could not read the Excel sheet");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Could not read the Excel sheet");
            e.printStackTrace();
        }
        return excelSheet;
    }

    public String getCellData(XSSFSheet sheet, int RowNum, int ColNum) {
        try {
            cell = sheet.getRow(RowNum).getCell(ColNum);
            return cell.getStringCellValue();
        } catch (Exception e) {
            logger.error("No value in this cell.");
            return "";
        }
    }

    public static ExcelReader getInstance() {
        if (instance == null) {
            instance = new ExcelReader();
        }
        return instance;
    }
}