package utils.excel;

public class SkuObject {

    private String scenario;
    private String sku;
    private String failedReason;
    private String desription;
    private boolean isOnlineOnly;
    private boolean isInstoreOnly;
    private boolean isOnlineAndInstore;


    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getScenario() {
        return scenario;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    public String getDesription() {
        return desription;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setIsOnlineOnly(boolean isOnlineOnly) {
        this.isOnlineOnly = isOnlineOnly;
    }

    public boolean getOnlineStatus() {
        return isOnlineOnly;
    }

    public void setInstoreStatus(boolean isInstoreOnly) {
        this.isInstoreOnly = isInstoreOnly;
    }

    public boolean getInstoreStatus() {
        return isInstoreOnly;
    }

    public void setOnlineAndInstoreStatus(boolean isOnlineAndInstore) {
        this.isOnlineAndInstore = isOnlineAndInstore;
    }

    public boolean getOnlineAndInstoreStatus() {
        return isOnlineAndInstore;
    }


}