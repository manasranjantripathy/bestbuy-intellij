package utils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import io.qameta.allure.Attachment;
import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.CsrPageFactory;
import pages.DesktopPageFactory;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by sucho on 7/12/2017.
 */
public final class Util {
    private final static Logger logger = Logger.getLogger(Util.class);

    public static WebDriver driver;

    private Util() {

    }

    public static int getRandomNumber() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt();
    }

    public static JSONObject createJSONObject(){
        return new JSONObject();
    }

    public static JSONObject jsonGenerator(int size) {
        return null;
    }

    public static void verifyTestEnvironment(String urlString){
        try {
            setCertification();
            URL pageURL = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) pageURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            if(connection.getResponseCode() == 200 || connection.getResponseCode() == 301){
                logger.info("Test environment : " + urlString + " is online.");
            }
            else{
                logger.info("Test environment : " + urlString + " is down." + "\n" + connection.getResponseCode() + " " + connection.getResponseMessage());
                throw new RuntimeException("Test environment : " + urlString + " is down." + "\n" + connection.getResponseCode() + " " + connection.getResponseMessage());
            }
        }
        catch (NullPointerException  e){
            logger.info("Certification error occurred", e);
            throw new RuntimeException("Certification error occurred", e);
        }
        catch (Exception e){
            logger.info(e.getMessage(), e);
            throw  new RuntimeException(e.getMessage(), e);
        }
    }

    public static void wait(final int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.getMessage();
        }
    }

    public static void emptyUserCartWithSignIn(final Object pages, final String username, final String password){
        if(pages instanceof DesktopPageFactory){
            ((DesktopPageFactory) pages).headerFooter().clickSignInButton();
            ((DesktopPageFactory) pages).signInPage().signInAsDefaultCustomer(username, password);

            ((DesktopPageFactory) pages).headerFooter().clickCartIcon();
            ((DesktopPageFactory) pages).cartPage().removeAllItemsInTheCart();

            ((DesktopPageFactory) pages).headerFooter().clickSignOutLink();
        }
    }

    public static void emptyUserCart(final Object pages) {
        if (pages instanceof DesktopPageFactory) {
            ((DesktopPageFactory) pages).headerFooter().clickCartIcon();
            ((DesktopPageFactory) pages).cartPage().removeAllItemsInTheCart();
        }
    }

    public static void popupHandler(Object pages, WebDriver driver){
        waitForAjax(driver);
        if(pages instanceof DesktopPageFactory) {
            if (((DesktopPageFactory) pages).warrantyPopup().isExpressCheckoutDisplayed()) {
                ((DesktopPageFactory) pages).warrantyPopup().clickViewCartLink();
            }
            else if (((DesktopPageFactory) pages).warrantyPopup().isWarrantyDisplayed()) {
                ((DesktopPageFactory) pages).warrantyPopup().clickNoThanksButton();

                if (((DesktopPageFactory) pages).interstitialPage().isAt()) {
                    ((DesktopPageFactory) pages).interstitialPage().clickEditCartButton();
                }
            }
        }
        else if(pages instanceof CsrPageFactory){
            if(((CsrPageFactory) pages).warrantyPopup().isExpressCheckoutDisplayed()){
                ((CsrPageFactory) pages).warrantyPopup().clickViewCartLink();
            }
            else if (((CsrPageFactory) pages).warrantyPopup().isWarrantyDisplayed()) {
                ((CsrPageFactory) pages).warrantyPopup().clickNoThanksButton();

                if (((CsrPageFactory) pages).interstitialPage().isAt()) {
                    ((CsrPageFactory) pages).interstitialPage().clickEditCartButton();
                }
            }
        }
    }


    public static void waitForAjax(final WebDriver driver){
        try {

            int timeout =0;
            while (timeout<=600) { //60 secs
                Boolean isAjaxCompleted = (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
                if (isAjaxCompleted) {
                    break;
                }
                Thread.sleep(100);
                timeout++;
            }

        }
        catch (InterruptedException e){
            logger.warn(e.getMessage());
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] saveScreenshotPNG(WebDriver driver){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * http://www.rgagnon.com/javadetails/java-fix-certificate-problem-in-HTTPS.html
     *
     *  fix for
     *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
     *       sun.security.validator.ValidatorException:
     *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
     *               unable to find valid certification path to requested target
     **/
    private static void setCertification() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

// Create all-trusting host name verifier
            HostnameVerifier allHostsValid = (s, sslSession) -> true;

// Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException | NullPointerException | KeyManagementException e) {
            //TODO implement later
        }

    }

    public static String getAuthenticationToken() {
        JSONObject jsonObject = null;
        try {
            ClientResponse clientResponse = getAuthentication();

            clientResponse.bufferEntity();
            String entity = clientResponse.getEntity(String.class);
            clientResponse.getEntityInputStream().reset();

            JSONParser parser = new JSONParser();
            jsonObject = (JSONObject) parser.parse(entity);

            if (!jsonObject.containsKey("access_token")) {
                throw new IllegalArgumentException("access token is not in the response entity.");
            }
        } catch (IOException | org.json.simple.parser.ParseException | IllegalArgumentException e) {
            //logger.error(e.getMessage(), e);
        }
        return jsonObject.get("access_token").toString();
    }

    public static ClientResponse getAuthentication() {
        setCertification();
        Client client = Client.create();

        WebResource webResource = client.resource(Constants.TestDataValidation.BASE_URI.toString() + Constants.TestDataValidation.TOKEN_URI.toString());

        StringBuilder body = new StringBuilder();
        body.append("grant_type").append("=").append("client_credentials").append("&")
                .append("client_id").append("=").append(Constants.TestDataValidation.CLIENT_ID.toString()).append("&")
                .append("client_secret").append("=").append(Constants.TestDataValidation.CLIENT_SECRET.toString());

        ClientResponse clientResponse = webResource.type(Constants.TestDataValidation.TOKEN_CONTENT_TYPE.toString())
                .accept(Constants.TestDataValidation.MEDIA_TYPE.toString())
                .acceptLanguage(Constants.TestDataValidation.LANGUAGE_TYPE.toString())
                .post(ClientResponse.class, body.toString());

        return clientResponse;
    }

    public static void switchWindow(WebDriver driver) {

        Set<String> handles = driver.getWindowHandles();
        String current = driver.getWindowHandle();
        handles.remove(current);
        String newTab = handles.iterator().next();
        driver.switchTo().window(newTab);
    }

    public static void switchTab(WebDriver driver, int nth){
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }

    public static void switchToDefaultWindow(WebDriver driver) {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    //Java version
    public static void xmlParserForReRunningFailedTests(String moduleName){
        try {
            // C:\Git\ecomm-ui-automation\ecomm_automation\target\surefire-reports\testng-failed.xml
            String filePath = String.format("..\\%s\\target\\surefire-reports\\testng-failed.xml", moduleName);
            String fileOutPath = String.format("..\\%s\\src\\test\\resources\\testrunners\\testng-failed.xml", moduleName);

            File inputFile = new File(filePath);
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile.getCanonicalPath());
            Element rootElement = document.getRootElement();

// turn off multi threading
            rootElement.setAttribute("parallel", "false");

// remove unnecessary code
            List<Element> tests = rootElement.getChildren("test");
            for(Element test : tests){
                test.getChild("classes").getChild("class").removeChild("methods");
            }

            // save it under testrunner
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            xmlOutput.output(document, new FileWriter(fileOutPath));

        }
        catch (Exception e){
            logger.warn(e.getMessage());
        }
    }


}