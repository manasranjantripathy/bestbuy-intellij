package pages.commons;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants.Payment;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 8/10/2017.
 */
public class PayPalPage extends StorefrontBasePage {
    private static final String EXPECTED_URL = "www.sandbox.paypal.com";
    private final static Logger logger = Logger.getLogger(PayPalPage.class);
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.ID, using = "btnLogin")
    private WebElement buttonLogin;

    @FindBy(how = How.ID, using = "confirmButtonTop")
    private WebElement buttonAgreeNcontinue;

    @FindBy(how = How.ID, using = "makePreferred")
    private WebElement checkboxPreferredShippingAddress;

    @FindBy(how = How.ID, using = "email")
    private WebElement fieldEmail;

    @FindBy(how = How.ID, using = "password")
    private WebElement fieldPassword;

    @FindBy(how = How.XPATH, using = "//iframe[@title='PayPal - Log in'] | //iframe[@title='PayPal - Connexion']")
    private WebElement frameLocator;

    @FindBy(how = How.XPATH, using = "//span[contains(@class, 'given-name')]")
    private WebElement labelName;

    @FindBy(how = How.XPATH, using = "//span[contains(@class, 'full-address')]")
    private WebElement labelAddress;

    @FindBy(xpath = "//button[@id='btnNext']")
    private WebElement btn_Next;

    @FindBy(xpath = "//span[@class='given-name ng-binding']")
    private WebElement txt_FullName;

    @FindBy(xpath = "//span[@class='full-address ng-binding']")
    private WebElement txt_FullAddress;

    @FindBy(xpath = "//span[@class='country adr-break ng-binding']")
    private WebElement txt_Country;

    @FindBy(xpath = "//*[@id='shippingAddress']/a")
    private WebElement link_ChangeShipping;

    @FindBy(xpath = "//span[@class='leftText ng-binding']")
    private WebElement link_Add;

    @FindBy(xpath = "//div[@id='selectShipping']")
    private WebElement listOfAddress;

    @FindBy(xpath = "//form[@id='shippingAddressForm']")
    private WebElement form_ShippingAddress;

    @FindBy(xpath = "//select[@id='country']")
    private WebElement country;

    @FindBy(xpath = "//input[@id='shippingFirstName']")
    private WebElement firstName;

    @FindBy(xpath = "//input[@id='shippingLastName']")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id='shippingLine1']")
    private WebElement shippingLine1;

    @FindBy(xpath = "//*[@id='shippingLine2']")
    private WebElement shippingLine2;

    @FindBy(xpath = "//*[@id='shippingCity']")
    private WebElement city;

    @FindBy(xpath = "//*[@id='shippingPostalCode']")
    private WebElement postalCode;

    @FindBy(xpath = "//select[@id='shippingState']")
    private WebElement province;

    @FindBy(xpath = "//input[@id='proceedButton']")
    private WebElement btn_Save;

    @FindBy(xpath = "//div[@class='msgPage msg-success']")
    private WebElement txt_AddressAddedConfirmation;

    @FindBy(xpath = "//*[@id='errorMessage']/h1/span")
    private WebElement txt_ErrMsgUSShippingAddress;

    private PayPalPage() {

    }

    public PayPalPage(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            wait.until(ExpectedConditions.urlContains(EXPECTED_URL));
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        } catch (Exception e) {
            logger.error("Paypal page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Paypal page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isShippingInformationFilledCorrectly() {
        Util.wait(10);

        return getTextOrValue(labelName).contains(TestUser.fullName) &&
                getTextOrValue(labelAddress).contains("ATS - 8800 GlenLyon Parkway, Burnaby BC V5K 0A1");
    }

    public boolean isAgreeAndContinueBtnDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(buttonAgreeNcontinue));

        return isDisplayed(buttonAgreeNcontinue);

    }

    public void loginToPayPal() {
        typeIn(fieldEmail, Payment.PAYPAL_USEREMAIL.toString());

        click(btn_Next);

        typeIn(fieldPassword, Payment.PAYPAL_PASSWORD.toString());

        click(buttonLogin);

        Util.wait(10);

    }

    public void continuePayment() {

        click(buttonAgreeNcontinue);

        Util.wait(20);

    }

    public void getTxt_ShippingAddress() {

        TestUser.fullName = getTextOrValue(txt_FullName);
        TestUser.fullAddress = getTextOrValue(txt_FullAddress);
        TestUser.country = getTextOrValue(txt_Country);
    }

    public void clickChangeLink() {
        click(link_ChangeShipping);
    }

    public boolean isAddressListDisplayed() {
        return isDisplayed(listOfAddress);
    }

    public void clickAddLink() {
        click(link_Add);
    }

    public boolean isShippingAddressFormDisplayed() {
        return isDisplayed(form_ShippingAddress);
    }

    public void addNewAddress() {
        select(country, TestUser.country);
        typeIn(firstName, TestUser.firstName);
        typeIn(lastName, TestUser.lastName);
        typeIn(shippingLine1, TestUser.streetAddress);
        typeIn(city, TestUser.city);
        select(province, TestUser.province);
        typeIn(postalCode, TestUser.postalCodeBC);
        click(btn_Save);
    }

    public boolean isAddressAdded() {
        waitUntilPageLoaded(5);
        return isDisplayed(txt_AddressAddedConfirmation);
    }

    public void loginToPayPalWithUSCredentials() {
        typeIn(fieldEmail, Payment.PAYPAL_US_USEREMAIL.toString());

        click(btn_Next);

        typeIn(fieldPassword, Payment.PAYPAL_PASSWORD.toString());

        click(buttonLogin);

        Util.wait(10);
    }

    public String getTxt_errMsgUSShippingAddress() {
        return getTextOrValue(txt_ErrMsgUSShippingAddress);
    }
}