package pages.commons;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

public class GeekSquadPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "/register-your-plan?";
    private static final String EXPECTED_LOGIN_URL = "/login?";
    private final static Logger logger = Logger.getLogger(GeekSquadPageDesktop.class);


    @FindBy(how = How.XPATH, using = "//a[@class='login-modal-trigger cboxElement']")
    private WebElement buttonLogin;

    @FindBy(how = How.XPATH, using = "//div[@id='login-modal']")
    private WebElement popUpLogin;

    @FindBy(how = How.XPATH, using = "//input[@id='header_0_ctl01_txtEmailAddress']")
    private WebElement fieldEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='header_0_ctl01_txtPassword']")
    private WebElement fieldPassword;

    @FindBy(how = How.XPATH, using = "//a[@id='header_0_ctl01_btnLoginSubmit']")
    private WebElement buttonSubmit;

    @FindBy(how = How.XPATH, using = "//div[@id='content_0_body_0_errorDiv']/ul/li")
    private WebElement textError;

    @FindBy(how = How.XPATH, using = "//input[@id='content_0_body_0_txtEmail']")
    private WebElement fieldLogin;

    @FindBy(how = How.XPATH, using = "//input[@id='content_0_body_0_txtPassword']")
    private WebElement fieldLoginPassword;

    @FindBy(how = How.XPATH, using = "//a[@id='content_0_body_0_btnSubmit']")
    private WebElement login;

    @FindBy(how = How.ID, using = "CaptchaArea")
    private WebElement captchaArea;


    private GeekSquadPageDesktop(){

    }

    public GeekSquadPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Register your plan page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Register your plan page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isLogin() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_LOGIN_URL);
        }
        catch (Exception e){
            logger.error("Login error page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Login error page is not displayed " + e.getMessage(), e);
        }
    }


    public void clickLoginButton() {click(buttonLogin);}

    public boolean checkLoginPopUp() {return isDisplayed(popUpLogin);}

    public void enterWrongPassword(String password) {
        typeIn(fieldEmail, TestUser.email2);
        typeIn(fieldPassword ,password );
        click(buttonSubmit);
    }

    public void enterWrongPasswordInLogin(String password) {
        typeIn(fieldLogin, TestUser.email2);
        typeIn(fieldLoginPassword ,password );
        click(login);
    }

    public String getErrorText() { return getTextOrValue(textError);}

    public boolean checkCaptcha() {return isDisplayed(captchaArea);}
}