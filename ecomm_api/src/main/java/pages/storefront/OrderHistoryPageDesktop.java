package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

import java.util.List;

/**
 * Created by sucho on 8/24/2017.
 */
public class OrderHistoryPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/orderhistory.aspx";
    private final static Logger logger = Logger.getLogger(OrderConfirmationPageDesktop.class);

    @FindBy(id = "ctl00_CP_UcOrderHistory_BtnSearchByDate")
    private WebElement btn_search;

    @FindBy(id = "ctl00_CP_UcOrderHistory_BtnSearchByOrderNumber")
    private WebElement btn_view;

    @FindBy(id = "ctl00_CP_UcOrderHistory_OrderDateFieldContainer_DdlTimePeriod")
    private WebElement fld_searchByOrderDate;

    @FindBy(id = "ctl00_CP_UcOrderHistory_OrderNumberContainer_TxtSearchByOrderNumber")
    private WebElement fld_viewByOrderNumber;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcOrderHistory_RptBottomOrderHistoryList_ctl00_LnkTopToOrderDetails']")
    private WebElement lnk_firstOrder;

    @FindBy(xpath = "//div[@class='row-fluid']//tbody//a")
    private List<WebElement> listOfOrders;

    @FindBy(xpath = "//table[contains(@class, 'viewpromocodes')]")
    private WebElement tbl_orderHistory;

    private OrderHistoryPageDesktop() {

    }

    public OrderHistoryPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Order History page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order History page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isSearchByOrderDateDisplayed(){

        return isDisplayed(fld_searchByOrderDate) && isDisplayed(btn_search);

    }

    public boolean isViewByOrderNumberDisplayed(){

        return isDisplayed(fld_viewByOrderNumber) && isDisplayed(btn_view);

    }

    public boolean isOrderHistoryTableDisplayed(){

        return isDisplayed(tbl_orderHistory);

    }

    public void clickOnFirstOrder() {

        click(lnk_firstOrder);
    }

    public void clickOrderNumber(final String orderNumber){
        boolean orderFound = false;
        WebElement order = null;
        for(WebElement element : listOfOrders){
            if(getTextOrValue(element).equalsIgnoreCase(orderNumber)){
                orderFound = true;
                order = element;
            }
        }

        if(orderFound){
            click(order);
        }
        else{
            click(lnk_firstOrder);
        }

    }
}