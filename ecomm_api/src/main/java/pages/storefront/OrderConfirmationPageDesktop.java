package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 6/30/2017.
 */
public class OrderConfirmationPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "/order/ordercomplete.aspx";
    private final static Logger logger = Logger.getLogger(OrderConfirmationPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_btnSubmitOrder']/span")
    private WebElement btn_ContinueShoppping;

    @FindBy(id = "ctl00_CP_GuestAccSigninUC_btnLogin")
    private WebElement btn_login;

    @FindBy(xpath = "//input[contains(@id,'_GuestAccSigninUC_PasswordContainer_txtPassword')]")
    private WebElement fld_password;

    @FindBy(xpath = "//input[contains(@id,'_GuestAccSigninUC_ConfirmPasswordContainer_txtConfirmPassword')]")
    private WebElement fld_retypePassword;

    @FindBy(xpath = "//*[@id='ctl00_CP_GuestAccSigninUC_btnCreateAccount']/span")
    private WebElement btn_createMyAccountNow;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC14_ValidationSummary1']//ul//li | //div[@id='ctl00_CP_ErrorSummaryUC5_ValidationSummary1']//ul//li")
    private WebElement msg_orderFailure;

    @FindBy(xpath = "//div[@id='pagecontent']/h1")
    private WebElement txt_thankYouForCreatingGuestAccount;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC14_ValidationSummary1'] | //div[@id='ctl00_CP_ErrorSummaryUC5_ValidationSummary1']")
    private WebElement txt_orderFailure;

    @FindBy(xpath = "//div[@class='num']/span")
    private WebElement txt_orderNumber;

    @FindBy(xpath = "//a[@id='ctl00_CP_LbMyAccount']")
    private WebElement lnk_MyAccount;

    private OrderConfirmationPageDesktop() {

    }

    public OrderConfirmationPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Order Confirmation page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order Confirmation page is not displayed " + e.getMessage(), e);
        }
    }

    public String getOrderNumber(){
        return getTextOrValue(txt_orderNumber);
    }

    public void loginAndSaveMyOrder(){
        typeIn(fld_password, TestUser.password);
        click(btn_login);
    }

    public void loginAndSaveMyOrder(final String pw){
        typeIn(fld_password, pw);
        click(btn_login);
    }

    public boolean isOrderSuccessfullyMade(){
        isAt();
        try{
            return isDisplayed(txt_orderNumber);
        }catch (Exception e){
            logger.error("Order is not successfully made. " + getTextOrValue(msg_orderFailure));
            throw new RuntimeException("Order is not successfully made. " + getTextOrValue(msg_orderFailure));
        }

//        if(isDisplayed(txt_orderFailure)){
//            logger.error("Order is not successfully made. " + getTextOrValue(msg_orderFailure));
//            throw new RuntimeException("Order is not successfully made. " + getTextOrValue(msg_orderFailure));
//        }
//        else{
//            return true;
//        }
    }

    public void enterPassword(String password) {
        typeIn(fld_password, password);
    }

    public void enterReTypePassword(String reTypePassword) {
        typeIn(fld_retypePassword, reTypePassword);
    }

    public void clickOnCreateAccountButton() {
        click(btn_createMyAccountNow);
    }

    public String getTextThankYouForCreatingGuestAccount() {
        return getTextOrValue(txt_thankYouForCreatingGuestAccount);
    }

    public void clickOnContinueShoppingButton() {
        scrollToElement(btn_ContinueShoppping);
        click(btn_ContinueShoppping);
    }

    public void waitForOrderProcessing() {
        Util.wait(180);
    }

    public void createNewAccountWithPassword(String password) {
        enterPassword(password);
        enterReTypePassword(password);
        clickOnCreateAccountButton();
        click(lnk_MyAccount);
    }
}