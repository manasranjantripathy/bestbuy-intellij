package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class SiteMapPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "sitemap-overview.aspx";
    private final static Logger logger = Logger.getLogger(SiteMapPageDesktop.class);

    @FindBy(xpath  = "//a[@id='ctl00_CP_ctl00_RepSiteMapList_ctl00_RepSubcategories_ctl01_LnkLevel2']")
    private WebElement privacyEnquiry;

    public SiteMapPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Site Map page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Site Map page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickPrivacyEnquiry() { click(privacyEnquiry); }
}