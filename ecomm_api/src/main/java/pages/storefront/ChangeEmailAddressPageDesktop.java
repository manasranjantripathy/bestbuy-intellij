package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;
import utils.Util;

public class ChangeEmailAddressPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/changeemailaddress.aspx";
    private final static Logger logger = Logger.getLogger(ChangeEmailAddressPageDesktop.class);

    @FindBy(xpath = "//input[@id='ctl00_CP_ChangeEmailUC1_CurrentEmailContainer_txtCurrentEmail']")
    private WebElement fld_oldEmail;

    @FindBy(xpath = "//input[@id='ctl00_CP_ChangeEmailUC1_NewEmailContainer_txtNewEmail']")
    private WebElement fld_newEmail;

    @FindBy(xpath = "//input[@id='ctl00_CP_ChangeEmailUC1_ConfirmNewEmailContainer_txtConfirmNewEmail']")
    private WebElement fld_retypeNewEmail;

    @FindBy(xpath = "//a[@id='ctl00_CP_ChangeEmailUC1_BtnChangeEmail']")
    private WebElement btn_submit;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC1_vsumErrorSummary']/ul/li")
    private WebElement msg_error;

    public ChangeEmailAddressPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Change Email Address page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Change Email Address page is not displayed " + e.getMessage(), e);
        }
    }

    public void enterNewEmailAddress(){
        String randomEmail = "bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com";
        TestUser.email = randomEmail;
        typeIn(fld_newEmail, TestUser.email);
        typeIn(fld_retypeNewEmail, TestUser.email);
    }

    public void enterOldEmailInNewEmail(TestUser testUser){
        typeIn(fld_newEmail, TestUser.newEmail);
        typeIn(fld_retypeNewEmail, TestUser.newEmail);
    }

    public void clickSubmit(){
        click(btn_submit);
    }

    public String verifyErrorMessage(){
        return getTextOrValue(msg_error);
    }

}