package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class ResetPasswordPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/resetpassword.aspx?";

    private final static Logger logger = Logger.getLogger(ResetPasswordPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//p[contains(@class,'reqfield')]")
    private WebElement emailRequiredText;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Continue')]")
    private WebElement textButtonContinue;

    @FindBy(how = How.XPATH, using = "//div[@id='pagecontent']/h1")
    private WebElement textPageContent;

    @FindBy(how = How.XPATH, using = "//div[contains(@id,'_vsumErrorSummary')]/UL")
    private WebElement errorMessage;

    private ResetPasswordPageDesktop() {

    }

    public ResetPasswordPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Reset Password page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Reset Password page is not displayed " + e.getMessage(), e);
        }
    }

    public String getRequireTextForEmail() { return getTextOrValue(emailRequiredText);}

    public String getContinueButtonText() { return getTextOrValue(textButtonContinue);}

    public String getForgetPasswordText() { return getTextOrValue(textPageContent); }

    public void clickContinueButton() {click(textButtonContinue);}

    public String getErrorMessageText() { return getTextOrValue(errorMessage); }
}