package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;
import utils.Util;

import java.util.List;

/**
 * Created by sucho on 6/29/2017.
 */
public class CartPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/basket.aspx";
    private final static Logger logger = Logger.getLogger(CartPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_btnSubmitOrder")
    private WebElement btn_qtyUpdate;

    @FindBy(how = How.ID, using = "AddWarranty")
    private WebElement btn_addToCartOnPSP;

    @FindBy(how = How.ID, using = "ctl00_CP_btnSubmitOrder")
    private WebElement btn_checkout;

    @FindBy(how = How.ID, using = "ctl00_CP_btnReserveInStore")
    private WebElement btn_reserveInStore;

    @FindBy(how = How.ID, using = "ctl00_CP_btnPayPal")
    private WebElement btn_checkoutWithPayPal;

    @FindBy(how = How.XPATH, using = "//img[@id='btnVisaCheckout']")
    private WebElement btn_visaCheckout;

    @FindBy(how = How.ID, using = "btn-save-changes")
    private WebElement btn_submitOnDeliveryOptions;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'View delivery options')] | //span[contains(text(), 'Consultez les options de livraison')]")
    private WebElement btn_viewDeliveryOptions;

    @FindBy(how = How.ID, using = "ctl00_CP_btnReserveInStore")
    private WebElement btn_reserveInStore2;

    @FindBy(how = How.XPATH, using = "//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ChkAcceptTerms')]")
    private WebElement chk_BoxAcceptTermsOnPSP;

    @FindBy(how = How.XPATH, using = "//div[@class='input-group']//input[contains(@class, 'search-input')]")
    private WebElement fld_changeDeliveryLocationInput;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_QuantityContainer_TxtQuantity")
    private WebElement fld_itemQty;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxPstTotal")
    private WebElement fld_estimatedPSTorQST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxGstTotal")
    private WebElement fld_estimatedGST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxHstTotal")
    private WebElement fld_estimatedHST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblOrderTotal")
    private WebElement fld_totalAmount;

    @FindBy(xpath = "//input[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_QuantityContainer_TxtQuantity']")
    private WebElement fld_quantityUpdate1;

    @FindBy(xpath = "//input[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl01_UcCartItem_LvCartItem_ctrl0_QuantityContainer_TxtQuantity']")
    private WebElement fld_quantityUpdate2;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_ucPriceBlockWithPromotional_hypEhfText")
    private WebElement label_envHandlingFeeInCartSection;

    @FindBy(className = "price-desc")
    private WebElement label_envHandlingFeeInOrderSection;

    @FindBy(how = How.XPATH, using = "//li[contains(@class,'warranty-item')]//a[contains(@id,'_ctrl0_UcOrderProduct_HypShortProductLabel')]")
    private WebElement label_pspInCart;

    @FindBy(xpath = "//div[@class='prod-saving']")
    private WebElement label_discount;

    @FindBy(xpath = "//li[span[@id[contains(.,'UcCartOrderTotals_LblSurcharge')]]]/span[1]")
    private WebElement label_ehf;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_LblSpecialDeliveryChargePrompt']")
    private WebElement label_scheduledDelivery;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_LblShippingChargePrompt']")
    private WebElement label_shippingDelivery;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_HypOrderDiscountsPrompt']")
    private WebElement label_orderLevelDiscount;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblTaxPstTotalPrompt']")
    private WebElement label_estimatedPSTorQST;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblTaxGstTotalPrompt']")
    private WebElement label_estimatedGST;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_pnlVisaCheckout']/div[1]/a")
    private WebElement lnk_visaCheckoutTellMeMore;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCartOrderTotals_HypChange")
    private WebElement lnk_deliveryLocation;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories")
    private WebElement lnk_seeAllAccessories;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl02_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories']")
    private WebElement lnk_seeAllAccessories2;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl04_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories']")
    private WebElement lnk_seeAllAccessories3;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl06_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories']")
    private WebElement lnk_seeAllAccessories4;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl04_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories']")
    private WebElement lnk_seeAllAccessories5;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl05_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories']")
    private WebElement lnk_seeAllAccessories6;

    @FindBy(how = How.ID, using = "ctl00_CP_MccpHyperLink1")
    private WebElement lnk_continueShipping;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_btnSubmitOrder")
    private WebElement lnk_quantityUpdate1;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl01_UcCartItem_LvCartItem_ctrl0_btnSubmitOrder']")
    private WebElement lnk_quantityUpdate2;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_LnkRemove")
    private WebElement lnk_removeItem;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl01_UcCartItem_LvCartItem_ctrl0_LnkRemove']")
    private WebElement lnk_removeItem2;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_UcOrderProduct_HypShortProductLabel']")
    private WebElement lnk_productTitle1;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl01_UcCartItem_LvCartItem_ctrl0_UcOrderProduct_HypShortProductLabel']")
    private WebElement lnk_productTitle2;

    @FindBy(xpath = "//a[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl03_UcCartItem_LvCartItem_ctrl0_UcOrderProduct_HypShortProductLabel']")
    private WebElement lnk_productTitle4;

    @FindBy(how = How.CLASS_NAME, using = "empty-cart-msg")
    private WebElement msg_emptyCart;

    @FindBy(xpath = "//span[@id='ctl00_CP_ConfirmationMessage_ConfirmationMessage']")
    private WebElement msg_confirmation;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorOrderUCOrders_ValidationSummary1']/ul/li")
    private WebElement msg_error;

    @FindBy(xpath = "//li[contains(@id, '_accessoriesWidgetTab')]//a[contains(text(), 'Protection Plans')] | //li[contains(@id, '_accessoriesWidgetTab')]//a[contains(text(), 'Plans de protection')]")
    private WebElement tab_psp;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblSurcharge']")
    private WebElement txt_priceEHF;

    @FindBy(xpath = "//div[@class='prod-saving prod-promo-saving']/span[1]")
    private WebElement txt_promotionalSavingsInTheCart;

    @FindBy(xpath = "//div[@class='prodprice price-onsale']/span")
    private WebElement txt_actualProductPriceInCartPage;

    @FindBy(xpath = "//span[@id='ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_LblTotalPrice']")
    private WebElement txt_priceAfterDiscountInCartPage;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_LblProductTotal']")
    private WebElement txt_productTotalPrice;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_LblSubTotal']")
    private WebElement txt_subTotalPrice;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_ucCartOrderSubTotals_LblShippingAndHandle']")
    private WebElement txt_shippingDeliveryPrice;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblTaxPstTotal']")
    private WebElement txt_estimatedPSTorQSTPrice;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblTaxGstTotal']")
    private WebElement txt_estimatedGSTPrice;

    @FindBy(xpath = "//span[@id='ctl00_CP_UcCartOrderTotals_LblOrderTotal']")
    private WebElement txt_totalPrice;

    @FindBy(how = How.XPATH, using = "//img[@class='checkout-logo']")
    private WebElement txt_visaCheckout;

    @FindBy(how = How.ID, using = "cart-accessories-popover")
    private WebElement cartAccessoriesPopup;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'shipment-title')]")
    private WebElement divShipmentTitleOnDeliveryOption;

    @FindBy(how = How.CLASS_NAME, using = "product-items")
    private WebElement itemsOnDeliveryOption;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'delivery-options')]")
    private WebElement shippingOptionsOnDeliveryOptions;

    @FindBy(how = How.XPATH, using = "//div[@id='VmeCheckout']//iFrame")
    private WebElement tellMeMoreFrame;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_UcCartOrderTotals_UcCartPromoCode_pnlPromoCode']/div/div[1]/h3/a | //*[contains(@id,'PromotionalCode')]/div/div[1]/h3/a")
    private WebElement expandPromoCodeButton;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucOrderPromoCodes_txtPromoCodeContainer_txtPromoCode'] | //*[@id='ctl00_CP_UcCartOrderTotals_UcCartPromoCode_txtPromoCodeContainer_txtPromoCode']")
    private WebElement promoCodeField;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_UcCartOrderTotals_UcCartPromoCode_LnkApplyToOrderAndAddMoreCodes')] | //*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucOrderPromoCodes_IbtnApplyToOrder']/span | //*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucOrderPromoCodes_IbtnApplyToOrder']")
    private WebElement applyPromoCodeToOrder;

    @FindBy(how =How.XPATH, using = "//*[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_pnlPromoCodeList']/div")
    private WebElement promoCodeApplied;



    private CartPageDesktop() {

    }

    public CartPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Cart page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Cart page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isCartEmpty(){

        return isDisplayed(msg_emptyCart);

    }



    public boolean isPspAddedInTheCart(){

        return isDisplayed(label_pspInCart);

    }

    public boolean isDiscountApplied(){

        return isDisplayed(label_discount);

    }

    public boolean isEnvHandlingFeeDisplayedInOrderSection(){

        return isDisplayed(label_envHandlingFeeInOrderSection);

    }

    public boolean isCartAccessoriesPopupDisplayed(){

        return isDisplayed(cartAccessoriesPopup);

    }

    public boolean isDeliveryOptionsDisplaysInformation(){

        return isDisplayed(divShipmentTitleOnDeliveryOption) && isDisplayed(itemsOnDeliveryOption) && isDisplayed(shippingOptionsOnDeliveryOptions);
    }

    public void clickCheckoutButton(){

        //click(buttonCheckout);
        clickByJs(btn_checkout);

    }

    public void clickPayPalButton(){
        scrollToElement(btn_checkoutWithPayPal);
        clickByJs(btn_checkoutWithPayPal);

    }

    public void clickDeliveryChangeLink(){

        click(lnk_deliveryLocation);

    }

    public void clickRemoveItem(){

        click(lnk_removeItem);

    }

    public void clickRemoveItem2() {
        click(lnk_removeItem2);
    }

    public void clickReserveInStore(){

        click(btn_reserveInStore);

    }

    public void clickSeeAllAccessoriesLink() {
        click(lnk_seeAllAccessories);
    }

    public void clickSeeAllAccessoriesLink2(){
        click(lnk_seeAllAccessories2);
    }

    public void clickSeeAllAccessoriesLink3(){
        click(lnk_seeAllAccessories3);
    }

    public void clickSeeAllAccessoriesLink4(){
        click(lnk_seeAllAccessories4);
    }

    public void clickSubmitButtonOnDeliveryOption(){

        click(btn_submitOnDeliveryOptions);

    }

    public void typePostalCodeOnChangeDeliveryLocationPopup(final String postalCode){

        typeIn(fld_changeDeliveryLocationInput, postalCode);

        click(btn_viewDeliveryOptions);

    }

    public void changeDeliveryOption(final String postalCode){

        click(lnk_deliveryLocation);

        typeIn(fld_changeDeliveryLocationInput, postalCode);

        click(btn_submitOnDeliveryOptions);
    }

    public void updateItemQuantity(final int quantity){

        typeIn(fld_itemQty, quantity);

        click(btn_qtyUpdate);

    }

    public int getItemQty(){

        return Integer.valueOf(getTextOrValue(fld_itemQty));

    }

    public int getTheNumOfItemsInCart(){
        try{
            List<WebElement> elements = driver.findElements(By.xpath("//li[@class='cart-line-item product-item cart_quantity']"));
            return elements.size();
        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String getItemTitleInTheCart(final int nth){
        try{
            List<WebElement> elements = driver.findElements(By.xpath("//ul[@class='cart-top-border cart-list']//li"));
            WebElement element = elements.get(nth).findElement(By.id("ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_UcOrderProduct_HypShortProductLabel"));
            return element.getAttribute("value").trim();
        }
        catch(Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String getEnvHandlingFeesFromCartSection(){

        return getTextOrValue(label_envHandlingFeeInCartSection);

    }

    public String getEstimatedPSTorQST(){

        return isDisplayed(fld_estimatedPSTorQST) ? getTextOrValue(fld_estimatedPSTorQST) : "";

    }

    public String getEstimatedGST(){

        return isDisplayed(fld_estimatedGST) ? getTextOrValue(fld_estimatedGST) : "";

    }

    public String getEstimatedHST(){

        return isDisplayed(fld_estimatedHST) ? getTextOrValue(fld_estimatedHST) : "";
    }

    public String getTotal(){

        return getTextOrValue(fld_totalAmount);

    }

    public void addProtectionServicePlan(final int years){
        try {
            waitUntilPageLoaded();

            click(tab_psp);

            WebElement pspCheckOutForm;
            if(years == 3){
                pspCheckOutForm = driver.findElement(By.xpath("//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ctl01')]"));
            }
            else { // by default add 2years plan
                pspCheckOutForm = driver.findElement(By.xpath("//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ctl07')]"));
            }

            click(pspCheckOutForm);

            scrollToElement(chk_BoxAcceptTermsOnPSP);

            click(chk_BoxAcceptTermsOnPSP);

            click(btn_addToCartOnPSP);

            Util.waitForAjax(driver);
            clickByJSQuery(".btnClose");
        }
        catch (Exception e){
            logger.error("PSP form element is not visible " + e.getMessage(), e);
            throw new RuntimeException("PSP form element is not visible " + e.getMessage(), e);
        }
    }

    public void removeAllItemsInTheCart(){
        waitUntilPageLoaded();
        String item = "//li[@class='cart-line-item product-item cart_quantity']";
        List<WebElement> cartItems = driver.findElements(By.xpath(item));

        for (WebElement cartItem : cartItems) {
            String removeLink = "//div[@class = 'remove-but']//a";
            WebElement remove = cartItem.findElement(By.xpath(removeLink));
            click(remove);
        }
    }

    public void clickReserveInStoreBtn() {
        click(btn_reserveInStore2);

    }

    public boolean isEHFLabelDisplayed(){

        return isDisplayed(label_ehf);

    }

    public void updateQuantityOfChargedItem(int quantity) {
        typeIn(fld_quantityUpdate2, quantity);
        click(lnk_quantityUpdate2);
    }

    public void updateQuantityOfFreeItem(int quantity) {
        typeIn(fld_quantityUpdate1, quantity);
        click(lnk_quantityUpdate1);
    }

    public void clickOnPromoCode() {click(expandPromoCodeButton);}

    public void enterPromoCode() {typeIn(promoCodeField, Constants.Payment.PROMOTION_CODE_VISA_CHECKOUT.toString());}

    public void clickPromoCodeApplyButton(){click(applyPromoCodeToOrder);}

    public boolean promoCodeAppliedText() {return isDisplayed(promoCodeApplied);}

    public String getQuantityOfFreeItem() {
        waitUntilPageLoaded();
        return getTextOrValue(fld_quantityUpdate1);
    }

    public String getQuantityOfChargedItem() {
        waitUntilPageLoaded();
        return getTextOrValue(fld_quantityUpdate2);
    }

    public boolean verifyItemsInCart(int items){
        waitUntilPageLoaded();
        return (getTheNumOfItemsInCart() == items);
    }

    public void clickVisaCheckoutButton() {

        click(btn_visaCheckout);

    }

    public void clickOnVisaCheckoutTellMeMoreLink() {

        click(lnk_visaCheckoutTellMeMore);

    }

    public void switchToTellMeMoreFrame() {

        switchToFrame(tellMeMoreFrame);

    }

    public boolean isVisaCheckoutTextDisplayed() {
        switchToFrame(tellMeMoreFrame);
        return isDisplayed(txt_visaCheckout);

    }

    public boolean isScheduledDeliveryLabelDisplayed() {

        return isDisplayed(label_scheduledDelivery);

    }

    public String getConfirmationText() {

        return getTextOrValue(msg_confirmation);

    }

    public String getTextProductTitle1() {

        return getTextOrValue(lnk_productTitle1);

    }

    public void clickProductTitle1() {

        click(lnk_productTitle1);

    }

    public String getTextProductTitle2() {

        return getTextOrValue(lnk_productTitle2);

    }

    public String getTextProductTitle4() {

        return getTextOrValue(lnk_productTitle4);

    }

    public String getTextErrorMessage() {

        return getTextOrValue(msg_error);

    }

    public double getTextActualProductPriceInCartPage() {

        return toDouble(getTextOrValue(txt_actualProductPriceInCartPage));

    }

    public double getTextPriceAfterDiscountInCartPage() {

        TestUser.productTotal = getTextOrValue(txt_priceAfterDiscountInCartPage);

        return toDouble(getTextOrValue(txt_priceAfterDiscountInCartPage));

    }

    public double getTextEHFPrice() {

        return toDouble(getTextOrValue(txt_priceEHF));

    }

    public double getTextEstimatedPSTorQSTPrice() {

        return toDouble(getTextOrValue(txt_estimatedPSTorQSTPrice));

    }

    public double getTextEstimatedGSTPrice() {

        return toDouble(getTextOrValue(txt_estimatedGSTPrice));

    }

    public double getTextSubTotalPriceInCartPage() {

        return toDouble(getTextSubTotalPrice());

    }

    public double getTextProductTotalPriceInCartPage() {

        return toDouble(getTextProductTotalPrice());

    }

    public double getTextTotalPrice() {

        return toDouble(getTextOrValue(txt_totalPrice));

    }

    public double getTextSubTotalPriceInCart() {

        return toDouble(getTextOrValue(txt_subTotalPrice));

    }

    public boolean verifyDiscountedPriceInCart(){
        Logger.getLogger("Actual Product Price: " + getTextActualProductPriceInCartPage() + ", " +
                "Price After Discount: " + getTextPriceAfterDiscountInCartPage());
        return (getTextActualProductPriceInCartPage() > getTextPriceAfterDiscountInCartPage());
    }

    public boolean verifyOrderLevelDiscountPriceAppliedInCart(){
        Logger.getLogger("ProductTotal:" + getTextProductTotalPriceInCartPage() + ", SubTotal: " + getTextSubTotalPriceInCartPage());
        return (getTextProductTotalPriceInCartPage() > getTextSubTotalPriceInCartPage());
    }

    public String getTextSubTotalPrice() {

        TestUser.subTotal = getTextOrValue(txt_subTotalPrice);

        return getTextOrValue(txt_subTotalPrice);
    }

    public String textPromotionalSavingsInTheCart() {

        return getTextOrValue(txt_promotionalSavingsInTheCart);

    }

    public String textTotalSavingsInTheCart() {

        return getTextOrValue(label_discount);

    }

    public String getTextProductTotalPrice() {

        return getTextOrValue(txt_productTotalPrice);

    }

    public String getTextShippingDeliveryLabel() {

        return getTextOrValue(label_shippingDelivery);

    }

    public String getTextShippingDeliveryPrice() {

        return getTextOrValue(txt_shippingDeliveryPrice);

    }

    public String getTextOrderLevelDiscount() {

        return getTextOrValue(label_orderLevelDiscount);

    }

    public boolean isEstimatedPSTorQSTDisplayed() {

        return isDisplayed(label_estimatedPSTorQST);

    }

    public boolean isEstimatedGSTDisplayed() {

        return isDisplayed(label_estimatedGST);

    }
}