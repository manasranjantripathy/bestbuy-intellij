package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

import java.util.List;

/**
 * Created by sucho on 7/18/2017.
 */
public class AccountSummaryPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/accountsummary.aspx";
    private final static Logger logger = Logger.getLogger(AccountSummaryPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_UcAccountSummaryOrderHistory_BtnSearchByDate")
    private WebElement btn_go;

    @FindBy(how = How.ID, using = "ctl00_CP_UcAccountSummaryOrderHistory_OrderDateFieldContainer_DdlTimePeriod")
    private WebElement fld_searchOrder;

    @FindBy(xpath = "//*[@id='ctl00_CP_UcOrderHistory_OrderNumberContainer_TxtSearchByOrderNumber']")
    private WebElement fld_enterOrderNumber;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkManageAddressBook']")
    private WebElement lnk_manageAddressBook;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkManageCreditCards']")
    private WebElement lnk_manageCreditCards;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'_LnkChangePassword')]")
    private WebElement lnk_changePassword;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkCheckGiftCardBalance']")
    private WebElement lnk_checkGiftCardBalance;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkChangeLanguagePreferences']")
    private WebElement lnk_changeLanguagePreferences;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAdditionalSettings_LnkPreferredStores']")
    private WebElement lnk_preferredStores;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAdditionalSettings_LnkWishList']")
    private WebElement lnk_wishList;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkChangeEmailAddress']")
    private WebElement lnk_changeEmailAddress;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountInfo_LnkChangePersonalInformation']")
    private WebElement lnk_changePersonalInformation;

    @FindBy(xpath = "//div[contains(@id,'ctl00_CP_ErrorSummary')]/ul/li")
    private WebElement msg_signInError;

    @FindBy(xpath = "//span[@id='ctl00_CP_ConfirmationMessage_ConfirmationMessage']")
    private WebElement msg_confirm;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcAccountSummaryOrderHistory_RptTopOrderHistoryList_ctl00_HypTopToOrderDetails']")
    private WebElement lnk_firstOrder;

    @FindBy(xpath = "//div[@class='row-fluid']//tbody//a")
    private List<WebElement> listOfOrders;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderHistory_BtnSearchByOrderNumber']")
    private WebElement btn_search;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcOrderHistory_RptBottomOrderHistoryList_ctl00_LnkTopToOrderDetails']")
    private WebElement filterOrder;



    private AccountSummaryPageDesktop(){

    }

    public AccountSummaryPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Account Summary page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Account Summary page is not displayed " + e.getMessage(), e);
        }
    }

    public void searchByOrderDate(final int nth){

        select(fld_searchOrder, String.valueOf(nth));

    }

    public void clickGo(){

        click(btn_go);

    }

    public void clickManageAddressBookLink(){
        click(lnk_manageAddressBook);
    }

    public void clickManageCreditCards() {
        click(lnk_manageCreditCards);
    }

    public void clickChangeInPassword() {
        click(lnk_changePassword);
    }

    public void clickCheckGiftCardBalance() {
        click(lnk_checkGiftCardBalance);
    }

    public void clickChangeLanguagePreferences() {
        click(lnk_changeLanguagePreferences);
    }

    public void clickPreferredStores() {
        click(lnk_preferredStores);
    }

    public void clickWishList() {
        click(lnk_wishList);
    }

    public void clickChangeEmailAddress() {
        click(lnk_changeEmailAddress);
    }

    public void clickChangePersonalInformation() {
        click(lnk_changePersonalInformation);
    }

    public String getErrorMessageForSignIn() {
        return getTextOrValue(msg_signInError);
    }

    public String verifyConfirmMessage() {
        return getTextOrValue(msg_confirm);
    }

    public void clickLinksInMyAccount(final String link){
        if(link.equalsIgnoreCase("manageCreditCard")){
            clickManageCreditCards();
        }
        else if(link.equalsIgnoreCase("changePersonalInfo")){
            clickChangePersonalInformation();
        }
        else if(link.equalsIgnoreCase("changePassword")){
            clickChangeInPassword();
        }
    }

    public void clickOrderNumber(final String orderNumber) {
        boolean orderFound = false;
        WebElement order = null;
        for(WebElement element : listOfOrders){
            if(getTextOrValue(element).equalsIgnoreCase(orderNumber)){
                orderFound = true;
                order = element;
            }
        }

        if(orderFound){
            click(order);
        }
        else{
            click(lnk_firstOrder);
        }
    }

    public void searchForOrderDetails(String orderNumber) {
        typeIn(fld_enterOrderNumber, orderNumber);
        clickByJs(btn_search);
        click(filterOrder);
    }
}