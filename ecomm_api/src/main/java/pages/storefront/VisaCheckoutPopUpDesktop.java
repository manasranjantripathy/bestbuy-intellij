package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;
import utils.Util;

public class VisaCheckoutPopUpDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private CartPageDesktop cartPage;

    private final static Logger logger = Logger.getLogger(AccountSummaryPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//iframe[@title='Visa Checkout']")
    private WebElement frame_id;

    @FindBy(how = How.XPATH, using = "//i[@class='header-img-visaLogo header-logo']")
    private WebElement TITLE;

    @FindBy(how = How.XPATH, using = "//input[name='Sign in to Visa Checkout']")
    private WebElement signInButton;

    @FindBy(how = How.XPATH, using = "//input[@class='viewInput viewInput--default']")
    private WebElement emailField;

    @FindBy(how = How.XPATH, using = "//input[@name='Continue']")
    private WebElement continueButton;

    @FindBy(how = How.XPATH, using = "//input[@class='viewInput viewInput--default'][@autocomplete='email']")
    private WebElement signInField;


    @FindBy(how = How.XPATH, using = "//input[@id='userName']")
    private WebElement userName;

    @FindBy(how = How.XPATH, using = "//*[@id='login']/form/div[2]/div/input")
    private WebElement loginButton;

    @FindBy(how = How.XPATH, using = "//input[@id='password']")
    private WebElement userPasswordField;

    @FindBy(how = How.XPATH, using = "//input[@class='viewInput viewInput--default'][@autocomplete='password']")
    private WebElement passwordField;

    @FindBy(how = How.XPATH, using = "//input[@class='viewButton-button'][@name='Sign in to Visa Checkout']")
    private WebElement signInToVisaCheckoutButton;

    @FindBy(how = How.XPATH, using = "//span[text()='Continue as new customer']/..")
    private WebElement continueAsNewUserButton;

    @FindBy(how = How.XPATH, using = "//iframe[@id='VMECheckoutIframe']")
    private WebElement registerFrame;

    @FindBy(how = How.XPATH, using = "//*[@id='first_name']")
    private WebElement firstName;


    @FindBy(how = How.XPATH, using = "//*[@id='last_name']")
    private WebElement lastName;


    @FindBy(how = How.XPATH, using = "//*[@id='address_line1']")
    private WebElement address;

    @FindBy(how = How.XPATH, using = "//*[@id='address_city']")
    private WebElement city;


    @FindBy(how = How.XPATH, using = "//*[@id='address_state_province_code']")
    private WebElement province;

    @FindBy(how = How.XPATH, using = "//*[@id='address_phone']")
    private WebElement phone;


    @FindBy(how = How.XPATH, using = "//*[@id='cardNumber-CC']")
    private WebElement cardNumber;

    @FindBy(how = How.XPATH, using = "//*[@id='expiry']")
    private WebElement expiryDate;

    @FindBy(how = How.XPATH, using = "//*[@id='addCardCVV']")
    private WebElement securityCode;


    @FindBy(how = How.XPATH, using = "//*[@id='cardholderPasswordId']")
    private WebElement customerPassword;


    @FindBy(how = How.XPATH, using = "//a[@aria-label='Continue without setting']")
    private WebElement noThanksButton;

    @FindBy(how = How.XPATH, using = "//div[@class='details']/p[2]/span[2]")
    private WebElement saveCardNumber;

    @FindBy(how = How.XPATH, using = "//input[@value='Continue to Merchant']")
    private WebElement continueMerchant;

    @FindBy(how = How.XPATH, using = "//*[@id='address_postal_code']")
    private WebElement postalCode;

    @FindBy(how = How.XPATH, using = "//span[@class='checkbox']")
    private WebElement checkBox;

    @FindBy(how = How.XPATH, using = "//input[@name='Sign Up & Continue'] | //input[@name='Sign Up']")
    private WebElement signUpButton;

    @FindBy(how = How.XPATH, using = "//*[@type='submit'] | //*[@aria-label='Update card']")
    private WebElement secondContinue;

    @FindBy(how = How.XPATH, using = "//*[@aria-label='Update card']")
    private WebElement updateCardButton;

    @FindBy(how = How.XPATH, using = "//a[@text='Cancel and return to Best Buy Canada']")
    private WebElement cancelButton;

    @FindBy(how = How.XPATH, using = "//*[@id='v-add-new-address-form']/div/div/fieldset[1]/section/div[4]/div[1]/label[2]")
    private WebElement visaCheckoutCustomerUSErrorProvince;

    @FindBy(how = How.XPATH, using = "//input[@id='view-address-not-found-add-button']")
    private WebElement addAddressButton;

    @FindBy(how = How.XPATH, using = "//*[@aria-label='Edit this card']")
    private WebElement editButton;

    @FindBy(how = How.XPATH, using = "//input[@name='Change']")
    private WebElement changeButton;


    private VisaCheckoutPopUpDesktop(){

    }

    public VisaCheckoutPopUpDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    private void switchToVisaFrame() {
        delay(10);
        driver.switchTo().frame(frame_id);

    }

    private void switchToMainContent() {
        driver.switchTo().defaultContent();
    }

    public boolean isAt() {
        try {
            switchToVisaFrame();
            isDisplayed(TITLE);
            switchToMainContent();
            return true;

        }
        catch(Exception e){
            logger.error("VISA CHECKOUT POPUP is not displayed" + e.getMessage(), e);
            throw new RuntimeException("VISA CHECKOUT POPUP is not displayed " + e.getMessage(), e);
        }

    }

    public void clickEditButton() {
//        delay(300);
        switchToRegistrationFrame();
        click(editButton);
        click(changeButton);
        switchToMainContent();
    }

    public boolean isSignInDisplayed() {
        switchToVisaFrame();
        boolean result = isDisplayed(signInButton);
        switchToMainContent();

        return result;
    }

    public void typeEmailAndContinue(String email) {
        switchToVisaFrame();
        click(emailField);
        typeIn(emailField, email);
        click(continueButton);
        switchToMainContent();
    }

    public void clickOnSignInLink() {
        isAt();
        typeEmailAndContinue(TestUser.visaUsername);
    }

    public void clickOnSignInVBVLink() {
        isAt();
        typeEmailAndContinue(TestUser.VisaCheckoutVBVUsername);
    }


    public void signIn(String password) {
        switchToVisaFrame();
//        delay(300);
        click(userPasswordField);
        typeIn(userPasswordField, password);
        click(loginButton);
        switchToMainContent();
    }

    public void signInWithEmailAndPassword(String email, String password) {
        switchToVisaFrame();
        click(userName);
        typeIn(userName,email);
        click(userPasswordField);
        typeIn(userPasswordField, password);
        click(loginButton);
        switchToMainContent();
    }

    public void continueAsNewUser() {
        switchToVisaFrame();
        try {
            click(continueAsNewUserButton);
        }
        catch (Exception ex)
        {}
        switchToMainContent();
    }

    public void switchToRegistrationFrame() {
        driver.switchTo().frame(registerFrame);
    }

    public boolean isRegisterPopupDisplayed() {
        boolean result = true;

        switchToRegistrationFrame();

        try {
            isDisplayed(firstName);
        }
        catch (Exception ex) {
            result = false;
        }

        switchToMainContent();

        return result;

    }

    public void register(String creditCardType) {
        fillInUserInfoAndContinue();
        fillInCardInfoAndContinue(creditCardType);
        typePasswordAndSignIn();
    }

    public void fillingCreditCard(String cardType) {

        fillInCardInfoAndContinue(cardType);

    }

    public void editCreditCard(String cardType) {

        editInCardInfoAndContinue(cardType);

    }



    public void fillInUserInfoAndContinue() {
//        if (isRegisterPopupDisplayed() == false)
//            logException("register popup is not displayed");

        switchToRegistrationFrame();

        String randomAddress = "Glenlyon Park " + String.valueOf(Util.getRandomNumber());

        click(firstName);
        typeIn(firstName,  TestUser.firstName);
        click(address);
        typeIn(address,    randomAddress.toString());
        click(lastName);
        typeIn(lastName,   TestUser.lastName);
        click(city);
        typeIn(city,       TestUser.city);
        click(province);
        typeIn(province,   TestUser.provinceVisaCheckout);
        click(postalCode);
        typeIn(postalCode, TestUser.postalCodeBC);
        click(phone);
        typeInHitEnter(phone,      TestUser.visaCheckoutCustomerPhone);


        click(continueButton);

        switchToMainContent();
    }

    public void fillInUserInfoWithUSProvince() {

        continueAsNewUser();
        typeEmailAndContinue("bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com");
//        closeAndReopen();
        switchToRegistrationFrame();
        String randomAddress = "Glenlyon Park " + String.valueOf(Util.getRandomNumber());
        click(firstName);
        typeIn(firstName,  TestUser.firstName);
        click(address);
        typeIn(address,    randomAddress.toString());
        click(lastName);
        typeIn(lastName,   TestUser.lastName);
        click(city);
        typeIn(city,       TestUser.city);
        click(province);
        typeIn(province,   TestUser.visaUSProvince);
        click(postalCode);
        typeIn(postalCode, TestUser.postalCodeBC);

    }

    public void fillInCardInfoAndContinue(String cardType) {
        switchToRegistrationFrame();
        delay(3);
        try{
            click(addAddressButton);
        }
        catch(Exception e) {}

        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            typeInHitEnter(cardNumber,  Constants.Payment.AMEX_CREDITCARD_TYPE.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.AMEX_CID.toString());
        }

        else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            typeInHitEnter(cardNumber,  Constants.Payment.MC_CREDITCARD_NUMBER.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.MC_CID.toString());
        }

        else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
//            cardNumber.click();
            typeInHitEnter(cardNumber,  Constants.Payment.VISA_CREDITCARD_NUMBER.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.VISA_CID.toString());
        }

        click(secondContinue);
        switchToMainContent();
    }

    public void editInCardInfoAndContinue(String cardType) {
        switchToRegistrationFrame();
        delay(3);
        try{
            click(addAddressButton);
        }
        catch(Exception e) {}

        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            typeInHitEnter(cardNumber,  Constants.Payment.AMEX_CREDITCARD_TYPE.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.AMEX_CID.toString());
        }

        else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            typeInHitEnter(cardNumber,  Constants.Payment.MC_CREDITCARD_NUMBER.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.MC_CID.toString());
        }

        else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
//            cardNumber.click();
            typeInHitEnter(cardNumber,  Constants.Payment.VISA2_CREDITCARD_NUMBER.toString());
            typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
            typeInHitEnter(securityCode, Constants.Payment.VISA_CID.toString());
        }

        click(updateCardButton);
        switchToMainContent();
    }

    public void typePasswordAndSignIn() {
        switchToRegistrationFrame();
        click(customerPassword);
        customerPassword.clear();
        typeInHitEnter(customerPassword, TestUser.visaCheckoutCustomerPassword);
//
//        try {
//            scrollToElement(signUpButton);
        scrollAndClick(signUpButton);
//        }
//        catch(Exception e) {}
        switchToMainContent();

        continueToMerchant();

    }
    public void continueToMerchant(){
        switchToRegistrationFrame();

        try {
            click(continueMerchant);
        }
        catch(Exception e) {}
        try {
            click(noThanksButton);
        }
        catch(Exception e) {}
        switchToMainContent();
    }

    public void registersForVisaCheckout(String creditCardType) {
        continueAsNewUser();
        typeEmailAndContinue("bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com");
//        closeAndReopen();
        register(creditCardType);
    }

    public void enterEmailAndShippingAddresForVisaCheckout() {
        continueAsNewUser();
        typeEmailAndContinue("bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com");
//        closeAndReopen();
        fillInUserInfoAndContinue();
    }

    public void visaCheckoutRegisteredUserLogin() {

        if (isSignInDisplayed() == true) {
            signInWithEmailAndPassword(TestUser.visaUsername, TestUser.password);
        } else {
            signIn(TestUser.password);
        }
        continueToMainSite();
    }

    public void visaCheckoutVBVRegisteredUserLogin() {

        if (isSignInDisplayed() == true) {
            signInWithEmailAndPassword(TestUser.VisaCheckoutVBVUsername, TestUser.password);
        } else {
            signIn(TestUser.password);
        }
        continueToMainSite();
    }

    public void continueToMainSite(){
        switchToRegistrationFrame();
        try {
            click(continueButton);
        }
        catch (Exception ex) {

        }

        clickOnNotThanksButton();
    }

    public void clickOnNotThanksButton() {
        try {
            click(noThanksButton);
        }
        catch (Exception ex)
        {}
    }



    public void closeAndReopen() {
        closePopup();
        cartPage.isAt();
        cartPage.clickVisaCheckoutButton();
    }

    private void closePopup() {
        switchToRegistrationFrame();

        click(cancelButton);

        switchToMainContent();
    }

    public void theUserEntersUsProvince(){
        continueAsNewUser();
        int i = 3;
        for (; i > 0; i--) {
            try {
                typeEmailAndContinue("bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com");
                if (isRegisterPopupDisplayed() == true)
                    break;
            }
            catch (Exception ex) {
                closeAndReopen();
            }//catch
        }//for
        if (i == 0)
            throw new RuntimeException("cannot register for visa checkout!");
        fillInUserInfoWithUSProvince();
    }

    public boolean errorTextForUSProvince() {return isDisplayed(visaCheckoutCustomerUSErrorProvince);}

    public void fillInCardInfoAndContinue(String ccNumber, String cid) {
        switchToRegistrationFrame();
        delay(3);
        try{
            click(addAddressButton);
        }
        catch(Exception e) {}

        typeInHitEnter(cardNumber,  ccNumber);
        typeInHitEnter(expiryDate,   Constants.Payment.DEFAULT_EXPIRYDATE_VISACHECKOUT.toString());
        typeInHitEnter(securityCode, cid);

        click(secondContinue);
        switchToMainContent();
    }
}