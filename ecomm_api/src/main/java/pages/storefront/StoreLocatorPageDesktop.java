package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

import java.util.List;

/**
 * Created by sucho on 8/18/2017.
 */
public class StoreLocatorPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "stores/store-locator.aspx";
    private final static Logger logger = Logger.getLogger(StoreLocatorPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_FindStoreUC1_PostalCodeContainer_PostalCode") //ctl00$CP$FindStoreUC1$PostalCodeContainer$PostalCode
    private WebElement fieldPostalCode;

    @FindBy(how = How.ID, using = "ctl00_CP_FindStoreUC1_CityContainer_txtCity")
    private WebElement fieldCity;

    @FindBy(how = How.ID, using = "ctl00_CP_FindStoreUC1_StateContainer_ddlStates")
    private WebElement selectProvince;

    @FindBy(how = How.ID, using = "ctl00_CP_FindStoreUC1_Submit")
    private WebElement buttonFindStores;

    @FindBy(how = How.XPATH, using = "//div[@class='searchresults']")
    private WebElement divSearchResults;

    @FindBy(how = How.XPATH, using = "//div[@class='right-side-direction']")
    private WebElement divDirectionMap;

    @FindBy(how = How.XPATH, using = "//span[@id='ctl00_CP_StoreResultsBlock_StoreDetailsPager']//a[contains(text(), 'Next')] | //span[@id='ctl00_CP_StoreResultsBlock_StoreDetailsPager']//a[contains(text(), 'magasins suivants')]")
    private WebElement linkNext5Stores;

    @FindBy(how = How.XPATH, using = "//span[@id='ctl00_CP_StoreResultsBlock_StoreDetailsPager']//a[contains(text(), 'Previous')] | //span[@id='ctl00_CP_StoreResultsBlock_StoreDetailsPager']//a[contains(text(), 'magasins précédents')]")
    private WebElement linkPrev5Stores;

    private StoreLocatorPageDesktop(){

    }

    public StoreLocatorPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("StoreLocator page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("StoreLocator page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isSearchResultsDisplayed(){

        return isDisplayed(divDirectionMap) && isDisplayed(divSearchResults);

    }

    public void fillInPostalCodeField(final String postalCode){

        typeIn(fieldPostalCode, postalCode);

        click(buttonFindStores);

    }

    public void clickNext5Stores(){

        scrollAndClick(linkNext5Stores);

    }

    public void clickPrev5Stores(){

        scrollAndClick(linkPrev5Stores);

    }

    public int getNumberOfStores(){

        List<WebElement> elements = driver.findElements(By.xpath("//div[@class='searchresults']//ul[contains(@class, 'list-items')]"));

        return elements.size();
    }

}