package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;

public class AddAddressPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/addaddress.aspx";
    private static final String UPDATE_ADDRESS_URL = "profile/updateaddress.aspx";
    private static final String Expected_ADDRESS_TIPS_URL = Constants.Messages.ADDRESS_TIPS_URL.toString();
    private final static Logger logger = Logger.getLogger(AddAddressPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnSave']")
    private WebElement btn_submit;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnUpdate']")
    private WebElement btn_submitUpdate;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_FirstNameContainer_TxtFirstName']")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_LastNameContainer_txtLastName']")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_AddressLine1Container_TxtAddressLine1']")
    private WebElement fld_streetAddress;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_CityContainer_TxtCity']")
    private WebElement fld_city;

    @FindBy(xpath = "//select[@id='ctl00_CP_addressUC_StateContainer_DdlState']")
    private WebElement fld_province;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PostalCodeContainer_TxtZipCode']")
    private WebElement fld_postalCode;

    @FindBy(xpath = "//select[@id='ctl00_CP_addressUC_CountryContainer_DdlCountry']")
    private WebElement fld_country;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneContainer_TxtPhone']")
    private WebElement fld_phone_0;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_Phone1Container_TxtPhone1']")
    private WebElement fld_phone_1;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_Phone2Container_TxtPhone2']")
    private WebElement fld_phone_2;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneExtContainer_TxtPhoneExt']")
    private WebElement fld_phone_3;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneOtherContainer_TxtPhoneOther']")
    private WebElement fld_other_phone_0;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneOther1Container_TxtPhoneOther1']")
    private WebElement fld_other_phone_1;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneOther2Container_TxtPhoneOther2']")
    private WebElement fld_other_phone_2;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressUC_PhoneOtherExtContainer_TxtPhoneOtherExt']")
    private WebElement fld_other_phone_3;

    @FindBy(xpath = "//a[@id='ctl00_CP_addressUC_LblAddressTips_HelpTopicLink']")
    private WebElement lnk_addressTips;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement msg_error;

    @FindBy(xpath = "//h1[@class='light margin-bottom-three margin-left-one']")
    private WebElement txt_addressTipsHeader;

    public AddAddressPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Add Address page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Add Address page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAtUpdateAddress(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(UPDATE_ADDRESS_URL);
        }
        catch (Exception e){
            logger.error("Update Address page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Update Address page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAtAddressTipPopup(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(Expected_ADDRESS_TIPS_URL);
        }
        catch (Exception e){
            logger.error("Address Tips popup is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Address Tips popup is not displayed " + e.getMessage(), e);
        }
    }

    public void fillInAddress() {
        typeIn(fld_postalCode, TestUser.postalCodeBC);
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_streetAddress, TestUser.streetAddress);
        typeIn(fld_city, TestUser.city);

        if(System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FR.toString()) ||
                System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FRENCH.toString())) {
            select(fld_province, Constants.AddressInformation.BC_PROVINCE_FR.toString());
        }
        else{
            select(fld_province, TestUser.province);
        }

        typeIn(fld_phone_0, TestUser.phone0);
        typeIn(fld_phone_1, TestUser.phone1);
        typeIn(fld_phone_2, TestUser.phone2);
        typeIn(fld_phone_3, TestUser.phone3);

        typeIn(fld_other_phone_0, TestUser.otherPhone0);
        typeIn(fld_other_phone_1, TestUser.otherPhone1);
        typeIn(fld_other_phone_2, TestUser.otherPhone2);
        typeIn(fld_other_phone_3, TestUser.otherPhone3);

        click(btn_submit);
    }


    public void fillInAddress(final String postalCode) {
        typeIn(this.fld_postalCode, postalCode);
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_streetAddress, TestUser.streetAddress);
        typeIn(fld_city, TestUser.city);

        if(System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FR.toString()) ||
                System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FRENCH.toString())) {
            select(fld_province, Constants.AddressInformation.BC_PROVINCE_FR.toString());
        }
        else{
            select(fld_province, TestUser.province);
        }

        typeIn(fld_phone_0, TestUser.phone0);
        typeIn(fld_phone_1, TestUser.phone1);
        typeIn(fld_phone_2, TestUser.phone2);
        typeIn(fld_phone_3, TestUser.phone3);

        typeIn(fld_other_phone_0, TestUser.otherPhone0);
        typeIn(fld_other_phone_1, TestUser.otherPhone1);
        typeIn(fld_other_phone_2, TestUser.otherPhone2);
        typeIn(fld_other_phone_3, TestUser.otherPhone3);

        click(btn_submit);
    }

    public String verifyErrorMessage(){
        return getTextOrValue(msg_error);
    }

    public void addShippingAddress(String firstName, String lastName, String address, String City,
                                   String province, String postalCode, String p0, String p1, String p2, String p3) {
        typeIn(fld_firstName, firstName);
        typeIn(fld_lastName, lastName);
        typeIn(fld_streetAddress, address);
        typeIn(fld_city, City);
        select(fld_province, province);
        typeIn(fld_postalCode, postalCode);
        typeIn(fld_phone_0, p0);
        typeIn(fld_phone_1, p1);
        typeIn(fld_phone_2, p2);
        typeIn(fld_phone_3, p3);
    }

    public void clickUpdate() {
        click(btn_submitUpdate);
    }

    public void clickSubmit() {
        click(btn_submit);
    }

    public void clickAddressTips() {
        click(lnk_addressTips);
    }

    public String getAddressTipsHeader() {
        return getTextOrValue(txt_addressTipsHeader);
    }
}