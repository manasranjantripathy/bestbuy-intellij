package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Util;

public class OrderDetailsPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/orderdetails.aspx";
    private final static Logger logger = Logger.getLogger(CartPageDesktop.class);

    @FindBy(xpath = "//div[@id='ctl00_CP_PurchaseOrderSection']/div[@class='small-4 columns']/div[@class='background-light-grey padding-bound-two margin-bottom-two']/div[@class='row collapse']/div[6]/span")
    private WebElement subTotalValue;

    @FindBy(xpath = "//span[@id='ctl00_CP_CheckoutOrderTotals_LitShipping']")
    private WebElement shippingText;

    @FindBy(xpath = "//div[@id='ctl00_CP_PurchaseOrderSection']/div[@class='small-4 columns']/div[@class='background-light-grey padding-bound-two margin-bottom-two']/div[@class='row collapse']/div[4]/span")
    private WebElement shippingValue;

    @FindBy(xpath = "//div[@class='background-light-grey margin-bottom-two padding-bottom-two clearfix']")
    private WebElement creditCardDetailSection;

    @FindBy(how = How.ID, using = "ctl00_CP_OrderGiftCards1_RptGiftCardsView_ctl00_LblEncodedGiftCardNumber")
    private WebElement giftCardsDetails;

    @FindBy(how = How.ID, using  = "ctl00_CP_CartRedeemPromotionCode_RptPromoCodesView_ctl01_LblPromoCode")
    private  WebElement promoCodeDetails;

    @FindBy(how = How.ID, using  = "ctl00_CP_BtnCancelBottom")
    private  WebElement cancelButton;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnSubmitChange']")
    private WebElement submitChangesButton;

    @FindBy(xpath = "//div[@id='ctl00_CP_PurchaseOrderSection']/div[@class='small-4 columns']/div[@class='background-light-grey padding-bound-two margin-bottom-two']/div[@class='row collapse']/div[7]/div")
    private WebElement surchargeText;

    @FindBy(xpath ="//div[@id='ctl00_CP_RepShipment_ctl00_UcOrderDetailLineItem_GrdOrderDetailLineItem_ctl04_DivWarrantyMessage']")
    private WebElement textPspAddedInOrderDetailsPage;



    @FindBy(xpath = "//a[@id='ctl00_CP_HypChangeShipping']")
    private WebElement changeShippingInformation;

    //todo remove this
    @FindBy(xpath = "//input[@id='ctl00_CP_RdoAddNewCreditCard']")
    private WebElement addNewCreditCardRadioButton;



    @FindBy(xpath = "//div[@id='ctl00_CP_DivEditCreditCard']")
    private WebElement blockAddCreditCard;


    @FindBy(id = "ctl00_CP_ConfirmationUC_ConfirmationMessage")
    private WebElement confirmationMessage;

    @FindBy(xpath = "//a[@id='ctl00_CP_HypChangePayment'] | //a[@id='ctl00_CP_HypChangePayment']")
    private WebElement changePaymentInformation;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC1_vsumErrorSummary']/p")
    private WebElement orderNotRetrieved;

    public OrderDetailsPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            Util.wait(1);
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Order Details page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order Details page is not displayed " + e.getMessage(), e);
        }
    }

    public String getTextSubTotalValue(){
        driver.navigate().refresh();
        return getTextOrValue(subTotalValue);
    }

    public String getTextShippingDeliveryText() {
        return getTextOrValue(shippingText);
    }

    public String getTextShippingDeliveryValue() {
        return getTextOrValue(shippingValue);
    }

    public String getTextSurchargeText() {
        return getTextOrValue(surchargeText);
    }

    public boolean isCreditCardDetailsSectionPresent() {
        return isDisplayed(creditCardDetailSection);
    }

    public boolean isGiftCardDetailsSectionPresent() {
        return (!getTextOrValue(giftCardsDetails).toLowerCase().contains("n/a"));
    }

    public boolean isPromoCodeDetailsSectionPresent() {
        return (!getTextOrValue(promoCodeDetails).toLowerCase().contains("n/a"));
    }

    public String getTextAddedPSP() {
        return getTextOrValue(textPspAddedInOrderDetailsPage);
    }


    public void clickChangeShippingInformation() {
        click(changeShippingInformation);
    }

    public String getTextConfirmationMessage() {
        return getTextOrValue(confirmationMessage);
    }

    public void clickChangePaymentInformation() {
        click(changePaymentInformation);
    }

    public void cancelOrder() {
        click(cancelButton);
        click(submitChangesButton);
    }
}