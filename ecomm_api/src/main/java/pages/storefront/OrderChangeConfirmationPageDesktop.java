package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Util;

public class OrderChangeConfirmationPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static String EXPECTED_URL = "Order/OrderChangeConfirmation";
    private final static Logger logger = Logger.getLogger(OrderChangeConfirmationPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnSubmitChange']")
    private WebElement btn_submitChange;

    public OrderChangeConfirmationPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            Util.wait(1);
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL.toLowerCase());
        }
        catch(Exception e){
            logger.error("Order change confirmation page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order change confirmation page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickSubmitChange() {
        click(btn_submitChange);
    }
}