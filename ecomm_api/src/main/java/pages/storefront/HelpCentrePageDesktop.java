package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class HelpCentrePageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "help.aspx";
    private final static Logger logger = Logger.getLogger(HelpCentrePageDesktop.class);

    private String expected;

    @FindBy(xpath = "//div[@id='help-topic-links']")
    private WebElement helpTopicLinks;

    @FindBy(xpath = "//a[@id='ctl00_CP_ShoppingOnline_lnkCategoryName']")
    private WebElement shoppingOnline;

    @FindBy(xpath = "//div[@id='help-topic-links']/div[1][@class='row-fluid']/div[1][@class='span4']/div[@class='padding-s-bound']")
    private WebElement categoryQuestions;

    @FindBy(xpath = "//div[@class='category-name']")
    private WebElement shoppingOnlinePageCategory;

    @FindBy(xpath = "//ul[@class='bulleted padding-left-two']")
    private WebElement extendedQuestions;

    @FindBy(xpath = "//a[@id='ctl00_CP_HelpCategoryListUC1_gvHelpTitle_ctrl0_hlHelpTitle']")
    private WebElement categoryQuestion1;

    @FindBy(xpath = "//div[@class='content-pmessage']")
    private WebElement categoryAnswer;

    @FindBy(xpath = "//a[@id='ctl00_CP_ShoppingOnline_lnkMore']")
    private WebElement shoppingOnlineLearnMore;


    public HelpCentrePageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Help Centre page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Help Centre page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean verifyHelpTopicLinksDisplayed() { return isDisplayed(helpTopicLinks); }

    public boolean verifyCategoryQuestions() { return isDisplayed(categoryQuestions); }

    public void clickLearnMoreLink(){ click(shoppingOnlineLearnMore); }

    public boolean verifyExtendedQuestions() { return isDisplayed(extendedQuestions); }

    public void clickCategoryQuestion1() { click(categoryQuestion1); }

    public boolean verifyCategoryAnswer() { return isDisplayed(categoryAnswer); }
}