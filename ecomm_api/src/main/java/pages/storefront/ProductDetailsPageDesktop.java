package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sucho on 6/29/2017.
 */
public class ProductDetailsPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "-ca/product";
    private final static Logger logger = Logger.getLogger(ProductDetailsPageDesktop.class);

    @FindBy(how = How.ID, using = "btn-cart")
    private WebElement buttonAddToCart;

    @FindBy(how = How.ID, using = "btn-cart")
    private WebElement buttonPreOrder;

    @FindBy(how = How.XPATH, using = "//button[contains(@class, 'search-button')]")
    private WebElement buttonChangeDeliveryLocation;

    @FindBy(how = How.XPATH, using = "//button[contains(@class, 'btn-close')]")
    private WebElement buttonCloseChangeDeliveryLocation;

    @FindBy(how = How.XPATH, using = "//input[contains(@class, 'search-input')]")
    private WebElement fieldChangeDeliveryLocation;

    @FindBy(how = How.ID, using = "delivery-location")
    private WebElement linkDeliveryChange;

    @FindBy(how = How.XPATH, using = "//div[@id='delivery-location-modal']//p[@class='modal-title']")
    private WebElement deliveryLocationPopupHeader;

    @FindBy(how = How.XPATH, using = "//*[contains(@class,'ui-modal-body')]/div/button")
    private WebElement deliveryLocationChangeBtn;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-availability']/div[@class='availability-section']/p[@class='availability-header']/span[@class='info']")
    private WebElement deliveryLocationHeader;

    @FindBy(how = How.ID, using = "pdp-nearby-stores")
    private WebElement linkSelectClosestStoreChange;

    @FindBy(how = How.XPATH, using="//*[@id='productdetail']/div[1]/div[1]/div[2]/span[2] | //*[@id='customer-reviews-module']/div/div/div[2]/div[1]/div/div[1]")
    private WebElement cusomterRating;

    @FindBy(how = How.XPATH, using="//*[@id='ctl00_CP_ctl00_pdpDynamicContent_Tab_custReview_RatingBreakdownControl_RatingsAndReviews']")
    private WebElement moreCustomerReview;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-nearby-stores-modal']/div[@class='ui-modal-dialog']/div[@class='ui-modal-header']/p[@class='modal-title']")
    private WebElement selectClosestStorePopupHeader;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-nearby-stores-search']/div/div/input")
    private WebElement fieldChangeStoreLocation;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-nearby-stores-list']/ul/li[1]/div[2]/a")
    private WebElement selectStoreBtn;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'ctl00_CP_ctl00_pdpDynamicContent')]/ul/li[1]/a")
    private WebElement tabDetails;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-availability']/div[@class='availability-section in-store']/p[@class='availability-header']/span[@class='info']")
    private WebElement storeLocationHeader;

    @FindBy(how = How.XPATH, using = "//*[@id='pdp-nearby-stores-search']/div/div/button")
    private WebElement searchBtnToLostNearbyStores;

    @FindBy(how = How.ID, using = "btn-cart")
    private WebElement addToCartBtn;

    @FindBy(how = How.XPATH, using = "//*[contains(@class, 'online-availability-status')]")
    private WebElement onlineAvailabilityStatu;

    @FindBy(how =How.XPATH, using="//a[@class='btn btn-primary disabled']")
    private WebElement addToButtonGreyOut;

    @FindBy(how = How.ID, using = "btn-reserve")
    private WebElement reserveInStoreBtn;

    @FindBy(how =How.XPATH, using="//*[@id='ctl00_CP_lblUsReviews'] | //*[@id='tab-cust-review']/ul/li[2]/a")
    private WebElement usReviewTab;

    @FindBy(how=How.XPATH, using ="//*[@id='pagecontent']/div/div[5]/div[1]/div[1]/div/ul/li[2] | //*[@id='tab-cust-review']/div[2]/div[2]/ul/li[1]/span[2]")
    private WebElement bestbuyText;

    @FindBy(how = How.ID, using = "ctl00_CP_ctl00_PD_PI_RepColors_ctl00_imgColorSample")
    private WebElement firstColorPickerOption;

    @FindBy(how = How.ID, using = "ctl00_CP_ctl00_PD_lblSku")
    private WebElement webcod;

    @FindBy(how = How.ID, using = "pdp-add-to-wishlist")
    private WebElement wishList;

    @FindBy(how = How.ID, using = "ctl00_CP_ctl00_PD_lblProductTitle")
    private WebElement productTitle;

    @FindBy(how = How.XPATH, using = "//a[@id='ctl00_CP_ctl00_pdpDynamicContent_Tab_custReview_hlRateReview']")
    private WebElement rateAndReview;

    @FindBy(how = How.ID, using="ctl00_CP_ctl00_pdpDynamicContent_Tab_custReview_hlReadMoreReviews")
    private WebElement usReview;

    @FindBy(how = How.XPATH, using="//span[@class='font-xs']")
    private WebElement totalCount;

    @FindBy(how = How.XPATH, using="//span[@id='ctl00_CP_lblUsReviews']")
    private WebElement usCount;

    @FindBy(how = How.XPATH, using="//a[@id='ctl00_CP_hlCanadaReviews']")
    private WebElement canadaCount;

    @FindBy(how = How.XPATH, using = "//h3[@class='font-s margin-bottom-one']")
    private WebElement noReviewYet;

    @FindBy(how = How.XPATH, using = "//div[@class='messaging store-availability-wrapper']/p[2]")
    private WebElement localSection;

    @FindBy(how = How.XPATH, using = "//strong[text()='No reviews yet']")
    private WebElement noReviewYetText;

    @FindBy(how = How.XPATH, using = "//a[@id='ctl00_CP_ctl00_CL_HypEmailFriend']")
    private WebElement linkEmailToFriend;

    @FindBy(xpath = "//div[@id='ctl00_CP_ctl00_pdpDynamicContent_PdpContent_PspBlock_PnlPsp']")
    private WebElement pspSection;

    @FindBy(xpath = "//h3[contains(text(),'in the Box')]")
    private WebElement tabDetailsContent;

    @FindBy(xpath = "//*[@id='Tab_FeatureSpecs'] | //*[contains(@id,'ctl00_CP_ctl00_pdpDynamicContent')]/ul/li[2]/a")
    private WebElement tabSpecification;

    @FindBy(xpath = "//*[@class[contains(.,'pdp-tab-content tab-details-spec')]] | //*[contains(@id,'ctl00_CP_ctl00_pdpDynamicContent')]/ul/li[2]/a")
    private WebElement specificationTabContent;

    @FindBy(xpath ="//div[@id='ctl00_CP_ctl00_pnlRecommend']")
    private WebElement recommendTab;

    @FindBy(xpath = "//*[contains(@id, 'pdpDynamicContent')]/p[1]/span/strong")
    private WebElement esrbRate;

    @FindBy(xpath="//span[@id='ctl00_CP_ctl00_PD_lblReleaseDate']")
    private WebElement releaseDate;

    @FindBy(how =How.XPATH, using="//ul[@class='prod-special-offer-list']")
    private WebElement promoCodeAvailability;

    @FindBy(how=How.XPATH, using="//strong[text()='No reviews yet']")
    private WebElement noReviewText;

    private final String addToCartButtonJS = "btn-cart";

    private final String reserveInStoreButton = "btn-reserve";


    private ProductDetailsPageDesktop() {

    }

    public ProductDetailsPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        delay(50);
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Product Details page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Product Details page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isDeliveryLocationChangeDisplayed(){

        return isDisplayed(deliveryLocationPopupHeader);

    }

    public boolean isNoReviewYetDisplayed(){return  isDisplayed(noReviewYet);}

    public boolean isChangeDeliveryLocationButtonEnabled(){

        return isEnabled(buttonChangeDeliveryLocation);

    }

    public void clickOnUsReview() {click(usReview);}

    public boolean isUsReviewTabDisplayed(){return isDisplayed(usReviewTab);}


    public String getpromoCodeAvailabilityText() {return getTextOrValue(promoCodeAvailability);}

    public boolean isErsbRateDisplayed() {return isDisplayed(esrbRate);}

    public boolean isCloseButtonOnChangeDeliveryLocationDisplayed(){

        return isDisplayed(buttonCloseChangeDeliveryLocation);

    }

    public void clickAddToCartButton() {
        scrollToElement(addToCartBtn);
        clickByJSId(addToCartButtonJS);

    }

    public boolean isCustomerRatingDisplayed(){ return isDisplayed(cusomterRating);}

    public String getMoreCutomerReviewText() {return getTextOrValue(moreCustomerReview);}

    public void clickReserveInStoreButton() {

        clickByJSId(reserveInStoreButton);

    }

    public boolean isReserveInStoreButtonEnabled() {
        delay(50);
        return isEnabled(reserveInStoreBtn);

    }

    public void clickSpecificationTab() {clickByJs(tabSpecification);}

    public String getSpecificationContent() {return getTextOrValue(specificationTabContent);}

    public void clickDeliveryChangeLink(){

        click(linkDeliveryChange);

    }

    public String getBestbuytext(){return getTextOrValue(bestbuyText);}

    public boolean isRecommendTabPresent() {return isDisplayed(recommendTab);}

    public String getDetailsContent() {return getTextOrValue(tabDetailsContent); }

    public void clickTabDetails() {
        click(tabDetails);}

    public void clickPreOrderButton(){
        click(buttonPreOrder);
    }

    public String getTextOnlineAvalibility() {return getTextOrValue(onlineAvailabilityStatu); }

    public String getTextPreOrder() {return getTextOrValue(buttonPreOrder);}

    public void fillInChangeDeliveryLocation(final String postalCode){

        typeIn(fieldChangeDeliveryLocation, postalCode);

    }

    public void clickDeliveryLocationChangeBtn() {

        click(deliveryLocationChangeBtn);
    }

    public String getTextFillInChangeDeliveryLocation() {

        return getTextOrValue(fieldChangeDeliveryLocation);

    }

    public String getTextDeliveryLocationHeader() {

        return getTextOrValue(deliveryLocationHeader);

    }

    public void clickStoreLocationChangeLink(){

        click(linkSelectClosestStoreChange);

    }

    public boolean isSelectClosestStoreDisplayed(){

        return isDisplayed(selectClosestStorePopupHeader);

    }

    public void fillInStoreClosestLocation(final String postalCode){

        typeIn(fieldChangeStoreLocation, postalCode);

    }

    public String getTextFillInChangeStoreLocation() {

        return getTextOrValue(fieldChangeStoreLocation);

    }

    public boolean compareDate() {
        Date date= new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);
        String relDate= getTextOrValue(releaseDate);
        if(relDate.compareToIgnoreCase(strDate)>0){
            return true;
        }
        else {
            return false;
        }

    }

    public void clickOnFirstEnabledSelectBtn() {

        //TBD: Need to loop over "select" buttons and find the first enabled one then click on it
        click(selectStoreBtn);

    }

    public String getTextStoreLocationHeader() {

        return getTextOrValue(storeLocationHeader);

    }

    public void clickSearchBtnToListNearbyStores() {

        click(searchBtnToLostNearbyStores);
    }

    public void clickAddToCartBtn() {

        click(addToCartBtn);

    }

    public void clickReserveInStoreBtn() {
        click(reserveInStoreBtn);

    }

    public String getLocalSectionText() {return getTextOrValue(localSection);}

    public void clickOnFirstColorPickerOption() {
        click(firstColorPickerOption);
    }

    public String getTextWebcode() {
        return getTextOrValue(webcod);
    }

    public void clickAddToWishList(){
        TestUser.wishListItem = getTextOrValue(productTitle);
        scrollAndClick(wishList);
    }

    public void clickOnRateAndReview() { clickByJs(rateAndReview);}

    public String getTextForNoReview() {return getTextOrValue(noReviewYet);}

    public String getTextForNoReviewMessage() {return getTextOrValue(noReviewYetText);}

    public boolean checkRateAndReviewButton() {return isDisplayed(rateAndReview);}


    public boolean  listOfApprovedReviews() {
        int result;
        result = driver.findElements(By.xpath("//div[contains(@class, 'customer-review-list')]")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkLinkEmailToFriend() { return isDisplayed(linkEmailToFriend);}

    public void clickLinkEmailToFriend() { click(linkEmailToFriend);}

    public boolean verifyPSPsection(){ return isDisplayed(pspSection); }

    public String getTextProductTitle() { return getTextOrValue(productTitle); }

    public boolean isCannotReserveButtonDisplayed() {
        return (getTextOrValue(reserveInStoreBtn).contains(Constants.Messages.CANNOT_RESERVE.toString()) ? true :
                false);
    }

    public boolean isAddToCartButtonEnabled() {
        if(isDisplayed(addToButtonGreyOut))
            return isEnabled(addToButtonGreyOut);
        else
            return false;
    }

    public boolean getTotalReviewsofTheProduct() {
        int totalReviews = Integer.parseInt(getTextOrValue(totalCount).replaceAll("\\D", ""));
        int canadaReviews = Integer.parseInt(getTextOrValue(canadaCount).replaceAll("\\D", ""));
        int uSReviews = Integer.parseInt(getTextOrValue(usCount).replaceAll("\\D", ""));

        return uSReviews + canadaReviews == totalReviews;
    }
}