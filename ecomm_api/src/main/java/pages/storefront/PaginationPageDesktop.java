package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

import java.util.List;

public class PaginationPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private String relativePath;
    private final static Logger logger = Logger.getLogger(ProductListingPageDesktop.class);

    @FindBy(xpath = "//ul[@id='search-tabs']/li[@class[contains(.,'active')]]/a")
    private WebElement currentTab;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='sorting-item  active']/a")
    private WebElement lnk_CurrentNumberOfResultsPerPage_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='sorting-item  active']/a")
    private WebElement lnk_CurrentNumberOfResultsPerPage_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='sorting-item  active']/a")
    private WebElement lnk_CurrentNumberOfResultsPerPage_HelpTopics;

    @FindBy(xpath = "//ul[@class='products-per-page-list inline-list solr_results_per_page']//a[text()='32']")
    private WebElement lnk_32ResultsPerPage;

    @FindBy(xpath = "//ul[@class='products-per-page-list inline-list solr_results_per_page']//a[text()='64']")
    private WebElement lnk_64ResultsPerPage;

    @FindBy(xpath = "//ul[@class='products-per-page-list inline-list solr_results_per_page']//a[text()='96']")
    private WebElement lnk_96ResultsPerPage;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//a[text()='12']")
    private WebElement lnk_12ResultsPerPage_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//a[text()='24']")
    private WebElement lnk_24ResultsPerPage_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//a[text()='36']")
    private WebElement lnk_36ResultsPerPage_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//a[text()='12']")
    private WebElement lnk_12ResultsPerPage_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//a[text()='24']")
    private WebElement lnk_24ResultsPerPage_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//a[text()='36']")
    private WebElement lnk_36ResultsPerPage_HelpTopics;

    @FindBy(xpath = "//ul[@class='listing-items util_equalheight clearfix']")
    private List<WebElement> itemsList;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//span[@class='display-number']")
    private WebElement currentProductsRange_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//span[@class='display-number']")
    private WebElement currentProductsRange_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//span[@class='display-number']")
    private WebElement currentProductsRange_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//span[@class='display-total']")
    private WebElement totalProductcount_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//span[@class='display-total']")
    private WebElement totalProductcount_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//span[@class='display-total']")
    private WebElement totalProductcount_HelpTopics;

    @FindBy(xpath = "//ul[@class='listing-items util_equalheight clearfix']/div/li")
    private List<WebElement> productsCountInPage_Products;

    @FindBy(xpath = "//div[@id='SearchArticleListing']//li[@class='content-listing-item clearfix']")
    private List<WebElement> productsCountInPage_Articles;

    @FindBy(xpath = "//div[@id='SearchHelpListing']//li[@class='content-listing-item clearfix']")
    private List<WebElement> productsCountInPage_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next']/a")
    private WebElement lnk_Next_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next']/a")
    private WebElement lnk_Next_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next']/a")
    private WebElement lnk_Next_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-prev']")
    private WebElement lnk_Prev_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-prev']")
    private WebElement lnk_Prev_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-prev']")
    private WebElement lnk_Prev_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next disabled']")
    private WebElement lnk_NextDisabled_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next disabled']")
    private WebElement lnk_NextDisabled_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next disabled']")
    private WebElement lnk_NextDisabled_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-prev disabled']")
    private WebElement lnk_PrevDisabled_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-prev disabled']")
    private WebElement lnk_PrevDisabled_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-prev disabled']")
    private WebElement lnk_PrevDisabled_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='current']")
    private WebElement lnk_CurrentPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='current']")
    private WebElement lnk_CurrentPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='current']")
    private WebElement lnk_CurrentPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[1]")
    private WebElement lnk_LastPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next disabled']//preceding-sibling::li[2]")
    private WebElement lnk_LastSecondPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[3]")
    private WebElement lnk_LastThirdPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[4]")
    private WebElement lnk_LastFourthPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[1]")
    private WebElement lnk_FirstPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[2]")
    private WebElement lnk_SecondPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[3]")
    private WebElement lnk_ThirdPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[4]")
    private WebElement lnk_FourthPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[5]")
    private WebElement lnk_FifthPageNumber_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[1]")
    private WebElement lnk_LastPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next disabled']//preceding-sibling::li[2]")
    private WebElement lnk_LastSecondPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[3]")
    private WebElement lnk_LastThirdPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[4]")
    private WebElement lnk_LastFourthPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[1]")
    private WebElement lnk_FirstPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[2]")
    private WebElement lnk_SecondPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[3]")
    private WebElement lnk_ThirdPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[4]")
    private WebElement lnk_FourthPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[5]")
    private WebElement lnk_FifthPageNumber_Articles;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[1]")
    private WebElement lnk_LastPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next disabled']//preceding-sibling::li[2]")
    private WebElement lnk_LastSecondPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[3]")
    private WebElement lnk_LastThirdPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[@class='pagi-next']//preceding-sibling::li[4]")
    private WebElement lnk_LastFourthPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[1]")
    private WebElement lnk_FirstPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[2]")
    private WebElement lnk_SecondPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[3]")
    private WebElement lnk_ThirdPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[4]")
    private WebElement lnk_FourthPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_HelpSearchResultListing_topPaging']//li[contains(@class,'pagi-prev')]//following-sibling::li[5]")
    private WebElement lnk_FifthPageNumber_HelpTopics;

    @FindBy(xpath = "//div[@id='ctl00_CC_ProductSearchResultListing_topPaging']//select[@class='margin-bottom-none select-height-four font-xxs']")
    private WebElement sortDropDown_Products;

    @FindBy(xpath = "//div[@id='ctl00_CC_ArticleSearchResultListing_SortByTop']//select[@class='margin-bottom-none select-height-four font-xxs']")
    private WebElement sortDropDown_Articles;

    public PaginationPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public String getTxt_CurrentTab() {
        return getTextOrValue(currentTab).trim();
    }

    public String getTxt_CurrentResultsPerPageLink(String tab) {
        String resultsPerPage = "null";

        if (tab.equalsIgnoreCase("products")) {
            resultsPerPage = getTextOrValue(lnk_CurrentNumberOfResultsPerPage_Products);

        } else if (tab.equalsIgnoreCase("articles")) {
            resultsPerPage = getTextOrValue(lnk_CurrentNumberOfResultsPerPage_Articles);

        } else if (tab.equalsIgnoreCase("helpTopics")) {
            resultsPerPage = getTextOrValue(lnk_CurrentNumberOfResultsPerPage_HelpTopics);
        }
        return resultsPerPage;
    }

    public void clickResultsPerPageLink(String tab, String resultsPerPage) {
        if(tab.equalsIgnoreCase("products")) {
            if (resultsPerPage.equalsIgnoreCase("32")) {
                click(lnk_32ResultsPerPage);
            } else if (resultsPerPage.equalsIgnoreCase("64")) {
                click(lnk_64ResultsPerPage);
            } else if (resultsPerPage.equalsIgnoreCase("96")) {
                click(lnk_96ResultsPerPage);
            }
        } else if(tab.equalsIgnoreCase("articles")) {
            if (resultsPerPage.equalsIgnoreCase("12")) {
                click(lnk_12ResultsPerPage_Articles);
            } else if (resultsPerPage.equalsIgnoreCase("24")) {
                click(lnk_24ResultsPerPage_Articles);
            } else if (resultsPerPage.equalsIgnoreCase("36")) {
                click(lnk_36ResultsPerPage_Articles);
            }
        } else if(tab.equalsIgnoreCase("helpTopics")) {
            if (resultsPerPage.equalsIgnoreCase("12")) {
                click(lnk_12ResultsPerPage_HelpTopics);
            } else if (resultsPerPage.equalsIgnoreCase("24")) {
                click(lnk_24ResultsPerPage_HelpTopics);
            } else if (resultsPerPage.equalsIgnoreCase("36")) {
                click(lnk_36ResultsPerPage_HelpTopics);
            }
        }
    }

    public String getTxt_CurrentProductsRange(String tab) {
        String productRange = "null";
        if(tab.equalsIgnoreCase("products")) {
            productRange = getTextOrValue(currentProductsRange_Products);

        } else if(tab.equalsIgnoreCase("articles")) {
            productRange = getTextOrValue(currentProductsRange_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            productRange = getTextOrValue(currentProductsRange_HelpTopics);
        }
        return productRange;
    }

    public boolean isTotalProductsCountDisplayed(String tab) {
        boolean totalProductCount = true;
        if(tab.equalsIgnoreCase("products")) {
            totalProductCount = isDisplayed(totalProductcount_Products);

        } else if(tab.equalsIgnoreCase("articles")) {
            totalProductCount = isDisplayed(totalProductcount_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            totalProductCount = isDisplayed(totalProductcount_HelpTopics);
        }
        return totalProductCount;
    }

    public String getTxt_TotalProductcount(String tab) {
        String totalProductCount = "null";
        if(tab.equalsIgnoreCase("products")) {
            totalProductCount =  getTextOrValue(totalProductcount_Products);

        } else if(tab.equalsIgnoreCase("articles")) {
            totalProductCount = getTextOrValue(totalProductcount_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            totalProductCount = getTextOrValue(totalProductcount_HelpTopics);
        }
        return totalProductCount;
    }

    public String productCountInPage(String tab) {
        String productCountInPage = "null";
        if(tab.equalsIgnoreCase("products")) {
            productCountInPage = Integer.toString(productsCountInPage_Products.size());

        } else if(tab.equalsIgnoreCase("articles")) {
            productCountInPage =  Integer.toString(productsCountInPage_Articles.size());

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            productCountInPage =  Integer.toString(productsCountInPage_HelpTopics.size());
        }
        return productCountInPage;
    }

    public void clickNextLink(String tab) {
        if (tab.equalsIgnoreCase("products")) {
            click(lnk_Next_Products);

        } else if(tab.equalsIgnoreCase("articles")) {
            click(lnk_Next_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            click(lnk_Next_HelpTopics);
        }
    }

    public void clickPreviousLink(String tab) {
        if (tab.equalsIgnoreCase("products")) {
            click(lnk_Prev_Products);

        } else if(tab.equalsIgnoreCase("articles")) {
            click(lnk_Prev_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            click(lnk_Prev_HelpTopics);
        }
    }

    public boolean isNextLinkDisabled(String tab) {
        boolean nextLink = false;
        if (tab.equalsIgnoreCase("products")) {
            nextLink = isDisplayed(lnk_NextDisabled_Products);

        } else if (tab.equalsIgnoreCase("articles")) {
            nextLink = isDisplayed(lnk_NextDisabled_Articles);

        } else if (tab.equalsIgnoreCase("helpTopics")) {
            nextLink = isDisplayed(lnk_NextDisabled_HelpTopics);
        }
        return nextLink;
    }

    public boolean isPrevLinkDisabled(String tab) {
        boolean prevLink = false;
        if (tab.equalsIgnoreCase("products")) {
            prevLink = isDisplayed(lnk_PrevDisabled_Products);

        } else if (tab.equalsIgnoreCase("articles")) {
            prevLink = isDisplayed(lnk_PrevDisabled_Articles);

        } else if (tab.equalsIgnoreCase("helpTopics")) {
            prevLink = isDisplayed(lnk_PrevDisabled_HelpTopics);
        }
        return false;
    }

    public String getTxt_CurrentPageNumber(String tab) {
        String currentPage = "null";
        if(tab.equalsIgnoreCase("products")) {
            TestUser.currentPageNumber = getTextOrValue(lnk_CurrentPageNumber_Products);
            currentPage = getTextOrValue(lnk_CurrentPageNumber_Products);

        } else if (tab.equalsIgnoreCase("articles")) {
            TestUser.currentPageNumber = getTextOrValue(lnk_CurrentPageNumber_Articles);
            currentPage = getTextOrValue(lnk_CurrentPageNumber_Articles);

        } else if (tab.equalsIgnoreCase("helpTopics")) {
            TestUser.currentPageNumber = getTextOrValue(lnk_CurrentPageNumber_HelpTopics);
            currentPage = getTextOrValue(lnk_CurrentPageNumber_HelpTopics);
        }
        return currentPage;
    }

    public void clickPageNumberLink(String tab, String pageNumber) {
        if(tab.equalsIgnoreCase("products")) {
            if (pageNumber.equalsIgnoreCase("first")) {
                click(lnk_FirstPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("second")) {
                click(lnk_SecondPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("Third")) {
                click(lnk_ThirdPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("fourth")) {
                click(lnk_FourthPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("fifth")) {
                click(lnk_FifthPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("lastFourth")) {
                click(lnk_LastFourthPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("lastThird")) {
                click(lnk_LastThirdPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("lastSecond")) {
                click(lnk_LastSecondPageNumber_Products);
            } else if (pageNumber.equalsIgnoreCase("last")) {
                click(lnk_LastPageNumber_Products);
            }
        } else if(tab.equalsIgnoreCase("articles")) {
            if (pageNumber.equalsIgnoreCase("first")) {
                click(lnk_FirstPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("second")) {
                click(lnk_SecondPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("Third")) {
                click(lnk_ThirdPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("fourth")) {
                click(lnk_FourthPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("fifth")) {
                click(lnk_FifthPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("lastFourth")) {
                click(lnk_LastFourthPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("lastThird")) {
                click(lnk_LastThirdPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("lastSecond")) {
                click(lnk_LastSecondPageNumber_Articles);
            } else if (pageNumber.equalsIgnoreCase("last")) {
                click(lnk_LastPageNumber_Articles);
            }
        } else if(tab.equalsIgnoreCase("helpTopics")) {
            if (pageNumber.equalsIgnoreCase("first")) {
                click(lnk_FirstPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("second")) {
                click(lnk_SecondPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("Third")) {
                click(lnk_ThirdPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("fourth")) {
                click(lnk_FourthPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("fifth")) {
                click(lnk_FifthPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("lastFourth")) {
                click(lnk_LastFourthPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("lastThird")) {
                click(lnk_LastThirdPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("lastSecond")) {
                click(lnk_LastSecondPageNumber_HelpTopics);
            } else if (pageNumber.equalsIgnoreCase("last")) {
                click(lnk_LastPageNumber_HelpTopics);
            }
        }
    }

    public boolean isSelectedPageDisplayed(String tab) {
        boolean selectedPage = true;
        if(tab.equalsIgnoreCase("products")) {
            selectedPage = driver.getCurrentUrl().contains("page=" + getTxt_CurrentPageNumber(tab)) ? true : false;
        } else if(tab.equalsIgnoreCase("articles")) {
            selectedPage = driver.getCurrentUrl().contains("page=" + getTxt_CurrentPageNumber(tab)) ? true : false;
        } else if(tab.equalsIgnoreCase("helpTopics")) {
            selectedPage = driver.getCurrentUrl().contains("page=" + getTxt_CurrentPageNumber(tab)) ? true : false;
        }
        return selectedPage;
    }

    public String lastPageNumber(WebElement element1, WebElement element2) {
        int totalResultNumber = Integer.parseInt(getTextOrValue(element1));
        int currentResultPerPage = Integer.parseInt(getTextOrValue(element2));

        int lastPage = (int) Math.ceil((double) (totalResultNumber)
                / (double) (currentResultPerPage));
        return Integer.toString(lastPage);
    }

    public String getTextLastPage(String tab) {
        String lastPage = "null";
        if(tab.equalsIgnoreCase("products")) {
            lastPage = lastPageNumber(totalProductcount_Products, lnk_CurrentNumberOfResultsPerPage_Products);
        } else if(tab.equalsIgnoreCase("articles")) {
            lastPage = lastPageNumber(totalProductcount_Articles, lnk_CurrentNumberOfResultsPerPage_Articles);
        } else if(tab.equalsIgnoreCase("helpTopics")) {
            lastPage = lastPageNumber(totalProductcount_HelpTopics, lnk_CurrentNumberOfResultsPerPage_HelpTopics);
        }
        return lastPage;
    }

    public final String getTxt_ExpectedPageIndexRangeForCurrentPage(String tab) {
        String pageIndexRange;

        int totalResultNumber = Integer.parseInt(getTxt_TotalProductcount(tab));
        int currentResultPerPage = Integer.parseInt(getTxt_CurrentResultsPerPageLink(tab));
        int currentPageNumber = Integer.parseInt(getTxt_CurrentPageNumber(tab));
        int lastPage = Integer.parseInt(getTextLastPage(tab));

        if (currentPageNumber != lastPage) {
            pageIndexRange = Integer.toString(currentPageNumber
                    * currentResultPerPage - (currentResultPerPage - 1))
                    + " - "
                    + Integer.toString(currentPageNumber * currentResultPerPage);
        } else {
            pageIndexRange = Integer.toString(currentPageNumber
                    * currentResultPerPage - (currentResultPerPage - 1))
                    + " - "
                    + Integer.toString(totalResultNumber);
        }
        return pageIndexRange;
    }

    public void sortBy(String tab, String sort) {
        if (tab.equalsIgnoreCase("products")) {
            switch (sort) {

                case "Best Match":
                    select(sortDropDown_Products, "Best Match");
                    break;

                case "Price: Low to High":
                    select(sortDropDown_Products, "Price: Low to High");
                    break;

                case "Price: High to Low":
                    select(sortDropDown_Products, "Price: High to Low");
                    break;

                case "Title: A-Z":
                    select(sortDropDown_Products, "Title: A-Z");
                    break;

                case "Title: Z-A":
                    select(sortDropDown_Products, "Title: Z-A");
                    break;

                case "Highest Rated":
                    select(sortDropDown_Products, "Highest Rated");
                    break;

                case "Newest":
                    select(sortDropDown_Products, "Newest");
                    break;
                default:
                    throw new RuntimeException("Undefined sort type! sort-type= " + sort);
            }//switch
        } else if (tab.equalsIgnoreCase("articles")) {
            switch (sort) {

                case "Best Match":
                    select(sortDropDown_Articles, "Best Match");
                    break;

                case "Most Recent":
                    select(sortDropDown_Articles, "Most Recent");
                    break;

                default:
                    throw new RuntimeException("Undefined sort type! sort-type= " + sort);
            }
        }
    }

    public boolean isSelectedSortOptionDisplayed(String tab, String sorted) {
        boolean sortedOption = true;
        if(tab.equalsIgnoreCase("products")) {
            if (sorted.equalsIgnoreCase("Best Match")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=relevance&sortDir=desc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Price: Low to High")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=price&sortDir=asc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Price: High to Low")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=price&sortDir=desc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Title: A-Z")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=name&sortDir=asc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Title: Z-A")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=name&sortDir=desc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Highest Rated")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=rating&sortDir=desc") ? true : false;
            } else if (sorted.equalsIgnoreCase("Newest")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=date&sortDir=desc") ? true : false;
            }
        } else if(tab.equalsIgnoreCase("articles")) {
            if(sorted.equalsIgnoreCase("Best Match")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=relevance&sortDir=desc") ? true : false;
            } else if(sorted.equalsIgnoreCase("Most Recent")) {
                sortedOption = driver.getCurrentUrl().contains("sortBy=publishingdate&sortDir=desc") ? true : false;
            }
        }
        return sortedOption;
    }
}