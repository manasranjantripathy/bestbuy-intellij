package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;
import utils.Util;

public class ChangePaymentPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static String EXPECTED_URL = "Order/ChangePayment.aspx";
    private final static Logger logger = Logger.getLogger(ChangePaymentPageDesktop.class);

    @FindBy(xpath = "//input[@id='ctl00_CP_RdoAddNewCreditCard']")
    private WebElement btn_addNewCreditCard;

    @FindBy(xpath = "//select[@id='ctl00_CP_UcProfileCCWithAddress_CreditCardUC_CardTypeContainer_DdlCardType']")
    private WebElement btn_creditCardType;

    @FindBy(xpath = "//div[@id='ctl00_CP_DivEditCreditCard']")
    private WebElement blk_AddCreditCard;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnContinue']")
    private WebElement btn_continue;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnSubmitChange']")
    private WebElement btn_submitChange;

    @FindBy(xpath = "//a[@id='ctl00_CP_OrderGiftCards1_BtnCalculate']")
    private WebElement btn_checkBalance;

    @FindBy(xpath = "//a[@id='ctl00_CP_OrderGiftCards1_BtnApplyToOrder']")
    private WebElement btn_applyGiftCard;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcProfileCCWithAddress_CreditCardUC_CreditCardNumberContainer_TxtCardNumber']")
    private WebElement fld_creditCardNumber;

    @FindBy(xpath = "//select[@id='ctl00_CP_UcProfileCCWithAddress_CreditCardUC_MonthContainer_DdlMonth']")
    private WebElement fld_creditCardExpiryDateMonth;

    @FindBy(xpath = "//select[@id='ctl00_CP_UcProfileCCWithAddress_CreditCardUC_YearContainer_DdlYear']")
    private WebElement fld_creditCardExpiryDateYear;

    @FindBy(xpath = "//input[contains(@id, '_FirstNameContainer_TxtFirstName')]")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[contains(@id, '_LastNameContainer_txtLastName')]")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[contains(@id, '_AddressLine1Container_TxtAddressLine1')]")
    private WebElement fld_streetAddress;

    @FindBy(xpath = "//input[contains(@id, 'CityContainer_TxtCity')]")
    private WebElement fld_city;

    @FindBy(xpath = "//select[contains(@id, '_StateContainer_DdlState')]")
    private WebElement fld_provinceDropDown;

    @FindBy(xpath = "//input[contains(@id, '_PostalCodeContainer_TxtZipCode')]")
    private WebElement fld_postalCodeField;

    @FindBy(xpath = "//select[contains(@id, '_CountryContainer_DdlCountry')]")
    private WebElement fld_country;

    @FindBy(xpath = "//input[contains(@id, '_PhoneContainer_TxtPhone')]")
    private WebElement fld_phone_0;

    @FindBy(xpath = "//input[contains(@id, '_Phone1Container_TxtPhone1')]")
    private WebElement fld_phone_1;

    @FindBy(xpath = "//input[contains(@id, '_Phone2Container_TxtPhone2')]")
    private WebElement fld_phone_2;

    @FindBy(xpath = "//input[contains(@id, '_PhoneExtContainer_TxtPhoneExt')]")
    private WebElement fld_phone_3;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOtherContainer_TxtPhoneOther')]")
    private WebElement fld_other_phone_0;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOther1Container_TxtPhoneOther1')]")
    private WebElement fld_other_phone_1;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOther2Container_TxtPhoneOther2')]")
    private WebElement fld_other_phone_2;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOtherExtContainer_TxtPhoneOtherExt')]")
    private WebElement fld_other_phone_3;

    @FindBy(xpath = "//input[@id='ctl00_CP_OrderGiftCards1_GrdGiftCardEntry_ctl02_TxtGiftCardNumberContainer_TxtGiftCardNumber']")
    private WebElement fld_giftCardNumber;

    @FindBy(xpath = "//input[@id='ctl00_CP_OrderGiftCards1_GrdGiftCardEntry_ctl02_TxtSecurityCodeContainer_TxtSecurityCode']")
    private WebElement fld_giftCardCID;

    @FindBy(xpath = "//span[@id='ctl00_CP_OrderGiftCards1_RptGiftCardsView_ctl00_LblEncodedGiftCardNumber']")
    private WebElement fld_appliedGiftCard;

    @FindBy(xpath = "//select[@id='ctl00_CP_UcProfileCCWithAddress_DdlAddresses']")
    private WebElement selectAddressDropDown;

    @FindBy(xpath = "//*[contains(text(),'PayPal')]")
    private WebElement paypal;

    public ChangePaymentPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            Util.wait(1);
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL.toLowerCase());
        }
        catch(Exception e){
            logger.error("Change Payment page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Change Payment page is not displayed " + e.getMessage(), e);
        }
    }

    public void addGiftCard(final int i) {
        switch (i) {
            case 1:
                typeIn(fld_giftCardNumber, Constants.Payment.GIFTCARD1_NUMBER.toString());
                typeIn(fld_giftCardCID, Constants.Payment.GIFTCARD1_CID.toString());
                break;
            case 2:
                typeIn(fld_giftCardNumber, Constants.Payment.GIFTCARD2_NUMBER.toString());
                typeIn(fld_giftCardCID, Constants.Payment.GIFTCARD2_CID.toString());
                break;
            case 3:
                typeIn(fld_giftCardNumber, Constants.Payment.GIFTCARD3_NUMBER.toString());
                typeIn(fld_giftCardCID, Constants.Payment.GIFTCARD3_CID.toString());
                break;
            case 4:
                typeIn(fld_giftCardNumber, Constants.Payment.GIFTCARD4_NUMBER.toString());
                typeIn(fld_giftCardCID, Constants.Payment.GIFTCARD4_CID.toString());
                break;
            default:
                break;
        }
    }

    public void selectAddNewCreditCardRadioButton() {
        click(btn_addNewCreditCard);
    }

    public boolean isAddNewCreditCardBlockDisplayed() {
        return isDisplayed(blk_AddCreditCard);
    }

    public void fillInCreditCardInformation(final String cardType) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            select(btn_creditCardType, Constants.Payment.AMEX_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.AMEX_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.AMEX_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.AMEX_EXPIRYDATE_YEAR.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            select(btn_creditCardType, Constants.Payment.MC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.MC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.MC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.MC_EXPIRYDATE_YEAR.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            select(btn_creditCardType, Constants.Payment.PLCC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.PLCC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.PLCC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.PLCC_EXPIRYDATE_YEAR.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            select(btn_creditCardType, Constants.Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.VISA2_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.VISA2_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.VISA2_EXPIRYDATE_YEAR.toString());
        }
    }

    public void fillCreditCardInformation(String ccType, String ccNumber, String expiryMonth, String expiryYear) {
        select(btn_creditCardType, ccType);
        typeIn(fld_creditCardNumber, ccNumber);
        select(fld_creditCardExpiryDateMonth, expiryMonth);
        select(fld_creditCardExpiryDateYear, expiryYear);
    }

    public void fillInBillingInformation(String postalCode, String province) {
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_streetAddress, TestUser.streetAddress);
        typeIn(fld_city, TestUser.city);
        select(fld_provinceDropDown, province);
        typeIn(fld_postalCodeField, postalCode);
        select(fld_country, TestUser.country);

        typeIn(fld_phone_0, TestUser.phone0);
        typeIn(fld_phone_1, TestUser.phone1);
        typeIn(fld_phone_2, TestUser.phone2);
        typeIn(fld_phone_3, TestUser.phone3);

        typeIn(fld_other_phone_0, TestUser.otherPhone0);
        typeIn(fld_other_phone_1, TestUser.otherPhone1);
        typeIn(fld_other_phone_2, TestUser.otherPhone2);
        typeIn(fld_other_phone_3, TestUser.otherPhone3);
    }

    public void clickContinue() {
        click(btn_continue);
    }

    public void clickSubmitChange() {
        click(btn_submitChange);
    }

    public void clickCheckBalance() {
        click(btn_checkBalance);
    }

    public void selectAddress() {selectByIndex(selectAddressDropDown, 1);

    }

    public boolean isGiftCardBalanceNotZero(){

        String textGcBalance = "//span[@id='ctl00_CP_OrderGiftCards1_GrdGiftCardEntry_ctl02_LblAvailableBalance']" ;
        WebElement webElement = driver.findElement(By.xpath(textGcBalance));
        String actual = webElement.getText();
        return !actual.trim().equals("$0.00");
    }

    public void clickApplyButton() {
        click(btn_applyGiftCard);
    }

    public boolean isGiftCardApplied() {
        return isDisplayed(fld_appliedGiftCard);
    }

    public boolean isPayPalDisplayed() {
        if(isDisplayed(paypal) == true) {
            return false;
        }else {
            return true;
        }
    }
}