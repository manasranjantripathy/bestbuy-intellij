package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

/**
 * Created by sucho on 7/4/2017.
 */
public class SignInPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/signin.aspx";
    private final static Logger logger = Logger.getLogger(SignInPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//a[@id='ctl00_CP_SignInUC1_BtnLoginButton']")
    private WebElement buttonSignIn;

    @FindBy(how = How.ID, using = "ctl00_CP_CreateAccountUC_BtnCreateAccount")
    private WebElement buttonCreateAccount;

    @FindBy(xpath = "//input[@id='ctl00_CP_SignInUC1_UserNameContainer_txtUserName']")
    private WebElement fieldEmailAddress;

    @FindBy(xpath = "//input[@id='ctl00_CP_SignInUC1_PasswordContainer_txtPassword']")
    private WebElement fieldPassword;

    @FindBy(how = How.XPATH, using = "//span[@id='Recognized' and @style='display: block;']")
    private WebElement textUserSignedIn;

    @FindBy(how = How.XPATH, using = "//div[contains(@id,'CaptchaContainer')]/p[2]")
    private WebElement captchaContainer;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC1_vsumErrorSummary']/ul/li")
    private WebElement errorMessage;

    @FindBy(how = How.XPATH, using = "//a[@id='ctl00_CP_SignInUC1_lnkForgotPassword']")
    private WebElement textIForgotMyPassword;

    private SignInPageDesktop() {

    }

    public SignInPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("SignIn Page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("SignIn Page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isUserSignedIn(){

        return isDisplayed(textUserSignedIn);

    }

    public void signInAsDefaultCustomer(){

        typeIn(fieldEmailAddress, TestUser.email);

        typeIn(fieldPassword, TestUser.password);

        click(buttonSignIn);

    }

    public void signInAsCustomer(){

        typeIn(fieldEmailAddress, TestUser.email3);

        typeIn(fieldPassword, TestUser.password3);

        click(buttonSignIn);

    }

    public void signInAsUser(){

        typeIn(fieldEmailAddress, TestUser.email2);
        typeIn(fieldPassword, TestUser.password);
        click(buttonSignIn);
    }

    public void signInAsDefaultCustomer(final String username, final String password){
        typeIn(fieldEmailAddress, username);

        typeIn(fieldPassword, password);

        click(buttonSignIn);

    }

    public void signInAsNewCustomer() {
        typeIn(fieldEmailAddress, TestUser.newEmail);
        typeIn(fieldPassword, TestUser.password);
        click(buttonSignIn);
    }

    public void signInWithNewPassword(String newPassword) {
        typeIn(fieldEmailAddress, TestUser.newEmail);
        typeIn(fieldPassword, newPassword);
        click(buttonSignIn);
    }

    public String getTextForCaptcha() { return getTextOrValue(captchaContainer); }

    public boolean checkEmailField() {return isDisplayed(fieldEmailAddress);}

    public boolean checkPasswordField() {return isDisplayed(fieldPassword);}

    public void enterUserNameAndPassword(final String input) {
        if(input.equalsIgnoreCase("notMatchingPassword")){
            signInAsDefaultCustomer(TestUser.newEmail, "mismatchPassword");
        }
        else if(input.equalsIgnoreCase("nonExistingEmail")){
            signInAsDefaultCustomer("invalid@yahoo.ca", TestUser.password);
        }
        else if(input.equalsIgnoreCase("invalidPassword")){
            signInAsDefaultCustomer(TestUser.newEmail, "!@#$%||er");
        }
    }

    public String getErrorMessage(){ return getTextOrValue(errorMessage); }

    public void clickOnForgetPasswordButton() {click(textIForgotMyPassword);}

    public void waitForSessionTimeOut(){
        delay(1000);
    }

}