package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.Util;

public class VerifiedByVisaPageDesktop extends StorefrontBasePage{
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/vbvwrapper.aspx";
    private final static Logger logger = Logger.getLogger(CheckoutPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//input[@name='external.field.password']")
    private WebElement passwordfield;

    @FindBy(how = How.XPATH, using = "//input[@name='UsernamePasswordEntry']")
    private WebElement submit;

    @FindBy(xpath = "//iframe[@id='ctl00_CP_ifBankFrame']")
    private WebElement frameLocator;

    public VerifiedByVisaPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
//            waitUntilPageLoaded();
            Util.waitForAjax(driver);
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        } catch (Exception e) {
            logger.error("Verified by Visa page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Verified by Visa page is not displayed " + e.getMessage(), e);
        }
    }

    public void fillInCIDNumber() {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
//        switchToFrame(frameLocator);
        scrollToElement(passwordfield);
        typeIn(passwordfield, Constants.Payment.VISA_CID.toString());
        switchFrameToDefault();
    }

    public void clickSubmit() {
        switchToFrame(frameLocator);
        click(submit);
        switchFrameToDefault();
    }
}