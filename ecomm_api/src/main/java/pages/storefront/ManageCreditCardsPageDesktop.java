package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class ManageCreditCardsPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/creditcardmanager.aspx";
    private final static Logger logger = Logger.getLogger(ManageCreditCardsPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnInsert']")
    private WebElement addNewCreditCard;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcCreditCardManager_LvCreditCard_ctrl0_ctl00_BtnEdit']")
    private WebElement edit;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcCreditCardManager_LvCreditCard_ctrl0_ctl00_BtnDelete']")
    private WebElement delete;

    @FindBy(xpath = "//div[@class='validation']")
    private WebElement confirmMessage;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement errorMessage;

    public ManageCreditCardsPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Manage Credit Cards page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Manage Credit Cards page is not displayed. Actual URL: " + driver
                    .getCurrentUrl() + e.getMessage(), e);
        }
    }

    public void clickAddNewCreditCard(){
        click(addNewCreditCard);
    }

    public void clickEdit() { click(edit); }

    public void clickDelete(){ click(delete); }

    public String getConfirmMessage(){ return getTextOrValue(confirmMessage); }

    public boolean confirmMessageDisplayed() { return isDisplayed(confirmMessage); }

    public String getErrorMessage(){ return getTextOrValue(errorMessage); }
}