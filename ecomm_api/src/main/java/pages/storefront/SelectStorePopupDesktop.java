package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;
import utils.Util;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

/**
 * Created by sucho on 6/30/2017.
 */
public class SelectStorePopupDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor jsExecutor;
    private final static Logger logger = Logger.getLogger(SelectStorePopupDesktop.class);

    @FindBy(how = How.ID, using = "stbtnSearch")
    private WebElement buttonSearch;

    @FindBy(how = How.XPATH, using = "//a[@class='btn btn-small enabled navigation-button btn-one-click")
    private WebElement buttonStoreSelect;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-check-products-in-cart navigation-button btn-primary']")
    private WebElement buttonContinue;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-reserve-now navigation-button btn-primary")
    private WebElement buttionReserve;

    @FindBy(how = How.XPATH, using = "//a[@class='btn-geo btn-geolocation']")
    private WebElement buttonGeoLocation;

    @FindBy(how = How.ID, using = "reservation-received-ok")
    private WebElement buttonOk;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-reserve-now navigation-button btn-primary']")
    private WebElement buttonReserveNow;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_TxtPromoCode')]")
    private WebElement fieldPromotionalCodes;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_EmailContainer_txtEmail')]")
    private WebElement fieldEmail;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_TxtFirstName')]")
    private WebElement fieldFirstName;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_txtLastName')]")
    private WebElement fieldLastName;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_PhoneContainer_TxtPhone")
    private WebElement fieldPhone0;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_Phone1Container_TxtPhone1")
    private WebElement fieldPhone1;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_Phone2Container_TxtPhone2")
    private WebElement fieldPhone2;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_PhoneExtContainer_TxtPhoneExt")
    private WebElement fieldPhone3;

    @FindBy(how = How.XPATH, using = "//div[@class='ui-lightbox-mvc-close']")
    private WebElement iconClose;

    @FindBy(how = How.ID, using = "stSearch")
    private WebElement textBoxSearch;

    @FindBy(how = How.CLASS_NAME, using = "ui-lightbox-mvc-iframe")
    private WebElement frameLocator;

    @FindBy(xpath = "//div[@id='totalsblock']//h1")
    private WebElement labelSummary;

    @FindBy(how = How.XPATH, using = "//li[@class='sl-error-message']")
    private WebElement errorMessage;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'LnkApplyToOrderAndAddMoreCodes')] | //input[@id='ctl00_CP_PromoCodesUC1_BtnUpdate']")
    private WebElement promoApply;

    @FindBy(how = How.XPATH, using = "//li[div[@class[contains(.,'ehf')]]]/div[1]")
    private WebElement environmentalFee;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Enter Promotional Code')]")
    private WebElement enterPromotionalCodes;

    @FindBy(how = How.XPATH, using = "//span[@class='lnk-reservation-details']/a")
    private WebElement linkReservationDetailsRIS;

    @FindBy(how = How.ID, using = "ctl00_CP_StoreInfoUC_BtnSelectStore3")
    private WebElement changeRIS;

    @FindBy(how = How.CLASS_NAME, using = "reserve-more-mode-trigger")
    private WebElement reserveMoreItems;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li | //*[@id='store-locator-list']/div/div[1]/div[2]/div[2]/div[2]/ul/li | //*[@id='store-locator-list']/div/div[1]/div[3]/div[2]/div[2]/ul/li")
    private WebElement textStaticErrorMessage;

    @FindBy(how = How.ID, using = "ctl00_CP_StoresMap_VEMap")
    private WebElement blockStoresInMap;

    @FindBy(how = How.XPATH, using = "//*[@id='store-locator-list']//div[2]/ul/li")
    private WebElement staticErrorMessage;

    @FindBy(how = How.XPATH, using = "//h3[text() = 'Reserve: Contact Information']")
    private WebElement titleLocator;

    @FindBy(how = How.XPATH, using = "//p[@class='success-message']")
    private WebElement successMessage;

    @FindBy(how = How.XPATH, using = "//*[@class='txt-support']/ol/li/strong")
    private WebElement reservedEmail;

    @FindBy(xpath = "//div[@class='price-desc prod-label-ehf']")
    private WebElement ehfText;

    @FindBy(xpath = "//div[@class='summary-container2 summary-container']/div/div[@id='totalsblock']/ul[@class='order-total']/li/div[@class='price-desc prod-label-ehf']")
    private WebElement ehfText2;

    @FindBy(xpath = "//div[@class='contact-info-container']/h2")
    private WebElement contactInformationText;

    @FindBy(xpath = "//a[@class='button navigation-button trigger-restore-cart']")
    private WebElement selectAnotherStoreButton;

    @FindBy(xpath = "//a[contains(@id,'LnkStoreHoursAndServices')]")
    private WebElement storeHoursAndServices;

    @FindBy(xpath = "//div[@class='additional-store-info']")
    private WebElement storeAdditionalDetails;

    @FindBy(xpath = "//div[contains(@id,'_vsumErrorSummary')]/ul/li[1]")
    private WebElement errorEmail;

    @FindBy(xpath = "//div[contains(@id,'_vsumErrorSummary')]/ul/li[2]")
    private WebElement errorFirstName;

    @FindBy(xpath = "//div[contains(@id,'_vsumErrorSummary')]/ul/li[3]")
    private WebElement errorLastName;

    @FindBy(xpath = "//div[contains(@id,'_vsumErrorSummary')]/ul/li[4]")
    private WebElement errorPhone;

    @FindBy(xpath = "//a[@id='ctl00_CP_PromoCodesUC1_LnkAddPromoCode']")
    private WebElement expandPromoCode;

    @FindBy(xpath = "//input[@id='ctl00_CP_PromoCodesUC1_TxtPromoCode']")
    private WebElement fieldPromoCode;

    @FindBy(xpath = "//input[@id='ctl00_CP_PromoCodesUC1_BtnUpdate']")
    private WebElement applyPromoCode;

    @FindBy(xpath = "//div[@class='summary-container1 summary-container']/div/div[@id='totalsblock']/ul[@class='order-total']/li/div[@class='prod-subtotal']")
    private WebElement subTotalPrice;

    @FindBy(xpath = "//div[@class='summary-container1 summary-container']/div/div[@id='totalsblock']/ul[@class='order-total']/li/div[@class='price-desc prod-label-saving']")
    private WebElement savingsLabel;

    @FindBy(xpath = "//div[@class='summary-container2 summary-container']/div/div[@id='totalsblock']/div[@class='label reserve-at']")
    private WebElement storeDetailsContactInfo;

    private String reserveNowJS = "document.querySelector(\"a[class='btn button btn-reserve-now navigation-button btn-primary']\")";

    private final String clickStoreJS = "var stores = document.getElementsByClassName"
            + "    (\"btn btn-small enabled navigation-button btn-one-click\");"
            + "var store = stores[%d];"
            + "store.click();";

    private final String selectStoreLocator = "(//a[@class='btn btn-small enabled navigation-button btn-one-click'])[%d]";

    private SelectStorePopupDesktop() {

    }

    public SelectStorePopupDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();

            switchToFrame(frameLocator);

            wait.until(visibilityOfElementLocated(By.xpath("//h3[text() = 'Reserve: Select a Store'] | //h3[text() = 'Réservation : Sélectionnez un magasin.']")));

            switchFrameToDefault();

            return true;
        } catch (Exception e) {
            logger.error("Select Store Popup is not displayed " + e.getMessage(), e);
            return false;
        }
    }

    public void searchForPostalCode(final String postalCode) {
        switchToFrame(frameLocator);

        typeIn(textBoxSearch, postalCode);

        click(buttonSearch);

        switchFrameToDefault();

    }

    public boolean isSuccessMessageDisplayed() {
        switchToFrame(frameLocator);

        if (getTextOrValue(successMessage).equalsIgnoreCase("Your reservation request has been sent to the store.") |
                getTextOrValue(successMessage).equalsIgnoreCase("Votre demande de réservation a été envoyée au magasin.")) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }

    }

    private void closeAndReopen() {
        try {
            click(iconClose);

            if (driver.getCurrentUrl().contains("Order/Checkout.aspx")) {
                By reserveInStoreLocator = By.id("ctl00_CP_btnReserveInStore");
                WebElement element = wait.until(visibilityOfElementLocated(reserveInStoreLocator));
                click(element);
            } else if (driver.getCurrentUrl().contains("en-CA/product")) {
                By reserveInStoreLocator = By.id("btn-reserve");
                WebElement element = wait.until(visibilityOfElementLocated(reserveInStoreLocator));
                click(element);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void selectStore(final int nth) {

        //TODO need to rewrite the code

        try {
            waitUntilPageLoaded();

            switchToFrame(frameLocator);

//try clicking by Javascript
            int index = 1;
            for (; index <= 3; index++) {
                try {
                    jsExecutor.executeScript(String.format(clickStoreJS, nth));
                    Util.waitForAjax(driver);
                    break;
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }
            //try clicking with explicit wait/expected condition
            if (index > 3) {
                try {
                    By locator = By.xpath(String.format(selectStoreLocator, nth));
                    WebElement store = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
                    store.click();
                    Util.waitForAjax(driver);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            switchFrameToDefault();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage() + " No store is available. Please check the Sku and its inventory.");
        }
    }

    public boolean isSummaryDisplayed() {
        switchToFrame(frameLocator);

        if (getTextOrValue(labelSummary).contains("Summary") | getTextOrValue(labelSummary).contains("Résumé")) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public void continueToNextStep() {
        switchToFrame(frameLocator);

        click(buttonContinue);

        switchFrameToDefault();
    }

    public void fillInContactInformation() {

        switchToFrame(frameLocator);

        typeIn(fieldEmail, TestUser.email);
        typeIn(fieldFirstName, TestUser.firstName);
        typeIn(fieldLastName, TestUser.lastName);
        typeIn(fieldPhone0, TestUser.phone0);
        typeIn(fieldPhone1, TestUser.phone1);
        typeIn(fieldPhone2, TestUser.phone2);
        typeIn(fieldPhone3, TestUser.phone3);

        switchFrameToDefault();
    }

    public void clickReserveNow() {

        switchToFrame(frameLocator);

        click(buttonReserveNow);
        Util.waitForAjax(driver);

        switchFrameToDefault();

    }

    public boolean isEHFTextDisplayed() {
        switchToFrame(frameLocator);
        if (getTextOrValue(ehfText).contains(Constants.Messages.ENVIRONMENTAL_HANDLING_FEES.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean isEHFTextDisplayedInContactInfo() {
        switchToFrame(frameLocator);
        if (getTextOrValue(ehfText2).contains(Constants.Messages.ENVIRONMENTAL_HANDLING_FEES.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean isContactInformationScreenDisplayed() {
        switchToFrame(frameLocator);
        if (getTextOrValue(contactInformationText).contains("Please enter your contact information")) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public void clickSelectAnotherStoreButton() {
        switchToFrame(frameLocator);
        click(selectAnotherStoreButton);
        switchFrameToDefault();
    }

    public void clickStoreHoursAndServicesButton() {
        switchToFrame(frameLocator);
        click(storeHoursAndServices);
        switchFrameToDefault();
    }

    public boolean isStoreAdditionalDetailsDisplayed() {
        switchToFrame(frameLocator);
        if (isDisplayed(storeAdditionalDetails)) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean verifyErrorEmail() {
        switchToFrame(frameLocator);
        if (getTextOrValue(errorEmail).contains(Constants.Messages.ISPU_MISSING_EMAIL.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean verifyErrorFirstName() {
        switchToFrame(frameLocator);
        if (getTextOrValue(errorFirstName).contains(Constants.Messages.ISPU_MISSING_FIRST_NAME.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean verifyErrorLastName() {
        switchToFrame(frameLocator);
        if (getTextOrValue(errorLastName).contains(Constants.Messages.ISPU_MISSING_LAST_NAME.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean verifyErrorPhone() {
        switchToFrame(frameLocator);
        if (getTextOrValue(errorPhone).contains(Constants.Messages.ISPU_MISSING_PHONE.toString())) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public boolean isSavingsLabelDisplayed() {
        switchToFrame(frameLocator);
        if (getTextOrValue(savingsLabel).contains("Savings")) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public void applyPromoCodeToOrder() {
        switchToFrame(frameLocator);
        click(expandPromoCode);
        typeIn(fieldPromotionalCodes, Constants.Payment.PROMOTION_CODE1.toString());
        click(applyPromoCode);
        switchFrameToDefault();
    }

    public void clickReservationDetails() {
        switchToFrame(frameLocator);
        click(linkReservationDetailsRIS);
        switchFrameToDefault();
    }

    public boolean isStoreNameDisplayedInContactInfo() {
        switchToFrame(frameLocator);

        if (isDisplayed(storeDetailsContactInfo)) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }

    public String getTextSubTotalPriceBeforePromoCode() {
        switchToFrame(frameLocator);
        String subTotal = getTextOrValue(subTotalPrice);
        switchFrameToDefault();
        return subTotal;
    }

    public Double subTotalPriceBeforePromoCode() {
        switchToFrame(frameLocator);
        Double subTotal = toDouble(getTextSubTotalPriceBeforePromoCode());
        TestUser.subTotalPrice = subTotal;
        switchFrameToDefault();
        return subTotal;
    }

    public String getTextSubTotalPriceAfterPromoCode() {
        switchToFrame(frameLocator);
        String subTotal = getTextOrValue(subTotalPrice);
        switchFrameToDefault();
        return subTotal;
    }

    public Double subTotalPriceAfterPromoCode() {
        switchToFrame(frameLocator);
        Double subTotal = toDouble(getTextSubTotalPriceAfterPromoCode());
        switchFrameToDefault();
        return subTotal;
    }

    public boolean isPromoCodeApplied() {
        switchToFrame(frameLocator);

        if (subTotalPriceBeforePromoCode() > subTotalPriceAfterPromoCode()) {
            switchFrameToDefault();
            return true;
        } else {
            switchFrameToDefault();
            return false;
        }
    }
}