package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class EmailFriendPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "/emailfriend.aspx?";

    private final static Logger logger = Logger.getLogger(EmailFriendPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//div[@id='CaptchaArea']")
    private WebElement captcha;
    private EmailFriendPageDesktop() {

    }

    public EmailFriendPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Email friend page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Email friend page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean checkCaptcha() { return isDisplayed(captcha);}

}