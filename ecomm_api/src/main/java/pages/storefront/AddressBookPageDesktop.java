package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class AddressBookPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/addressbook.aspx";
    private final static Logger logger = Logger.getLogger(AddressBookPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_BtnInsert")
    private WebElement btn_addNewAddress;

    @FindBy(how = How.ID, using = "ctl00_CP_addressBookUC1_LvAddress_ctrl0_BtnEdit")
    private WebElement btn_editAddress;

    @FindBy(how = How.ID, using = "ctl00_CP_addressBookUC1_LvAddress_ctrl0_BtnDelete")
    private WebElement btn_deleteAddress;

    @FindBy(xpath = "//input[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl1_RbtDefaultAddress']")
    private WebElement chk_saveAsDefault;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[1]")
    private WebElement txt_firstAndLastName;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[2]")
    private WebElement txt_streetAddress;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[3]")
    private WebElement txt_cityProvincePostalCode;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[4]")
    private WebElement txt_country;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[5]")
    private WebElement txt_firstPhone;

    @FindBy(xpath = "//fieldset[@id='ctl00_CP_addressBookUC1_LvAddress_ctrl0_AddressBookUCFieldSetContainer']/div[@class='nobold']/ul[@class='std-bottomspace']/li[6]")
    private WebElement txt_secondPhone;

    @FindBy(xpath = "//span[@id='ctl00_CP_ConfirmationUC_ConfirmationMessage']")
    private WebElement msg_confirmation;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement msg_error;


    public AddressBookPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Address Book page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Address Book page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickAddNewAddress(){
        click(btn_addNewAddress);
    }

    public void clickEditButton(){
        click(btn_editAddress);
    }

    public void clickDeleteButton(){
        click(btn_deleteAddress);
    }

    public String getTextFirstAndLastName(){
        return getTextOrValue(txt_firstAndLastName);
    }

    public String getTextStreetAddress(){
        return getTextOrValue(txt_streetAddress);
    }

    public String getTextCityProvincePostalCode(){
        return getTextOrValue(txt_cityProvincePostalCode);
    }

    public String getTextCountry(){
        return getTextOrValue(txt_country);
    }

    public String getTextFirstPhone(){
        return getTextOrValue(txt_firstPhone);
    }

    public String getTextSecondPhone(){
        return getTextOrValue(txt_secondPhone);
    }

    public String getTextConfirmationMessage(){
        return getTextOrValue(msg_confirmation);
    }

    public boolean confirmMessageDisplayed() {
        return isDisplayed(msg_confirmation);
    }

    public void addMultipleAddress(int addressCount) {
        AddAddressPageDesktop address = new AddAddressPageDesktop(driver);
        for(int i = 0; i < addressCount; i++){
            clickAddNewAddress();
            address.fillInAddress();
            isAt();
            confirmMessageDisplayed();
        }
    }

    public String getMsg_error() {
        return getTextOrValue(msg_error);
    }

    public void clickSaveAsDefault() {
        click(chk_saveAsDefault);
    }
}