package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

/**
 * Created by sucho on 6/29/2017.
 */
public class InterstitialPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/interstitial.aspx";
    private final static Logger logger = Logger.getLogger(InterstitialPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'btn-primary')]//a")
    private WebElement buttonCheckout;

    @FindBy(how = How.XPATH, using = "//div[@class='btn']//a[@href='/order/basket.aspx']")
    private WebElement buttonEditCart;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'btn-paypal')]")
    private WebElement buttonPaypal;

    @FindBy(how = How.ID, using = "btnVisaCheckout")
    private WebElement buttonVisaCheckout;

    @FindBy(how = How.ID, using = "btn-save-changes")
    private WebElement buttonSubmitDeliveryOptions;

    @FindBy(how = How.XPATH, using = "//div[@class='input-group']//input[contains(@class, 'search-input')]")
    private WebElement fieldChangeDeliveryLocationInput;

    @FindBy(how = How.ID, using = "delivery-location")
    private WebElement linkDeliveryLocation;

    private String buttonEditCartJs = ".btn a[href='/order/basket.aspx']";

    protected InterstitialPageDesktop() {

    }

    public InterstitialPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Interstitial page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Interstitial page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickCheckoutButton() {

        click(buttonCheckout);

    }

    public void clickEditCartButton(){

        try {
            click(buttonEditCart);
        }
        catch (RuntimeException e){
            clickByJSQuery(buttonEditCartJs);
        }

    }

    public void clickDeliveryChangeLink(){

        click(linkDeliveryLocation);

    }

    public void clickPayPalButton(){

        click(buttonPaypal);

    }

    public void typePostalCodeOnChangeDeliveryLocationPopup(final String postalCode){

        typeIn(fieldChangeDeliveryLocationInput, postalCode);

        click(buttonSubmitDeliveryOptions);

    }
}