package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.Constants.Payment;
import utils.TestUser;

public class AddCreditCardPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/creditcardmanageradd.aspx";
    private final static Logger logger = Logger.getLogger(AddCreditCardPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnSave']")
    private WebElement btn_submit;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcCreditCardManager_LvCreditCard_ctrl0_ctl01_RbtDefaultCreditCard']")
    private WebElement chk_setAsDefault;

    @FindBy(xpath = "//select[@id='ctl00_CP_CreditCardWithAddressUC_CreditCardUC_CardTypeContainer_DdlCardType']")
    private WebElement fld_creditCardType;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_CreditCardUC_CreditCardNumberContainer_TxtCardNumber']")
    private WebElement fld_creditCardNumber;

    @FindBy(xpath = "//select[@id='ctl00_CP_CreditCardWithAddressUC_CreditCardUC_MonthContainer_DdlMonth']")
    private WebElement fld_creditCardExpiryDateMonth;

    @FindBy(xpath = "//select[@id='ctl00_CP_CreditCardWithAddressUC_CreditCardUC_YearContainer_DdlYear']")
    private WebElement fld_creditCardExpiryDateYear;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_FirstNameContainer_TxtFirstName']")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_LastNameContainer_txtLastName']")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_AddressLine1Container_TxtAddressLine1']")
    private WebElement fld_streetAddress;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_CityContainer_TxtCity']")
    private WebElement fld_city;

    @FindBy(xpath = "//select[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_StateContainer_DdlState']")
    private WebElement fld_province;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PostalCodeContainer_TxtZipCode']")
    private WebElement fld_postalCode;

    @FindBy(xpath = "//select[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_CountryContainer_DdlCountry']")
    private WebElement country;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneContainer_TxtPhone']")
    private WebElement fld_phone_0;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_Phone1Container_TxtPhone1']")
    private WebElement fld_phone_1;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_Phone2Container_TxtPhone2']")
    private WebElement fld_phone_2;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneExtContainer_TxtPhoneExt']")
    private WebElement fld_phone_3;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneOtherContainer_TxtPhoneOther']")
    private WebElement fld_other_phone_0;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneOther1Container_TxtPhoneOther1']")
    private WebElement fld_other_phone_1;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneOther2Container_TxtPhoneOther2']")
    private WebElement fld_other_phone_2;

    @FindBy(xpath = "//input[@id='ctl00_CP_CreditCardWithAddressUC_AddressUC_PhoneOtherExtContainer_TxtPhoneOtherExt']")
    private WebElement fld_other_phone_3;

    @FindBy(xpath = "//span[contains(@id,'_ConfirmationUC_ConfirmationMessage')]")
    private WebElement msg_confirmation;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement msg_error;

    public AddCreditCardPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Manage Credit Cards page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Manage Credit Cards page is not displayed " + e.getMessage(), e);
        }
    }

    public void fillInCreditCardInformation(final String cardType) {
        if(cardType.toLowerCase().equalsIgnoreCase("amex")||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress")||
                cardType.toLowerCase().equalsIgnoreCase("american express")){
            select(fld_creditCardType, Constants.Payment.AMEX_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.AMEX_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.AMEX_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.AMEX_EXPIRYDATE_YEAR.toString());

        }
        else if(cardType.toLowerCase().equalsIgnoreCase("mc")||
                cardType.toLowerCase().equalsIgnoreCase("mastercard")||
                cardType.toLowerCase().equalsIgnoreCase("master card")){
            select(fld_creditCardType, Constants.Payment.MC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.MC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.MC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.MC_EXPIRYDATE_YEAR.toString());

        }
        else if(cardType.toLowerCase().equalsIgnoreCase("plcc")){
            select(fld_creditCardType, Constants.Payment.PLCC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.PLCC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.PLCC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.PLCC_EXPIRYDATE_YEAR.toString());

        }
        else if(cardType.toLowerCase().equalsIgnoreCase("visa")){
            select(fld_creditCardType, Constants.Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Constants.Payment.VISA_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Constants.Payment.VISA_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Constants.Payment.VISA_EXPIRYDATE_YEAR.toString());

        }
    }

    public void fillAddressForCreditCard(){
        typeIn(fld_postalCode, TestUser.postalCodeBC);
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_streetAddress, TestUser.streetAddress);
        typeIn(fld_city, TestUser.city);

        if(System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FR.toString()) ||
                System.getProperty(Constants.Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(Constants.TestLanguage.FRENCH.toString())) {
            select(fld_province, Constants.AddressInformation.BC_PROVINCE_FR.toString());
        }
        else{
            select(fld_province, TestUser.province);
        }

        typeIn(fld_phone_0, TestUser.phone0);
        typeIn(fld_phone_1, TestUser.phone1);
        typeIn(fld_phone_2, TestUser.phone2);
        typeIn(fld_phone_3, TestUser.phone3);

        typeIn(fld_other_phone_0, TestUser.otherPhone0);
        typeIn(fld_other_phone_1, TestUser.otherPhone1);
        typeIn(fld_other_phone_2, TestUser.otherPhone2);
        typeIn(fld_other_phone_3, TestUser.otherPhone3);
    }

    public void clickSubmit(){
        click(btn_submit);
    }

    public String getConfirmMessage(){
        return getTextOrValue(msg_confirmation);
    }

    public void addBillingAddress(String firstNameF, String lastNameF, String addressF, String CityF,
                                  String provinceF, String postalCodeF, String p0, String p1, String p2, String p3){
        typeIn(fld_firstName, firstNameF);
        typeIn(fld_lastName, lastNameF);
        typeIn(fld_streetAddress, addressF);
        typeIn(fld_city, CityF);
        select(fld_province, provinceF);
        typeIn(fld_postalCode, postalCodeF);
        typeIn(fld_phone_0, p0);
        typeIn(fld_phone_1, p1);
        typeIn(fld_phone_2, p2);
        typeIn(fld_phone_3, p3);
    }

    public String verifyErrorMessage(){
        return getTextOrValue(msg_error);
    }

    public void addCreditCardDetails(final String ccType, final String ccNumber, final String expiryMonth, final String expiryYear) {

        if(ccType != null && !ccType.isEmpty()) {
            select(fld_creditCardType, ccType);
        }
        if(ccNumber != null && !ccNumber.isEmpty()) {
            typeIn(fld_creditCardNumber, ccNumber);
        }
        if(expiryMonth != null && !expiryMonth.isEmpty()){
            select(fld_creditCardExpiryDateMonth, expiryMonth);
        }
        if(expiryYear != null && !expiryYear.isEmpty()){
            select(fld_creditCardExpiryDateYear, expiryYear);
        }
    }

    public void addCreditCardDetails(final Payment ccType, final Payment ccNumber, final Payment expiryMonth, final Payment expiryYear) {
        select(fld_creditCardType, ccType.toString());
        typeIn(fld_creditCardNumber, ccNumber.toString());
        select(fld_creditCardExpiryDateMonth, expiryMonth.toString());
        select(fld_creditCardExpiryDateYear, expiryYear.toString());
    }

    public void setSecondCCAsDefault(){
        check(chk_setAsDefault);
    }

    public void addAddressForCreditCard(final String input) {
        if (input.equalsIgnoreCase("firstName")) {
            addBillingAddress("", TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, TestUser.province, TestUser.postalCodeBC, TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if (input.equalsIgnoreCase("lastName")) {
            addBillingAddress(TestUser.firstName, "", TestUser.streetAddress,
                    TestUser.city, TestUser.province, TestUser.postalCodeBC, TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("streetAddress")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, "",
                    TestUser.city, TestUser.province, TestUser.postalCodeBC, TestUser.phone0,TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("city")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    "", TestUser.province, TestUser.postalCodeBC, TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("province")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, "-- Select --", TestUser.postalCodeBC, TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if (input.equalsIgnoreCase("postalCode")) {
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, TestUser.province, "", TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("phone")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, TestUser.province, TestUser.postalCodeBC, "", "",
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("provinceNotMatchingPC")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, "British Columbia", "M5B2L6", TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
        else if(input.equalsIgnoreCase("postalCodeNotRecognized")){
            addBillingAddress(TestUser.firstName, TestUser.lastName, TestUser.streetAddress,
                    TestUser.city, TestUser.province, "X1Y1Z1", TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
        }
    }

    public String checkErrorMessage(String errorMessage){
        if (errorMessage.equalsIgnoreCase("firstName")) {
            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_FIRST_NAME.toString();
        }
        else if (errorMessage.equalsIgnoreCase("lastName")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_LAST_NAME.toString();
        }
        else if (errorMessage.equalsIgnoreCase("streetAddress")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_STREET_ADDRESS.toString();
        }
        else if (errorMessage.equalsIgnoreCase("city")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_CITY.toString();
        }
        else if (errorMessage.equalsIgnoreCase("province")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_PROVINCE.toString();
        }
        else if (errorMessage.equalsIgnoreCase("postalCode")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_POSTAL_CODE.toString();
        }
        else if (errorMessage.equalsIgnoreCase("phone")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_PHONE.toString();
        }
        else if (errorMessage.equalsIgnoreCase("provinceNotMatchingPC")) {

            errorMessage = Constants.Messages.ERR_MSG_PROVINCE_NOT_MATCHING_PC.toString();
        }
        else if (errorMessage.equalsIgnoreCase("postalCodeNotRecognized")) {

            errorMessage = Constants.Messages.ERR_MSG_POSTAL_CODE_NOT_RECOGNIZED.toString();
        }
        return errorMessage;
    }

    public void addCreditCardToAccount(final String creditCard) {
        if(creditCard.equalsIgnoreCase("differentExpiryDate")) {
            addCreditCardDetails(Payment.VISA_CREDITCARD_TYPE, Payment.VISA_CREDITCARD_NUMBER,
                    Payment.VISA2_EXPIRYDATE_MONTH, Payment.VISA2_EXPIRYDATE_YEAR);
            fillAddressForCreditCard();
            clickSubmit();
        }
        else if(creditCard.equalsIgnoreCase("CCWithDiffBillingAddress")) {
            addCreditCardDetails(Payment.MC_CREDITCARD_TYPE, Payment.MC_CREDITCARD_NUMBER,
                    Payment.MC_EXPIRYDATE_MONTH, Payment.MC_EXPIRYDATE_YEAR);
            addBillingAddress(TestUser.firstName, TestUser.lastName, "8800 GlenLyon Parkway",
                    TestUser.city, TestUser.province, TestUser.postalCodeBC, TestUser.phone0, TestUser.phone1,
                    TestUser.phone2, TestUser.phone3);
            clickSubmit();
        }
        else if(creditCard.equalsIgnoreCase("CCWithSameBillingAddress")) {
            addCreditCardDetails(Payment.MC_CREDITCARD_TYPE, Payment.MC_CREDITCARD_NUMBER,
                    Payment.MC_EXPIRYDATE_MONTH, Payment.MC_EXPIRYDATE_YEAR);
            fillAddressForCreditCard();
            clickSubmit();
        }
        if(creditCard.equalsIgnoreCase("differentCCWithSameType")) {
            addCreditCardDetails(Payment.VISA2_CREDITCARD_TYPE, Payment.VISA2_CREDITCARD_NUMBER,
                    Payment.VISA2_EXPIRYDATE_MONTH, Payment.VISA2_EXPIRYDATE_YEAR);
            fillAddressForCreditCard();
            clickSubmit();
        }
    }

    public void addMultipleCC(int ccCount) {
        ManageCreditCardsPageDesktop cc = new ManageCreditCardsPageDesktop(driver);

        for(int i = 1; i <= ccCount; i++){
            cc.clickAddNewCreditCard();
            select(fld_creditCardType, Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Payment.VISA_CREDITCARD_NUMBER.toString());

            selectByIndex(fld_creditCardExpiryDateMonth, i);
            select(fld_creditCardExpiryDateYear, Payment.VISA_EXPIRYDATE_YEAR.toString());

            fillAddressForCreditCard();
            clickSubmit();
            cc.isAt();
            cc.confirmMessageDisplayed();
        }
    }
}