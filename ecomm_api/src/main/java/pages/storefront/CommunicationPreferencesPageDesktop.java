package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class CommunicationPreferencesPageDesktop extends StorefrontBasePage {

    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/communicationpreferences.aspx";
    private final static Logger logger = Logger.getLogger(CommunicationPreferencesPageDesktop.class);

    @FindBy(xpath = "//select[@id='ctl00_CP_LanguagePreferenceUC_DdlLanguages']")
    private WebElement languagePreferenceDropdown;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnChangePreferences']")
    private WebElement saveChanges;


    public CommunicationPreferencesPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Communication Preferences page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Communication Preferences page is not displayed " + e.getMessage(), e);
        }
    }

    public void selectLanguage(final String language){
        select(languagePreferenceDropdown, language);
        click(saveChanges);
    }


}