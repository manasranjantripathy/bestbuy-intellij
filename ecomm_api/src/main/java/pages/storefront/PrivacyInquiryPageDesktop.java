package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;

public class PrivacyInquiryPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "privacy-inquiry.aspx";
    private final static Logger logger = Logger.getLogger(PrivacyInquiryPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_ucPrivacyInquiry_BtnSubmit']")
    private WebElement btn_submit;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_FirstNameContainer_txtFirstName']")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_LastNameContainer_txtLastName']")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_AddressContainer_txtAddress']")
    private WebElement fld_address;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_CityContainer_txtCity']")
    private WebElement fld_city;

    @FindBy(xpath = "//select[@id='ctl00_CP_ucPrivacyInquiry_ProvinceContainer_ddlProvince']")
    private WebElement fld_province;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_PostalCodeContainer_txtPostalCode']")
    private WebElement fld_postalCode;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_EmailConatiner_txtEmail']")
    private WebElement fld_email;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_PhoneContainer_TxtPhone']")
    private WebElement fld_Phone0;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_Phone1Container_TxtPhone1']")
    private WebElement fld_Phone1;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_Phone2Container_TxtPhone2']")
    private WebElement fld_Phone2;

    @FindBy(xpath = "//input[@id='ctl00_CP_ucPrivacyInquiry_PhoneExtContainer_TxtPhoneExt']")
    private WebElement fld_Phone3;

    @FindBy(xpath = "//textarea[@id='ctl00_CP_ucPrivacyInquiry_PrivacyRequestDetailsContainer_txtPriavcyRequestDetails']")
    private WebElement fld_PrivacyRequest;

    @FindBy(xpath = "//div[@id='ctl00_CP_ucPrivacyInquiry_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement txt_errorMessage;

    @FindBy(xpath ="//span[@id='ctl00_CP_confirmationMessage_ConfirmationMessage']")
    private WebElement txt_confirmMessage;

    public PrivacyInquiryPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Privacy Inquiry page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Privacy Inquiry page is not displayed " + e.getMessage(), e);
        }
    }

    public void fillPrivacyInquiry(String firstName, String lastName, String address, String city, Constants.Messages province,
                                   String postalCode, String email, String phone0, String phone1, String phone2,
                                   String Phone3, Constants.Messages privacyRequest){
        typeIn(fld_firstName, firstName);
        typeIn(fld_lastName, lastName);
        typeIn(fld_address, address);
        typeIn(fld_city, city);
        select(fld_province, province.toString());
        typeIn(fld_postalCode, postalCode);
        typeIn(fld_email, email);
        typeIn(fld_Phone0, phone0);
        typeIn(fld_Phone1, phone1);
        typeIn(fld_Phone2, phone2);
        typeIn(fld_Phone3, Phone3);
        typeIn(fld_PrivacyRequest, privacyRequest.toString());
        click(btn_submit);
    }

    public String getErrorMessage(){ return getTextOrValue(txt_errorMessage); }

    public String getConfirmMessage(){ return getTextOrValue(txt_confirmMessage); }

}