package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class ReviewPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "catalog/reviewandrateproduct.aspx?";
    private final static Logger logger = Logger.getLogger(ReviewPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_txtName')]")
    private WebElement fieldName;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_LocationContainer_txtLocation')]")
    private WebElement fieldLocation;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_txtTitleOfReview')]")
    private WebElement fieldReviewTitle;

    @FindBy(how = How.XPATH, using = "//textarea[contains(@id,'_txtReview')]")
    private WebElement fieldReview;

    @FindBy(how = How.XPATH, using = "//a[@id='ctl00_CC_ucRateWriteReview_ucSubmit']")
    private WebElement buttonSubmit;

    @FindBy(how = How.XPATH, using = "//div[contains(@id,'_ucRateWriteReview_ErrorSummaryUC_vsumErrorSummary')]")
    private WebElement textStaticErrorMessage;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_ucOverallRating_RadioButton1')]")
    private WebElement buttonPerformance;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_ucOverallRating_RadioButton2')]")
    private WebElement buttonScreenQuality;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_ucOverallRating_RadioButton4')]")
    private WebElement buttonBatteryLife;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_ucOverallRating_RadioButton5')]")
    private WebElement buttonStyle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"page\"]/div/div[2]/div[2]/div[2]/div/h2")
    private WebElement submitText;

    private ReviewPageDesktop(){

    }

    public ReviewPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Review page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Review page is not displayed " + e.getMessage(), e);
        }
    }

    public void enterLocation(String location) {
        typeIn(fieldLocation, location);
    }

    public void enterName(String name) {
        typeIn(fieldName, name);
    }

    public void enterReview(String review) {
        typeIn(fieldReview, review);
    }

    public void enterReviewTitle(String reviewTitle) {
        typeIn(fieldReviewTitle, reviewTitle);
    }

    public void clickOnSubmitButton() { click(buttonSubmit);}

    public String getErrorMessageForRating() { return getTextOrValue(textStaticErrorMessage);}

    public void rateProduct() {
        clickByJs(buttonPerformance);
        clickByJs(buttonScreenQuality);
        clickByJs(buttonBatteryLife);
        clickByJs(buttonStyle);
    }

    public String getConfirmationText() {return getTextOrValue(submitText);}
}