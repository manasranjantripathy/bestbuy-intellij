package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

public class ChangeShippingPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static String EXPECTED_URL = "Order/ChangeShipping.aspx";
    private final static Logger logger = Logger.getLogger(ChangeShippingPageDesktop.class);

    @FindBy(xpath = "//input[@id='ctl00_CP_RdoAddNewAddress']")
    private WebElement btn_addNewAddress;

    @FindBy(xpath = "//a[@id='ctl00_CP_BtnContinue']")
    private WebElement btn_continue;

    @FindBy(xpath = "//input[contains(@id, '_FirstNameContainer_TxtFirstName')]")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[contains(@id, '_LastNameContainer_txtLastName')]")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[contains(@id, '_AddressLine1Container_TxtAddressLine1')]")
    private WebElement fld_streetAddress;

    @FindBy(xpath = "//input[contains(@id, 'CityContainer_TxtCity')]")
    private WebElement fld_city;

    @FindBy(xpath = "//select[contains(@id, '_StateContainer_DdlState')]")
    private WebElement fld_provinceDropDown;

    @FindBy(xpath = "//input[contains(@id, '_PostalCodeContainer_TxtZipCode')]")
    private WebElement fld_postalCodeField;

    @FindBy(xpath = "//select[contains(@id, '_CountryContainer_DdlCountry')]")
    private WebElement fld_country;

    @FindBy(xpath = "//input[contains(@id, '_PhoneContainer_TxtPhone')]")
    private WebElement fld_phone_0;

    @FindBy(xpath = "//input[contains(@id, '_Phone1Container_TxtPhone1')]")
    private WebElement fld_phone_1;

    @FindBy(xpath = "//input[contains(@id, '_Phone2Container_TxtPhone2')]")
    private WebElement fld_phone_2;

    @FindBy(xpath = "//input[contains(@id, '_PhoneExtContainer_TxtPhoneExt')]")
    private WebElement fld_phone_3;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOtherContainer_TxtPhoneOther')]")
    private WebElement fld_other_phone_0;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOther1Container_TxtPhoneOther1')]")
    private WebElement fld_other_phone_1;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOther2Container_TxtPhoneOther2')]")
    private WebElement fld_other_phone_2;

    @FindBy(xpath = "//input[contains(@id, '_PhoneOtherExtContainer_TxtPhoneOtherExt')]")
    private WebElement fld_other_phone_3;


    public ChangeShippingPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL.toLowerCase());
        }
        catch (Exception e){
            logger.error("Order Details page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order Details page is not displayed " + e.getMessage(), e);
        }
    }

    public void selectAddNewAddressRadioButton() {
        click(btn_addNewAddress);
    }

    public void fillInShippingInformation(String postalCode, String province) {
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_streetAddress, TestUser.streetAddress);
        typeIn(fld_city, TestUser.city);
        select(fld_provinceDropDown, province);
        typeIn(fld_postalCodeField, postalCode);
        select(fld_country, TestUser.country);

        typeIn(fld_phone_0, TestUser.phone0);
        typeIn(fld_phone_1, TestUser.phone1);
        typeIn(fld_phone_2, TestUser.phone2);
        typeIn(fld_phone_3, TestUser.phone3);

        typeIn(fld_other_phone_0, TestUser.otherPhone0);
        typeIn(fld_other_phone_1, TestUser.otherPhone1);
        typeIn(fld_other_phone_2, TestUser.otherPhone2);
        typeIn(fld_other_phone_3, TestUser.otherPhone3);
    }

    public void clickContinue() {
        click(btn_continue);
    }
}