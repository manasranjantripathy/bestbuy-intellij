package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class PressReleasePageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_PRESS_ROOM_URL = "pressroominfo.aspx";
    private static final String EXPECTED_PRESS_RELEASE_URL = "pressroomnews.aspx";
    private final static Logger logger = Logger.getLogger(PressReleasePageDesktop.class);

    @FindBy(xpath  = "//a[text()='PRESS RELEASES >']")
    private WebElement pressReleases;

    public PressReleasePageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAtPressRoom() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_PRESS_ROOM_URL);
        }
        catch(Exception e){
            logger.error("Press Release page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Press Release page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAtPressRelease() {
        try {
            waitUntilPageLoaded(12);
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_PRESS_RELEASE_URL);
        }
        catch(Exception e){
            logger.error("Press Release page is not loaded within 12 seconds " + e.getMessage(), e);
            throw new RuntimeException("Press Release page is not loaded within 12 seconds " + e.getMessage(), e);
        }
    }

    public void clickPressReleases(){ click(pressReleases); }
}