package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.StorefrontBasePage;
import utils.Constants.Property;
import utils.Util;

import java.util.Set;

/**
 * Created by sucho on 6/29/2017.
 */
public class HomePageDesktop extends StorefrontBasePage {
    private final static Logger logger = Logger.getLogger(HomePageDesktop.class);
    private WebDriver driver;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignIn")
    private WebElement linkSignIn;

    private HomePageDesktop() {

    }

    public HomePageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        waitUntilPageLoaded();
        return driver.getCurrentUrl().contains("bestbuy");
    }

    public boolean isUserSignedIn(){

        return isDisplayed(linkSignIn);

    }

    public void openPage(final String testEnv) {
        Util.verifyTestEnvironment(testEnv);
        driver.get(testEnv);
        waitUntilPageLoaded();

    }

    public void openPage(){
        String testEnvFromCmd = System.getProperty(Property.TEST_ENVIRONMENT.toString());
        if(testEnvFromCmd == null || testEnvFromCmd.isEmpty()) {
            throw new RuntimeException("Test Environment(URL) is null or empty");
        }

        Util.verifyTestEnvironment(testEnvFromCmd);
        driver.get(testEnvFromCmd);
        waitUntilPageLoaded();
    }

    public void turnOffExpressCheckout(){

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("return window.config.enableExpressCheckout=false;");

    }

    public void turnOnExpressCheckout(){

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("window.config.enableExpressCheckout=true;");

    }

    public void theOpensURLInNewTab() {
        String url = System.getProperty(Property.TEST_ENVIRONMENT.toString());
        if(url == null || url.isEmpty()) {
            throw new RuntimeException("Test Environment(URL) is null or empty");
        }
        String script = "var anchor=document.createElement('a');anchor.target='_blank';anchor.href='%s';anchor.innerHTML='.';document.body.appendChild(anchor);return anchor";
        Object element = ((JavascriptExecutor) driver).executeScript(
                String.format(script, url));
        if (element instanceof WebElement) {
            WebElement anchor = (WebElement) element;
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", anchor);
            ((JavascriptExecutor) driver).executeScript(
                    "var a=arguments[0];a.parentNode.removeChild(a);", anchor);
        }
        else {
            throw new RuntimeException("Unable to open tab: " + url);
        }
        Set<String> handles = driver.getWindowHandles();
        String current = driver.getWindowHandle();
        handles.remove(current);
        String newTab = handles.iterator().next();
        driver.switchTo().window(newTab);

    }

}

