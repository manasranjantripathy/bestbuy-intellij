package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

public class PreferredStoresPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/my-stores.aspx";
    private final static Logger logger = Logger.getLogger(PreferredStoresPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_FindStoreUC1_Submit']")
    private WebElement btn_findStores;

    @FindBy(xpath = "//a[@id='ctl00_CP_StoreResultsBlock_StoreDetailsGrid_ctrl0_LnkAddPreferredStore']")
    private WebElement btn_addToPreferredStore;

    @FindBy(xpath = "//img[@id='ctl00_CP_PreferredStoreUC1_StoreDetailsGrid_ctrl0_ImgExpandable']")
    private WebElement btn_addedStore;

    @FindBy(xpath = "//a[@id='ctl00_CP_PreferredStoreUC1_StoreDetailsGrid_ctrl0_LnkRemove']")
    private WebElement btn_remove;

    @FindBy(xpath = "//img[@id='ctl00_CP_StoreResultsBlock_StoreDetailsGrid_ctrl1_ImgExpandable']")
    private WebElement btn_expandSecondStore;

    @FindBy(xpath = "//a[@id='ctl00_CP_StoreResultsBlock_StoreDetailsGrid_ctrl1_LnkAddPreferredStore']")
    private WebElement btn_addToPreferredStoreSecondStore;

    @FindBy(xpath = "//input[@id='ctl00_CP_FindStoreUC1_PostalCodeContainer_PostalCode']")
    private WebElement fld_postalCode;

    @FindBy(xpath = "//select[@id='ctl00_CP_FindStoreUC1_StateContainer_ddlStates']")
    private WebElement fld_province;

    @FindBy(xpath = "//div[@class='searchresults']")
    private WebElement searchResults;


    public PreferredStoresPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Preferred Store page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Preferred Store page is not displayed " + e.getMessage(), e);
        }
    }

    public void searchStores(){
        typeIn(fld_postalCode, TestUser.postalCodeBC);
        select(fld_province, TestUser.province);
        click(btn_findStores);
    }

    public void clickAddToPreferredStoresLink(){
        click(btn_addToPreferredStore);
    }

    public boolean verifyAddedPreferredStore(){
        return isDisplayed(btn_addedStore);
    }

    public void removeStore(){
        click(btn_addedStore);
        click(btn_remove);
    }

    public boolean verifySearchResults(){
        return isDisplayed(searchResults);
    }

    public void addSecondStoreToPreferredStore(){
        scrollAndClick(btn_expandSecondStore);
        click(btn_addToPreferredStoreSecondStore);
    }
}