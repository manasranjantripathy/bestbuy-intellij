package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.Constants.AddressInformation;
import utils.Constants.Payment;
import utils.Constants.Property;
import utils.Constants.TestLanguage;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 6/29/2017.
 */
public class CheckoutPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/checkout.aspx";
    private final static Logger logger = Logger.getLogger(CheckoutPageDesktop.class);

    private final String fld_postalCodeJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PostalCodeContainer_TxtZipCode";

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_btnContinueFromShipping')] | //*[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_btnContinueFromShipping']")
    private WebElement btn_continueShipping;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'btnSubmitOrder')] | //*[@id='ctl00_CP_BtnSaveBasket']")
    private WebElement btn_submit;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'CP_checkoutSections_ctl03_ucPaymentEdit_lnkCreditCard')]")
    private WebElement btn_creditCardSelect;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_BtnApplyToOrder")
    private WebElement btn_giftCardApplyToOrder;

    @FindBy(how = How.XPATH, using = "//select[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CardTypeContainer_DdlCardType']")
    private WebElement btn_creditCardType;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'Add to Cart')]")
    private WebElement btn_addtoCartOnPSP;

    @FindBy(xpath = "//select[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UcExistingCreditCard_DdlCreditCard']")
    private WebElement btn_creditCardDropDown;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_GiftCardEditLink1")
    private WebElement btn_editGiftCard;

    @FindBy(xpath = "//div[@class='ui-accord-section giftcard']/div/h3/a")
    private WebElement btn_giftCardExpand;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_BtnCalculate")
    private WebElement btn_checkGCBalance;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_BtnApplyToOrder')] | //*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_BtnApplyToOrder']")
    private WebElement btn_applyGCToOrder;

    @FindBy(xpath = "//select[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_ddlShippingAddress']")
    private WebElement btn_shippingAddressDropDown;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_BtnEditAddress")
    private WebElement btn_billingAddressEdit;

    @FindBy(id = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnEditCCCancel")
    private WebElement btn_billingAddressCancel;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_lnkCreditCard']")
    private WebElement btn_selectCC;

    @FindBy(xpath = "//span[@id='ctl00_CP_checkoutSections_Span2']")
    private WebElement btn_changePaymentType;

    @FindBy(xpath = "//a[contains(@id,'_IbtnApplyToOrder')]")
    private WebElement btn_applyPromoCode;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_btnNewAddress']")
    private WebElement btn_useANewAddress;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_btnNewAddressDone']/span")
    private WebElement btn_newShippingAddressDone;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnSave']")
    private WebElement btn_billingAddressDone;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_LbtnAddNewCard']")
    private WebElement btn_addANewCreditCardForPayment;

    @FindBy(xpath = "//select[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_CountryContainer_DdlCountry']/option[2]")
    private WebElement btn_USBillingAddressCountryDropDown;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_CountryContainer_DdlCountry")
    private WebElement btn_billingAddressCountryDropDown;

    @FindBy(xpath = "//input[contains(@id,'CPCL-EXPEDITED')]")
    private WebElement btn_radioButtonExpeditedDelivery;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_lnkPayPal']")
    private WebElement btn_PayPalSelect;

    @FindBy(xpath="//*[@id='ctl00_CP_checkoutSections_ctl00_ShipmentListingUC_LblEditAddress'] | //*[contains(@id,'EditAddress')]")
    private WebElement btn_editAddressInfo;

    @FindBy(xpath = "//div[@class='ui-accord-section promo-code']/div/h3/a")
    private WebElement btn_expandPromotionalCode;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnContinueFromPaymentPaypal']")
    private WebElement btn_continueToPaypal;

    @FindBy(how = How.XPATH, using = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_ChkSameAsShipping']")
    private WebElement chk_boxSameAsShipping;

    @FindBy(xpath = "//span[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucLoyaltyProgram_Label1']")
    private WebElement btn_checkoutPageRewardZoneApply;

    @FindBy(xpath = "//input[contains(@id,'_oeaUseNew_chkSave')]")
    private WebElement chk_saveAddressCheckBox;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_ChkSaveThisCardToProfile']")
    private WebElement chk_saveCreditCardCheckBoxForPayment;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_ChkSetThisCardAsDefault']")
    private WebElement chk_setAsDefaultCheckBoxForPayment;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_chkSetAsDefault']")
    private WebElement chk_setAsDefaultCheckBox;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaEditExisting_addressUC_FirstNameContainer_TxtFirstName']")
    private WebElement fld_firstName;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaEditExisting_addressUC_LastNameContainer_txtLastName']")
    private WebElement fld_lastName;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaEditExisting_addressUC_AddressLine1Container_TxtAddressLine1']")
    private WebElement fld_assressStreet;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CreditCardNumberContainer_TxtCardNumber")
    private WebElement fld_creditCardNumber;

    @FindBy(how = How.XPATH, using = "//input[@id = 'ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CIDNumberContainer_TxtCID']")
    private WebElement fld_creditCardCVV;

    @FindBy(how = How.XPATH, using = "//input[@id = 'ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_ucCCPaymentApplied_LVCreditCardsView_ctrl0_ctl00_CIDNumberContainer_TxtCID']")
    private WebElement fld_creditCardCID;

    @FindBy(how = How.XPATH, using = "//*[@id[contains(.,'TxtEmailAddress')]]")
    private WebElement fld_confirmationEmail;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_StateContainer_DdlState")
    private WebElement fld_province;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_MonthContainer_DdlMonth")
    private WebElement fld_creditCardExpiryDateMonth;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_YearContainer_DdlYear")
    private WebElement fld_creditCardExpiryDateYear;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_RptGiftCardEntry_ctl00_TxtGiftCardNumberContainer_TxtGiftCardNumber")
    private WebElement fld_giftCardNumber;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_RptGiftCardEntry_ctl00_TxtSecurityCodeContainer_TxtSecurityCode")
    private WebElement fld_giftCardCID;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UcExistingCreditCard_CIDNumberContainer_TxtCID")
    private WebElement fld_cidAfterEditCC;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CIDNumberContainer_TxtCID")
    private WebElement fld_VisaCidAfterEditCC;

    @FindBy(xpath = "//select[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_CountryContainer_DdlCountry']")
    private WebElement fld_countryInNewAddress;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_StateContainer_DdlState")
    private WebElement fld_billingAddressProvince;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucOrderPromoCodes_txtPromoCodeContainer_txtPromoCode']")
    private WebElement fld_promotionalCode;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_pnlPayment")
    private WebElement form_selectPaymentMethod;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_pnlReview")
    private WebElement form_reviewAndSubmit;

    @FindBy(xpath = "//img[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_imgPayPal']")
    private WebElement img_PayPal;

    @FindBy(xpath = "//span[@class='price-desc' and contains(text(), 'Environmental Handling Fee')] | //span[@class='price-desc' and contains(text(), 'Écofrais')]")
    private WebElement label_envHandlingFeeInOrderSection;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_ucCCPaymentApplied_LVCreditCardsView_ctrl0_ctl00_lnkCreditCardEdit']")
    private WebElement lnk_editCreditCard;

    @FindBy(xpath = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_AddAnotherGiftCardLink")
    private WebElement lnk_addAnotherGiftCard;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_PayPalEditLink']")
    private WebElement lnk_EditPayPal;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC12_ValidationSummary1']/ul/li")
    private WebElement msg_error;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC5_ValidationSummary1']/ul/li")
    private WebElement msg_errorMessageForAddingMoreCC;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl00_ShipmentListingUC_rptShipmentsContainer_ctl00_shipmentLineItemsUC_rptLineItems_ctl00_lblReleaseDate")
    private WebElement txt_preOrderReleaseDate;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl00_ShipmentListingUC_rptShipmentsContainer_ctl00_shipmentLineItemsUC_rptLineItems_ctl00_lblPreOrder")
    private WebElement txt_preOrder;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_pnlShippingContent")
    private WebElement txt_shippingContent;

    @FindBy(xpath = "//div[@class='cc-num']")
    private WebElement txt_maskedCCNumber;

    @FindBy(xpath = "//div[@class='existing-address-info']//p[1]") // //div[@class='shipping-address-container span6']
    private WebElement txt_shippingAddress;

    @FindBy(xpath = "//div[@class='shipping-address-container span6']")
    private WebElement txt_FullShippingAddress;

    @FindBy(xpath = "//div[@class='cc-billing-address']/p[2]")
    private WebElement txt_billingAddressFullName;

    @FindBy(xpath = "//div[@class='cc-billing-address']/p[4]")
    private WebElement txt_billingStreetAddress;

    @FindBy(xpath = "//div[@class='cc-billing-address']/p[6]")
    private WebElement txt_billingAddressProvincePC;

    @FindBy(xpath = "//div[@class='cc-billing-address']/p[7]")
    private WebElement txt_billingAddressCountry;

    @FindBy(xpath = "//div[@class='cc-billing-address']/p[8]")
    private WebElement txt_billingAddressPhone;

    @FindBy(xpath = "//div[@id='totalsblock']/ul/li[9]")
    private WebElement txt_giftCardTotal;

    @FindBy(xpath = "//ul[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucGiftCards_ulGiftCards']/li/div[@class='gift-card-final-balance inline-block border-none vertical-align-bottom']")
    private WebElement txt_giftCardFinalBalance;

    @FindBy(xpath = "//div[@class='exp-date']")
    private WebElement txt_exipryDate;

    @FindBy(xpath = "//div[@class='gc-card-num']")
    private WebElement txt_maskedGiftCard;

    @FindBy(xpath = "//span[@id='ctl00_CP_yourCartItems_ucYourCartItemsOrderSummary_LitProductTotalValue']")
    private WebElement txt_productTotal;

    @FindBy(xpath = "//span[@id='ctl00_CP_yourCartItems_ucYourCartItemsOrderSummary_LitShipping']")
    private WebElement txt_shippingDelivery;

    @FindBy(xpath = "//span[@id='ctl00_CP_yourCartItems_ucYourCartItemsOrderSummary_LabelShipping']")
    private WebElement txt_shippingDeliveryValue;

    @FindBy(xpath = "//span[@id='ctl00_CP_yourCartItems_ucYourCartItemsOrderSummary_LabelShipping']/../following-sibling::li[@class='line-sub']/span[@class='price']")
    private WebElement txt_subTotalValue;

    @FindBy(xpath = "//select[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_CountryContainer_DdlCountry']/option[1]")
    private WebElement txt_canadaBillingAddressCountryDropDown;

    @FindBy(xpath ="//div[@id='ctl00_CP_checkoutSections_ctl00_ShipmentListingUC_rptShipmentsContainer_ctl00_ucLevelOfServiceOptions_RptLevelOfServiceOptions_ctl00_PnlShipmentLosOptions']/p/span")
    private WebElement txt_DeliveryPromiseMessage;

    @FindBy(xpath = "//div[@id='ctl00_CP_checkoutSections_ctl00_ShipmentListingUC_rptShipmentsContainer_ctl00_ucLevelOfServiceOptions_RptLevelOfServiceOptions_ctl00_PnlShipmentLosOptions']/p[3]")
    private WebElement txt_DeliveryType;

    @FindBy(xpath = "//div[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_PnlCreditCardApplied']")
    private WebElement ccAndBillingSection;

    @FindBy(xpath = "//div[@class='gift-card-section font-xxs']")
    private WebElement giftCardSection;

    @FindBy(xpath = "//div[@class='promo-applied']")
    private WebElement promoCodeAppliedSection;

    @FindBy(xpath = "//div[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_pnlPayPalBeforeLogin']")
    private WebElement selectedPaypalOption;

    @FindBy(xpath = "//div[contains(@id,'_ErrorSummaryUC9_ValidationSummary1')]/ul/li")
    private WebElement errorMessageUnsuccessfulVBV;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_RewardZoneEditLink']")
    private WebElement paymentCartEditLinkForRewardZoneId;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_ucLoyaltyProgram_MemberIdContainer_TxtMemberId']")
    private WebElement rewardZoneInputPaymentField;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CIDNumberContainer_TxtCID']")
    private WebElement cidNumberAfterVisaCheckouttCidNumber;

    @FindBy(xpath = "//div[contains(@id,'_pnlEditShippingAddress')]")
    private WebElement blockEditShippingAddress;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CreditCardNumberContainer_TxtCardNumber']")
    private WebElement ccNumberEditCC;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CardTypeContainer_DdlCardType']")
    private WebElement ccType;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CIDNumberContainer_TxtCID']")
    private WebElement cidNumberEditCC;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_MonthContainer_DdlMonth']")
    private WebElement ccExpMonthEditCC;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_YearContainer_DdlYear']")
    private WebElement ccExpYearEditCC;

    @FindBy(xpath = "//*[contains(@id,'CP_checkoutSections_ctl03_ucPaymentEdit_lnkCreditCard')]")
    private WebElement buttonCreditCardSelect;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UcExistingCreditCard_DdlCreditCard']")
    private WebElement selectCreditCardDropDown;

    @FindBy(xpath = "//*[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_EditCreditCardUC_CIDNumberContainer_TxtCID']")
    private WebElement cidFieldAfterEnterGC;

    @FindBy(xpath = "//div[@id='ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_PnlNoGiftCardApplied']")
    private WebElement section_giftCardApplied;

    @FindBy(xpath = "//div[@class='promo-confirm-wrapper']")
    private WebElement section_PromoApplied;

    private final String fld_firstNameJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_FirstNameContainer_TxtFirstName";
    private final String fld_lastNameJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_LastNameContainer_txtLastName";

    private final String fld_suiteNumberJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_SuiteContainer_TxtSuite";
    private final String fld_streetAddressJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_AddressLine1Container_TxtAddressLine1";
    private final String fld_cityJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_CityContainer_TxtCity";
    private final String fld_phone_0JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneContainer_TxtPhone";
    private final String fld_phone_1JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone1Container_TxtPhone1";
    private final String fld_phone_2JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone2Container_TxtPhone2";

    private final String fld_phone_3JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneExtContainer_TxtPhoneExt";
    private final String fld_other_phone_0JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOtherContainer_TxtPhoneOther";
    private final String fld_other_phone_1JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOther1Container_TxtPhoneOther1";
    private final String fld_other_phone_2JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOther2Container_TxtPhoneOther2";
    private final String fld_other_phone_3JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOtherExtContainer_TxtPhoneOtherExt";
    private final String btn_continuePaymentJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnContinueFromPayment";
    private final String btn_continueShippingJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_btnContinueFromShipping";

    private final String fld_billingAddressCityJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_CityContainer_TxtCity";
    private final String fld_billingAddressFirstNameJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_FirstNameContainer_TxtFirstName";
    private final String fld_billingAddressLastNameJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_LastNameContainer_txtLastName";
    private final String fld_billingAddressAddressJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_AddressLine1Container_TxtAddressLine1";
    private final String fld_billingAddressPostalCodeJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_PostalCodeContainer_TxtZipCode";
    private final String fld_billingAddressPhone_0JS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_PhoneContainer_TxtPhone";
    private final String fld_billingAddressPhone_1JS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_Phone1Container_TxtPhone1";
    private final String fld_billingAddressPhone_2JS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_Phone2Container_TxtPhone2";
    private final String fld_billingAddressPhone_3JS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_NewAddressUC_PhoneExtContainer_TxtPhoneExt";


    public CheckoutPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        } catch (Exception e) {
            logger.error("Checkout page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Checkout page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isCreditCardSelectionDisplayed() {

        return isDisplayed(form_selectPaymentMethod);

    }

    public boolean isReviewAndSubmitFormDisplayed() {

        return isDisplayed(form_reviewAndSubmit);

    }

    public boolean isDeliveryInformationCorrectlyDisplayed() {
        //TODO add more detail check. ex)name, address, item to be shipped and so on
        return isDisplayed(txt_shippingContent);
    }

    public boolean isPayPalDisplayedAsMethodOfPayment() {
        scrollToElement(lnk_EditPayPal);
        return isDisplayed(img_PayPal);

    }

    public void selectCreditCard() { click(buttonCreditCardSelect);}

    public boolean isPreOrderInfoDisplayed(final String releaseDate) {

        return isDisplayed(txt_preOrder) && getTextOrValue(txt_preOrderReleaseDate).contains(releaseDate);

    }

    public boolean isEnvHandlingFeeDisplayedInOrderSection() {

        //scrollToElement(labelEnvHandlingFeeInOrderSection);
        return isDisplayed(label_envHandlingFeeInOrderSection);

    }

    public void checkSameAsShipping() {

        //check(checkBoxSameAsShipping);
        scrollAndCheck(chk_boxSameAsShipping);

    }

    public void clickContinuePaymentButton() {

        clickByJSId(btn_continuePaymentJS);

    }

    public void clickSubmitOrderButton() {

        clickByJs(btn_submit);

    }

    public void clickContinueShippingButton() {

        //click(btn_continueShipping);
        clickByJSId(btn_continueShippingJS);

    }

    public void clickCreditCardSelectButton() {

        //click(buttonCreditCardSelect);
        clickByJs(btn_creditCardSelect);

    }

    public void fillInCreditCardInformation(final String cardType) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            select(btn_creditCardType, Payment.AMEX_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Payment.AMEX_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Payment.AMEX_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.AMEX_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.AMEX_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            select(btn_creditCardType, Payment.MC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Payment.MC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Payment.MC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.MC_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.MC_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            select(btn_creditCardType, Payment.PLCC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Payment.PLCC_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Payment.PLCC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.PLCC_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.PLCC_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            select(btn_creditCardType, Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, Payment.VISA2_CREDITCARD_NUMBER.toString());
            select(fld_creditCardExpiryDateMonth, Payment.VISA2_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.VISA2_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.VISA_CID.toString());

        }
    }

    public void fillInCreditCardInformation(final String cardType, final String creditCardNumber) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            select(btn_creditCardType, Payment.AMEX_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, creditCardNumber);
            select(fld_creditCardExpiryDateMonth, Payment.AMEX_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.AMEX_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.AMEX_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            select(btn_creditCardType, Payment.MC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, creditCardNumber);
            select(fld_creditCardExpiryDateMonth, Payment.MC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.MC_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.MC_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            select(btn_creditCardType, Payment.PLCC_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, creditCardNumber);
            select(fld_creditCardExpiryDateMonth, Payment.PLCC_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.PLCC_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.PLCC_CID.toString());

        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            select(btn_creditCardType, Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fld_creditCardNumber, creditCardNumber);
            select(fld_creditCardExpiryDateMonth, Payment.VISA2_EXPIRYDATE_MONTH.toString());
            select(fld_creditCardExpiryDateYear, Payment.VISA2_EXPIRYDATE_YEAR.toString());
            typeIn(fld_creditCardCVV, Payment.VISA_CID.toString());

        }
    }

    public void fillCCInformation(final String ccType, final String ccNumber, final String expiryMonth, final String
            expiryYear, final String cid) {
        select(btn_creditCardType, ccType);
        typeIn(fld_creditCardNumber, ccNumber);
        select(fld_creditCardExpiryDateMonth, expiryMonth);
        select(fld_creditCardExpiryDateYear, expiryYear);
        typeIn(fld_creditCardCVV, cid);
    }

    public void fillInCIDNumber(final String cardType) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            typeIn(fld_creditCardCID, Payment.AMEX_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            typeIn(fld_creditCardCID, Payment.MC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            typeIn(fld_creditCardCID, Payment.PLCC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            typeIn(fld_creditCardCID, Payment.VISA_CID.toString());
        }

    }

    public boolean isEditAddressButtonDisplayed() {
        return isDisplayed(btn_editAddressInfo);
    }

    public void clickButtonEditAddressInfo() {
        waitUntilPageLoaded();
        click(btn_editAddressInfo);
        waitUntilPageLoaded();
    }

    public void fillInShippingInformation() {
        typeIn(fld_postalCodeJS, TestUser.postalCodeBC);
        typeIn(fld_firstNameJS, TestUser.firstName);
        typeIn(fld_lastNameJS, TestUser.lastName);
        typeIn(fld_suiteNumberJS, TestUser.suiteName);
        typeIn(fld_streetAddressJS, TestUser.streetAddress);
        typeIn(fld_cityJS, TestUser.city);

        if (System.getProperty(Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(TestLanguage.FR.toString()) ||
                System.getProperty(Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(TestLanguage.FRENCH.toString())) {
            select(fld_province, AddressInformation.BC_PROVINCE_FR.toString());
        } else {
            select(fld_province, TestUser.province);
        }

        typeIn(fld_phone_0JS, TestUser.phone0);
        typeIn(fld_phone_1JS, TestUser.phone1);
        typeIn(fld_phone_2JS, TestUser.phone2);
        typeIn(fld_phone_3JS, TestUser.phone3);

        typeIn(fld_other_phone_0JS, TestUser.otherPhone0);
        typeIn(fld_other_phone_1JS, TestUser.otherPhone1);
        typeIn(fld_other_phone_2JS, TestUser.otherPhone2);
        typeIn(fld_other_phone_3JS, TestUser.otherPhone3);

    }

    public void editShippingInformation() {
        waitUntilPageLoaded();
        fld_firstName.click();
        fld_firstName.clear();
        typeIn(fld_firstName, TestUser.firstName);
        fld_lastName.click();
        fld_lastName.clear();
        typeIn(fld_lastNameJS, TestUser.lastName);
        fld_assressStreet.clear();
        typeIn(fld_streetAddressJS, TestUser.streetAddress);

    }

    public void fillInShippingInformationForYuKon() {
        typeIn(fld_postalCodeJS, TestUser.postalCodeBC);
        typeIn(fld_firstNameJS, TestUser.firstName);
        typeIn(fld_lastNameJS, TestUser.lastName);
        typeIn(fld_suiteNumberJS, TestUser.suiteName);
        typeIn(fld_streetAddressJS, TestUser.streetAddress);
        typeIn(fld_cityJS, TestUser.city);

        if (System.getProperty(Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(TestLanguage.FR.toString()) ||
                System.getProperty(Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(TestLanguage.FRENCH.toString())) {
            select(fld_province, AddressInformation.Yokun_PROVINCE_EN.toString());
        } else {
            select(fld_province, TestUser.provinceYukon);
        }

        typeIn(fld_phone_0JS, TestUser.phone0);
        typeIn(fld_phone_1JS, TestUser.phone1);
        typeIn(fld_phone_2JS, TestUser.phone2);
        typeIn(fld_phone_3JS, TestUser.phone3);

        typeIn(fld_other_phone_0JS, TestUser.otherPhone0);
        typeIn(fld_other_phone_1JS, TestUser.otherPhone1);
        typeIn(fld_other_phone_2JS, TestUser.otherPhone2);
        typeIn(fld_other_phone_3JS, TestUser.otherPhone3);

    }


    public void fillInConfirmationEmail() {

        typeIn(fld_confirmationEmail, TestUser.email);

    }

    public void fillInConfirmationGuestEmail() {

        typeIn(fld_confirmationEmail, TestUser.defaultGuestUser);

    }

    public void fillInRandomEmail() {
        String randomEmail = "bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com";
        TestUser.newEmail = randomEmail;
        typeIn(fld_confirmationEmail, TestUser.newEmail);

    }

    public void clickEditGCButton() {

        click(btn_editGiftCard);

    }

    public boolean isGiftCardSectionPresent() {

        return isDisplayed(giftCardSection);

    }

    public void clickGiftCardButton() {

        click(btn_giftCardExpand);

    }

    public void addGiftCard(final int i) {
        switch (i) {
            case 1:
                typeIn(fld_giftCardNumber, Payment.GIFTCARD1_NUMBER.toString());
                typeIn(fld_giftCardCID, Payment.GIFTCARD1_CID.toString());
                break;
            case 2:
                typeIn(fld_giftCardNumber, Payment.GIFTCARD2_NUMBER.toString());
                typeIn(fld_giftCardCID, Payment.GIFTCARD2_CID.toString());
                break;
            case 3:
                typeIn(fld_giftCardNumber, Payment.GIFTCARD3_NUMBER.toString());
                typeIn(fld_giftCardCID, Payment.GIFTCARD3_CID.toString());
                break;
            case 4:
                typeIn(fld_giftCardNumber, Payment.GIFTCARD4_NUMBER.toString());
                typeIn(fld_giftCardCID, Payment.GIFTCARD4_CID.toString());
                break;
            default:
                break;
        }
    }

    public void checkGiftCardBalance() {

        click(btn_checkGCBalance);

    }

    public boolean isGiftCardBalanceNotZero(){

        String textGcBalance = "//*[contains(@id,'_ucGiftCards_RptGiftCardEntry_ctl00_liGiftCard')]//div[contains(@class,'gift-card-balance')]" ;
        WebElement webElement = driver.findElement(By.xpath(textGcBalance));
        String actual = webElement.getText();
        return !actual.trim().equals("$0.00");
    }

    public void clickApplyGCToOrderButton(){

        scrollAndClick(btn_applyGCToOrder);

    }

    public void checkSameAsShippingAddress() {

        check(chk_boxSameAsShipping);

    }

    public String verifyErrorMessage() {

        return getTextOrValue(msg_error);

    }

    public boolean verifyCCAndBillingAddress() {

        return isDisplayed(ccAndBillingSection);

    }

    public String getCCMaskedNumber(){
        scrollToElement(txt_maskedCCNumber);
        return getTextOrValue(txt_maskedCCNumber);
    }

    public void clickEditCreditCard() {

        scrollAndClick(lnk_editCreditCard);

    }

    public void selectCreditCardFromDropDown(int index){
        scrollToElement(btn_creditCardDropDown);
        Select option = new Select(btn_creditCardDropDown);
        option.selectByIndex(index);

    }

    public String getSelectedOption(WebElement dpOption){
        Select select = new Select(dpOption);
        WebElement option = select.getFirstSelectedOption();
        return option.getText();
    }

    public String getSelectedCC() {

        return getSelectedOption(btn_creditCardDropDown);

    }

    public boolean isShippingAddressDisplayed() {
        return isDisplayed(txt_FullShippingAddress);
    }

    public String getShippingAddress() {

        return getTextOrValue(txt_shippingAddress);

    }

    public String getTxt_FullShippingAddress() {
        Util.waitForAjax(driver);
        return getTextOrValue(txt_FullShippingAddress);
    }

    public String getTxt_billingAddressFullName() {

        return getTextOrValue(txt_billingAddressFullName);
    }

    public String getBillingAddressStreetAddress() {

        return getTextOrValue(txt_billingStreetAddress);

    }

    public String getTxt_billingAddressProvincePC() {

        return getTextOrValue(txt_billingAddressProvincePC);

    }

    public String getTxt_billingAddressCountry() {

        return getTextOrValue(txt_billingAddressCountry);

    }

    public String getBillingAddressFirstPhone() {

        return getTextOrValue(txt_billingAddressPhone);

    }

    public String getDefaultShippingAddressDropDown() {

        return getSelectedOption(btn_shippingAddressDropDown);

    }

    public boolean isExpiryDateDisplayed() {

        return isDisplayed(txt_exipryDate);

    }

    public void clickBillingAddressEditButton() {

        clickByJs(btn_billingAddressEdit);

    }

    public void clickBillingAddressCancelButton() {

        clickByJs(btn_billingAddressCancel);

    }

    public boolean isGiftCardFinalBalancePresent() {

        return isDisplayed(txt_giftCardFinalBalance);
    }

    public void isExpandGCButtonDisplayed() {
        if (isDisplayed(btn_giftCardExpand)) {
            click(btn_giftCardExpand);
        }
    }

    public void enterCIDAfterEditCC() {
        click(fld_cidAfterEditCC);

        typeIn(fld_cidAfterEditCC, Payment.MC_CID.toString());
    }

    public void enterCIDAfterEditVisaCC() {
        click(fld_cidAfterEditCC);

        typeIn(fld_cidAfterEditCC, Payment.VISA_CID.toString());
    }

    public void enterVisaCIDAfterEditCC(String cidNumber) {

        typeIn(fld_VisaCidAfterEditCC, cidNumber);
    }

    public void selectCreditCardMethodOfPayment() {

        click(btn_selectCC);

    }

    public void clickChangePaymentType() {

        scrollAndClick(btn_changePaymentType);

    }

    public String getTextMaskedGiftCard() {

        return getTextOrValue(txt_maskedGiftCard);

    }

    public String getTextProductTotalValue() {

        return getTextOrValue(txt_productTotal);

    }

    public String getTextShippingDeliveryText() {

        return getTextOrValue(txt_shippingDelivery);

    }

    public String getTextShippingDeliveryValue() {

        return getTextOrValue(txt_shippingDeliveryValue);

    }

    public String getTextSubTotalValue() {

        return getTextOrValue(txt_subTotalValue);

    }

    public boolean isAnotherGiftCardLinkPresent() {

        return isDisplayed(lnk_addAnotherGiftCard);

    }

    public boolean isGiftCardTotalPresent() {
        scrollToElement(txt_giftCardTotal);

        return isDisplayed(txt_giftCardTotal);
    }

    public void clickPromotionalCodeButton() {

        click(btn_expandPromotionalCode);

    }

    public void addPromotionalCode(final int i) {
        switch (i) {
            case 1:
                typeIn(fld_promotionalCode, Payment.PROMOTION_CODE1.toString());
                break;
            case 2:
                typeIn(fld_promotionalCode, Payment.PROMOTION_CODE2.toString());
            default:
                break;
        }
    }

    public void clickApplyPromoCodeButton() {

        click(btn_applyPromoCode);

    }

    public boolean isPromoCodeApplied() {

        return isDisplayed(promoCodeAppliedSection);

    }

    public void clickUseANewAddressButton() {

        click(btn_useANewAddress);

    }

    public void addShippingAddress(String firstNameF, String lastNameF, String addressF, String CityF,
                                   String provinceF, String postalCodeF, String p0, String p1, String p2, String p3){
        typeIn(fld_firstNameJS, firstNameF);
        typeIn(fld_lastNameJS, lastNameF);
        typeIn(fld_streetAddressJS, addressF);
        typeIn(fld_cityJS, CityF);
        select(fld_province, provinceF);
        typeIn(fld_postalCodeJS, postalCodeF);
        typeIn(fld_phone_0JS, p0);
        typeIn(fld_phone_1JS, p1);
        typeIn(fld_phone_2JS, p2);
        typeIn(fld_phone_3JS, p3);
    }

    public void unCheckSaveAddress() {

        unCheck(chk_saveAddressCheckBox);

    }

    public void unCheckSetAsDefault() {

        unCheck(chk_setAsDefaultCheckBox);

    }

    public void clickNewShippingAddressDoneButton() {

        click(btn_newShippingAddressDone);

    }

    public void unCheckSaveCreditCardCheckBoxForPayment() {

        unCheck(chk_saveCreditCardCheckBoxForPayment);

    }

    public void unCheckSetAsDefaultCheckBoxForPayment() {

        unCheck(chk_setAsDefaultCheckBoxForPayment);

    }

    public void clickAddANewCreditCardButton() {

        click(btn_addANewCreditCardForPayment);

    }

    public void clickBillingAddressDoneButton() {

        click(btn_billingAddressDone);

    }

    public String errorMessageForAddingMoreCC() {

        return getTextOrValue(msg_errorMessageForAddingMoreCC);

    }

    public String textOfCountryFieldInNewAddress() {

        return getTextOrValue(fld_countryInNewAddress);

    }

    public String textOfCanadaBillingAddressCountryDropDown() {

        return getTextOrValue(txt_canadaBillingAddressCountryDropDown);

    }

    public String textOfUSBillingAddressCountryDropDown() {

        return getTextOrValue(btn_USBillingAddressCountryDropDown);

    }

    public void fillInBillingInformationForUS() {
        select(btn_billingAddressCountryDropDown, TestUser.countryUS);
        typeIn(fld_billingAddressPostalCodeJS, TestUser.defaultUSPostalCode);
        typeIn(fld_billingAddressFirstNameJS, TestUser.firstName);
        typeIn(fld_billingAddressLastNameJS, TestUser.lastName);
        typeIn(fld_suiteNumberJS, TestUser.suiteName);
        typeIn(fld_billingAddressAddressJS, TestUser.streetAddress);
        typeIn(fld_billingAddressCityJS, TestUser.defaultUSCity);
        select(fld_billingAddressProvince, TestUser.defaultUSProvince);
        typeIn(fld_billingAddressPhone_0JS, TestUser.phone0);
        typeIn(fld_billingAddressPhone_1JS, TestUser.phone1);
        typeIn(fld_billingAddressPhone_2JS, TestUser.phone2);
        typeIn(fld_billingAddressPhone_3JS, TestUser.phone3);
    }

    public void checkRadioButtonExpeditedDelivery() {

        check(btn_radioButtonExpeditedDelivery);

    }

    public void clickPayPalSelectButton() {

        click(btn_PayPalSelect);

    }

    public boolean isPayPalPaymentSelected() {
        Util.waitForAjax(driver);
        return isDisplayed(selectedPaypalOption);

    }

    public void clickContinueToPayPal() {

        click(btn_continueToPaypal);

    }

    public String errorMessageUnsuccessfulVBVOrMCSC() {

        return getTextOrValue(errorMessageUnsuccessfulVBV);

    }

    public void clickOnPaymentCartEditLinkForRewardZoneId() {

        click(paymentCartEditLinkForRewardZoneId);

    }

    public void enterRewardId() {

        typeIn(rewardZoneInputPaymentField, Payment.DEFAULT_REWARD_ZONE_ID.toString());

    }

    public void clickOnRewardZoneButton() {

        scrollAndClick(btn_checkoutPageRewardZoneApply);

    }

    public void clickAndEnterVisaCheckoutCIDNumber() {

        scrollAndClick(cidNumberAfterVisaCheckouttCidNumber);
        typeIn(cidNumberAfterVisaCheckouttCidNumber, Constants.Payment.VISA_CID.toString());
    }

    public void enterVBVCard(final String creditCardNumber) {
        if(creditCardNumber.equalsIgnoreCase("SuccessfulVBVAuthentication")) {
            fillInCreditCardInformation("visa", "4000000000000002");
        }
        else if(creditCardNumber.equalsIgnoreCase("UnableToAuthenticate")) {
            fillInCreditCardInformation("visa", "4000000000000036");
        }
        else if(creditCardNumber.equalsIgnoreCase("ErrorOnPaReq")) {
            fillInCreditCardInformation("visa", "4000000000000093");
        }
        else if(creditCardNumber.equalsIgnoreCase("RIBASuccessChallenge")) {
            fillInCreditCardInformation("visa", "4000010000000027");
        }
        else if(creditCardNumber.equalsIgnoreCase("UnsuccessfulVBVAuthentication")) {
            fillInCreditCardInformation("visa", "4000000000000010");
        }
        else if(creditCardNumber.equalsIgnoreCase("AuthenticationFailureVBVFailure")) {
            fillCCInformation("Visa", "4000000000000028", "10", "2029", "123");
        }
        else if(creditCardNumber.equalsIgnoreCase("RIBAFailureChallenge")) {
            fillInCreditCardInformation("visa", "4000010000000035");
        }
        else if(creditCardNumber.equalsIgnoreCase("AttemptsWithCAVVAndXID")) {
            fillInCreditCardInformation("visa", "4000000000000994");
        }
        else if(creditCardNumber.equalsIgnoreCase("CardHolderNotParticipating")) {
            fillInCreditCardInformation("visa", "4000000000000051");
        }
        else if(creditCardNumber.equalsIgnoreCase("UnableToAuthenticateTwo")) {
            fillInCreditCardInformation("visa", "4000000000000069");
        }
        else if(creditCardNumber.equalsIgnoreCase("MerchantNotActive")) {
            fillInCreditCardInformation("visa", "4000000000000077");
        }
        else if(creditCardNumber.equalsIgnoreCase("ErrorOnVeReqVeRes")) {
            fillInCreditCardInformation("visa", "4000000000000085");
        }
        else if(creditCardNumber.equalsIgnoreCase("TimeoutOnLookup")) {
            fillInCreditCardInformation("visa", "4000000000000044");
        }
        else if(creditCardNumber.equalsIgnoreCase("RIBAPassiveSuccess")) {
            fillInCreditCardInformation("visa", "4000020000000000");
        }
        else if(creditCardNumber.equalsIgnoreCase("RIBASuccess")) {
            fillInCreditCardInformation("visa", "4000010000000001");
        }
    }

    public void enterMCSecureCodeCard(final String creditCardNumber) {
        if (creditCardNumber.equalsIgnoreCase("MandatoryVerification")) {
            fillInCreditCardInformation("mc", "5192876244609236");
        } else if (creditCardNumber.equalsIgnoreCase("SCVAuthentication")) {
            fillInCreditCardInformation("mc", "5204740000001002");
        }else if (creditCardNumber.equalsIgnoreCase("StaffAccount")) {
            fillInCreditCardInformation("mc", "5299050250000019");
        }
    }

    public String lastFourDigitsOfVisaCardOnCheckoutPage() {
        System.out.println(getCCMaskedNumber().trim().substring(12,16));
        return getCCMaskedNumber().trim().substring(12,16) ;
    }

    public boolean isBlockEditShippingAddressPresent() {
        return isDisplayed(blockEditShippingAddress);
    }


    public void enterCreditCardDetailAfterEdit() {
        scrollToElement(ccType);

        selectByIndex(ccType,3);
        scrollAndClick(ccNumberEditCC);

        typeIn(ccNumberEditCC, Payment.AMEX_CREDITCARD_NUMBER.toString());
        select(ccExpMonthEditCC,Payment.AMEX_EXPIRYDATE_MONTH.toString());
        select(ccExpYearEditCC,Payment.AMEX_EXPIRYDATE_YEAR.toString());
        typeIn(cidNumberEditCC,Payment.AMEX_CID.toString());
    }

    public void updateExpDate() {
        scrollToElement(ccType);
        select(ccExpMonthEditCC,Payment.AMEX_EXPIRYDATE_MONTH2.toString());
        select(ccExpYearEditCC,Payment.AMEX_EXPIRYDATE_YEAR2.toString());
        typeIn(cidNumberEditCC,Payment.AMEX_CID.toString());
    }

    public void changeCreditCard() {select(selectCreditCardDropDown, "VISA ************1111");}

    public void enterCidNumberAfterAddingGC() {typeIn(cidFieldAfterEnterGC,Payment.VISA_CID.toString());}

    public boolean isDeliveryPromiseDisplayed() {
        return isDisplayed(txt_DeliveryPromiseMessage);
    }

    public boolean isDeliveryTypeDisplayed() {
        return isDisplayed(txt_DeliveryType);
    }

    public boolean isGiftCardSectionDisplayedInSubmitOrder() {
        return isDisplayed(section_giftCardApplied);
    }

    public boolean isPromoSectionDisplayedInSubmitOrder() {
        return isDisplayed(section_PromoApplied);
    }

}