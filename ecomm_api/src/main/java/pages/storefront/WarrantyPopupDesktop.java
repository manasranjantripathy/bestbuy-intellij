package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

/**
 * Created by sucho on 6/29/2017.
 */
public class WarrantyPopupDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private final static Logger logger = Logger.getLogger(WarrantyPopupDesktop.class);

    @FindBy(how = How.XPATH, using = "//*[@id='warranty-modal' and contains(@class,'active')]")
    private WebElement warrantyPopupHeader;

    @FindBy(how = How.XPATH, using = "//div[@class='btn warranty-opt-out-trigger']")
    private WebElement buttonNoThanks;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'warranty-add-trigger')]")
    private WebElement buttonYes;

    @FindBy(xpath = "//button[contains(@class, 'add-warranty')]")
    private WebElement buttonAddWarranty;

    @FindBy(id = "express-checkout")
    private WebElement buttonCheckout;

    @FindBy(how = How.ID, using = "accept-terms")
    private WebElement checkAcceptTerms;

    @FindBy(how = How.XPATH, using = "//*[@class='plans-trigger ui-dropdown-trigger']/span")
    private WebElement seeOtherOptions;

    @FindBy(xpath = "//a[@id='proceed-to-cart']")
    private WebElement linkViewCart;

    @FindBy(xpath = "//div[@class='terms-agreement']//div[@class='error']")
    private WebElement textErrorMessage;

    @FindBy(xpath = "//div[@class='confirmation-message']//span")
    private WebElement textConfirmationMessage;

    @FindBy(xpath = "//div[@id='warranty-modal']//div//div")
    private WebElement warrantyPopup;

    //    @FindBy(xpath = "//div[@id='express-checkout-modal']")
    @FindBy(xpath = "//div[@class='ui-modal-body']")
    private WebElement expressCheckoutPopup;

    @FindBy(xpath = "//*[@id='add-warranty']/div[@class='ui-dropdown-wrapper ui_dropdown']/a")
    private WebElement warrantySelector;

    @FindBy(xpath = "//*[@id='add-warranty']/section/div")
    private WebElement selectedWarrantyInfo;

    private WarrantyPopupDesktop() {

    }

    public WarrantyPopupDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isExpressCheckoutDisplayed(){
        wait.until(ExpectedConditions.elementToBeClickable(linkViewCart));
        return isDisplayed(linkViewCart);

    }

    public boolean isWarrantyDisplayed(){

        return isDisplayed(buttonNoThanks);

    }

    public boolean isErrorMessageDisplayed(){

        return isDisplayed(textErrorMessage);

    }

    public boolean isWarrantyConfirmationMsgDisplayed(){

        return isDisplayed(textConfirmationMessage);

    }

    public String getSelectedWarrantyName() {
        return getTextOrValue(selectedWarrantyInfo);
    }

    public void selectWarranty(final String warrantySku){

        WebElement warrantySelection = driver.findElement(By.xpath("//*[@id=\"add-warranty\"]/div[@class='ui-dropdown-wrapper ui_dropdown']/a/ul/li[@data-sku='"+ warrantySku +"']"));
        click(warrantySelector);
        click(warrantySelection);

    }

    public void clickCheckout(){

        click(buttonCheckout);

    }

    public void clickNoThanksButton() {

        click(buttonNoThanks);

    }

    public void clickViewCartLink(){
        wait.until(ExpectedConditions.visibilityOf(linkViewCart));
        clickByJs(linkViewCart);

    }

    public void clickAddWarrantyButton(){

        click(buttonAddWarranty);
    }

    public void checkWarrantyAgreement(){

        check(checkAcceptTerms);

    }

    public void addProtectionPlan(){

        check(checkAcceptTerms);

        click(buttonYes);

        waitUntilPageLoaded();

    }

    public void addProtectionPlan(final int years){

        click(seeOtherOptions);

        WebElement option;

        if(years == 2){
            option = driver.findElement(By.xpath("//li[contains(@class, 'warranty-option-item') and contains(text(), '2')]"));
            click(option);
        }
        else {
            option = driver.findElement(By.xpath("//li[contains(@class, 'warranty-option-item') and contains(text(), '3')]"));
            click(option);
        }

        check(checkAcceptTerms);

        click(buttonYes);

        waitUntilPageLoaded();
    }
}