package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

public class CheckOrderStatusPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/checkorderstatus.aspx";
    private final static Logger logger = Logger.getLogger(CheckOrderStatusPageDesktop.class);

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderStatus_BtnCheckOrderButton']")
    private WebElement btn_checkOrderStatus;

    @FindBy(xpath = "//a[@id='ctl00_CP_UcSignIn_BtnLoginButton']")
    private WebElement btn_signin;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcSignIn_UserNameContainer_txtUserName']")
    private WebElement fld_email;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcSignIn_PasswordContainer_txtPassword']")
    private WebElement fld_password;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderStatus_OrderNumberContainer_TxtOrderNumber']")
    private WebElement fld_orderNumber;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderStatus_FirstNameContainer_TxtFirstName']")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderStatus_LastNameContainer_TxtLastName']")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[@id='ctl00_CP_UcOrderStatus_PostalCodeContainer_TxtPostalCode']")
    private WebElement fld_billingPostalCode;

    @FindBy(xpath = "//div[contains(@id,'ctl00_CP_ErrorSummary')]/ul/li")
    private WebElement txt_errorTextForWrongPassword;

    public CheckOrderStatusPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Order History page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Order History page is not displayed " + e.getMessage(), e);
        }
    }

    public void signInForOrderHistory(){
        typeIn(fld_email, TestUser.email);
        typeIn(fld_password, TestUser.password);
        click(btn_signin);
    }

    public void signInWithWrongPassword(String wrongPassword){
        typeIn(fld_email, TestUser.newEmail);
        typeIn(fld_password, wrongPassword);
        click(btn_signin);
    }

    public String getErrorMessage() {

        return getTextOrValue(txt_errorTextForWrongPassword);

    }

    public void searchOrderNumber(final String orderNumber, final String firstName, final String lastName, final String postalCode) {
        typeIn(fld_orderNumber, orderNumber);
        typeIn(fld_firstName, firstName);
        typeIn(fld_lastName, lastName);
        typeIn(fld_billingPostalCode, postalCode);
        click(btn_checkOrderStatus);
    }

}