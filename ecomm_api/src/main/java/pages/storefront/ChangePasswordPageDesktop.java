package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;

import static utils.Constants.Messages;

public class ChangePasswordPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/changepassword.aspx?";

    private final static Logger logger = Logger.getLogger(ChangePasswordPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'_BtnChangePassword')]")
    private WebElement btn_submit;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_TxtCurrentPassword')]")
    private WebElement fld_oldPassword;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_txtNewPassword')]")
    private WebElement fld_newPassword;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_txtConfirmNewPassword')]")
    private WebElement fld_retypePassword;

    @FindBy(how = How.XPATH, using = "//span[contains(@id,'_ConfirmationMessage_ConfirmationMessage')] | //h1[@class='margin-s-left']")
    private WebElement msg_passwordChangeConfirmation;

    @FindBy(how = How.XPATH, using = "//div[contains(@id,'_vsumErrorSummary')]/UL")
    private WebElement msg_error;

    public ChangePasswordPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Change Password page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Change Password page is not displayed " + e.getMessage(), e);
        }
    }

    public void enterOldPasswordField(final String input) {

        typeIn(fld_oldPassword, input);

    }

    public void enterNewPasswordInOldPasswordField(String password) {

        typeIn(fld_oldPassword, password);

    }

    public void enterNewPasswordFiled(String password) {

        typeIn(fld_newPassword, password);

    }

    public void enterRetypePasswordFiled(String password) {

        typeIn(fld_retypePassword, password);

    }

    public void clickOnSubmitButton() {

        click(btn_submit);

    }

    public boolean checkStateDisplayWithCheckPoint() {
        int result;
        result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[@class='ng-binding false']")).size();
        return (result > 0);

    }

    public boolean checkStrongPasswordStrength() {
        int result;
        result = driver.findElements(By.xpath("//*[@class='score-bar strong']")).size();
        return (result > 0);

    }

    public boolean isCheckBoxChecked() {
        int result;
        result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'checkmark')]")).size();
        return (result > 0);

    }

    public boolean  checkAcceptablePassword() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='score-bar acceptable']")).size();
        return (result > 0);

    }

    public boolean  checkAcceptableEnterAPasswordText() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding acceptable']")).size();
        return (result > 0);

    }

    public String getPasswordChangedConfirmationMessage() {

        return getTextOrValue(msg_passwordChangeConfirmation);

    }

    public String getErrorMessageText() {

        return getTextOrValue(msg_error);

    }

    public String getTextMessage(String errorMessage){
        if (errorMessage.equalsIgnoreCase("password")) {
            errorMessage = Constants.Messages.ERR_MSG_MISSING_PASSWORD.toString();
        }
        else if (errorMessage.equalsIgnoreCase("reTypePassword")) {
            errorMessage = Messages.ERR_MSG_MISSING_RETYPE_PASSWORD.toString();
        }
        else if (errorMessage.equalsIgnoreCase("samePassword")) {
            errorMessage = Messages.ERR_MSG_SAME_PASSWORD.toString();
        }
        else if (errorMessage.equalsIgnoreCase("misMatch")) {
            errorMessage = Messages.ERR_MSG_MIS_MATCH.toString();
        }
        else if (errorMessage.equalsIgnoreCase("oldPassword")){
            errorMessage = Messages.ERR_MSG_OLD_PASSWORD.toString();
        }
        return errorMessage;
    }

}