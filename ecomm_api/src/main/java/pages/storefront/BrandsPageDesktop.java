package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class BrandsPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "brands.aspx";
    private final static Logger logger = Logger.getLogger(BrandsPageDesktop.class);

    @FindBy(how = How.XPATH, using = "//input[@class='search-text ng-pristine ng-invalid ng-invalid-required']")
    private WebElement fld_searchBox;

    @FindBy(how = How.XPATH, using = "//li[contains(@class,'tier')]/a[contains(@class,'brand-logo')]")
    private WebElement lnk_searchedBrandLogo;

    private String searchedBrandLogoBegin = "//li[contains(@style,'";
    private String searchedBrandLogoEnd = "')]";


    public BrandsPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Brands page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Brands page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAt(final String brandName) {
        try {
            waitUntilPageLoaded();
            logger.info("driver.getCurrentUrl()= " + driver.getCurrentUrl().toLowerCase());
            logger.info("brandName= " + brandName);
            return driver.getCurrentUrl().toLowerCase().contains(brandName);
        }
        catch(Exception e){
            logger.error("The brand page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("The brand page is not displayed " + e.getMessage(), e);
        }
    }

    public void searchForBrand(final String brand) {
        driver.navigate().refresh();
        typeInHitEnter(fld_searchBox, brand);

    }

    public void clickOnSearchedBrandLogo() {
        clickByJs(lnk_searchedBrandLogo);

    }

    public boolean isTheLogoDisplayed(final String brand) {
        String element= searchedBrandLogoBegin + brand + searchedBrandLogoEnd;
        logger.info("element= " + element);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
        return driver.findElement(By.xpath(element)).isDisplayed();

    }

}