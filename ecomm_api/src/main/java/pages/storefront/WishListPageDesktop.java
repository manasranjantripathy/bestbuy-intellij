package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class WishListPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/orderwishlist.aspx";
    private final static Logger logger = Logger.getLogger(WishListPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_UcWishListControl_ucSkuListingList_SL_ctl00_ctl04_PL_SN")
    private WebElement productTitle;

    @FindBy(how = How.ID, using = "ctl00_CP_UcWishListControl_ucSkuListingList_SL_ctl00_ctl04_HypWishListRemove")
    private WebElement remove;

    public WishListPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Wish List page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Wish List page is not displayed " + e.getMessage(), e);
        }
    }

    public String verifyAddedProduct(){
        return getTextOrValue(productTitle);
    }

    public void removeProduct(){
        click(remove);
    }

    public boolean isProductDisplayed(){
        return isDisplayed(productTitle);
    }

}