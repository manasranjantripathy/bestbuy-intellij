package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

/**
 * Created by sucho on 9/5/2017.
 */
public class AgeVerificationPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/ageverification.aspx";
    private final static Logger logger = Logger.getLogger(AgeVerificationPageDesktop.class);

    @FindBy(id = "ctl00_CP_BtnReturnToCarts")
    private WebElement btn_returnToCart;

    @FindBy(id = "ctl00_CP_BtnAgree")
    private WebElement btn_IAgree;

    private AgeVerificationPageDesktop(){

    }

    public AgeVerificationPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Age verification page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Age verification page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickReturnToCartButton(){

        click(btn_returnToCart);

    }

    public void clickIAgreeButton(){

        click(btn_IAgree);

    }
}