package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants.Payment;

public class CheckGiftCardBalancePageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/checkgiftcardbalance.aspx";
    private final static Logger logger = Logger.getLogger(CheckGiftCardBalancePageDesktop.class);

    @FindBy(xpath = "//input[@id='ctl00_CP_checkGiftCardDetail1_giftCard1_TxtGiftCardNumberContainer_TxtGiftCardNumber']")
    private WebElement fld_giftCardNumber;

    @FindBy(xpath = "//input[@id='ctl00_CP_checkGiftCardDetail1_giftCard1_securityCodeContainer_TxtSecurityCode']")
    private WebElement fld_securityCode;

    @FindBy(xpath = "//a[@id='ctl00_CP_checkGiftCardDetail1_checkBalanceButton']")
    private WebElement btn_checkBalance;

    public CheckGiftCardBalancePageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Check Gift Card Balance page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Check Gift Card Balance page is not displayed " + e.getMessage(), e);
        }
    }

    public void checkGiftCardBalance(){
        typeIn(fld_giftCardNumber, Payment.GIFTCARD1_NUMBER.toString());
        typeIn(fld_securityCode, Payment.GIFTCARD1_CID.toString());
        click(btn_checkBalance);
    }
}