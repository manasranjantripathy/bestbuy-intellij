package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 7/18/2017.
 */
public class CreateAccountPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/createaccount.aspx";
    private final static Logger logger = Logger.getLogger(CreateAccountPageDesktop.class);

    @FindBy(how = How.ID, using = "ctl00_CP_btnSubmit")
    private WebElement btn_submitAccountInfo;

    @FindBy(how = How.ID, using = "ctl00_CP_BtnSave")
    private WebElement btn_submitAddress;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_EmailNotificationUC_repEmailNotificationOptions_ctl00_chkEmailNotificationOption")
    private WebElement chk_emailNotification;

    @FindBy(how = How.XPATH, using = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement chk_weatherEmailIsAlreadyUsed;

    @FindBy(how = How.ID, using = "_ConfirmationMessage")
    private WebElement chk_weatherAccountIsCreated;

    @FindBy(how = How.XPATH, using = "//div[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul")
    private WebElement fld_errorMessage;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_FirstNameContainer_txtFirstName")
    private WebElement fld_firstName;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_LastNameContainer_txtLastName")
    private WebElement fld_lastName;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_PostalCodeContainer_txtPostalCode")
    private WebElement fld_postalCode;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_EmailContainer_txtEmail")
    private WebElement fld_email;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_ConfirmEmailContainer_txtConfirmEmail")
    private WebElement fld_retypeEmail;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_PasswordContainer_txtPassword")
    private WebElement fld_password;

    @FindBy(how = How.ID, using = "ctl00_CP_registrationForm_ConfirmPasswordContainer_txtConfirmPassword")
    private WebElement fld_retypePassword;

    @FindBy(how = How.ID, using = "_ConfirmationMessage")
    private WebElement txt_accountCreated;

    @FindBy(how = How.ID, using = "_vsumErrorSummary")
    private WebElement txt_createAccountErrMsg;

    @FindBy(how = How.ID, using = "ctl00_CP_ErrorSummaryUC_vsumErrorSummary")
    private WebElement txt_errorCodeForWeatherEmailIsAlreadyUsed;

    @FindBy(how = How.XPATH, using = "//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'false')]")
    private WebElement blockCheckPointWithBullet;

    private CreateAccountPageDesktop() {

    }

    public CreateAccountPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        } catch (Exception e) {
            logger.error("Create account page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Create account page is not displayed " + e.getMessage(), e);
        }
    }

    public void createRandomAccount() {
        String randomEmail = "bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com";
        TestUser.email = randomEmail;

        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_email, TestUser.email);
        typeIn(fld_retypeEmail, TestUser.email);

        TestUser.newEmail = randomEmail;

        typeIn(fld_password, TestUser.password);
        typeIn(fld_retypePassword, TestUser.password);
        typeIn(fld_postalCode, TestUser.postalCodeBC);

        click(chk_emailNotification);

        click(btn_submitAccountInfo);

    }

    public void entersDefaultFirstName() {

        typeIn(fld_firstName, TestUser.firstName);
    }

    public void entersDefaultLastName() {

        typeIn(fld_lastName, TestUser.lastName);
    }

    public void entersNewEmail() {

        typeIn(fld_email, TestUser.email);
        typeIn(fld_retypeEmail, TestUser.email);
    }

    public void entersNewAccountPassword(String password) {
        typeIn(fld_password, password);

    }

    public String errorMessageForBankField() {
        return getTextOrValue(fld_errorMessage);
    }

    public void entersWeekPassword(String password) {
        typeIn(fld_password, password);
    }

    public void enterPassword(String password) {
        typeIn(fld_password, password);
    }

    public void clickOnPasswordField() {
        click(fld_password);
    }

    public boolean checkStateDisplayWithCheckPoint() {
        int result;
        result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'false')]")).size();
        return (result > 0 ? true : false);
    }

    public boolean checkStrongPasswordStrength() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='score-bar strong']")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkStrongPasswordText() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding strong']")).size();
        return (result > 0 ? true : false);

    }

    public boolean isCheckBoxChecked() {
        int result;
        result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'checkmark')]")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkPasswordStrengthVariation(final String input) {
        int result = 0;
        if (input.equals("")) {
            result = driver.findElements(By.xpath("//div[@class='score-bar']")).size();
        }
        if (input.equals("123456")) {
            result = driver.findElements(By.xpath("//div[@class='score-bar weak']")).size();
        }
        if (input.equals("143bestbuy")) {
            result = driver.findElements(By.xpath("//div[@class='score-bar strong']")).size();
        }
        return (result > 0 ? true : false);
    }

    public boolean checkPasswordText(final String input) {
        int result = 0;
        if (input.equals("")) {
            result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding']")).size();
        }
        if (input.equals("123456")) {
            result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding weak']")).size();
        }
        if (input.equals("143bestbuy")) {
            result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding strong']")).size();
        }
        return (result > 0 ? true : false);
    }

    public void setBtn_submitAccountInfo(WebElement btn_submitAccountInfo) {
        this.btn_submitAccountInfo = btn_submitAccountInfo;
    }

    public void cleanPasswordField() {
        WebElement element = driver.findElement(By.id("ctl00_CP_registrationForm_PasswordContainer_txtPassword"));
        waitUntilPageLoaded(1);
        element.clear();
    }


    public boolean checkPasswordStrengthGreyedOut() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='score-bar']")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkEnterAPasswordText() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding']")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkWeakPassword() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='score-bar weak']")).size();
        return (result > 0 ? true : false);

    }

    public boolean checkWeakEnterAPasswordText() {
        int result;
        result = driver.findElements(By.xpath("//div[@class='password-strength-text ng-binding weak']")).size();
        return (result > 0 ? true : false);
    }

    public boolean checkPaswordToolTipCheckStates(final String input) {
        int result = 0;
        if (input.equals("")) {
            result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'false')]")).size();
        }
        if (input.equals("143bestbuy")) {
            result = driver.findElements(By.xpath("//ul[@class='password-requirement-wrapper colour-dark-grey']/li[contains(@class,'checkmark')]")).size();
        }
        return (result > 0 ? true : false);

    }


    public void createAccountExistingEmail(String uppercase) {



        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_email, TestUser.newEmail);

        if (uppercase.equalsIgnoreCase("true")) {
            typeIn(fld_retypeEmail, (TestUser.newEmail).toUpperCase());
        } else {
            typeIn(fld_retypeEmail, TestUser.newEmail);
        }
        typeIn(fld_retypeEmail, TestUser.newEmail);

        typeIn(fld_password, TestUser.password);
        typeIn(fld_retypePassword, TestUser.password);
        typeIn(fld_postalCode, TestUser.postalCodeBC);

        unCheck(chk_emailNotification);

        click(btn_submitAccountInfo);

    }

    public String getErrorMessageForDuplicateEmail() {
        return getTextOrValue(chk_weatherEmailIsAlreadyUsed);
    }

    public void createRandomAccountWithPostalCodeParameter(String postalCode) {

        String randomEmail = "bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com";

        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_email, TestUser.email);
        typeIn(fld_retypeEmail, TestUser.email);

        TestUser.newEmail = randomEmail;

        typeIn(fld_password, TestUser.password);
        typeIn(fld_retypePassword, TestUser.password);
        typeIn(fld_postalCode, postalCode);

        unCheck(chk_emailNotification);

        click(btn_submitAccountInfo);

    }

    public void createRandomAccountWithPassingParameters(String email, String reTypeEmail, String password, String reTypePassword) {

        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_email, email);
        typeIn(fld_retypeEmail, reTypeEmail);
        typeIn(fld_password, password);
        typeIn(fld_retypePassword, reTypePassword);
        typeIn(fld_postalCode, TestUser.postalCodeBC);

        unCheck(chk_emailNotification);

        click(btn_submitAccountInfo);
    }

    public void fillCreateAccountFields() {



        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_password, TestUser.password);
        typeIn(fld_retypePassword, TestUser.password);
        typeIn(fld_postalCode, TestUser.postalCodeBC);
        unCheck(chk_emailNotification);
    }

    public void enterEmailField(String email) {
        fld_email.sendKeys(email);
    }

    public void enterReTypeEmailField(String email) {
        fld_retypeEmail.sendKeys(email);
    }

    public void createAccountWithPassingParameters(String firstName, String lastName, String postalCode) {

        String randomEmail = "bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com";

        typeIn(fld_firstName, firstName);
        typeIn(fld_lastName, lastName);
        typeIn(fld_email, TestUser.email);
        typeIn(fld_retypeEmail, TestUser.email);

        TestUser.newEmail = randomEmail;

        typeIn(fld_password, TestUser.password);
        typeIn(fld_retypePassword, TestUser.password);
        typeIn(fld_postalCode, postalCode);

        unCheck(chk_emailNotification);

        click(btn_submitAccountInfo);

    }


    public String getTextMessage(String errorMessage) {

        if (errorMessage.equalsIgnoreCase("differCase")) {

            errorMessage = Constants.Messages.ERR_MSG_MIS_MATCH_EMAIL.toString();
        } else if (errorMessage.equalsIgnoreCase("wrongFormat")) {

            errorMessage = Constants.Messages.ERR_MSG_EMAIL_FORMAT.toString();
        } else if (errorMessage.equalsIgnoreCase("misMatchPassword")) {

            errorMessage = Constants.Messages.ERR_MSG_MIS_MATCH.toString();
        } else if (errorMessage.equalsIgnoreCase("missingFirstName")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_FIRST_NAME.toString();
        } else if (errorMessage.equalsIgnoreCase("missingLastName")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_LAST_NAME.toString();
        } else if (errorMessage.equalsIgnoreCase("missingPostalCode")) {

            errorMessage = Constants.Messages.ERR_MSG_MISSING_CC_POSTAL_CODE.toString();
        } else if (errorMessage.equalsIgnoreCase("notRecognizedPostalCode")) {

            errorMessage = Constants.Messages.ERR_MSG_POSTAL_CODE_NOT_RECOGNIZED.toString();
        } else if (errorMessage.equalsIgnoreCase("blackListPassword")) {

            errorMessage = Constants.Messages.ERR_MSG_SIMPLE_PASSWORD.toString();
        }

        return errorMessage;
    }

    public void clickSubmit() {
        click(btn_submitAccountInfo);
    }

    public String getTextFromEmailField() {
        return getTextOrValue(fld_email);
    }

    public String getTextFromReTypeEmailField() {
        return getTextOrValue(fld_retypeEmail);
    }


}