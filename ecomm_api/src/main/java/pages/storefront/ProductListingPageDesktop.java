package pages.storefront;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants.Sku;
import utils.Util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by sucho on 9/1/2017.
 */
public class ProductListingPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "search/searchresults.aspx";
    private final static Logger logger = Logger.getLogger(ProductListingPageDesktop.class);

    @FindBy(xpath = "//li[contains(@class, 'search-product')]")
    private WebElement tabProducts;

    @FindBy(xpath = "//li[contains(@class, 'search-article')]")
    private WebElement tabArticles;

    @FindBy(xpath = "//li[contains(@class, 'search-forum')]")
    private WebElement tabForum;

    @FindBy(xpath = "//li[contains(@class, 'search-help')]")
    private WebElement tabHelpTopics;

    @FindBy(xpath = "//*[@id='bby-content'] //li[contains(@class,'breadcrumb-current')]")
    private WebElement plpTitle;

    @FindBy(xpath = "//ul[@class='facet-list facet-brandName-list']/li[@class='see-all filter-expand-trigger']")
    private WebElement link_SeeAllBrandFacet;

    @FindBy(xpath = "//ul[@class='facet-list facet-brandName-list facet-expanded']")
    private WebElement expandedBrandFacet;

    @FindBy(xpath = "//ul[@class= 'facet-list facet-brandName-list']//li[1]//span[@class='item']")
    private WebElement chkBox_BrandCategorySelection;

    @FindBy(xpath = "//section[@class='facet-section facet-brandName']//li[1]")
    private WebElement txt_selectedFirstBrand;

    @FindBy(xpath = "//li[@class='breadcrumb-current']/span")
    private WebElement txt_CurrentBreadCrumb;

    @FindBy(xpath = "//div[contains(@class, 'grid-view')]")
    private WebElement view_Grid;

    @FindBy(xpath = "//div[contains(@class, 'list-view')]")
    private WebElement view_List;

    @FindBy(xpath = "//div[@id='SearchArticleListing']//ul[@class='listing-items clearfix']")
    private WebElement view_List_Articles;

    @FindBy(xpath = "//div[@id='SearchHelpListing']//ul[@class='listing-items clearfix']")
    private WebElement view_List_HelpTopics;

    @FindBy(xpath = "//li[@id='ctl00_CC_ProductSearchResultListing_LayoutListViewItem']")
    private WebElement icon_ListView;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[1]/li[1]/div/div[1]/div/label/span")
    private WebElement chkBox_FirstCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[1]/li[2]/div/div[1]/div/label/span")
    private WebElement chkBox_SecondCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[1]/li[3]/div/div[1]/div/label/span")
    private WebElement chkBox_ThirdCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[1]/li[4]/div/div[1]/div/label/span")
    private WebElement chkBox_FourthCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[2]/li[1]/div/div[1]/div/label/span")
    private WebElement chkBox_FifthCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[2]/li[2]/div/div[1]/div/label/span")
    private WebElement chkBox_SixthCompare;

    @FindBy(xpath = "//*[@id='ctl00_CC_ProductSearchResultListing_SearchProductListing']/ul/div[contains(@class, 'listing-row compare-checked')]/li[1]/div/div[1]/div/a")
    private WebElement btn_ComapareSelections;

    @FindBy(xpath = "//h1[@id='ctl00_CC_SearchHeading']")
    private WebElement txt_ResultsForSearchKeyword;

    @FindBy(xpath = "//*[@class='rating-score font-xs colour-dark-grey inline-block margin-right-one']")
    private WebElement textRatingsCount;

    @FindBy(xpath = "//*[@class='rating-num']")
    private WebElement textReviewsCount;

    private ProductListingPageDesktop() {

    }

    public ProductListingPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Product Listing page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Product Listing page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickSearchTab(String tab) {
        if (tab.equalsIgnoreCase("products")) {
            click(tabProducts);
        } else if (tab.equalsIgnoreCase("articles")) {
            click(tabArticles);
        } else if (tab.equalsIgnoreCase("helpTopics")) {
            click(tabHelpTopics);
        }
    }

    public boolean isAtTab(String tab) {
        if (tab.equalsIgnoreCase("products")) {
            return driver.getCurrentUrl().contains("product") ? true : false;
        } else if (tab.equalsIgnoreCase("articles")) {
            return driver.getCurrentUrl().contains("article") ? true : false;
        } else if (tab.equalsIgnoreCase("helpTopics")) {
            return driver.getCurrentUrl().contains("help") ? true : false;
        }
        return false;
    }

    public boolean isSkuDisplayed(final Sku sku){
        String skuLocator = String.format("//div[@class='plp-background has-sticky-nav']//li[@data-sku='%s']", sku.toString());

        return isDisplayed(skuLocator);

    }

    public String getPlpTitle() {
        return getTextOrValue(plpTitle);
    }

    public void getAvailableOnlineItem(){
        try {
            String pathToAvailableOnlineItem = "//li[(@class='availability-status available') and (contains(text(), 'Available online'))]//ancestor::div[@class='prod-info']//h4//a";

            List<WebElement> listOfItems = Lists.newLinkedList(driver.findElements(By.xpath(pathToAvailableOnlineItem)));
            Iterator<WebElement> iterator = listOfItems.iterator();
            while (iterator.hasNext()) {
                if (driver.getCurrentUrl().toLowerCase().contains("search/searchresults.aspx")) {
                    scrollAndClick(iterator.next());
                    break;
                }
            }
            logger.warn("There is no available online item in PLP page. Using smoketest sku(10204251) instead");
            new HeaderFooterDesktop(driver).searchFor(Sku.smoketestSku);
        }
        catch (Exception e){
            logger.error("Unknown error happened.");
            throw new RuntimeException("Unknown error happened.", e);
        }
    }

    public void getAvailableAtNearbyStoresItem(){
        try {
            String pathToAvailableAtNearByStoresItem = "//li[(@class='availability-status available') and (contains(text(), 'Available at nearby stores'))]//ancestor::div[@class='prod-info']//h4//a";

            LinkedList<WebElement> listOfItems = new LinkedList(driver.findElements(By.xpath(pathToAvailableAtNearByStoresItem)));
            while (listOfItems.size() > 0) {
                if (driver.getCurrentUrl().toLowerCase().contains("search/searchresults.aspx")) {
                    WebElement element = listOfItems.poll();
                    scrollAndClick(element);
                    break;
                }
            }
            logger.warn("There is no available at nearby store item in PLP page. Using smoketest(10204251) sku instead");
            new HeaderFooterDesktop(driver).searchFor(Sku.smoketestSku);
        }
        catch (Exception e){
            logger.error("Unknown error happened.");
            throw new RuntimeException("Unknown error happened.", e);
        }
    }

    public void clickSeeAllLinkOfBrandFacet() { click(link_SeeAllBrandFacet); }

    public boolean isExpandedListOfBrandsDisplayed() { return isDisplayed(expandedBrandFacet); }

    public void setChkBox_BrandCategorySelection() { click(chkBox_BrandCategorySelection); }

    public String getTxt_SelectedBrandInCategory() { return getTextOrValue(chkBox_BrandCategorySelection); }

    public boolean isSelectedBrandDisplayedInBreadCrumb() {
        waitUntilPageLoaded(5);
        return isDisplayed(txt_CurrentBreadCrumb); }

    public String getTxt_CurrentDisplayedBreadCrumb() { return getTextOrValue(txt_CurrentBreadCrumb); }

    public boolean isGridViewDisplayed() { return isDisplayed(view_Grid); }

    public void clickListView(String tab) {
        if(tab.equalsIgnoreCase("products")) {
            click(icon_ListView);
        }else if(tab.equalsIgnoreCase("articles") | (tab.equalsIgnoreCase("helpTopics"))) {

        }
    }

    public boolean isListViewDisplayed(String tab) {
        Util.waitForAjax(driver);
        boolean listView = true;
        if(tab.equalsIgnoreCase("products")) {
            listView = isDisplayed(view_List);

        } else if(tab.equalsIgnoreCase("articles")) {
            listView = isDisplayed(view_List_Articles);

        } else if(tab.equalsIgnoreCase("helpTopics")) {
            listView = isDisplayed(view_List_HelpTopics);
        }
        return listView;
    }

    public boolean verifyViewType(String tab, final String view) {
        boolean viewType = true;
        if(view.equalsIgnoreCase("grid")) {
            viewType = isGridViewDisplayed();
        } else if(view.equalsIgnoreCase("list")) {
            clickListView(tab);
            viewType = isListViewDisplayed(tab);
        }
        return viewType;
    }

    public void clickCompareCheckBox(int checkBox) {
        if( checkBox == 1) {
            click(chkBox_FirstCompare);
        } else if( checkBox == 2) {
            click(chkBox_FirstCompare);
            click(chkBox_SecondCompare);
        } else if( checkBox == 3) {
            click(chkBox_FirstCompare);
            click(chkBox_SecondCompare);
            click(chkBox_ThirdCompare);
        } else if( checkBox == 4) {
            click(chkBox_FirstCompare);
            click(chkBox_SecondCompare);
            click(chkBox_ThirdCompare);
            click(chkBox_FourthCompare);
        } else if( checkBox == 5) {
            click(chkBox_FirstCompare);
            click(chkBox_SecondCompare);
            click(chkBox_ThirdCompare);
            click(chkBox_FourthCompare);
            click(chkBox_FifthCompare);
        } else if( checkBox == 6) {
            click(chkBox_FirstCompare);
            click(chkBox_SecondCompare);
            click(chkBox_ThirdCompare);
            click(chkBox_FourthCompare);
            click(chkBox_FifthCompare);
            click(chkBox_SixthCompare);
        }
    }

    public boolean isCompareSelectionsBtnDisplayed() { return isDisplayed(btn_ComapareSelections); }

    public void clickCompareSelections() { clickByJs(btn_ComapareSelections); }

    public String getTxt_ResultsForSearchKeyword() { return getTextOrValue(txt_ResultsForSearchKeyword); }

    public boolean isRatingCountDisplayed(){return isDisplayed(textRatingsCount);}

    public boolean isReviewsCountDisplayed() {return isDisplayed(textReviewsCount);}
}