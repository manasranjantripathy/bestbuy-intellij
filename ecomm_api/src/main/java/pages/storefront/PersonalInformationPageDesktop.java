package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

public class PersonalInformationPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "profile/updateaccount.aspx";
    private final static Logger logger = Logger.getLogger(PersonalInformationPageDesktop.class);

    @FindBy(xpath="//a[@id='ctl00_CP_BtnLoginButton']")
    private WebElement btn_submit;

    @FindBy(xpath = "//input[@id='ctl00_CP_registrationForm_FirstNameContainer_txtFirstName']")
    private WebElement fld_firstName;

    @FindBy(xpath = "//input[@id='ctl00_CP_registrationForm_LastNameContainer_txtLastName']")
    private WebElement fld_lastName;

    @FindBy(xpath = "//input[@id='ctl00_CP_registrationForm_PostalCodeContainer_txtPostalCode']")
    private WebElement fld_postalCode;

    public PersonalInformationPageDesktop(final WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);

        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Personal Information page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Personal Information page is not displayed " + e.getMessage(), e);
        }
    }

    public void fillMandatoryDetails(){
        typeIn(fld_firstName, TestUser.firstName);
        typeIn(fld_lastName, TestUser.lastName);
        typeIn(fld_postalCode, TestUser.postalCodeBC);
        click(btn_submit);
    }
}