package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.Constants;
import utils.Constants.Sku;
import utils.TestUser;
import utils.Util;

import java.util.List;

/**
 * Created by sucho on 6/29/2017.
 */
public class HeaderFooterDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private final static Logger logger = Logger.getLogger(HeaderFooterDesktop.class);
    private static final String Expected_MasterCard_Help_Url = "en-ca/help/what-is-mastercard-securecode/hc8350.aspx";
    private static final String Expected_Verified_By_Visa_Help_Url = "en-ca/help/what-is-verified-by-visa/hc1125.aspx";
    private static final String Expected_PayPal_Help_Url = "en-ca/help/paypal-information/hc8296.aspx";

    @FindBy(how = How.XPATH, using = "//i[@class='icon-search']")
    private WebElement btn_search;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignIn")
    private WebElement btn_SignIn;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignOut")
    private WebElement lnk_signOut;

    @FindBy(how = How.ID, using = "my-account-link")
    private WebElement lnk_myAccount;

    @FindBy(how = How.ID, using = "my-account-dropdown")
    private WebElement lnk_myAccountDropDown;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Shop Gift Cards')]")
    private WebElement lnk_giftCard;

    @FindBy(how = How.ID, using = "utility-toolbar-create-account")
    private WebElement lnk_createAccount;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_linkFindAStore")
    private WebElement lnk_findAStore;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_linkOrderStatus")
    private WebElement lnk_orderStatus;

    @FindBy(xpath = "//a[contains(@id, 'ctl00_MasterHeader_ctl00_uchead_languageToggle')]")
    private WebElement lnk_language;

    @FindBy(how = How.ID, using  = "services-menu-link")
    private WebElement lnk_service;

    @FindBy(how = How.XPATH, using  = "//a[@class='services-plans']")
    private WebElement lnk_registerYourPlan;

    @FindBy(xpath = "//a[text()='Site Map']")
    private WebElement lnk_siteMap;

    @FindBy(xpath = "//a[text()='Pressroom']")
    private WebElement lnk_pressRoom;

    @FindBy(xpath = "//a[@id='ctl00_MasterHeader_ctl00_uchead_linkHelpCentre']")
    private WebElement lnk_helpCentre;

    @FindBy(xpath = "//ul[@id='my-account-dropdown']/li[4]/a")
    private WebElement lnk_myBestBuyCard;

    @FindBy(xpath = "//ul[@id='my-account-dropdown']/li[3]/a")
    private WebElement lnk_myOrder;

    @FindBy(xpath = "//a[@id='ctl00_MasterHeader_ctl00_uchead_languageToggle_english']")
    private WebElement lnk_english;

    @FindBy(xpath = "//a[@id='ctl00_MasterHeader_ctl00_uchead_languageToggle_french']")
    private WebElement lnk_french;

    @FindBy(how = How.XPATH, using = "//a[@class='link margin-bottom-two margin-left-two font-xs']")
    private WebElement lnk_shopAllBrands;

    @FindBy(id = "FirstNameLabel")
    private WebElement label_firstName;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_HypHeaderLogo")
    private WebElement logo;

    @FindBy(xpath = "//i[@class = 'icon-cart-icon']")
    private WebElement icn_cart;

    @FindBy(how = How.XPATH, using = "//input[@id='ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword']")
    private WebElement searchBox;

    @FindBy(how = How.ID, using = "brands-menu-link")
    private WebElement brands;

    @FindBy(xpath = "//ul[@id='my-account-dropdown']/li[2]")
    private WebElement accountSummary;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignOut")
    private WebElement signOutId;

    @FindBy(xpath = "//a[contains(@id,'ctl00_MasterHeader_ctl00_uchead_languageToggle_')]")
    private WebElement language;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_QC_CIC")
    private WebElement cartQuantity;

    @FindBy(xpath = "//footer[@class='footer fdn']")
    private WebElement footer;

    @FindBy(xpath = "//div[@class='footer-main-links flex-vertical-align']")
    private WebElement links_FooterMain;

    @FindBy(xpath = "//div[@class='footer-social-bar core-components']")
    private WebElement links_FooterSocialBar;

    @FindBy(xpath = "//div[@class='footer-accessibility-links flex-vertical-align']")
    private WebElement links_Accessibility;

    @FindBy(xpath = "//div[contains(text(),'Product Support')]")
    private WebElement txt_ProductSupport;

    @FindBy(xpath = "//div[contains(text(),'Product Support')]/following-sibling::ul")
    private WebElement links_ProductSupport;

    @FindBy(xpath = "//div[contains(text(),'Order Support')]")
    private WebElement txt_OrderSupport;

    @FindBy(xpath = "//div[contains(text(),'Order Support')]/following-sibling::ul")
    private WebElement links_OrderSupport;

    @FindBy(xpath = "//div[contains(text(),'Corporate Info')]")
    private WebElement txt_CorporateInfo;

    @FindBy(xpath = "//div[contains(text(),'Corporate Info')]/following-sibling::ul")
    private WebElement links_CorporateInfo;

    @FindBy(xpath = "//div[@class='footer-subtitle margin-l-top']")
    private WebElement txt_MobileApps;

    @FindBy(xpath = "//div[contains(text(),'Mobile Apps')]/following-sibling::ul")
    private WebElement links_MobileApps;

    @FindBy(xpath = "//div[contains(text(),'Legal')]")
    private WebElement txt_Legal;

    @FindBy(xpath = "//ul[@class='inline-list right']")
    private WebElement paymentLogos;

    @FindBy(xpath = "//a[@class='footer-logo-master-card']")
    private WebElement img_MasterCardLogo;

    @FindBy(xpath = "//div[@class='help-detail-content']/h1")
    private WebElement txt_Header;

    @FindBy(xpath = "//a[@class='footer-logo-visa']")
    private WebElement img_VerifiedByVisaLogo;

    @FindBy(xpath = "//a[@class='footer-logo-paypal']")
    private WebElement img_PayPalLogo;

    @FindBy(xpath = "//*[contains(@class,'search-autocomplete')]")
    private WebElement autoCompleteArea;

    @FindBy(xpath = "//span[text()='in Your Search History']")
    private WebElement autocompleteRecentHistory;

    @FindBy(xpath = "//a[@class='footer-logo-blog']")
    private WebElement img_Blog;

    @FindBy(xpath = "//a[@class='footer-logo-facebook']")
    private WebElement img_Facebook;

    @FindBy(xpath = "//a[@class='footer-logo-youtube']")
    private WebElement img_Youtube;

    @FindBy(xpath = "//a[@class='footer-logo-linkedin']")
    private WebElement img_LinkedIn;

    @FindBy(xpath = "//a[@class='footer-logo-instagram']")
    private WebElement img_Instagram;

    @FindBy(xpath = "//a[@class='footer-logo-pinterest']")
    private WebElement img_Pinterest;

    @FindBy(xpath = "//a[@class='footer-logo-twitter']")
    private WebElement img_Twitter;

    @FindBy(xpath = "//a[contains(@href,'android;en')]")
    private WebElement link_AndroidApp;

    @FindBy(xpath = "//a[contains(@href,'iphone;en')]")
    private WebElement link_IPhoneApp;

    @FindBy(xpath = "//div[@class='bestbuy-cards padding-xl-bottom background-white']/div/div[3]")
    private WebElement block_CreditCard;

    @FindBy(xpath = "//a[contains(@href,'footer;legal;terms')]")
    private WebElement link_TermsAndConditions;

    @FindBy(xpath = "//a[contains(@href,'footer;legal;conditions')]")
    private WebElement link_ConditionsOfUse;

    @FindBy(xpath = "//a[contains(@href,'footer;legal;policies')]")
    private WebElement link_OnlinePolicies;

    @FindBy(xpath = "//a[contains(@href,'footer;legal;privacy')]")
    private WebElement link_PrivacyPolicy;

    @FindBy(xpath = "//a[contains(@href,'shopping')]")
    private WebElement link_SafeShopping;

    @FindBy(xpath = "//a[contains(@href,'accessibility-policy')]")
    private WebElement link_AccessibilityPolicy;

    @FindBy(xpath = "//a[text()='Manage Credit Card']")
    private WebElement link_ManageCreditCard;

    @FindBy(xpath = "//img[@class='logo-desjardins']")
    private WebElement logo_Desjardins;

    @FindBy(xpath = "//ol[@id='stream-items-id']")
    private WebElement twitterTweets;

    @FindBy(xpath = "//a[@id='shop-menu-link']")
    private WebElement shopTab;

    @FindBy(xpath  = "//div[@class='navigation-dropdown open']")
    private WebElement shopdepartmentDropDown;

    @FindBy(xpath = "//li[@class='nae-flyout']/following-sibling::li[1]/a")
    private WebElement shopDepartment;

    @FindBy(xpath = "//li[@class='active-tab']/div[@class='navigation-dropdown-content']")
    private WebElement shopcategories;

    @FindBy(xpath = "//li[@class='active-tab']/div[@class='navigation-dropdown-content']/div[@class='nav-text-container']/div[@class='padding-left-four']/div[@class='row']/div[2]/ul/li[1]")
    private WebElement shopSubCategory;

    @FindBy(xpath="//a[@id='ctl00_MasterHeader_ctl00_uchead_languageToggle_french']")
    private WebElement frenchLink;

    @FindBy(xpath="//a[@id='ctl00_MasterHeader_ctl00_uchead_languageToggle_english']")
    private WebElement englishLink;


    private HeaderFooterDesktop() {

    }

    public HeaderFooterDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isCartEmpty(){

        // cartQuantity is displayed == cart is not empty
        return !isDisplayed(cartQuantity);

    }

    public void clickCartIconQuantity () {

        click(cartQuantity);
    }

    public String textMyAccount() {return getTextOrValue(lnk_myAccount);}

    public void clickSignInButton() {

        click(btn_SignIn);

    }

    public void clickFrenchLink() {click(frenchLink);}

    public void clickEnglishLink() {click(englishLink);}

    public String getTextEnglink() {return getTextOrValue(frenchLink);}

    public String getTextFrenchLink() {return getTextOrValue(englishLink);}

    public void clickOrderStatus(){
        waitUntilPageLoaded();
        click(lnk_orderStatus);

    }

    public void clickGiftCard() {scrollAndClick(lnk_giftCard);}

    public void clickMyBestBuyCard() {click(lnk_myBestBuyCard);}

    public void clickMyOrder() {click(lnk_myOrder);}

    public void clickSignOutLink(){

        click(lnk_signOut);

    }

    public void clickLogo(){

        click(logo);

    }

    public void clickFindAStoreLink(){

        click(lnk_findAStore);

    }

    public void clickCartIcon(){

        click(icn_cart);

    }

    public void searchFor(final Sku sku) {
        waitUntilPageLoaded();
        typeIn(searchBox, sku.toString());

        click(btn_search);

    }

    public void searchFor(final String item) {

        typeIn(searchBox, item);

        click(btn_search);

    }

    public void typeInSearchBox(final String item) {

        typeIn(searchBox, item);
    }

    public void navigateToCreateAccountPage() {

        click(lnk_myAccount);

        click(lnk_createAccount);

    }

    public void clickBrands() {
        click(brands);
    }

    public void clickShopAllBrandsLink() {
        click(lnk_shopAllBrands);
    }

    public void switchLanguage(){

        click(lnk_language);

    }

    public void clickMyAccountSummary(){
        click(lnk_myAccount);
        click(accountSummary);
    }

    public boolean isSignedIn(){
        return isDisplayed(lnk_signOut);
    }

    public boolean isSignedOut(){
        return isDisplayed(btn_SignIn);
    }

    public void clickLanguageLink(String language) {
        if(language.equalsIgnoreCase("English")) {
            click(lnk_english);
        } else if(language.equalsIgnoreCase("French")) {
            click(lnk_french);
        }
    }

    public String languageDisplayed(){
        refreshPage();
        return getTextOrValue(language);
    }

    public String getFirstNameText() { return getTextOrValue(label_firstName);}

    public void clickMyAccount() {
        click(lnk_myAccount);
    }

    public boolean checkLinkCreateAccount() {
        return isDisplayed(lnk_createAccount);

    }

    public void clickService() {click(lnk_service);}

    public void clickRegisterYourPlan() {click(lnk_registerYourPlan);}

    public void clickSiteMap() { click(lnk_siteMap); }

    public void clickPressRoom() { click(lnk_pressRoom); }

    public void clickHelpCentre(){ click(lnk_helpCentre); }

    public void clickFindStore() {click(lnk_findAStore);}

    public boolean isFooterDisplayed() { return isDisplayed(footer); }

    public boolean isProductSupportHeaderTextDisplayedAndNotClickable() {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(txt_ProductSupport));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean isProductSupportLinksDisplayed() { return isDisplayed(links_ProductSupport); }

    public boolean isOrderSupportHeaderTextDisplayedAndNotClickable() {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(txt_OrderSupport));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean isOrderSupportLinksDisplayed() { return isDisplayed(links_OrderSupport); }

    public boolean isCorporateInfoHeaderTextDisplayedAndNotClickable() {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(txt_CorporateInfo));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean isCorporateInfoLinksDisplayed() { return isDisplayed(links_CorporateInfo); }

    public boolean isMobileAppsHeaderTextDisplayedAndNotClickable() {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(txt_MobileApps));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean isMobileAppsLinksDisplayed() { return isDisplayed(links_MobileApps); }

    public boolean isLegalHeaderTextDisplayedAndNotClickable() {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(txt_Legal));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean isPaymentLogosDisplayed() { return isDisplayed(paymentLogos); }

    public void clickMasterCardLogo() { click(img_MasterCardLogo); }

    public void clickVerifiedByVisaLogo() { click(img_VerifiedByVisaLogo); }

    public void clickPayPalLogo() { click(img_PayPalLogo); }

    public boolean isAtMasterCardHelpPage() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(Expected_MasterCard_Help_Url);
        }
        catch (Exception e){
            logger.error("MasterCard Help page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("MasterCard Help page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAtVerifiedByVisaHelpPage() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(Expected_Verified_By_Visa_Help_Url);
        }
        catch (Exception e){
            logger.error("MasterCard Help page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("MasterCard Help page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isAtPayPalHelpPage() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(Expected_PayPal_Help_Url);
        }
        catch (Exception e){
            logger.error("MasterCard Help page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("MasterCard Help page is not displayed " + e.getMessage(), e);
        }
    }

    public String getTxt_PageHeader() {
        return getTextOrValue(txt_Header);
    }

    public boolean isMainFooterLinksDisplayed() { return isDisplayed(links_FooterMain);  }

    public boolean isAccessibilityLinksDisplayed() { return isDisplayed(links_Accessibility); }

    public boolean isSocialBarsDisplayed() { return isDisplayed(links_FooterSocialBar); }

    public boolean isBlogLogoDisplayed() { return isDisplayed(img_Blog); }

    public void clickBlogLogo() { click(img_Blog); }

    public boolean isAtBestBuyBlogPage() {
        try{
            return driver.getTitle().contains("Best Buy Blog");
        }
        catch (Exception e) {
            logger.error("Best Buy Blog page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Blog page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isFacebookLogoDisplayed() { return isDisplayed(img_Facebook); }

    public void clickFacebookLogo() {click(img_Facebook); }

    public boolean isAtBestBuyFacebookPage() {
        try{
            return driver.getTitle().contains("Facebook");
        }
        catch (Exception e) {
            logger.error("Best Buy Facebook page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Facebook page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isYoutubeLogoDisplayed() { return isDisplayed(img_Youtube); }

    public void clickYoutubeLogo() { click(img_Youtube); }

    public boolean isAtBestBuyYoutubePage() {
        try{
            return driver.getTitle().contains("YouTube");
        }
        catch (Exception e) {
            logger.error("Best Buy Youtube page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Youtube page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isLinkedInLogoDisplayed() { return isDisplayed(img_LinkedIn); }

    public void clickLinkedInLogo() { click(img_LinkedIn); }

    public boolean isAtBestBuyLinkedInPage() {
        try{
            return driver.getTitle().contains("LinkedIn");
        }
        catch (Exception e) {
            logger.error("Best Buy LinkedIn page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy LinkedIn page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isInstagramLogoDisplayed() { return isDisplayed(img_Instagram); }

    public void clickInstagramLogo() { click(img_Instagram); }

    public boolean isAtBestBuyInstagramInPage() {
        try{
            return driver.getTitle().contains("Instagram");
        }
        catch (Exception e) {
            logger.error("Best Buy Instagram page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Instagram page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isPInterestLogoDisplayed() { return isDisplayed(img_Pinterest); }

    public void clickPinterestLogo() { click(img_Pinterest); }

    public boolean isAtBestBuyPinterestInPage() {
        try{
            return driver.getTitle().contains("Pinterest");
        }
        catch (Exception e) {
            logger.error("Best Buy Pinterest page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Pinterest page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isTwitterLogoDisplayed() { return isDisplayed(img_Twitter); }

    public void clickTwitterLogo() { click(img_Twitter); }

    public boolean isAtBestBuyTwitterInPage() {
        try{
            return driver.getTitle().contains("Twitter");
        }
        catch (Exception e) {
            logger.error("Best Buy Twitter page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Best Buy Twitter page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isTwitterTweetsDisplayed() { return isDisplayed(twitterTweets); }

    public String getTxt_AndroidAppsLink() { return getTextOrValue(link_AndroidApp); }

    public String getTxt_IPhoneAppsLink() { return getTextOrValue(link_IPhoneApp); }

    public boolean isCreditCardBlockDisplayed() { return isDisplayed(block_CreditCard); }

    public String getTxt_TermsAndConditions() { return getTextOrValue(link_TermsAndConditions); }

    public String getTxt_ConditionsOfUse() { return getTextOrValue(link_ConditionsOfUse); }

    public String getTxt_OnlinePolicies() { return getTextOrValue(link_OnlinePolicies); }

    public String getTxt_PrivacyPolicy() { return getTextOrValue(link_PrivacyPolicy); }

    public String getTxt_SafeShopping() { return getTextOrValue(link_SafeShopping); }

    public String getTxt_AccessibilityPolicy() { return getTextOrValue(link_AccessibilityPolicy); }

    public void clickManageCreditCardLink() { click(link_ManageCreditCard); }

    public boolean isDesjardinsPageDisplayed() { return isDisplayed(logo_Desjardins); }

    public boolean isSearchAutoCompleteAreaDisplayed() {
        waitUntilPageLoaded(3);
        return isDisplayed(autoCompleteArea);
    }

    public boolean isAutoCompleteSearchHistoryDisplayed() {

        return isDisplayed(autocompleteRecentHistory);
    }

    public void clickShopTab() { click(shopTab); }

    public boolean isShopDepartmentsDisplayed() { return isDisplayed(shopdepartmentDropDown); }

    public void clickOnShopDepartment() { click(shopDepartment); }

    public boolean isShopCategoriesDisplayed() { return isDisplayed(shopcategories); }

    public void clickShopSubCategory() {
        TestUser.subCategory = getTextOrValue(shopSubCategory);
        click(shopSubCategory);
    }
}

