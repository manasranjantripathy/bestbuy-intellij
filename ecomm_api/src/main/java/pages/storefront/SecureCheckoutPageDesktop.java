package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;
import utils.TestUser;

/**
 * Created by sucho on 6/29/2017.
 */
public class SecureCheckoutPageDesktop extends StorefrontBasePage {
    private static final String EXPECTED_URL = "/order/selectcheckoutmethod.aspx";
    private final static Logger logger = Logger.getLogger(SecureCheckoutPageDesktop.class);
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCheckoutSignInUC_radioButtonNew")
    private WebElement buttonGuestRadio;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCheckoutSignInUC_radioButtonSignIn")
    private WebElement buttonMemberRadio;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCheckoutSignInUC_btnSubmitOrder")
    private WebElement buttonContinueCheckout;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCheckoutSignInUC_UserNameContainer_txtUserName")
    private WebElement fieldMemberEmailAddress;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCheckoutSignInUC_PasswordContainer_txtPassword")
    private WebElement fieldMemberPassword;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'lnkForgotPassword')]")
    private WebElement buttonForgetPassword;

    @FindBy(how = How.XPATH, using = "//ul[@class='returning-customers']/li/label/label/span")
    private WebElement textReturningCustomers;

    @FindBy(how = How.XPATH, using = "//ul[@class='guest-customers']/li/label/label/span")
    private WebElement textGuestCustomers;

    private SecureCheckoutPageDesktop() {

    }

    public SecureCheckoutPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            this.wait = new WebDriverWait(driver, 60);
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Secure Checkout page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Secure Checkout page is not displayed " + e.getMessage(), e);
        }
    }

    public void continueCheckoutAsGuest() {

        click(buttonGuestRadio);

        click(buttonContinueCheckout);

    }

    public void continueCheckoutAsMember() {

        click(buttonMemberRadio);

        typeIn(fieldMemberEmailAddress, TestUser.email);

        typeIn(fieldMemberPassword, TestUser.password);

        click(buttonContinueCheckout);

    }

    public void clickOnForgetPasswordButton() { click(buttonForgetPassword); }

    public String getMembersCustomerText() { return getTextOrValue(textReturningCustomers);}

    public String getGuestCustomerText() { return getTextOrValue(textGuestCustomers);}

    public void continueCheckoutAsNewlyCreatedMember() {
        click(buttonMemberRadio);
        typeIn(fieldMemberEmailAddress, TestUser.newEmail);
        typeIn(fieldMemberPassword, TestUser.password);
        click(buttonContinueCheckout);
    }



}