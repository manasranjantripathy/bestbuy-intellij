package pages.storefront;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.StorefrontBasePage;

public class CompareResultsPageDesktop extends StorefrontBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "catalog/compare-result.aspx";
    private final static Logger logger = Logger.getLogger(ProductListingPageDesktop.class);

    @FindBy(xpath = "//a[@id='ctl00_CP_ucCompareProductResults_RptRemoveLink_ctl03_lnkRemoveProductTop']")
    private WebElement btn_Remove4;

    @FindBy(xpath = "//a[@id='ctl00_CP_ucCompareProductResults_RptProductLabelTop_ctl03_ucProductLabelTop_SN']")
    private WebElement product4;

    @FindBy(xpath = "//a[@id='ctl00_CP_ucCompareProductResults_RptRemoveLink_ctl02_lnkRemoveProductTop']")
    private WebElement btn_Remove3;

    @FindBy(xpath = "//a[@id='ctl00_CP_ucCompareProductResults_RptProductLabelTop_ctl02_ucProductLabelTop_SN']")
    private WebElement product3;

    @FindBy(xpath = "//a[@id='ctl00_CP_ucCompareProductResults_RptRemoveLink_ctl01_lnkRemoveProductTop']")
    private WebElement btn_Remove2;

    @FindBy(xpath = "//a[@id='ctl00_CP_HypBack']")
    private WebElement link_BackToSearchResults;

    @FindBy(xpath = "//button[@class='btn btn-addtocart btn-primary availability core_models_availability_undefined']")
    private WebElement btn_AddToCart;

    public CompareResultsPageDesktop(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch(Exception e){
            logger.error("Compare Results page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Compare Results page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickRemoveButton4() { click(btn_Remove4); }

    public void clickRemoveButton3() { click(btn_Remove3); }

    public boolean isProduct4Displayed() {
        return !isDisplayed(product4) ? true: false;
    }

    public boolean isProduct3Displayed() {
        return !isDisplayed(product3) ? true: false;
    }

    public boolean isRemove2ButtonDisabled() {
        Boolean result = true;
        try {
            String value = btn_Remove2.getAttribute("disabled").toString();
            System.out.println(btn_Remove2.getAttribute("disabled").toString());
            if(value == "disabled") {
                result = true;
            }
        } catch (Exception e) {
            return false;
        }
        return result;
    }

    public void clickBackToSearchResultsLink() { click(link_BackToSearchResults); }

    public void clickAddToCartButton() {
        if (!isEnabled(btn_AddToCart)) {
            throw new RuntimeException("Add to Cart button is disabled");
        } else {
            click(btn_AddToCart);
        }
    }
}