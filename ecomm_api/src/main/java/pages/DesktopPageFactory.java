package pages;

import org.openqa.selenium.WebDriver;
import pages.commons.GeekSquadPageDesktop;
import pages.commons.PayPalPage;
import pages.storefront.*;

/**
 * Created by sucho on 6/29/2017.
 */
public class DesktopPageFactory {
    private WebDriver driver;

    private AccountSummaryPageDesktop accountSummaryPage;
    private AddAddressPageDesktop addAddressPage;
    private AddCreditCardPageDesktop addCreditCardPage;
    private AddressBookPageDesktop addressBookPage;
    private AgeVerificationPageDesktop ageVerificationPageDesktop;
    private BrandsPageDesktop brandsPageDesktop;
    private CartPageDesktop cartPageDesktop;
    private ChangeEmailAddressPageDesktop changeEmailAddressPage;
    private ChangePasswordPageDesktop changePasswordPage;
    private ChangePaymentPageDesktop changePaymentPageDesktop;
    private ChangeShippingPageDesktop changeShippingPageDesktop;
    private CheckGiftCardBalancePageDesktop checkGiftCardBalancePage;
    private CheckOrderStatusPageDesktop checkOrderStatusPage;
    private CheckoutPageDesktop checkoutPageDesktop;
    private CommunicationPreferencesPageDesktop communicationPreferencesPage;
    private CompareResultsPageDesktop compareResultsPage;
    private CreateAccountPageDesktop createAccountPage;
    private EmailFriendPageDesktop emailFriendPage;
    private HeaderFooterDesktop headerFooterDesktop;
    private HelpCentrePageDesktop helpCentrePage;
    private HomePageDesktop homePageDesktop;
    private InterstitialPageDesktop interstitialPageDesktop;
    private ManageCreditCardsPageDesktop manageCreditCardsPage;
    private MasterCardSecureCodePageDesktop masterCardSecureCodePage;
    private OrderDetailsPageDesktop orderDetailsPage;
    private OrderChangeConfirmationPageDesktop orderChangeConfirmationPageDesktop;
    private OrderConfirmationPageDesktop orderConfirmationPageDesktop;
    private OrderHistoryPageDesktop orderHistoryPageDesktop;
    private PaginationPageDesktop paginationPage;
    private PersonalInformationPageDesktop personalInformationPage;
    private PreferredStoresPageDesktop preferredStoresPage;
    private PressReleasePageDesktop pressReleasePage;
    private PrivacyInquiryPageDesktop privacyInquiryPage;
    private ProductDetailsPageDesktop productDetailsPageDesktop;
    private ProductListingPageDesktop productListingPageDesktop;
    private GeekSquadPageDesktop geekSquadPageDesktop;
    private ResetPasswordPageDesktop resetPasswordPageDesktop;
    private ReviewPageDesktop reviewPageDesktop;
    private SecureCheckoutPageDesktop secureCheckoutPageDesktop;
    private SelectStorePopupDesktop selectStorePopupDesktop;
    private SignInPageDesktop signInPageDesktop;
    private SiteMapPageDesktop siteMapPage;
    private StoreLocatorPageDesktop storeLocatorPageDesktop;
    private VerifiedByVisaPageDesktop verifiedByVisaPage;
    private VisaCheckoutPopUpDesktop visaCheckoutPopUpDesktop;
    private WarrantyPopupDesktop warrantyPopupDesktop;
    private PayPalPage paypalPage;
    private WishListPageDesktop wishListPage;


    private DesktopPageFactory() {

    }

    public DesktopPageFactory(WebDriver driver) {
        this.driver = driver;
    }

    public AccountSummaryPageDesktop accountSummaryPage(){
        if (accountSummaryPage == null) {
            accountSummaryPage = new AccountSummaryPageDesktop(driver);
        }
        return accountSummaryPage;
    }

    public AddCreditCardPageDesktop addCreditCardPage(){
        if (addCreditCardPage == null) {
            addCreditCardPage = new AddCreditCardPageDesktop(driver);
        }
        return addCreditCardPage;
    }

    public AddressBookPageDesktop addressBookPage(){
        if(addressBookPage == null) {
            addressBookPage = new AddressBookPageDesktop(driver);
        }
        return addressBookPage;
    }

    public AddAddressPageDesktop addAddressPage(){
        if(addAddressPage == null){
            addAddressPage = new AddAddressPageDesktop(driver);
        }
        return addAddressPage;
    }

    public AgeVerificationPageDesktop ageVerificationPage(){
        if(ageVerificationPageDesktop == null){
            ageVerificationPageDesktop = new AgeVerificationPageDesktop(driver);
        }
        return ageVerificationPageDesktop;
    }

    public BrandsPageDesktop brandsPageDesktop() {
        if (brandsPageDesktop == null) {
            brandsPageDesktop = new BrandsPageDesktop(driver);
        }
        return brandsPageDesktop;
    }

    public CartPageDesktop cartPage() {
        if (cartPageDesktop == null) {
            cartPageDesktop = new CartPageDesktop(driver);
        }
        return cartPageDesktop;
    }

    public ChangeEmailAddressPageDesktop changeEmailAddressPage() {
        if (changeEmailAddressPage== null) {
            changeEmailAddressPage = new ChangeEmailAddressPageDesktop(driver);
        }
        return changeEmailAddressPage;
    }

    public ChangePasswordPageDesktop changePasswordPage(){
        if(changePasswordPage == null){
            changePasswordPage = new ChangePasswordPageDesktop(driver);
        }
        return changePasswordPage;
    }

    public ChangePaymentPageDesktop changePaymentPage(){
        if(changePaymentPageDesktop == null){
            changePaymentPageDesktop = new ChangePaymentPageDesktop(driver);
        }
        return changePaymentPageDesktop;
    }

    public ChangeShippingPageDesktop changeShippingPage(){
        if(changeShippingPageDesktop == null){
            changeShippingPageDesktop = new ChangeShippingPageDesktop(driver);
        }
        return changeShippingPageDesktop;
    }

    public CheckGiftCardBalancePageDesktop checkGiftCardBalancePage() {
        if(checkGiftCardBalancePage == null) {
            checkGiftCardBalancePage = new CheckGiftCardBalancePageDesktop(driver);
        }
        return checkGiftCardBalancePage;
    }

    public CheckOrderStatusPageDesktop checkOrderStatusPage(){
        if (checkOrderStatusPage == null) {
            checkOrderStatusPage = new CheckOrderStatusPageDesktop(driver);
        }
        return checkOrderStatusPage;
    }

    public CheckoutPageDesktop checkoutPage() {
        if (checkoutPageDesktop == null) {
            checkoutPageDesktop = new CheckoutPageDesktop(driver);
        }
        return checkoutPageDesktop;
    }

    public CommunicationPreferencesPageDesktop communicationPreferencesPage(){
        if(communicationPreferencesPage == null) {
            communicationPreferencesPage = new CommunicationPreferencesPageDesktop(driver);
        }
        return communicationPreferencesPage;
    }

    public CompareResultsPageDesktop compareResultsPage() {
        if(compareResultsPage== null) {
            compareResultsPage = new CompareResultsPageDesktop(driver);
        }
        return compareResultsPage;
    }

    public CreateAccountPageDesktop createAccountPage() {
        if (createAccountPage == null) {
            createAccountPage = new CreateAccountPageDesktop(driver);
        }
        return createAccountPage;
    }

    public HeaderFooterDesktop headerFooter() {
        if (headerFooterDesktop == null) {
            headerFooterDesktop = new HeaderFooterDesktop(driver);
        }
        return headerFooterDesktop;
    }

    public HelpCentrePageDesktop helpCentrePage() {
        if (helpCentrePage == null) {
            helpCentrePage = new HelpCentrePageDesktop(driver);
        }
        return helpCentrePage;
    }

    public EmailFriendPageDesktop emailFriendPage() {
        if (emailFriendPage == null) {
            emailFriendPage = new EmailFriendPageDesktop(driver);
        }
        return emailFriendPage;
    }

    public HomePageDesktop homePage() {
        if (homePageDesktop == null) {
            homePageDesktop = new HomePageDesktop(driver);
        }
        return homePageDesktop;
    }

    public InterstitialPageDesktop interstitialPage() {
        if (interstitialPageDesktop == null) {
            interstitialPageDesktop = new InterstitialPageDesktop(driver);
        }
        return interstitialPageDesktop;
    }

    public ManageCreditCardsPageDesktop manageCreditCardsPage(){
        if (manageCreditCardsPage == null) {
            manageCreditCardsPage = new ManageCreditCardsPageDesktop(driver);
        }
        return manageCreditCardsPage;
    }

    public MasterCardSecureCodePageDesktop masterCardSecureCodePage() {
        if (masterCardSecureCodePage == null) {
            masterCardSecureCodePage = new MasterCardSecureCodePageDesktop(driver);
        }
        return masterCardSecureCodePage;
    }

    public OrderDetailsPageDesktop orderDetailsPage(){
        if(orderDetailsPage == null) {
            orderDetailsPage = new OrderDetailsPageDesktop(driver);
        }
        return orderDetailsPage;
    }

    public OrderChangeConfirmationPageDesktop orderChangeConfirmationPage(){
        if(orderChangeConfirmationPageDesktop == null){
            orderChangeConfirmationPageDesktop = new OrderChangeConfirmationPageDesktop(driver);
        }
        return orderChangeConfirmationPageDesktop;
    }

    public OrderConfirmationPageDesktop orderConfirmationPage() {
        if (orderConfirmationPageDesktop == null) {
            orderConfirmationPageDesktop = new OrderConfirmationPageDesktop(driver);
        }
        return orderConfirmationPageDesktop;
    }

    public OrderHistoryPageDesktop orderHistoryPage() {
        if (orderHistoryPageDesktop == null) {
            orderHistoryPageDesktop = new OrderHistoryPageDesktop(driver);
        }
        return orderHistoryPageDesktop;
    }

    public PaginationPageDesktop paginationPage() {
        if (paginationPage == null) {
            paginationPage = new PaginationPageDesktop(driver);
        }
        return paginationPage;
    }

    public PersonalInformationPageDesktop personalInformationPage() {
        if (personalInformationPage == null) {
            personalInformationPage = new PersonalInformationPageDesktop(driver);
        }
        return personalInformationPage;
    }

    public PreferredStoresPageDesktop preferredStoresPage() {
        if (preferredStoresPage == null) {
            preferredStoresPage = new PreferredStoresPageDesktop(driver);
        }
        return preferredStoresPage;
    }

    public PressReleasePageDesktop pressReleasePage() {
        if (pressReleasePage == null) {
            pressReleasePage = new PressReleasePageDesktop(driver);
        }
        return pressReleasePage;
    }

    public PrivacyInquiryPageDesktop privacyInquiryPage() {
        if (privacyInquiryPage == null) {
            privacyInquiryPage = new PrivacyInquiryPageDesktop(driver);
        }
        return privacyInquiryPage;
    }

    public ProductDetailsPageDesktop productDetailsPage() {
        if (productDetailsPageDesktop == null) {
            productDetailsPageDesktop = new ProductDetailsPageDesktop(driver);
        }
        return productDetailsPageDesktop;
    }

    public ProductListingPageDesktop productListingPage() {
        if (productListingPageDesktop == null) {
            productListingPageDesktop = new ProductListingPageDesktop(driver);
        }
        return productListingPageDesktop;
    }

    public GeekSquadPageDesktop geekSquadPage() {
        if (geekSquadPageDesktop == null) {
            geekSquadPageDesktop = new GeekSquadPageDesktop(driver);
        }
        return geekSquadPageDesktop;
    }

    public ResetPasswordPageDesktop resetPasswordPage(){
        if(resetPasswordPageDesktop == null){
            resetPasswordPageDesktop = new ResetPasswordPageDesktop(driver);
        }
        return resetPasswordPageDesktop;
    }

    public ReviewPageDesktop reviewPage() {
        if(reviewPageDesktop == null) {
            reviewPageDesktop = new ReviewPageDesktop(driver);
        }
        return reviewPageDesktop;
    }

    public SecureCheckoutPageDesktop secureCheckoutPage() {
        if (secureCheckoutPageDesktop == null) {
            secureCheckoutPageDesktop = new SecureCheckoutPageDesktop(driver);
        }
        return secureCheckoutPageDesktop;
    }

    public SelectStorePopupDesktop selectStorePopup() {
        if (selectStorePopupDesktop == null) {
            selectStorePopupDesktop = new SelectStorePopupDesktop(driver);
        }
        return selectStorePopupDesktop;
    }

    public SignInPageDesktop signInPage(){
        if(signInPageDesktop == null){
            signInPageDesktop = new SignInPageDesktop(driver);
        }
        return signInPageDesktop;
    }

    public SiteMapPageDesktop siteMapPage(){
        if(siteMapPage == null){
            siteMapPage = new SiteMapPageDesktop(driver);
        }
        return siteMapPage;
    }

    public StoreLocatorPageDesktop storeLocatorPage(){
        if(storeLocatorPageDesktop == null){
            storeLocatorPageDesktop = new StoreLocatorPageDesktop(driver);
        }
        return storeLocatorPageDesktop;
    }

    public VerifiedByVisaPageDesktop verifiedByVisaPage() {
        if(verifiedByVisaPage == null) {
            verifiedByVisaPage = new VerifiedByVisaPageDesktop(driver);
        }
        return verifiedByVisaPage;
    }

    public VisaCheckoutPopUpDesktop visaCheckoutPopUp() {
        if (visaCheckoutPopUpDesktop == null) {
            visaCheckoutPopUpDesktop = new VisaCheckoutPopUpDesktop(driver);
        }
        return visaCheckoutPopUpDesktop;
    }

    public WarrantyPopupDesktop warrantyPopup() {
        if (warrantyPopupDesktop == null) {
            warrantyPopupDesktop = new WarrantyPopupDesktop(driver);
        }
        return warrantyPopupDesktop;
    }

    public PayPalPage paypalPage(){
        if(paypalPage == null){
            paypalPage = new PayPalPage(driver);
        }
        return paypalPage;
    }

    public WishListPageDesktop wishListPage() {
        if (wishListPage == null) {
            wishListPage = new WishListPageDesktop(driver);
        }
        return wishListPage;
    }


}