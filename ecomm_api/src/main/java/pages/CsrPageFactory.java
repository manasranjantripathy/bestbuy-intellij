package pages;

import org.openqa.selenium.WebDriver;
import pages.csr.*;

/**
 * Created by sucho on 7/6/2017.
 */
public class CsrPageFactory {
    private WebDriver driver;

    private CartPageCSR cartPageCSR;
    private CheckoutPageCSR checkoutPageCSR;
    private CustomerSearchPageCSR customerSearchPageCSR;
    private HeaderFooterCSR headerFooterCSR;
    private HomePageCSR homePageCSR;
    private InterstitialPageCSR interstitialPageCSR;
    private OrderConfirmationPageCSR orderConfirmationPageCSR;
    private LandingPageCSR landingPageCSR;
    private ProductDetailsPageCSR productDetailsPageCSR;
    private SelectStorePopupCSR selectStorePopupCSR;
    private WarrantyPopupCSR warrantyPopupCSR;

    private CsrPageFactory() {

    }

    public CsrPageFactory(WebDriver driver) {
        this.driver = driver;
    }

    public CartPageCSR cartPage(){
        if(cartPageCSR == null){
            cartPageCSR = new CartPageCSR(driver);
        }
        return cartPageCSR;
    }

    public CheckoutPageCSR checkoutPage(){
        if(checkoutPageCSR == null){
            checkoutPageCSR = new CheckoutPageCSR(driver);
        }
        return checkoutPageCSR;
    }

    public CustomerSearchPageCSR customerSearchPage(){
        if(customerSearchPageCSR == null){
            customerSearchPageCSR = new CustomerSearchPageCSR(driver);
        }
        return customerSearchPageCSR;
    }

    public HeaderFooterCSR headerFooter(){
        if(headerFooterCSR == null){
            headerFooterCSR = new HeaderFooterCSR(driver);
        }
        return headerFooterCSR;
    }

    public HomePageCSR homePage(){
        if(homePageCSR == null){
            homePageCSR = new HomePageCSR(driver);
        }
        return homePageCSR;
    }

    public InterstitialPageCSR interstitialPage(){
        if(interstitialPageCSR == null){
            interstitialPageCSR = new InterstitialPageCSR(driver);
        }
        return interstitialPageCSR;
    }

    public LandingPageCSR landingPage(){
        if(landingPageCSR == null){
            landingPageCSR = new LandingPageCSR(driver);
        }
        return landingPageCSR;
    }

    public OrderConfirmationPageCSR orderConfirmationPage(){
        if(orderConfirmationPageCSR == null){
            orderConfirmationPageCSR = new OrderConfirmationPageCSR(driver);
        }
        return orderConfirmationPageCSR;
    }

    public ProductDetailsPageCSR productDetailsPage(){
        if(productDetailsPageCSR == null){
            productDetailsPageCSR = new ProductDetailsPageCSR(driver);
        }
        return productDetailsPageCSR;
    }

    public SelectStorePopupCSR selectStorePopup(){
        if(selectStorePopupCSR == null){
            selectStorePopupCSR = new SelectStorePopupCSR(driver);
        }
        return selectStorePopupCSR;
    }

    public WarrantyPopupCSR warrantyPopup(){
        if(warrantyPopupCSR == null){
            warrantyPopupCSR = new WarrantyPopupCSR(driver);
        }
        return warrantyPopupCSR;
    }
}