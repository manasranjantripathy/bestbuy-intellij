package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;

import java.util.List;

/**
 * Created by sucho on 10/5/2017.
 */
public class CartPageCSR extends CsrBasePage {

    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/basket.aspx";
    private final static Logger logger = Logger.getLogger(CartPageCSR.class);

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_btnSubmitOrder")
    private WebElement buttonQtyUpdate;

    @FindBy(how = How.ID, using = "AddWarranty")
    private WebElement buttonAddToCartOnPSP;

    @FindBy(how = How.ID, using = "ctl00_CP_btnSubmitOrder")
    private WebElement buttonCheckout;

    @FindBy(how = How.ID, using = "ctl00_CP_btnReserveInStore")
    private WebElement buttonReserveInStore;

    @FindBy(how = How.ID, using = "ctl00_CP_btnPayPal")
    private WebElement buttonCheckoutWithPayPal;

    @FindBy(how = How.ID, using = "btnVisaCheckout")
    private WebElement buttonVisaCheckout;

    @FindBy(how = How.ID, using = "btn-save-changes")
    private WebElement buttonSubmitOnDeliveryOptions;

    @FindBy(how = How.XPATH, using = "//span[contains(text(), 'View delivery options')]")
    private WebElement buttonViewDeliveryOptions;

    @FindBy(how = How.XPATH, using = "//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ChkAcceptTerms')]")
    private WebElement checkBoxAcceptTermsOnPSP;

    @FindBy(how = How.XPATH, using = "//div[@class='input-group']//input[contains(@class, 'search-input')]")
    private WebElement fieldChangeDeliveryLocationInput;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_QuantityContainer_TxtQuantity")
    private WebElement fieldItemQty;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxPstTotal")
    private WebElement fieldEstimatedPSTorQST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxGstTotal")
    private WebElement fieldEstimatedGST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblTaxHstTotal")
    private WebElement fieldEstimatedHST;

    @FindBy(id = "ctl00_CP_UcCartOrderTotals_LblOrderTotal")
    private WebElement fieldTotalAmount;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_ucPriceBlockWithPromotional_hypEhfText")
    private WebElement labelEnvHandlingFeeInCartSection;

    @FindBy(className = "price-desc")
    private WebElement getLabelEnvHandlingFeeInOrderSection;

    @FindBy(how = How.XPATH, using = "//li[contains(@class,'warranty-item')]//a[contains(@id,'_ctrl0_UcOrderProduct_HypShortProductLabel')]")
    private WebElement labelPspInCart;

    @FindBy(xpath = "//div[@class='prod-saving']")
    private WebElement labelDiscount;

    @FindBy(how = How.ID, using = "ctl00_CP_UcCartOrderTotals_HypChange")
    private WebElement linkDeliveryLocation;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_LnkSeeAllAccessories")
    private WebElement linkSeeAllAccessories;

    @FindBy(how = How.ID, using = "ctl00_CP_MccpHyperLink1")
    private WebElement linkContinueShipping;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_btnSubmitOrder")
    private WebElement linkQuantityUpdate;

    @FindBy(how = How.ID, using = "ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_LnkRemove")
    private WebElement linkRemoveItem;

    @FindBy(how = How.CLASS_NAME, using = "empty-cart-msg")
    private WebElement emtpyCartMsg;

    @FindBy(how = How.ID, using = "cart-accessories-popover")
    private WebElement cartAccessoriesPopup;

    @FindBy(how = How.ID, using = "btnModalDialogClose")
    private WebElement iconCloseOnPSP;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'shipment-title')]")
    private WebElement divShipmentTitleOnDeliveryOption;

    @FindBy(how = How.CLASS_NAME, using = "product-items")
    private WebElement itemsOnDeliveryOption;

    @FindBy(how = How.XPATH, using = "//div[contains(@class, 'delivery-options')]")
    private WebElement shippingOptionsOnDeliveryOptions;

    private String buttonCheckoutJS = "ctl00_CP_btnSubmitOrder";

    private CartPageCSR() {

    }

    public CartPageCSR(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Cart page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Cart page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isCartEmpty(){

        return isDisplayed(emtpyCartMsg);

    }

    public boolean isPspAddedInTheCart(){

        return isDisplayed(labelPspInCart);

    }

    public boolean isDiscountApplied(){

        return isDisplayed(labelDiscount);

    }

    public boolean isEnvHandlingFeeDisplayedInOrderSection(){

        return isDisplayed(getLabelEnvHandlingFeeInOrderSection);

    }

    public boolean isCartAccessoriesPopupDisplayed(){

        return isDisplayed(cartAccessoriesPopup);

    }

    public boolean isDeliveryOptionsDisplaysInformation(){

        return isDisplayed(divShipmentTitleOnDeliveryOption) && isDisplayed(itemsOnDeliveryOption) && isDisplayed(shippingOptionsOnDeliveryOptions);
    }

    public void clickCheckoutButton(){

        clickByJSId(buttonCheckoutJS);

    }

    public void clickDeliveryChangeLink(){

        click(linkDeliveryLocation);

    }

    public void clickRemoveItem(){

        click(linkRemoveItem);

    }

    public void clickReserveInStore(){

        click(buttonReserveInStore);

    }

    public void clickSeeAllAccessoriesLink(){

        click(linkSeeAllAccessories);

    }

    public void clickSubmitButtonOnDeliveryOption(){

        click(buttonSubmitOnDeliveryOptions);

    }

    public void typePostalCodeOnChangeDeliveryLocationPopup(final String postalCode){

        typeIn(fieldChangeDeliveryLocationInput, postalCode);

        click(buttonViewDeliveryOptions);

    }

    public void changeDeliveryOption(final String postalCode){

        click(linkDeliveryLocation);

        typeIn(fieldChangeDeliveryLocationInput, postalCode);

        click(buttonSubmitOnDeliveryOptions);
    }

    public void updateItemQuantity(final int quantity){

        typeIn(fieldItemQty, quantity);

        click(buttonQtyUpdate);

    }

    public int getItemQty(){

        return Integer.valueOf(getTextOrValue(fieldItemQty));

    }

    public int getTheNumOfItemsInCart(){
        try{
            List<WebElement> elements = driver.findElements(By.xpath("//ul[@class='cart-top-border cart-list']//li"));
            return elements.size();
        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String getItemTitleInTheCart(final int nth){
        try{
            List<WebElement> elements = driver.findElements(By.xpath("//ul[@class='cart-top-border cart-list']//li"));
            WebElement element = elements.get(nth).findElement(By.id("ctl00_CP_RepCartItemsBySeller_ctl00_ProductsBySellerSection_RepCartItems_ctl00_UcCartItem_LvCartItem_ctrl0_UcOrderProduct_HypShortProductLabel"));
            return element.getAttribute("value").trim();
        }
        catch(Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public String getEnvHandlingFeesFromCartSection(){

        return getTextOrValue(labelEnvHandlingFeeInCartSection);

    }

    public String getEstimatedPSTorQST(){

        return isDisplayed(fieldEstimatedPSTorQST) ? getTextOrValue(fieldEstimatedPSTorQST) : "";

    }

    public String getEstimatedGST(){

        return isDisplayed(fieldEstimatedGST) ? getTextOrValue(fieldEstimatedGST) : "";

    }

    public String getEstimatedHST(){

        return isDisplayed(fieldEstimatedHST) ? getTextOrValue(fieldEstimatedHST) : "";
    }

    public String getTotal(){

        return getTextOrValue(fieldTotalAmount);

    }

    public void addProtectionServicePlan(final int years){
        try {
            waitUntilPageLoaded();

            WebElement pspCheckOutForm;
            if(years == 3){
                pspCheckOutForm = driver.findElement(By.xpath("//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ctl01')]"));
            }
            else { // by default add 2years plan
                pspCheckOutForm = driver.findElement(By.xpath("//input[contains(@id, 'ucAdditionalProductsContainer_Recommended2_RepWarrantyItems_ctl00_ctl07')]"));
            }

            click(pspCheckOutForm);

            click(checkBoxAcceptTermsOnPSP);

            click(buttonAddToCartOnPSP);

            click(iconCloseOnPSP);

        }
        catch (Exception e){
            logger.error("PSP form element is not visible " + e.getMessage(), e);
            throw new RuntimeException("PSP form element is not visible " + e.getMessage(), e);
        }
    }

    public void removeAllItemsInTheCart(){
        waitUntilPageLoaded();
        String item = "//li[@class='cart-line-item product-item cart_quantity']";
        List<WebElement> cartItems = driver.findElements(By.xpath(item));

        for (WebElement cartItem : cartItems) {
            String removeLink = "//div[@class = 'remove-but']//a";
            WebElement remove = cartItem.findElement(By.xpath(removeLink));
            click(remove);
        }
    }
}