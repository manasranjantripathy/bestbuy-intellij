package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;
import utils.TestUser;

/**
 * Created by sucho on 7/6/2017.
 */
public class CustomerSearchPageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/csrordercustomersearch.aspx";
    private final static Logger logger = Logger.getLogger(CustomerSearchPageCSR.class);

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_BtnSearch')]")
    private WebElement buttonSearch;

    @FindBy(how = How.XPATH, using = "//span[contains(@id,'_FirstNameContainer')]/input")
    private WebElement fieldFirstName;

    @FindBy(how = How.XPATH, using = "//span[contains(@id,'_LastNameContainer')]/input")
    private WebElement fieldLastName;

    @FindBy(how = How.XPATH, using = "//span[contains(@id,'_EmailContainer')]/input")
    private WebElement fieldEmail;

    @FindBy(how = How.ID, using = "ctl00_CP_SearchCustomerUC_ZipPostalContainer_TxtZipPostalCode")
    private WebElement fieldPostalCode;

    @FindBy(how = How.ID, using = "ctl00_CP_LnkCreateAccount")
    private WebElement linkCreateAccount;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_LblTitle')]")
    private WebElement textCreateAnAccountHeader;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_LblTitle')]")
    private WebElement textStaticMyAccount;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li")
    private WebElement textCreateAccountErrMsg;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_QC_CIC")
    private WebElement cartQuantity;

    private CustomerSearchPageCSR() {

    }

    public CustomerSearchPageCSR(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Customer Search page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Customer Search page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickCreateAccount(){

        click(linkCreateAccount);

    }

    public void clickSearchButton(){

        click(buttonSearch);

    }

    public void fillInUserInfo(){

        typeIn(fieldFirstName, TestUser.firstName);

        typeIn(fieldLastName, TestUser.lastName);

        typeIn(fieldEmail, TestUser.email);

        typeIn(fieldPostalCode, TestUser.postalCodeBC);

    }

}