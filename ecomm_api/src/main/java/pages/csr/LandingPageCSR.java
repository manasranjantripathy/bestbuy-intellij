package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;
import utils.Constants.Property;
import utils.Constants.TestLanguage;
import utils.Util;

/**
 * Created by sucho on 7/7/2017.
 */
public class LandingPageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String expectedTitle = "Landing Page - BestBuy Canada";
    private final static Logger logger = Logger.getLogger(LandingPageCSR.class);

    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'BrandSelectionUC_BtnGoToOnlineStore')]")
    private WebElement buttonGoToOnlineStore;

    @FindBy(how = How.ID, using = "ctl00_CP_BrandSelectionUC_StateContainer_DdlState")
    private WebElement provinceLocator;

    protected LandingPageCSR(){

    }

    public LandingPageCSR(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            wait.until(ExpectedConditions.titleContains(expectedTitle));
            return true;
        }
        catch (Exception e) {
            logger.error("CSR HOME PAGE IS NOT DISPLAYED! " + e.getMessage(), e);
            throw new RuntimeException("CSR HOME PAGE IS NOT DISPLAYED! ", e);
        }
    }

    public void openPage(final String testEnv) {
        Util.verifyTestEnvironment(testEnv);
        driver.get(testEnv);
        waitUntilPageLoaded();

    }

    public void openPage(){
        String testEnvFromCmd = System.getProperty(Property.TEST_ENVIRONMENT.toString());
        if(testEnvFromCmd == null || testEnvFromCmd.isEmpty()) {
            throw new RuntimeException("Test Environment(URL) is null or empty");
        }

        Util.verifyTestEnvironment(testEnvFromCmd);
        driver.get(testEnvFromCmd);
        waitUntilPageLoaded();
    }

    public void navigateToOnlineStoreByProvince(final String province){

        select(provinceLocator, province);

        click(buttonGoToOnlineStore);

        // In CSR, since we can't change the language through the URL, we change it on the main page.
        if(System.getProperty(Property.TEST_LANGUAGE.toString()).equalsIgnoreCase(TestLanguage.FR.toString())){
            new HeaderFooterCSR(driver).switchToFrench();
        }
    }

}