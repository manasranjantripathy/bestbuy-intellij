package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;

/**
 * Created by sucho on 7/6/2017.
 */
public class WarrantyPopupCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private final static Logger logger = Logger.getLogger(WarrantyPopupCSR.class);

    @FindBy(how = How.XPATH, using = "//*[@id='warranty-modal' and contains(@class,'active')]")
    private WebElement warrantyPopupHeader;

    @FindBy(how = How.XPATH, using = "//div[@class='btn warranty-opt-out-trigger']")
    private WebElement buttonNoThanks;

    @FindBy(xpath = "//button[contains(@class, 'add-warranty')]")
    private WebElement buttonAddWarranty;

    @FindBy(id = "express-checkout")
    private WebElement buttonCheckout;

    @FindBy(id = "proceed-to-cart")
    private WebElement linkViewCart;

    @FindBy(how = How.ID, using = "accept-terms")
    private WebElement checkAcceptTerms;

    @FindBy(xpath = "//div[@class='terms-agreement']//div[@class='error']")
    private WebElement textErrorMessage;

    @FindBy(xpath = "//div[@class='confirmation-message']//span")
    private WebElement textConfirmationMessage;

    @FindBy(xpath = "//div[@id='express-checkout-modal']//div[@class='ui-modal-body']")
    private WebElement expressCheckoutPopup;

    @FindBy(xpath = "//div[@id='warranty-modal']//div//div")
    private WebElement warrantyPopup;

    @FindBy(xpath = "//*[@id=\"add-warranty\"]/div[@class='ui-dropdown-wrapper ui_dropdown']/a")
    private WebElement warrantySelector;

    @FindBy(xpath = "//*[@id=\"add-warranty\"]/section/div")
    private WebElement selectedWarrantyInfo;

    private WarrantyPopupCSR(){

    }

    public WarrantyPopupCSR(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isWarrantyDisplayed(){

        return isDisplayed(warrantyPopup);

    }

    public boolean isExpressCheckoutDisplayed(){

        return isDisplayed(expressCheckoutPopup);

    }

    public boolean isErrorMessageDisplayed(){

        return isDisplayed(textErrorMessage);

    }

    public boolean isWarrantyConfirmationMsgDisplayed(){

        return isDisplayed(textConfirmationMessage);

    }

    public String getSelectedWarrantyName() {
        return getTextOrValue(selectedWarrantyInfo);
    }

    public void selectWarranty(final String warrantySku){

        WebElement warrantySelection = driver.findElement(By.xpath("//*[@id=\"add-warranty\"]/div[@class='ui-dropdown-wrapper ui_dropdown']/a/ul/li[@data-sku='"+ warrantySku +"']"));
        click(warrantySelector);
        click(warrantySelection);

    }

    public void clickCheckout(){

        click(buttonCheckout);

    }

    public void clickNoThanksButton() {

        //click(buttonNoThanks);
        clickByJs(buttonNoThanks);

    }

    public void clickViewCartLink(){

        //click(linkViewCart);
        clickByJs(linkViewCart);

    }

    public void clickAddWarrantyButton(){

        click(buttonAddWarranty);
    }

    public void checkWarrantyAgreement(){

        check(checkAcceptTerms);

    }

}