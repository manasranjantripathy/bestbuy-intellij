package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;
import utils.TestUser;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

/**
 * Created by sucho on 7/7/2017.
 */
public class SelectStorePopupCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor jsExecutor;
    private final static Logger logger = Logger.getLogger(SelectStorePopupCSR.class);

    @FindBy(how = How.XPATH, using = "//div[@class='ui-lightbox-mvc-close']")
    private WebElement iconClose;

    @FindBy(how = How.ID, using = "stbtnSearch")
    private WebElement buttonSearch;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-check-products-in-cart navigation-button btn-primary']")
    private WebElement buttonContinue;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-reserve-now navigation-button btn-primary")
    private WebElement buttonReserve;

    @FindBy(how = How.XPATH, using = "//a[@class='btn btn-small enabled navigation-button btn-one-click")
    private WebElement buttonStoreSelect;

    @FindBy(how = How.XPATH, using = "//a[@class='btn-geo btn-geolocation']")
    private WebElement buttonGeoLocation;

    @FindBy(how = How.ID, using = "reservation-received-ok")
    private WebElement buttonOk;

    @FindBy(how = How.ID, using = "stSearch")
    private WebElement textBoxSearch;

    @FindBy(how = How.CLASS_NAME, using = "ui-lightbox-mvc-iframe")
    private WebElement frameLocator;

    @FindBy(how = How.XPATH, using = "//li[@class='sl-error-message']")
    private WebElement errorMessage;

    @FindBy(how = How.XPATH, using = "//input[contains(@id,'_TxtPromoCode')]")
    private WebElement fieldPromotionalCodes;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_EmailContainer_txtEmail')]")
    private WebElement fieldEmail;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_TxtFirstName')]")
    private WebElement fieldFirstName;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_txtLastName')]")
    private WebElement fieldLastName;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_PhoneContainer_TxtPhone")
    private WebElement fieldPhone0;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_Phone1Container_TxtPhone1")
    private WebElement fieldPhone1;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_Phone2Container_TxtPhone2")
    private WebElement fieldPhone2;

    @FindBy(how = How.ID, using = "ctl00_CP_ContactInfoUC_PhoneExtContainer_TxtPhoneExt")
    private WebElement fieldPhone3;

    @FindBy(how = How.ID, using = "totalsblock")
    private WebElement labelSummary;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'LnkApplyToOrderAndAddMoreCodes')] | //input[@id='ctl00_CP_PromoCodesUC1_BtnUpdate']")
    private WebElement linkPromoApply;

    @FindBy(how = How.XPATH, using = "//a[@class='btn button btn-reserve-now navigation-button btn-primary']")
    private WebElement linkReserveNow;

    @FindBy(how = How.XPATH, using = "//li[div[@class[contains(.,'ehf')]]]/div[1]")
    private WebElement environmentalFee;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Enter Promotional Code')]")
    private WebElement enterPromotionalCodes;

    @FindBy(how = How.XPATH, using = "//span[@class='lnk-reservation-details']/a")
    private WebElement reservationDetailsRIS;

    @FindBy(how = How.ID, using = "ctl00_CP_StoreInfoUC_BtnSelectStore3")
    private WebElement changeRIS;

    @FindBy(how = How.CLASS_NAME, using = "reserve-more-mode-trigger")
    private WebElement reserveMoreItems;

    @FindBy(how = How.XPATH, using = "//*[@id='ctl00_CP_ErrorSummaryUC_vsumErrorSummary']/ul/li | //*[@id='store-locator-list']/div/div[1]/div[2]/div[2]/div[2]/ul/li | //*[@id='store-locator-list']/div/div[1]/div[3]/div[2]/div[2]/ul/li")
    private WebElement textStaticErrorMessage;

    @FindBy(how = How.ID, using = "ctl00_CP_StoresMap_VEMap")
    private WebElement blockStoresInMap;

    @FindBy(how = How.XPATH, using = "//*[@id='store-locator-list']//div[2]/ul/li")
    private WebElement staticErrorMessage;

    @FindBy(how = How.XPATH, using = "//h3[text() = 'Reserve: Contact Information']")
    private WebElement titleLocator;

    @FindBy(how = How.XPATH, using = "//p[@class='success-message']")
    private WebElement successMessage;

    @FindBy(how = How.XPATH, using = "//*[@class='txt-support']/ol/li/strong")
    private WebElement reservedEmail;

    private String reserveNowJS = "document.querySelector(\"a[class='btn button btn-reserve-now navigation-button btn-primary']\")";

    private final String clickStoreJS = "var stores = document.getElementsByClassName"
            + "    (\"btn btn-small enabled navigation-button btn-one-click\");"
            + "var store = stores[%d];"
            + "store.click();";

    private final String selectStoreLocator = "(//a[@class='btn btn-small enabled navigation-button btn-one-click'])[%d]";

    private SelectStorePopupCSR() {

    }

    public SelectStorePopupCSR(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
        jsExecutor = (JavascriptExecutor) driver;
    }

    public boolean isAt() {
        try {
            switchToFrame(frameLocator);

            wait.until(visibilityOfElementLocated(By.xpath("//h3[text() = 'Reserve: Select a Store'] | //h3[text() = 'Réservation : Sélectionnez un magasin.']")));

            switchFrameToDefault();

            return true;
        } catch(Exception e){
            logger.error("Select Store Popup is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Select Store Popup is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isDetailsPageDisplayed(){
        try{
            switchToFrame(frameLocator);

            wait.until(visibilityOfElementLocated(By.xpath("//h3[text() = 'Reserve: Details']")));

            switchFrameToDefault();

            return true;
        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public boolean isContactInformationPageDisplayed(){
        try{
            switchToFrame(frameLocator);

            wait.until(visibilityOfElementLocated(By.xpath("//h3[text() = 'Reserve: Contact Information']")));

            switchFrameToDefault();

            return true;
        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public boolean isSummaryDisplayed() {

        switchToFrame(frameLocator);

        if(getTextOrValue(labelSummary).contains("Summary") | getTextOrValue(labelSummary).contains("Résumé")){
            switchFrameToDefault();
            return true;
        }
        else{
            switchFrameToDefault();
            return false;
        }
    }

    public boolean isSuccessMessageDisplayed() {
        switchToFrame(frameLocator);

        if (getTextOrValue(successMessage).equalsIgnoreCase("Your reservation request has been sent to the store.") |
                getTextOrValue(successMessage).equalsIgnoreCase("Votre demande de réservation a été envoyée au magasin.")) {
            switchFrameToDefault();
            return true;
        }
        else {
            switchFrameToDefault();
            return false;
        }
    }

    public void searchForPostalCode(String postalCode) {

        switchToFrame(frameLocator);

        typeIn(textBoxSearch, postalCode);

        click(buttonSearch);

        switchFrameToDefault();

    }

    private void closeAndReopen() {
        try {
            click(iconClose);

            if (driver.getCurrentUrl().contains("Order/Checkout.aspx")) {
                By reserveInStoreLocator = By.id("ctl00_CP_btnReserveInStore");
                WebElement element = wait.until(visibilityOfElementLocated(reserveInStoreLocator));
                click(element);
            } else if (driver.getCurrentUrl().contains("en-CA/product")) {
                By reserveInStoreLocator = By.id("btn-reserve");
                WebElement element = wait.until(visibilityOfElementLocated(reserveInStoreLocator));
                click(element);
            }
        }
        catch (Exception e){
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void selectStore(int nth) {
        //TODO need to rewrite the code

        try {
            waitUntilPageLoaded();

            switchToFrame(frameLocator);

//try clicking by Javascript
            int index = 1;
            for (; index <= 3; index++) {
                try {
                    jsExecutor.executeScript(String.format(clickStoreJS, nth));
                    break;
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            }
            //try clicking with explicit wait/expected condition
            if (index > 3) {
                try {
                    By locator = By.xpath(String.format(selectStoreLocator, nth));
                    WebElement store = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
                    store.click();
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            }

            switchFrameToDefault();
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public void continueToNextStep() {

        switchToFrame(frameLocator);

        click(buttonContinue);

        switchFrameToDefault();

    }

    public void fillInContactInformation() {
        switchToFrame(frameLocator);

        typeIn(fieldEmail, TestUser.email);
        typeIn(fieldFirstName, TestUser.firstName);
        typeIn(fieldLastName, TestUser.lastName);
        typeIn(fieldPhone0, TestUser.phone0);
        typeIn(fieldPhone1, TestUser.phone1);
        typeIn(fieldPhone2, TestUser.phone2);
        typeIn(fieldPhone3, TestUser.phone3);

        switchFrameToDefault();
    }

    public void clickReserveNow() {

        switchToFrame(frameLocator);

        click(linkReserveNow);

        switchFrameToDefault();

    }
}