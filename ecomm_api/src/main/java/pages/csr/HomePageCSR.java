package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;

/**
 * Created by sucho on 7/6/2017.
 */
public class HomePageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "csr.ca.bestbuy.com";
    private final static Logger logger = Logger.getLogger(HomePageCSR.class);

    private HomePageCSR(){

    }

    public HomePageCSR(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt(){
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Home page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Home page is not displayed " + e.getMessage(), e);
        }
    }

    public void turnOffExpressCheckout(){

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("return window.config.enableExpressCheckout=false;");

    }

    public void turnOnExpressCheckout(){

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("window.config.enableExpressCheckout=true;");

    }
}