package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;

/**
 * Created by sucho on 7/6/2017.
 */
public class ProductDetailsPageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "-ca/product";
    private final static Logger logger = Logger.getLogger(ProductDetailsPageCSR.class);

    @FindBy(how = How.ID, using = "btn-cart")
    private WebElement buttonAddToCart;

    @FindBy(how = How.ID, using = "btn-reserve")
    private WebElement buttonReserveInStore;

    private String buttonAddToCartJS = "btn-cart";
    private String buttonReserveInStoreJS = "btn-reserve";

    private ProductDetailsPageCSR(){

    }

    public ProductDetailsPageCSR(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            logger.error("Product details page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Product details page is not displayed " + e.getMessage(), e);
        }
    }

    public void clickAddToCartButton() {

        //click(buttonAddToCart);
        clickByJs(buttonAddToCart);

    }

    public void clickReserveInStoreButton() {

        //click(buttonReserveInStore);
        clickByJs(buttonReserveInStore);
    }
}