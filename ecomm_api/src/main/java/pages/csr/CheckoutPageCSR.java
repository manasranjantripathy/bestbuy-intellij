package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;
import utils.Constants.Payment;
import utils.TestUser;

/**
 * Created by sucho on 7/6/2017.
 * <p>
 * This class should contain CSR specific elements and behavior on Checkout page.
 */
public class CheckoutPageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "order/checkout.aspx";
    private final static Logger logger = Logger.getLogger(CheckoutPageCSR.class);

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'_btnContinueFromShipping')] | //*[@id='ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_btnContinueFromShipping']")
    private WebElement buttonContinueShipping;

    @FindBy(how = How.XPATH, using = "//*[contains(@id,'CP_checkoutSections_ctl03_ucPaymentEdit_lnkCreditCard')]")
    private WebElement buttonCreditCardSelect;

    @FindBy(how = How.XPATH, using = "//a[contains(@id,'btnSubmitOrder')] | //*[@id='ctl00_CP_BtnSaveBasket']")
    private WebElement buttonSubmit;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CardTypeContainer_DdlCardType")
    private WebElement buttonCreditCardType;

    @FindBy(how = How.XPATH, using = "//input[@id='ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_ChkSameAsShipping']")
    private WebElement checkBoxSameAsShipping;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CreditCardNumberContainer_TxtCardNumber")
    private WebElement fieldCreditCardNumber;

    @FindBy(how = How.XPATH, using = "//input[@id = 'ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CIDNumberContainer_TxtCID']")
    private WebElement fieldCreditCardCVV;

    @FindBy(how = How.XPATH, using = "//input[@id = 'ctl00_CP_checkoutSections_ctl02_ucPaymentSummary_ucCCPaymentApplied_LVCreditCardsView_ctrl0_ctl00_CIDNumberContainer_TxtCID']")
    private WebElement fieldCreditCardCID;

    @FindBy(how = How.XPATH, using = "//*[@id[contains(.,'TxtEmailAddress')]]")
    private WebElement fieldConfirmationEmail;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_pnlPayment")
    private WebElement selectPaymentMethodForm;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_pnlReview")
    private WebElement reviewAndSubmitForm;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_StateContainer_DdlState")
    private WebElement province;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_MonthContainer_DdlMonth")
    private WebElement creditCardExpiryDateMonth;

    @FindBy(how = How.ID, using = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_YearContainer_DdlYear")
    private WebElement creditCardExpiryDateYear;

    private final String postalCodeJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PostalCodeContainer_TxtZipCode";
    private final String firstNameJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_FirstNameContainer_TxtFirstName";
    private final String lastNameJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_LastNameContainer_txtLastName";
    private final String suiteNumberJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_SuiteContainer_TxtSuite";
    private final String streetAddressJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_AddressLine1Container_TxtAddressLine1";
    private final String cityJS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_CityContainer_TxtCity";
    private final String phone_0JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneContainer_TxtPhone";
    private final String phone_1JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone1Container_TxtPhone1";
    private final String phone_2JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_Phone2Container_TxtPhone2";
    private final String phone_3JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneExtContainer_TxtPhoneExt";
    private final String other_phone_0JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOtherContainer_TxtPhoneOther";
    private final String other_phone_1JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOther1Container_TxtPhoneOther1";
    private final String other_phone_2JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOther2Container_TxtPhoneOther2";
    private final String other_phone_3JS = "ctl00_CP_checkoutSections_ctl01_DeliveryOptionTabs1_manageShippingAddress_oeaUseNew_addressUC_PhoneOtherExtContainer_TxtPhoneOtherExt";
    private final String creditCardTypeJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_CardTypeContainer_DdlCardType";
    private final String creditCardExpiryDateMonthJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_MonthContainer_DdlMonth";
    private final String creditCardExpiryDateYearJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_UCEditCreditCard_AddCreditCardUC_YearContainer_DdlYear";
    private final String continuePaymentButtonJS = "ctl00_CP_checkoutSections_ctl03_ucPaymentEdit_BtnContinueFromPayment";

    private CheckoutPageCSR() {

    }

    public CheckoutPageCSR(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        } catch (Exception e) {
            logger.error("Checkout page is not displayed " + e.getMessage(), e);
            throw new RuntimeException("Checkout page is not displayed " + e.getMessage(), e);
        }
    }

    public boolean isCreditCardSelectionDisplayed() {
        return isDisplayed(selectPaymentMethodForm);
    }

    public boolean isReviewAndSubmitFormDisplayed() {
        return isDisplayed(reviewAndSubmitForm);
    }

    public void clickContinuePaymentButton() {
        clickByJSId(continuePaymentButtonJS);
    }

    public void clickSubmitButton() {
        scrollAndClick(buttonSubmit);
    }

    public void clickContinueShippingButton() {
        click(buttonContinueShipping);
    }

    public void clickCreditCardSelectButton() {
        click(buttonCreditCardSelect);
    }

    public void checkSameAsShipping() {
        check(checkBoxSameAsShipping);
    }

    public void fillInShippingInformation() {
        typeIn(postalCodeJS, TestUser.postalCodeBC);
        typeIn(firstNameJS, TestUser.firstName);
        typeIn(lastNameJS, TestUser.lastName);
        typeIn(suiteNumberJS, TestUser.suiteName);
        typeIn(streetAddressJS, TestUser.streetAddress);
        typeIn(cityJS, TestUser.city);

        select(province, TestUser.province);

        typeIn(phone_0JS, TestUser.phone0);
        typeIn(phone_1JS, TestUser.phone1);
        typeIn(phone_2JS, TestUser.phone2);
        typeIn(phone_3JS, TestUser.phone3);

        typeIn(other_phone_0JS, TestUser.otherPhone0);
        typeIn(other_phone_1JS, TestUser.otherPhone1);
        typeIn(other_phone_2JS, TestUser.otherPhone2);
        typeIn(other_phone_3JS, TestUser.otherPhone3);
    }

    public void fillInCreditCardInformation(final String cardType) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            select(buttonCreditCardType, Payment.AMEX_CREDITCARD_TYPE.toString());
            typeIn(fieldCreditCardNumber, Payment.AMEX_CREDITCARD_NUMBER.toString());
            select(creditCardExpiryDateMonth, Payment.AMEX_EXPIRYDATE_MONTH.toString());
            select(creditCardExpiryDateYear, Payment.AMEX_EXPIRYDATE_YEAR.toString());
            typeIn(fieldCreditCardCVV, Payment.AMEX_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            select(buttonCreditCardType, Payment.MC_CREDITCARD_TYPE.toString());
            typeIn(fieldCreditCardNumber, Payment.MC_CREDITCARD_NUMBER.toString());
            select(creditCardExpiryDateMonth, Payment.MC_EXPIRYDATE_MONTH.toString());
            select(creditCardExpiryDateYear, Payment.MC_EXPIRYDATE_YEAR.toString());
            typeIn(fieldCreditCardCVV, Payment.MC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            select(buttonCreditCardType, Payment.PLCC_CREDITCARD_TYPE.toString());
            typeIn(fieldCreditCardNumber, Payment.PLCC_CREDITCARD_NUMBER.toString());
            select(creditCardExpiryDateMonth, Payment.PLCC_EXPIRYDATE_MONTH.toString());
            select(creditCardExpiryDateYear, Payment.PLCC_EXPIRYDATE_YEAR.toString());
            typeIn(fieldCreditCardCVV, Payment.PLCC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            select(buttonCreditCardType, Payment.VISA_CREDITCARD_TYPE.toString());
            typeIn(fieldCreditCardNumber, Payment.VISA_CREDITCARD_NUMBER.toString());
            select(creditCardExpiryDateMonth, Payment.VISA_EXPIRYDATE_MONTH.toString());
            select(creditCardExpiryDateYear, Payment.VISA_EXPIRYDATE_YEAR.toString());
            typeIn(fieldCreditCardCVV, Payment.VISA_CID.toString());
        }
    }

    public void fillInCIDNumber(final String cardType) {
        if (cardType.toLowerCase().equalsIgnoreCase("amex") ||
                cardType.toLowerCase().equalsIgnoreCase("americanexpress") ||
                cardType.toLowerCase().equalsIgnoreCase("american express")) {
            typeIn(fieldCreditCardCID, Payment.AMEX_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("mc") ||
                cardType.toLowerCase().equalsIgnoreCase("mastercard") ||
                cardType.toLowerCase().equalsIgnoreCase("master card")) {
            typeIn(fieldCreditCardCID, Payment.MC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("plcc")) {
            typeIn(fieldCreditCardCID, Payment.PLCC_CID.toString());
        } else if (cardType.toLowerCase().equalsIgnoreCase("visa")) {
            typeIn(fieldCreditCardCID, Payment.VISA_CID.toString());
        }
    }

    public void fillInConfirmationEmail() {
        typeIn(fieldConfirmationEmail, TestUser.email);
    }

}