package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;

/**
 * Created by sucho on 7/6/2017.
 */
public class OrderConfirmationPageCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final String EXPECTED_URL = "/order/ordercomplete.aspx";
    private final static Logger logger = Logger.getLogger(OrderConfirmationPageCSR.class);

    @FindBy(id = "ctl00_CP_ErrorSummaryUC1_ValidationSummary1")
    private WebElement orderFailure;

    @FindBy(xpath = "//div[@id='ctl00_CP_ErrorSummaryUC1_ValidationSummary1']//ul//li")
    private WebElement orderFailureMessage;

    private OrderConfirmationPageCSR() {

    }

    public OrderConfirmationPageCSR(final WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isAt() {
        try {
            waitUntilPageLoaded();
            return driver.getCurrentUrl().toLowerCase().contains(EXPECTED_URL);
        }
        catch (Exception e){
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public boolean isOrderSuccessfullyMade(){
        if(isDisplayed(orderFailure)){
            logger.error("Order is not successfully made. " + getTextOrValue(orderFailureMessage));
            throw new RuntimeException("Order is not successfully made. " + getTextOrValue(orderFailureMessage));
        }
        else{
            return true;
        }
    }
}