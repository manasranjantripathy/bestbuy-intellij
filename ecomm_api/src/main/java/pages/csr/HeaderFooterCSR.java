package pages.csr;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CsrBasePage;
import utils.Constants.Sku;

/**
 * Created by sucho on 7/6/2017.
 */
public class HeaderFooterCSR extends CsrBasePage {
    private WebDriver driver;
    private WebDriverWait wait;
    private final static Logger logger = Logger.getLogger(HeaderFooterCSR.class);

    @FindBy(how = How.XPATH, using = "//i[@class='icon-search']")
    private WebElement buttonSearch;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignIn")
    private WebElement buttonSignIn;

    @FindBy(how = How.XPATH, using = "//input[@id='ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword']")
    private WebElement searchBox;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_QC_CIC")
    private WebElement cartQuantity;

    @FindBy(how = How.ID, using = "my-account-link")
    private WebElement linkMyAccount;

    @FindBy(how = How.ID, using = "my-account-dropdown")
    private WebElement linkMyAccountDropDown;

    @FindBy(how = How.ID, using = "utility-toolbar-create-account")
    private WebElement linkCreateAccount;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignOut")
    private WebElement linkSignOut;

    @FindBy(how = How.ID, using = "ctl00_MasterHeader_ctl00_uchead_languageToggle_french")
    private WebElement linkToFrench;

    private HeaderFooterCSR(){

    }

    public HeaderFooterCSR(final WebDriver driver){
        super(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        PageFactory.initElements(driver, this);
    }

    public boolean isCartEmpty(){

        return getTextOrValue(cartQuantity).isEmpty();

    }

    public void clickSignInButton(){

        click(buttonSignIn);

    }

    public void searchForSku(Sku sku) {

        typeIn(searchBox, sku.toString());

        click(buttonSearch);

    }

    public void searchFor(final Sku sku) {

        typeIn(searchBox, sku.toString());

        click(buttonSearch);

    }

    public void searchFor(final String item) {

        typeIn(searchBox, item);

        click(buttonSearch);

    }

    public void navigateToCreateAccountPage() {

        click(linkMyAccount);

        click(linkCreateAccount);
    }

    public void switchToFrench(){

        click(linkToFrench);

    }

}