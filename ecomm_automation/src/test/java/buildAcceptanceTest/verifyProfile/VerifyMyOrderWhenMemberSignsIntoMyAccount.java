package buildAcceptanceTest.verifyProfile;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;

/**
 * Created by sucho on 8/22/2017.
 */
public class VerifyMyOrderWhenMemberSignsIntoMyAccount extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyMyOrderWhenMemberSignsIntoMyAccount(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserClicksOrderStatusAfterLoggingIn() {
        TestUser testUser = new TestUser();
        pages.headerFooter().clickSignInButton();
        pages.signInPage().signInAsDefaultCustomer(testUser);
        pages.headerFooter().clickOrderStatus();

        Assert.assertTrue(pages.orderHistoryPage().isAt());
    }

    @Test(priority = 3)
    public void verifyOrderStatusPage(){
        Assert.assertTrue(pages.orderHistoryPage().isSearchByOrderDateDisplayed());
        Assert.assertTrue(pages.orderHistoryPage().isViewByOrderNumberDisplayed());
        Assert.assertTrue(pages.orderHistoryPage().isOrderHistoryTableDisplayed());
    }
}