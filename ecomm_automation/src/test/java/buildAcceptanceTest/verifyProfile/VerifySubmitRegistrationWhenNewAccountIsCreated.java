package buildAcceptanceTest.verifyProfile;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;

/**
 * Created by sucho on 8/22/2017.
 */
public class VerifySubmitRegistrationWhenNewAccountIsCreated extends BaseTest {
    private DesktopPageFactory pages;

    public VerifySubmitRegistrationWhenNewAccountIsCreated(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserClicksOnCreateAccountLink(){
        pages.headerFooter().navigateToCreateAccountPage();

        Assert.assertTrue(pages.createAccountPage().isAt());
    }

    @Test(priority = 3)
    public void verifyConfirmationPageAfterUserCreateAccount(){
        pages.createAccountPage().createRandomAccount();

        Assert.assertTrue(pages.accountSummaryPage().isAt());
    }


}