package buildAcceptanceTest.verifyLocationService;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;

/**
 * Created by Manas on 8/18/2017.
 */
public class VerifyStoreDetailsWhenBrowsingStoreLocatorPage extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyStoreDetailsWhenBrowsingStoreLocatorPage(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserNavigateToStoreLocatorPage(){
        pages.headerFooter().clickFindAStoreLink();

        Assert.assertTrue(pages.storeLocatorPage().isAt());
    }

    @Test(priority = 3)
    public void theUserSearchesByPostalCode(){
        pages.storeLocatorPage().fillInPostalCodeField(new TestUser().getPostalCodeBC());

        Assert.assertTrue(pages.storeLocatorPage().isSearchResultsDisplayed());
    }

}