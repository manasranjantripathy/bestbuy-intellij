package buildAcceptanceTest.verifyCart;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.Util;

/**
 * Created by Manas on 8/16/2017.
 */
public class VerifyPSPWhenUserIsOnThePDPAndOnTheCartPage extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyPSPWhenUserIsOnThePDPAndOnTheCartPage(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClicksSeeAllAccessoriesAndGetProtectionPlan(){
        pages.cartPage().clickSeeAllAccessoriesLink();
        pages.cartPage().addProtectionServicePlan(3);

        Assert.assertTrue(pages.cartPage().isPspAddedInTheCart());
    }

    @Test(priority = 5)
    public void theUserRemovesItemAndVerifyPspIsRemoved(){
        pages.cartPage().clickRemoveItem();

        Assert.assertTrue(pages.cartPage().isCartEmpty());
    }
}