package buildAcceptanceTest.verifySearch;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;

/**
 * Created by Manas on 9/5/2017.
 */
public class VerifySkuOnPLP extends BaseTest {
    private final static String TEST_ENV = "http://test3-bbyca.ca.bestbuy.com/";
    private DesktopPageFactory pages;

    public VerifySkuOnPLP(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    @Parameters("testKeyword")
    public void theUserSearchForSku(final String testKeyword) {
        pages.headerFooter().searchFor(Sku.valueOf(testKeyword));

        Assert.assertTrue(pages.productlistingPage().isAt(), "Product listing page is not displayed.");
    }

    @Test(priority = 3)
    @Parameters("expectedOutput")
    public void verifyThatPlpAvailableSkuIsDisplayed(final String expectedOutput) {

        Assert.assertTrue(pages.productlistingPage().isSkuDisplayed(Sku.valueOf(expectedOutput)), Sku.valueOf(expectedOutput) + " is not displayed.");
    }
}