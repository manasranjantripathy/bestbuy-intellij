package buildAcceptanceTest.verifyInterstitial;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;

/**
 * Created by sucho on 8/16/2017.
 */
public class VerifyInterstitialPageWhenSelectingEditCart extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyInterstitialPageWhenSelectingEditCart(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }
    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Sku.wgEhfIspuSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();
        pages.warrantyPopup().addProtectionPlan();

        Assert.assertTrue(pages.interstitialPage().isAt(), "Interstitial page is not displayed.");
    }

    @Test(priority = 4)
    public void verifyProtectionPlanInCartPage(){
        pages.interstitialPage().clickEditCartButton();

        Assert.assertTrue(pages.cartPage().isPspAddedInTheCart());
    }
}