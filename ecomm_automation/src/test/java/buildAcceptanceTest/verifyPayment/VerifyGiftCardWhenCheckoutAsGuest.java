package buildAcceptanceTest.verifyPayment;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 8/21/2017.
 */
public class VerifyGiftCardWhenCheckoutAsGuest extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyGiftCardWhenCheckoutAsGuest() {
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 5)
    public void theUserContinueCheckoutAsGuest() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 6)
    public void theUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation(new TestUser());
        pages.checkoutPage().clickContinueShippingButton();
        pages.checkoutPage().clickGiftCardButton();

        Assert.assertTrue(pages.checkoutPage().isGiftCardSectionPresent());
    }

    @Test(priority = 7)
    public void theUserSelectGiftCardAsPaymentType() {
        pages.checkoutPage().addGiftCard(4);
        pages.checkoutPage().checkGiftCardBalance();

        Assert.assertTrue(pages.checkoutPage().isGiftCardBalanceNotZero());

        pages.checkoutPage().clickApplyGCToOrderButton();
    }

    @Test(priority = 8)
    public void fillInCardInformationAndContinue() {
        pages.checkoutPage().checkSameAsShippingAddress();
        pages.checkoutPage().fillInConfirmationEmail(new TestUser());
        pages.checkoutPage().clickContinuePaymentButton();
    }

    @Test(priority = 9)
    public void selectSubmitOrder() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}