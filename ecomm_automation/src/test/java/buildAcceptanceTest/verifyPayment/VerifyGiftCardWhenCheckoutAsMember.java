package buildAcceptanceTest.verifyPayment;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 8/21/2017.
 */
public class VerifyGiftCardWhenCheckoutAsMember extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyGiftCardWhenCheckoutAsMember() {
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        TestUser testUser = new TestUser();
        pages.headerFooter().clickSignInButton();
        pages.signInPage().signInAsCustomer(testUser);

        Util.emptyUserCart(pages);

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSkuAfterLoggingIn() {

        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserContinueCheckOutAsMember() {
        pages.cartPage().clickCheckoutButton();

        pages.checkoutPage().clickContinueShippingButton();

    }

    @Test(priority = 5)
    public void theUserEditGiftCardAsPaymentType() {

        pages.checkoutPage().clickGiftCardButton();

        Assert.assertTrue(pages.checkoutPage().isGiftCardSectionPresent());

        pages.checkoutPage().addGiftCard(4);
        pages.checkoutPage().checkGiftCardBalance();

        Assert.assertTrue(pages.checkoutPage().isGiftCardBalanceNotZero());

        pages.checkoutPage().clickApplyGCToOrderButton();

    }

    @Test(priority = 6)
    public void continueCheckoutWithGC() {
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 7)
    public void fillInCardInformationAndContinue() {
        pages.checkoutPage().checkSameAsShippingAddress();
        pages.checkoutPage().clickContinuePaymentButton();
    }

    @Test(priority = 8)
    public void selectSubmitOrder() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }

}