package buildAcceptanceTest.verifyPayment;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by Manas on 8/21/2017.
 */
public class VerifyCreditCardWhenCheckoutAsGuest extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyCreditCardWhenCheckoutAsGuest(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }
    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 5)
    public void theUserContinueCheckoutAsGuest() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 6)
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation(new TestUser());
        pages.checkoutPage().clickContinueShippingButton();

        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Test(priority = 7)
    @Parameters("paymentType")
    public void theUserClickContinuePaymentButtonAfterFillInRequiredInformation(@Optional("visa") final String paymentType) {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().fillInCreditCardInformation(paymentType);
        pages.checkoutPage().checkSameAsShipping();
        pages.checkoutPage().fillInConfirmationEmail(new TestUser());
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Test(priority = 8)
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}