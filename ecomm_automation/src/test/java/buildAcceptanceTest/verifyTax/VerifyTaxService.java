package buildAcceptanceTest.verifyTax;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 9/6/2017.
 */
public class VerifyTaxService extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyTaxService(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    @Parameters("testSku")
    public void theUserSearchForSku(final String testSku) {
        pages.headerFooter().searchFor(Sku.valueOf(testSku));

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Interstitial page is not displayed.");
    }

    @Test(priority = 4)
    @Parameters("province")
    public void theUserEntersBCPostalCode(final String province){
        pages.cartPage().clickDeliveryChangeLink();

        if(province.equalsIgnoreCase("bc")) {
            pages.cartPage().typePostalCodeOnChangeDeliveryLocationPopup(new TestUser().getPostalCodeBC());
        }
        else if(province.equalsIgnoreCase("on")){
            pages.cartPage().typePostalCodeOnChangeDeliveryLocationPopup(new TestUser().getPostalCodeON());
        }
        else if(province.equalsIgnoreCase("mb")){
            pages.cartPage().typePostalCodeOnChangeDeliveryLocationPopup(new TestUser().getPostalCodeMB());
        }
        else if(province.equalsIgnoreCase("qc")){
            pages.cartPage().typePostalCodeOnChangeDeliveryLocationPopup(new TestUser().getPostalCodeMB());
        }

        Assert.assertTrue(pages.cartPage().isDeliveryOptionsDisplaysInformation());

        pages.cartPage().clickSubmitButtonOnDeliveryOption();
    }

    @Test(priority = 5)
    @Parameters({"pst", "gst", "hst", "total"})
    public void verifyTaxInCartPage(final String pst, final String gst, final String hst, final String total){
        Assert.assertTrue(pages.cartPage().getEstimatedPSTorQST().contains(pst), String.format("Expected output: %s | Actual output: %s", pst, pages.cartPage().getEstimatedPSTorQST()));
        Assert.assertTrue(pages.cartPage().getEstimatedGST().contains(gst), String.format("Expected output: %s | Actual output: %s", gst, pages.cartPage().getEstimatedGST()));
        Assert.assertTrue(pages.cartPage().getEstimatedHST().contains(hst), String.format("Expected output: %s | Actual output: %s", hst, pages.cartPage().getEstimatedHST()));
        Assert.assertTrue(pages.cartPage().getTotal().contains(total), String.format("Expected output: %s | Actual output: %s", total, pages.cartPage().getTotal()));
    }

}