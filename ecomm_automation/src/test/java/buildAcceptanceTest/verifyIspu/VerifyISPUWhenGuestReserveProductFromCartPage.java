package buildAcceptanceTest.verifyIspu;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by Manas on 8/16/2017.
 */
public class VerifyISPUWhenGuestReserveProductFromCartPage extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyISPUWhenGuestReserveProductFromCartPage(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClickOnReserveInStoreButtonFromCartPage() {
        pages.cartPage().clickReserveInStore();

        Assert.assertTrue(pages.selectStorePopup().isAt(), "Select store popup is not displayed.");
    }

    @Test(priority = 5)
    public void theUserSearchForStoresByBCPostalCodeAndSelectOne() {
        pages.selectStorePopup().searchForPostalCode(new TestUser().getPostalCodeBC());
        pages.selectStorePopup().selectStore(1);

        Assert.assertTrue(pages.selectStorePopup().isSummaryDisplayed(), "Summary is not displayed.");
    }

    @Test(priority = 6)
    public void theUserClickOnReserveNowButtonAfterFillInContactInformation() {
        pages.selectStorePopup().continueToNextStep();
        pages.selectStorePopup().fillInContactInformation(new TestUser());
        pages.selectStorePopup().clickReserveNow();

        Assert.assertTrue(pages.selectStorePopup().isSuccessMessageDisplayed(), "Success message is not displayed.");
    }

}