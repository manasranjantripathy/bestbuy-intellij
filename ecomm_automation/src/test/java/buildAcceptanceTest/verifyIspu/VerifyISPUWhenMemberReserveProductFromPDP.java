package buildAcceptanceTest.verifyIspu;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;

/**
 * Created by sucho on 8/16/2017.
 */
public class VerifyISPUWhenMemberReserveProductFromPDP extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyISPUWhenMemberReserveProductFromPDP(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSkuAfterLoggingIn() {
        TestUser testUser = new TestUser();
        pages.headerFooter().clickSignInButton();
        pages.signInPage().signInAsDefaultCustomer(testUser);
        pages.headerFooter().searchFor(Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClickOnReserveInStoreButtonFromProductDetailsPage() {
        pages.productDetailsPage().clickReserveInStoreButton();
        pages.warrantyPopup().clickNoThanksButton();

        Assert.assertTrue(pages.selectStorePopup().isAt(), "Select store popup is not displayed.");
    }

    @Test(priority = 4)
    public void theUserSearchForStoresByBCPostalCodeAndSelectOne() {
        pages.selectStorePopup().searchForPostalCode(new TestUser().getPostalCodeBC());
        pages.selectStorePopup().selectStore(1);

        Assert.assertTrue(pages.selectStorePopup().isSummaryDisplayed(), "Summary is not displayed.");
    }

    @Test(priority = 5)
    public void theUserClickOnReserveNowButton() {
        pages.selectStorePopup().continueToNextStep();
        pages.selectStorePopup().clickReserveNow();

        Assert.assertTrue(pages.selectStorePopup().isSuccessMessageDisplayed(), "Success message is not displayed.");
    }
}