package buildAcceptanceTest.verifyPDP;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.Sku;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 8/22/2017.
 */
public class VerifyShippingPageDeliveryOptionsAvailableForePreOrderItem extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyShippingPageDeliveryOptionsAvailableForePreOrderItem(){

    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForPreOrderSkuSameReleaseDateSku() {
        pages.headerFooter().searchFor(Sku.preOrderSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Interstitial page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 5)
    public void theUserContinueCheckoutAsMemberAndVerifyPreOrder(){
        TestUser testUser = new TestUser();
        pages.secureCheckoutPage().continueCheckoutAsMember(testUser);

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
        Assert.assertTrue(pages.checkoutPage().isPreOrderInfoDisplayed("Dec. 09, 2020"));
    }
}