package utils;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import utils.Constants.Property;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.environments.*;
import utils.environments.macfortress.ChromeDriverServiceFactoryMacFortress;
import utils.environments.macfortress.DesiredCapabilitiesFactoryMacFortress;
import utils.environments.windows.ChromeDriverServiceFactoryWindows;
import utils.environments.windows.DesiredCapabilitiesFactoryWindows;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by manas on 8/4/2017.
 */
public class BaseTest {
    private final static Logger logger = Logger.getLogger(BaseTest.class);
    private static EnvironmentChecks environmentChecks = new EnvironmentChecks();
    private static List<ChromeDriverServiceFactory> chromeDriverServiceFactories = Lists.newArrayList(
            new ChromeDriverServiceFactoryWindows(environmentChecks),
            new ChromeDriverServiceFactoryMacFortress(environmentChecks)
    );
    private static List<DesiredCapabilitiesFactory> desiredCapabilitiesFactories = Lists.newArrayList(
            new DesiredCapabilitiesFactoryWindows(environmentChecks),
            new DesiredCapabilitiesFactoryMacFortress(environmentChecks)
    );
    private static ChromeDriverServiceFactory chromeDriverServiceFactory;
    private static DesiredCapabilitiesFactory desiredCapabilitiesFactory;

    static {
        try {
            chromeDriverServiceFactory =
                    new ChromeDriverServiceFactoryLocator(chromeDriverServiceFactories).getChromeDriverServiceFactory();
            desiredCapabilitiesFactory =
                    new DesiredCapabilitiesFactoryLocator(desiredCapabilitiesFactories).getDesiredCapabilitiesFactory();
        } catch ( NoSuchElementException e ) {
            new SetupInstructions().print();
            throw ( e );
        }
    }

    private static ChromeDriverService service;
    protected WebDriver driver;
    private final static String URL_PATTERN = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    protected static String TEST_PLATFORM;
    protected static String TEST_ENVIRONMENT;
    protected static String TEST_LANGUAGE;

    @BeforeSuite(alwaysRun = true)
    @Parameters({"testPlatform", "testEnvironment", "testLanguage"})
    public static void createAndStartService(@Optional("")final String testPlatform,
                                             @Optional("")final String testEnvironment,
                                             @Optional("")final String testLanguage) {
        try {
            service = chromeDriverServiceFactory.makeChromeDriverService();
            service.start();
            setTestConfiguration(testPlatform, testEnvironment, testLanguage);

            // check test environment status
            try {
                Util.verifyTestEnvironment(System.getProperty(Property.TEST_ENVIRONMENT.toString()));
            }
            catch(Exception e){
                System.exit(1);
            }
        } catch (IOException e) {
            e.getMessage();
        }
    }

    @AfterSuite(alwaysRun = true)
    public void createAndStopService() {
        service.stop();
    }

    @BeforeTest(alwaysRun = true)
    @Parameters("testPlatform")
    public void createDriver(@Optional("") String testPlatform) {
        if(!testPlatform.isEmpty()){
            System.setProperty("testPlatform", testPlatform);
            logger.info("Test platform is defined from the testng.xml file: " + testPlatform);
        }
        switch (System.getProperty(Property.TEST_PLATFORM.toString()).toLowerCase()){
            case "storefront":
                driver = new RemoteWebDriver(service.getUrl(), desiredCapabilitiesFactory.makeDesiredCapabilities());
                //driver.manage().window().maximize();
                break;
            case "csr":
                driver = new RemoteWebDriver(service.getUrl(), desiredCapabilitiesFactory.makeDesiredCapabilities());
                //driver.manage().window().maximize();
                break;
            case "mobile":
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "iPhone 6");

                Map<String, Object> chromeOptions = new HashMap<>();
                chromeOptions.put("mobileEmulation", mobileEmulation);

                DesiredCapabilities capabilities = desiredCapabilitiesFactory.makeDesiredCapabilities();
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

                driver = new RemoteWebDriver(service.getUrl(), capabilities);
                driver.manage().window().setSize(new Dimension(120, 800));
                break;
            case "tablet_768_1024":
                Map<String, String> tabletEmulation1 = new HashMap<>();
                tabletEmulation1.put("deviceName", "iPad");

                Map<String, Object> chromeOptions1 = new HashMap<>();
                chromeOptions1.put("mobileEmulation", tabletEmulation1);

                DesiredCapabilities capabilities1 = desiredCapabilitiesFactory.makeDesiredCapabilities();
                capabilities1.setCapability(ChromeOptions.CAPABILITY, chromeOptions1);

                driver = new RemoteWebDriver(service.getUrl(), capabilities1);
                driver.manage().window().setSize(new Dimension(768, 1024));

                break;
            case "tablet_1024_1366":
                Map<String, String> tabletEmulation2 = new HashMap<>();
                tabletEmulation2.put("deviceName", "iPad Pro");

                Map<String, Object> chromeOptions2 = new HashMap<>();
                chromeOptions2.put("mobileEmulation", tabletEmulation2);

                DesiredCapabilities capabilities2 = desiredCapabilitiesFactory.makeDesiredCapabilities();
                capabilities2.setCapability(ChromeOptions.CAPABILITY, chromeOptions2);

                driver = new RemoteWebDriver(service.getUrl(), capabilities2);
                driver.manage().window().setSize(new Dimension(1024, 1366));
                break;

            case "tablet_600_800":
                Map<String, Object> deviceMetrics = new HashMap<>();
                deviceMetrics.put("width", 600);
                deviceMetrics.put("height", 800);
                deviceMetrics.put("pixelRatio", 3.0);

                Map<String, Object> mobileEmulations = new HashMap<>();
                mobileEmulations.put("deviceMetrics", deviceMetrics);
                mobileEmulations.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");

                ChromeOptions chromeOptionss = new ChromeOptions();
                chromeOptionss.setExperimentalOption("mobileEmulation", mobileEmulations);

                DesiredCapabilities capabilities3 = desiredCapabilitiesFactory.makeDesiredCapabilities();
                capabilities3.setCapability(ChromeOptions.CAPABILITY, capabilities3);

                driver = new RemoteWebDriver(service.getUrl(), chromeOptionss);
                driver.manage().window().setSize(new Dimension(600, 950));

                break;
            default:
                driver = new RemoteWebDriver(service.getUrl(), desiredCapabilitiesFactory.makeDesiredCapabilities());
                break;
        }

    }

    @AfterTest(alwaysRun = true)
    public void quitDriver() {
        driver.manage().deleteAllCookies();
        driver.quit();
    }

    private static void setTestConfiguration(String testPlatform, String testEnvironment, String testLanguage){
        // set testPlatform
        //if user is not using mvn cmd
        if(System.getProperty(Property.TEST_PLATFORM.toString()) == null || System.getProperty(Property.TEST_PLATFORM.toString()).isEmpty()){
            // if user is using test class
            if(testPlatform == null || testPlatform.isEmpty()){
                if(TEST_PLATFORM == null || TEST_PLATFORM.isEmpty()){
                    // nothing is provided, using storefront by default
                    logger.info("No test platform is defined, using storefront by default: " + TestPlatform.STOREFRONT.toString());
                    System.setProperty(Property.TEST_PLATFORM.toString(), TestPlatform.STOREFRONT.toString());
                }
                else {
                    logger.info("Test platform is defined from the testClass.java file: " + TEST_PLATFORM);
                    System.setProperty(Property.TEST_PLATFORM.toString(), TEST_PLATFORM);
                }
            }
            // if user is using test runner xml file
            else {
                logger.info("Test platform is defined from the testng.xml file: " + testPlatform);
                System.setProperty(Property.TEST_PLATFORM.toString(), testPlatform);
            }
        }
        else{
            logger.info("Test platform is defined from the mvn command through -DtestPlatform = " + System.getProperty(Property.TEST_PLATFORM.toString()));
        }

        // set testLanguage
        if(System.getProperty(Property.TEST_LANGUAGE.toString()) == null || System.getProperty(Property.TEST_LANGUAGE.toString()).isEmpty()){
            if(testLanguage == null || testLanguage.isEmpty()){
                if(TEST_LANGUAGE == null || TEST_LANGUAGE.isEmpty()){
                    // nothing is provided, using English by default
                    logger.info("No test language is defined, using English by default: " + TestLanguage.EN.toString());
                    System.setProperty(Property.TEST_LANGUAGE.toString(), TestLanguage.EN.toString());
                }
                else {
                    logger.info("Test language is defined from the testClass.java file: " + TEST_LANGUAGE);
                    System.setProperty(Property.TEST_LANGUAGE.toString(), TEST_LANGUAGE);
                }
            }
            else {
                logger.info("Test language is defined from the testng.xml file: " + testLanguage);
                System.setProperty(Property.TEST_LANGUAGE.toString(), testLanguage);
            }
        }
        else{
            logger.info("Test language is defined from the mvn command through -DtestLanguage = " + System.getProperty(Property.TEST_LANGUAGE.toString()));
        }

        // set testEnvironment
        if(System.getProperty(Property.TEST_ENVIRONMENT.toString()) == null || System.getProperty(Property.TEST_ENVIRONMENT.toString()).isEmpty()){
            if(testEnvironment == null || testEnvironment.isEmpty()){
                if(TEST_ENVIRONMENT == null || TEST_ENVIRONMENT.isEmpty()){
                    // nothing is provided, using test03 by default
                    logger.info("No test environment is defined, using Test03 by default: " + TestEnvironment.STOREFRONT_TEST03.toString());
                    System.setProperty(Property.TEST_ENVIRONMENT.toString(), TestEnvironment.STOREFRONT_TEST03.toString());
                }
                else {
                    System.setProperty(Property.TEST_ENVIRONMENT.toString(), getTestEnvironment(TEST_ENVIRONMENT, System.getProperty(Property.TEST_LANGUAGE.toString())));
                    logger.info("Test environment is defined from the testClass.java file: " + System.getProperty(Property.TEST_ENVIRONMENT.toString()));
                }
            }
            else {
                logger.info("Test environment is defined from the testng.xml file: " + testEnvironment);
                System.setProperty(Property.TEST_ENVIRONMENT.toString(), getTestEnvironment(testEnvironment, System.getProperty(Property.TEST_LANGUAGE.toString())));
            }
        }
        else{
            // do not change order in these 2 lines.
            System.setProperty(Property.TEST_ENVIRONMENT.toString(), getTestEnvironment(System.getProperty(Property.TEST_ENVIRONMENT.toString()), System.getProperty(Property.TEST_LANGUAGE.toString())));
            logger.info("Test environment is defined from the mvn command through -DtestEnvironment = " + System.getProperty(Property.TEST_ENVIRONMENT.toString()));
        }
    }

    private static String getTestEnvironment(String testEnvironment, String testLanguage){
        if(!testEnvironment.matches(URL_PATTERN)){
            logger.error("Invalid URL: " + testEnvironment);
            throw new RuntimeException("Invalid URL: " + testEnvironment);
        }
        else {
            // set test environment by the language type
            if (testLanguage.equalsIgnoreCase("fr") | testLanguage.equalsIgnoreCase("french")) {
                if (testEnvironment.toLowerCase().contains("en-ca")) {
                    testEnvironment = testEnvironment.toLowerCase().replace("en-ca", "fr-ca");
                    return testEnvironment;
                } else if (testEnvironment.toLowerCase().contains("fr-ca")) {
                    // do nothing
                    return testEnvironment;
                } else {
                    if (System.getProperty(Property.TEST_PLATFORM.toString()).equalsIgnoreCase(TestPlatform.CSR.toString())) {
                        // csr has to go through landing page. we cannot change language through the url.
                        return testEnvironment;
                    }
                    else if (System.getProperty(Property.TEST_PLATFORM.toString()).equalsIgnoreCase(TestPlatform.STOREFRONT.toString())) {
                        testEnvironment = testEnvironment.concat("/fr-ca/home.aspx");
                        return testEnvironment;
                    }
                    else{
                        testEnvironment = testEnvironment.concat("/fr-ca");
                        return testEnvironment;
                    }
                }
            } else if (testLanguage.equalsIgnoreCase("en") | testLanguage.equalsIgnoreCase("english")) {
                if (testEnvironment.toLowerCase().contains("fr-ca")) {
                    testEnvironment = testEnvironment.toLowerCase().replace("fr-ca", "en-ca");
                    return testEnvironment;
                } else {
                    return testEnvironment;
                }
            } else {
                logger.error("testLanguage is not defined. Using English by default");
                return testEnvironment;
            }
        }
    }

}