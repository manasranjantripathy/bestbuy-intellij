package utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import org.testng.annotations.Test;

import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * * Created by sucho on 2/27/2017.
 *
 * what this has
 *      - update test result on Jira
 *      - skip following steps if there is any failure in preceding steps
 *      - set testPlatform, testEnvironment and testLanguage to display correct info on allure report
 *
 * what this doesn't have
 *
 */
public class RegressionListener implements ISuiteListener, ITestListener, IInvokedMethodListener {
    private boolean hasFailures = false;

    private JiraUpdater jiraUpdater = new JiraUpdater();
    private static final String JIRAID = "jiraId";
    private static final String PASSED = "passed";
    private static final String FAILED = "failed";
    private static final String WIP = "wip";

    private final static Logger logger = Logger.getLogger(RegressionListener.class);

    @Override
    public void onFinish(ISuite iSuite){

    }

    @Override
    public void onStart(ISuite iSuite){
        // set current date time
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        String suiteName = iSuite.getXmlSuite().getName();
        suiteName = suiteName + " " + localDateTime.format(formatter);
        iSuite.getXmlSuite().setName(suiteName);

        // set testPlatform, testEnvironment and testLanguage to display correct info on allure report
        if(System.getProperty(Constants.Property.TEST_PLATFORM.toString()) != null ||
                System.getProperty(Constants.Property.TEST_ENVIRONMENT.toString()) != null ||
                System.getProperty(Constants.Property.TEST_LANGUAGE.toString())!= null) {
            HashMap<String, String> parameter = new HashMap<>();
            parameter.put(Constants.Property.TEST_PLATFORM.toString(), System.getProperty(Constants.Property.TEST_PLATFORM.toString()));
            parameter.put(Constants.Property.TEST_ENVIRONMENT.toString(), System.getProperty(Constants.Property.TEST_ENVIRONMENT.toString()));
            parameter.put(Constants.Property.TEST_LANGUAGE.toString(), System.getProperty(Constants.Property.TEST_LANGUAGE.toString()));
            iSuite.getXmlSuite().setParameters(parameter);
        }
    }

    /**
     * This method is being called before each test case.
     * It will update the Jira test status to WIP(Work In Progress).
     * When it finishes, with 'onFinish' listener method, the result will be updated.
     * @param context
     */
    @Override
    public void onStart(ITestContext context) {

        // Following feature is currently disabled due to ZAPI license.
        // If JiraID is not provided from the testNg.xml, do not update result on Jira.
        /*
        if(context.getCurrentXmlTest().getParameter(JIRAID) != null && !context.getCurrentXmlTest().getParameter(JIRAID).isEmpty()){
            jiraUpdater.updateResult(context.getCurrentXmlTest().getParameter(JIRAID), WIP);
        }
        */


        /**
         * If labels contain 'passed' or 'failed' already, remove them
         */
        if(context.getCurrentXmlTest().getParameter(JIRAID) != null && !context.getCurrentXmlTest().getParameter(JIRAID).isEmpty()){

            jiraUpdater.resetLabels(context.getCurrentXmlTest().getParameter(JIRAID));

        }
    }

    /**
     * This method is being called after each test case.
     * It will update the Jira test status.
     * @param context
     */
    @Override
    public void onFinish(ITestContext context) {

        // Following feature is currently disabled due to ZAPI license.
        // If JiraID is not provided from the testNg.xml, do not update result on Jira.
        /*
        if(context.getCurrentXmlTest().getParameter(JIRAID) != null && !context.getCurrentXmlTest().getParameter(JIRAID).isEmpty()){

            if(context.getFailedTests().size() > 0){
                jiraUpdater.updateResult(context.getCurrentXmlTest().getParameter(JIRAID), FAILED);
            }
            else{
                jiraUpdater.updateResult(context.getCurrentXmlTest().getParameter(JIRAID), PASSED);
            }
        }
        */

        /**
         * depending on the test result add 'passed' or 'failed' label
         */
        if(context.getCurrentXmlTest().getParameter(JIRAID) != null && !context.getCurrentXmlTest().getParameter(JIRAID).isEmpty()){

            if(context.getFailedTests().size() > 0){
                jiraUpdater.updateLabels(context.getCurrentXmlTest().getParameter(JIRAID), FAILED);
            }
            else{
                jiraUpdater.updateLabels(context.getCurrentXmlTest().getParameter(JIRAID), PASSED);
            }
        }
    }


    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        synchronized (this) {
            if (method.isTestMethod() && hasFailures) {
                throw new SkipException("Skipping this test");
            }
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (!method.isTestMethod()) {
            hasFailures = false;
            return;
        }
        if (!testResult.isSuccess()) {
            synchronized (this) {
                hasFailures = true;
            }
        }
    }

    @Override
    public void onTestStart(ITestResult result) {
        if(isDataProviderUsed(result)){
            jiraUpdater.resetLabels(((String[])result.getParameters()[0])[0].toString());
        }
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        if(isDataProviderUsed(result)){
            jiraUpdater.updateLabels(((String[])result.getParameters()[0])[0].toString(), PASSED);
        }
    }

    @Override
    public void onTestFailure(ITestResult result) {
        if(isDataProviderUsed(result)) {
            jiraUpdater.updateLabels(((String[])result.getParameters()[0])[0].toString(), FAILED);
        }

        Object testClass = result.getInstance();
        WebDriver driver = ((BaseTest) testClass).driver;

        if(driver instanceof WebDriver){
            Util.saveScreenshotPNG(driver);
        }

    }

    @Override
    public void onTestSkipped(ITestResult result) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    private boolean isDataProviderUsed(ITestResult result){
        // check if parameter is used in test
        if(result.getParameters().length > 0){
            Annotation[] annotations = result.getMethod().getConstructorOrMethod().getMethod().getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                if(annotation instanceof Test){
                    Test test = (Test) annotation;
                    // check if dataProvider is used in test
                    if(test.dataProvider() != null && !test.dataProvider().isEmpty()){
                        return true;
                    }
                }
            }
        }

        return false;
    }
}