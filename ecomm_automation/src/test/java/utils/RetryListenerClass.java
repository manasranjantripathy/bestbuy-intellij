package utils;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import utils.email.EmailMessage;
import utils.email.EmailObject;
import utils.email.EmailSender;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RetryListenerClass implements IAnnotationTransformer, IRetryAnalyzer, ISuiteListener {
    private final static Logger logger = Logger.getLogger(RetryListenerClass.class);

    private int retryCnt = 0;
    private int maxRetryCnt = 2;
    private int minute = 1;

    protected ClassPathXmlApplicationContext context;
    private String accessToken;

    private EmailSender emailSender;
    private EmailObject emailObject;

    @Override
    public void transform(ITestAnnotation testannotation, Class testClass, Constructor testConstructor, Method testMethod) {
        IRetryAnalyzer retry = testannotation.getRetryAnalyzer();

        if (retry == null) {
            testannotation.setRetryAnalyzer(RetryListenerClass.class);
        }

    }

    @Override
/**
 * This method will be called everytime a test fails. It will return TRUE if a test fails and need to be retried, else it returns FALSE
 * @param result
 * @return
 */
    public boolean retry(ITestResult result) {
        long startTime = 0;
        try {
            startTime = System.nanoTime();
            Thread.sleep(minute * 10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long duration = System.nanoTime() - startTime;
        logger.info(retryCnt + " time to retry " + TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
        if (retryCnt < maxRetryCnt) {
            if (!result.isSuccess()) {
                logger.info("Retrying " + result.getName() + " again and the count is " + (retryCnt + 1));
                retryCnt++;
                return true;
            }
        }
        return false;
    }

    @Override
    public void onStart(ISuite iSuite) {
        new TestDataValidationUtil().createDataInventoryInOracle();
        getAuthenticationToken();
    }

    @Override
    public void onFinish(ISuite iSuite) {
        Map<String, ISuiteResult> results = iSuite.getResults();
        results.forEach((key, value) -> {
            if(value.getTestContext().getFailedTests().size() < 1){
                logger.info("Test has passed.");
            }
            else {
                setupEmail();
                emailSender.sendMessage(emailObject);
            }
        });
    }

    private void getAuthenticationToken() {
        accessToken = Util.getAuthenticationToken();

    }

    private void setupEmail() {
        context = new ClassPathXmlApplicationContext("testFiles/EmailBean.xml");
        emailSender = this.context.getBean(EmailSender.class);
        emailObject = new EmailMessage().getEmailObject();
    }
}