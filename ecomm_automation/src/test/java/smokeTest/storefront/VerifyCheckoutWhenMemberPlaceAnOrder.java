package smokeTest.storefront;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by Manas on 7/4/2017.
 */
public class VerifyCheckoutWhenMemberPlaceAnOrder extends BaseTest {
    private final static String DEFAULT_SEARCH_ITEM = "speaker";
    private final static String DEFAULT_PAYMENT_TYPE = "visa";

    private DesktopPageFactory pages;

    public VerifyCheckoutWhenMemberPlaceAnOrder(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Util.emptyUserCartWithSignIn(pages, new TestUser().getEmail(), new TestUser().getPassword());

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.smoketestSku);
        //pages.productlistingPage().getAvailableOnlineItem();

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4)
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 5)
    public void theUserContinueCheckoutAsMember(){
        TestUser testUser = new TestUser();
        pages.secureCheckoutPage().continueCheckoutAsMember(testUser);

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 6)
    public void theUserSubmitOrderAfterEnteringCreditCardSecurityCode(){
        pages.checkoutPage().fillInCIDNumber(DEFAULT_PAYMENT_TYPE);
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}