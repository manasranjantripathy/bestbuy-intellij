package smokeTest.storefront;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;

/**
 * Created by sucho on 6/30/2017.
 * <p>
 * verifyCheckout_when guest user reserves product in store
 */
public class VerifyCheckoutWhenGuestReservesProductInStore extends BaseTest {
    private final static String DEFAULT_SEARCH_ITEM = "speaker";

    private DesktopPageFactory pages;

    public VerifyCheckoutWhenGuestReservesProductInStore(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.smoketestSku);
        //pages.productlistingPage().getAvailableAtNearbyStoresItem();

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3)
    public void theUserClickOnReserveInStoreButtonFromProductDetailsPage() {
        pages.productDetailsPage().clickReserveInStoreButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.selectStorePopup().isAt(), "Select store popup is not displayed.");
    }

    @Test(priority = 4)
    public void theUserSearchForStoresByBCPostalCodeAndSelectOne() {
        pages.selectStorePopup().searchForPostalCode(new TestUser().getPostalCodeBC());
        pages.selectStorePopup().selectStore(1);

        Assert.assertTrue(pages.selectStorePopup().isSummaryDisplayed(), "Summary is not displayed.");
    }

    @Test(priority = 5)
    public void theUserClickOnReserveNowButtonAfterFillInContactInformation() {
        pages.selectStorePopup().continueToNextStep();
        pages.selectStorePopup().fillInContactInformation(new TestUser());
        pages.selectStorePopup().clickReserveNow();

        Assert.assertTrue(pages.selectStorePopup().isSuccessMessageDisplayed(), "Success message is not displayed.");
    }

}