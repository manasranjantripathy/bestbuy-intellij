package smokeTest.storefront;

import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
import utils.TestUser;
import utils.Util;


/**
 * Created by sucho on 6/14/2017.
 * <p>
 * Verify that a guest user is able to reserve a product from PDP
 */
public class VerifyCheckoutWhenGuestPlaceAnOrder extends BaseTest {
    private final static String DEFAULT_SEARCH_ITEM = "tv";
    private final static String DEFAULT_PAYMENT_TYPE = "mc";

    private DesktopPageFactory pages;

    public VerifyCheckoutWhenGuestPlaceAnOrder(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Step ("The user is in homepage and the cart is empty")
    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Step ("The user search for sku")
    @Test(priority = 2)
    public void theUserSearchForAvailableItem() {
        pages.headerFooter().searchFor(Constants.Sku.smoketestSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Step ("The user clicks on Add to Cart button")
    @Test(priority = 3)
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Step("The user clicks on checkout button")
    @Test(priority = 4)
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Step("The user continues checkout as guest")
    @Test(priority = 5)
    public void theUserContinueCheckoutAsGuest() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Step("The user continues shipping after fill in shipping information")
    @Test(priority = 6)
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation(new TestUser());

        pages.checkoutPage().clickContinueShippingButton();

        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Step("The user clicks continue payment button after fill in required information")
    @Parameters("testPaymentType")
    @Test(priority = 7)
    public void theUserClickContinuePaymentButtonAfterFillInRequiredInformation(@Optional(DEFAULT_PAYMENT_TYPE)final String testPaymentType) {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().fillInCreditCardInformation(testPaymentType);
        pages.checkoutPage().checkSameAsShipping();
        pages.checkoutPage().fillInConfirmationEmail(new TestUser());
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Step("The user clicks on submit order button")
    @Test(priority = 8)
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}