package utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * Created by sucho on 7/5/2017.
 *
 * what this has
 *      - skip following steps if there is any failure in preceding steps
 *      - set testPlatform, testEnvironment and testLanguage to display correct info on allure report
 *
 * what this doesn't have
 *      - test status updater on JIRA
 *
 *
 */
public class IntegrationListener implements ISuiteListener, ITestListener, IInvokedMethodListener {
    private boolean hasFailures = false;

    private final static Logger logger = Logger.getLogger(IntegrationListener.class);

    @Override
    public void onFinish(ISuite iSuite){
        Util.xmlParserForReRunningFailedTests("ecomm_checkout");

    }

    @Override
    public void onStart(ISuite iSuite){
        // set current date time
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        String suiteName = iSuite.getXmlSuite().getName();
        suiteName = suiteName + " " + localDateTime.format(formatter);
        iSuite.getXmlSuite().setName(suiteName);

// set testPlatform, testEnvironment and testLanguage to display correct info on allure report
        if(System.getProperty(Constants.Property.TEST_PLATFORM.toString()) != null ||
                System.getProperty(Constants.Property.TEST_ENVIRONMENT.toString()) != null ||
                System.getProperty(Constants.Property.TEST_LANGUAGE.toString())!= null) {
            HashMap<String, String> parameter = new HashMap<>();
            parameter.put(Constants.Property.TEST_PLATFORM.toString(), System.getProperty(Constants.Property.TEST_PLATFORM.toString()));
            parameter.put(Constants.Property.TEST_ENVIRONMENT.toString(), System.getProperty(Constants.Property.TEST_ENVIRONMENT.toString()));
            parameter.put(Constants.Property.TEST_LANGUAGE.toString(), System.getProperty(Constants.Property.TEST_LANGUAGE.toString()));
            iSuite.getXmlSuite().setParameters(parameter);
        }
    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }


    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        synchronized (this) {
            if (method.isTestMethod() && hasFailures) {
                throw new SkipException("Skipping this test");
            }
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (!method.isTestMethod()) {
            hasFailures = false;
            return;
        }
        if (!testResult.isSuccess()) {
            synchronized (this) {
                hasFailures = true;
            }
        }
    }

    @Override
    public void onTestStart(ITestResult result) {
    }

    @Override
    public void onTestSuccess(ITestResult result) {

    }

    @Override
    public void onTestFailure(ITestResult result) {
        Object testClass = result.getInstance();
        WebDriver driver = ((BaseTest) testClass).driver;

        if(driver instanceof WebDriver){
            Util.saveScreenshotPNG(driver);
        }

    }

    @Override
    public void onTestSkipped(ITestResult result) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

}
