package storefront.visaCheckout;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5774
 */

public class CheckoutVisaCheckoutMemberCustomerSelectingSavedCC extends BaseTest {

    private DesktopPageFactory pages;

    public CheckoutVisaCheckoutMemberCustomerSelectingSavedCC(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "Open storefront site.")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();
        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description="the user Sign in with default credential")
    public void theUserSignInToAccountWithDefaultCredential() {
        pages.headerFooter().clickSignInButton();
        pages.signInPage().signInAsDefaultCustomer();

        Assert.assertTrue(pages.headerFooter().isSignedIn(), "User is not Signed in");
    }


    @Test(priority = 3, description = "Search for default sku")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 4, description = "Click on Add to Cart button")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 5, description = "Click on Visa checkout button")
    public void theUserClicksOnVisaCheckoutButton() {
        pages.cartPage().clickVisaCheckoutButton();
        Assert.assertTrue(pages.visaCheckoutPopUp().isAt(), "Visa checkout pop up is not displayed.");
    }

    @Test(priority = 6, description = "Click on Visa checkout SignIn button")
    public void theUserClicksOnVisaCheckoutSignInButton() {
        pages.visaCheckoutPopUp().clickOnSignInLink();
    }

    @Test(priority = 7, description = "Registered user login to Visa Checkout")
    public void theUserLogInToVisaCheckout() {
        pages.visaCheckoutPopUp().visaCheckoutRegisteredUserLogin();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 8, description = "Select any CC from drop down and enter ")
    public void theUserSelectAnyCCByEnteringBillingAddressAndContinue() {
        pages.checkoutPage().clickChangePaymentType();
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().selectCreditCardFromDropDown(0);
        Assert.assertTrue(pages.checkoutPage().getSelectedCC().equalsIgnoreCase("VISA ************1111") || pages.checkoutPage().getSelectedCC().equalsIgnoreCase("MC ************2449"));
    }

    @Test(priority = 9, description = "the user enters the CID for the credit card and submit the order")
    public void theUserEntersCIDAndSubmitsOrder() {
        pages.checkoutPage().enterCIDAfterEditCC();
        pages.checkoutPage().clickContinuePaymentButton();
        pages.checkoutPage().clickSubmitOrderButton();
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}