package storefront.paypal;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5251
 */

public class VerifyPayPalAndGCWhenExistingCustomerOrdersBundleSKUWithPPAndGCAndPromo extends BaseTest{
    private DesktopPageFactory pages;

    public VerifyPayPalAndGCWhenExistingCustomerOrdersBundleSKUWithPPAndGCAndPromo(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user creates the new account")
    public void theUserCreatesNewAccount() {
        pages.headerFooter().navigateToCreateAccountPage();
        Assert.assertTrue(pages.createAccountPage().isAt(), "Create An Account page is not displayed");

        pages.createAccountPage().createRandomAccount();
        Assert.assertTrue(pages.accountSummaryPage().isAt(), "Account summary page is not displayed");
    }

    @Test(priority = 3, description = "the user logout from the account")
    public void theUserLogoutFromAccount() {
        pages.headerFooter().clickSignOutLink();

        Assert.assertTrue(pages.headerFooter().isSignedOut(), "User is not Signed out");
    }

    @Test(priority = 4, description = "the user searches for Bundle sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.bundleSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 5, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 6, description = "the user clicks on 'Checkout' button and navigates to Secure Checkout page")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure Checkout page is not displayed.");
    }

    @Test(priority = 7, description = "the user continue checkout as returning customer with newly created credentials")
    public void theUserContinueCheckoutAsReturningCustomer() {
        pages.secureCheckoutPage().continueCheckoutAsNewlyCreatedMember();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 8, description = "the user enters the shipping information and clicks continue button")
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation();
        pages.checkoutPage().clickContinueShippingButton();
    }

    @Test(priority = 9, description = "the user select PayPal method of payment")
    public void theUserSelectPalPalMethodOfPayment() {
        pages.checkoutPage().clickPayPalSelectButton();
        Assert.assertTrue(pages.checkoutPage().isPayPalPaymentSelected(), "PayPal method of payment is not selected");

        pages.checkoutPage().clickContinueToPayPal();
    }

    @Test(priority = 10, description = "the user login to PayPal and clicks Agree and Continue button")
    public void logInToPayPalWithValidCredential() {
        pages.paypalPage().loginToPayPal();
        Assert.assertTrue(pages.paypalPage().isAgreeAndContinueBtnDisplayed(), "Actual Url: " + driver.getCurrentUrl());

        pages.paypalPage().continuePayment();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 11, description = "the user verifies PayPal is displayed as method of payment")
    public void verifySubmitPageInformation() {
        Assert.assertTrue(pages.checkoutPage().isDeliveryInformationCorrectlyDisplayed());

        Assert.assertTrue(pages.checkoutPage().isPayPalDisplayedAsMethodOfPayment());
    }

    @Test(priority = 12, description = "the user adds the gift card to the payment ")
    public void theUserAddsGiftCard() {
        pages.checkoutPage().clickEditGCButton();

        Assert.assertTrue(pages.checkoutPage().isGiftCardSectionPresent());

        pages.checkoutPage().addGiftCard(2);
        pages.checkoutPage().checkGiftCardBalance();

        pages.checkoutPage().isExpandGCButtonDisplayed();

        pages.checkoutPage().isGiftCardBalanceNotZero();
        pages.checkoutPage().clickApplyGCToOrderButton();
    }

    @Test(priority = 13, description = "the user adds the Promo code to the order")
    public void theUserAddsThePromoCodeToOrder() {
        pages.checkoutPage().clickPromotionalCodeButton();
        pages.checkoutPage().addPromotionalCode(1);
        pages.checkoutPage().clickApplyPromoCodeButton();
        pages.checkoutPage().isPromoCodeApplied();
    }
    @Test(priority = 14, description = "the clicks on submit Order button and order is submitted successfully")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickContinuePaymentButton();
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}