package storefront.bundleScripts;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5623
 */

public class verifyBundleOrderTotalWhenOrderingBundleDiscountedNonConstituentBG extends BaseTest {
    private DesktopPageFactory pages;

    public verifyBundleOrderTotalWhenOrderingBundleDiscountedNonConstituentBG(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();
        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user searches for bundle sku and navigates to PDP page")
    public void theUserSearchForBundleSku() {
        pages.headerFooter().searchFor(Constants.Sku.bundleSku);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4, description = "the user searches for Simple discount sku and navigates to cart page")
    public void theUserSearchForSimpleDiscountSkuAndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.simpleDiscountSku);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }


    @Test(priority = 5, description = "the user verifies the simple discount is applied in Cart page")
    public void theUserVerifiesSimpleDiscountInCartPage() {
        Assert.assertTrue(pages.cartPage().isDiscountApplied(), "Discount is not displayed.");

    }


    @Test(priority = 7, description = "Click on checkout button")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();
        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 8, description = "the user continue checkout as member customer")
    public void theUserContinueCheckoutAsMember() {
        pages.secureCheckoutPage().continueCheckoutAsMember();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 9, description = "the user enters the CID for the credit card and submits the order")
    public void theUserEntersCIDAndSubmitsOrder() {
        driver.navigate().refresh();
        pages.checkoutPage().fillInCIDNumber("visa");
        pages.checkoutPage().clickSubmitOrderButton();
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }
}