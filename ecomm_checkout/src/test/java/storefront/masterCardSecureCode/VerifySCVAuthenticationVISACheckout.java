package storefront.masterCardSecureCode;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5097
 */

public class VerifySCVAuthenticationVISACheckout extends BaseTest {
    private DesktopPageFactory pages;

    public VerifySCVAuthenticationVISACheckout(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user searches for sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 5, description = "the user clicks on VisaCheckout and adds the Mastercard")
    public void theUserRegistersInVisaCheckout() {
        pages.cartPage().clickVisaCheckoutButton();
        pages.visaCheckoutPopUp().continueAsNewUser();
        pages.visaCheckoutPopUp().typeEmailAndContinue("bestbuy" + String.valueOf(Util.getRandomNumber()) + "@domain.com");
        pages.visaCheckoutPopUp().fillInUserInfoAndContinue();
        pages.visaCheckoutPopUp().fillInCardInfoAndContinue("5204740000001002", "123");
        pages.visaCheckoutPopUp().typePasswordAndSignIn();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed");
    }

    @Test(priority = 9, description = "the user clicks on submit Order button and navigates to MastercardSecureCode page")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.masterCardSecureCodePage().isAt(), "MastercardSecureCode page is not displayed. Actual Url: " +
                "" + driver.getCurrentUrl());
    }
}