package storefront.masterCardSecureCode;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5093
 * https://jira.ca.bestbuy.com/browse/EQS-5099
 */

public class VerifyOrderConfirmationSCVAuthenticationsRegisteredUser extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyOrderConfirmationSCVAuthenticationsRegisteredUser() {
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user has created an account")
    public void theUserCreatesRandomAccount() {
        pages.headerFooter().navigateToCreateAccountPage();
        Assert.assertTrue(pages.createAccountPage().isAt(), "Create An Account page is not displayed");

        pages.createAccountPage().createRandomAccount();
        Assert.assertTrue(pages.accountSummaryPage().isAt(), "Account summary page is not displayed");
    }

    @Test(priority = 4, description = "the user searches for default sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 5, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 6, description = "the user clicks on 'Checkout' button and navigates to checkout page")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 7, description = "the user enters the shipping information and clicks continue button")
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation();
        pages.checkoutPage().clickContinueShippingButton();

        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Test(priority = 8, description = "the user enters credit card details and clicks Continue button so that Submit " +
            "Order page is displayed")
    @Parameters("creditCardNumber")
    public void theUserClickContinuePaymentButtonAfterFillInRequiredInformation(@Optional("MandatoryVerification") final
                                                                                String creditCardNumber) {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().enterMCSecureCodeCard(creditCardNumber);
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Test(priority = 9, description = "the clicks on submit Order button and navigates to MastercardSecureCode page")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.masterCardSecureCodePage().isAt(), "MastercardSecureCode page is not displayed. Actual Url: " +
                "" + driver.getCurrentUrl());
    }

    @Test(priority = 10, description = "the user enters the CID and clicks Submit in the MastercardSecureCode page")
    public void theUserEntersCIDInSecureCodePage() {
        pages.masterCardSecureCodePage().fillInCIDNumber();

        pages.masterCardSecureCodePage().clickSubmit();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is nto displayed");
    }

    @Test(priority = 9, description = "the user verifies order confirmation after clicking Submit Order button")
    public void theUserSubmitTheOrder() {
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade(), "Order is not submitted");
    }
}