package storefront.multiLine;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5726
 */


public class verifyCreateMultiLineOrderCheckoutGuestUserPlaceOrderWithMultipleProductsInCart extends BaseTest {
    private DesktopPageFactory pages;

    public verifyCreateMultiLineOrderCheckoutGuestUserPlaceOrderWithMultipleProductsInCart(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();
        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku1AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku1);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 3, description = "Search for a multiLineSku2")
    public void theUserSearchFormultiLineSku2AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku2);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4, description = "Search for a multiLineSku3")
    public void theUserSearchFormultiLineSku3AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku3);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 5, description = "Search for a multiLineSku4")
    public void theUserSearchFormultiLineSku4AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku4);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 6, description = "Search for a multiLineSku5")
    public void theUserSearchFormultiLineSku5AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku5);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 7, description = "Search for a multiLineSku6")
    public void theUserSearchFormultiLineSku6AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku6);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 8, description = "Search for a multiLineSku")
    public void theUserSearchFormultiLineSku7AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku7);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 9, description = "Search for a multiLineSku")
    public void theUserSearchFormultiLineSku8AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku8);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 10, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku9AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku9);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 11, description = "Search for a multiLineSku")
    public void theUserSearchFormultiLineSku10AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku10);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 12, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku11AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku11);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 13, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku12AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku11);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 14, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku13AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku11);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 15, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku14AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku11);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 16, description = "Search for a multiLineSku1")
    public void theUserSearchFormultiLineSku15AndClicksOnAddToCartButton() {
        pages.headerFooter().searchFor(Constants.Sku.multiLineSku11);
        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
        pages.productDetailsPage().clickAddToCartButton();
        Util.popupHandler(pages, driver);
        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 17, description = "Click on checkout button")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();
        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure checkout page is not displayed.");
    }

    @Test(priority = 18, description = "checkout as guest customer by entering shipping address and continue")
    public void theUserCheckoutAsGuestCustomerByEnteringShippingAddressAndContine() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
        pages.checkoutPage().fillInShippingInformation();
        pages.checkoutPage().clickContinueShippingButton();
        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Test(priority = 19, description = "Select any CC by entering billing address and continue")
    public void theUserSelectAnyCCByEnteringBillingAddressAndContinue() {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().fillInCreditCardInformation("visa");
        pages.checkoutPage().checkSameAsShipping();
        pages.checkoutPage().fillInConfirmationGuestEmail();
        pages.checkoutPage().clickContinuePaymentButton();
        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Test(priority = 20, description = "Click on submit order & verify order is submitted")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade());
    }

}