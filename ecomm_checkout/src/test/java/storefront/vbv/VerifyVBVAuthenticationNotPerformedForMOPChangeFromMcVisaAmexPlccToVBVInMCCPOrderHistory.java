package storefront.vbv;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.TestUser;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5142
 * https://jira.ca.bestbuy.com/browse/EQS-5143
 * https://jira.ca.bestbuy.com/browse/EQS-5144
 * https://jira.ca.bestbuy.com/browse/EQS-5145
 */

public class VerifyVBVAuthenticationNotPerformedForMOPChangeFromMcVisaAmexPlccToVBVInMCCPOrderHistory extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyVBVAuthenticationNotPerformedForMOPChangeFromMcVisaAmexPlccToVBVInMCCPOrderHistory(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user searches for sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4, description = "the user clicks on 'Checkout' button and navigates to Secure Checkout page")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure Checkout page is not displayed.");
    }

    @Test(priority = 5, description = "the user continue checkout as Guest customer")
    public void theUserContinueCheckoutAsGuest() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 6, description = "the user enters the shipping information and clicks continue button")
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation();
        pages.checkoutPage().clickContinueShippingButton();

        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Test(priority = 7, description = "the user enters Master/Amex/Visa/PLCC card details and clicks Continue button " +
            "so that Submit Order page is displayed")
    @Parameters("cardType")
    public void theUserClickContinuePaymentButtonAfterFillInRequiredInformation(@Optional("mc") final String cardType) {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().fillInCreditCardInformation(cardType);
        pages.checkoutPage().checkSameAsShipping();
        pages.checkoutPage().fillInRandomEmail();
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Test(priority = 8, description = "the user submits the order by clicking on Submit Order button")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();
        Assert.assertTrue(pages.orderConfirmationPage().isAt(), "Order Confirmation page is not displayed");
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade(), "Order is not submitted");
    }

    @Test(priority = 9, description = "the user waits until the order is processed and creates new account by entering password")
    public void theUserWaitsForOrderProcessingAndCreatesNewAccount() {
        pages.orderConfirmationPage().waitForOrderProcessing();
        TestUser.orderNumber = pages.orderConfirmationPage().getOrderNumber();

        pages.orderConfirmationPage().createNewAccountWithPassword(TestUser.password);
        Assert.assertTrue(pages.accountSummaryPage().isAt(), "My Account page is not displayed. Actual Url: "+ driver
                .getCurrentUrl());
    }

    @Test(priority = 10, description = "the user clicks on order number in My Account and navigates to Order Details page")
    public void theUserSelectsTheOrderNumber() {
        pages.accountSummaryPage().clickOrderNumber(TestUser.orderNumber);

        Assert.assertTrue(pages.orderDetailsPage().isAt(), "Order Details page is nto displayed");
    }

    @Test(priority = 11, description = "the user clicks on Change Payment information link and navigates to Change Payment page")
    public void theUserClicksChangePaymentInformationLink() {
        pages.orderDetailsPage().clickChangePaymentInformation();

        Assert.assertTrue(pages.changePaymentPage().isAt());
    }

    @Test(priority = 12, description = "the User applies Visa VBV credit card to the payment")
    public void theUserAddsPLCCCreditCardToPayment() {
        pages.changePaymentPage().selectAddNewCreditCardRadioButton();
        Assert.assertTrue(pages.changePaymentPage().isAddNewCreditCardBlockDisplayed(), "Add new Credit card block is not displayed");
        pages.changePaymentPage().fillCreditCardInformation("Visa", "4111111111111111", "03", "2025");
        pages.changePaymentPage().fillInBillingInformation("V5K 0A1", "British Columbia");
    }

    @Test(priority = 13, description = "the user clicks Continue button and navigates to Order Change Confirmation page")
    public void theUserClicksContinueButton() {
        pages.changePaymentPage().clickContinue();

        Assert.assertTrue(pages.orderChangeConfirmationPage().isAt(), "Order Change Confirmation page is not " +
                "displayed. Actual Url:"+ driver.getCurrentUrl());
    }

    @Test(priority = 14, description = "the user clicks on Submit Change button and verifies VBV page is not " +
            "displayed and verifies the Payment Confirmation")
    public void theUserVerifiesPaymentChangeConfirmation() {
        pages.orderChangeConfirmationPage().clickSubmitChange();

        Assert.assertEquals(pages.orderDetailsPage().getTextConfirmationMessage(), Constants.Messages.PAYMENT_CHANGE_CONFIRMATION.toString());
    }
}