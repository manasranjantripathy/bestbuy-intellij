package storefront.vbv;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-5147
 * https://jira.ca.bestbuy.com/browse/EQS-5151
 * https://jira.ca.bestbuy.com/browse/EQS-5157
 * https://jira.ca.bestbuy.com/browse/EQS-5162
 */

public class VerifyOrderConfirmationAfterSuccessfulVBVAuthenticationForGuestUser extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyOrderConfirmationAfterSuccessfulVBVAuthenticationForGuestUser(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user searches for sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 3, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 4, description = "the user clicks on 'Checkout' button and navigates to Secure Checkout page")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.secureCheckoutPage().isAt(), "Secure Checkout page is not displayed.");
    }

    @Test(priority = 5, description = "the user continue checkout as Guest customer")
    public void theUserContinueCheckoutAsGuest() {
        pages.secureCheckoutPage().continueCheckoutAsGuest();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 6, description = "the user enters the shipping information and clicks continue button")
    public void TheUserContinueShippingAfterFillInShippingInformation() {
        pages.checkoutPage().fillInShippingInformation();
        pages.checkoutPage().clickContinueShippingButton();

        Assert.assertTrue(pages.checkoutPage().isCreditCardSelectionDisplayed(), "Credit card selection is not displayed.");
    }

    @Test(priority = 7, description = "the user enters credit card details and clicks Continue button so that Submit " +
            "Order page is displayed")
    @Parameters("creditCardNumber")
    public void theUserClickContinuePaymentButtonAfterFillInRequiredInformation(@Optional("4000000000000002") final String creditCardNumber) {
        pages.checkoutPage().clickCreditCardSelectButton();
        pages.checkoutPage().enterVBVCard(creditCardNumber);
        pages.checkoutPage().checkSameAsShipping();
        pages.checkoutPage().fillInRandomEmail();
        pages.checkoutPage().clickContinuePaymentButton();

        Assert.assertTrue(pages.checkoutPage().isReviewAndSubmitFormDisplayed(), "Review and submit form is not displayed.");
    }

    @Test(priority = 8, description = "the clicks on submit Order button and navigates to VBV page")
    public void theUserClicksOnSubmitOrderButton() {
        pages.checkoutPage().clickSubmitOrderButton();

        Assert.assertTrue(pages.verifiedByVisaPage().isAt(), "Verified by Visa page is not displayed. Actual Url: " +
                ""+driver.getCurrentUrl());
    }

    @Test(priority = 9, description = "the user enters the CID and clicks Submit in the Verifies By Visa page")
    public void theUserEntersCIDInVBVPage() {
        pages.verifiedByVisaPage().fillInCIDNumber();

        pages.verifiedByVisaPage().clickSubmit();
    }

    @Test(priority = 10, description = "the user verifies order confirmation")
    public void theUserSubmitTheOrder() {
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade(), "Order is not submitted");
    }
}