package storefront.vbv;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants;
import utils.Util;

/**
 * https://jira.ca.bestbuy.com/browse/EQS-9611
 */

public class VerifyOrderConfirmationAfterSuccessfulVBVAuthenticationForRegisteredUser extends BaseTest {
    private DesktopPageFactory pages;

    public VerifyOrderConfirmationAfterSuccessfulVBVAuthenticationForRegisteredUser(){
        TEST_PLATFORM = Constants.TestPlatform.STOREFRONT.toString();
        TEST_ENVIRONMENT = Constants.TestEnvironment.STOREFRONT_TEST03.toString();
        TEST_LANGUAGE = Constants.TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1, description = "the user is in home page and and has no item in the cart")
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2, description = "the user has created an account")
    public void theUserCreatesRandomAccount(){
        pages.headerFooter().navigateToCreateAccountPage();
        Assert.assertTrue(pages.createAccountPage().isAt(), "Create An Account page is not displayed");

        pages.createAccountPage().createRandomAccount();
        Assert.assertTrue(pages.accountSummaryPage().isAt(), "Account summary page is not displayed");
    }

    @Test(priority = 3, description = "the user adds the credit card to the account")
    public void theUserAddsCreditCardToAccount(){
        pages.accountSummaryPage().clickManageCreditCards();
        Assert.assertTrue(pages.manageCreditCardsPage().isAt());

        pages.manageCreditCardsPage().clickAddNewCreditCard();
        Assert.assertTrue(pages.addCreditCardPage().isAt());

        pages.addCreditCardPage().addCreditCardDetails("Visa", "4000000000000002", "02", "2025");
        pages.addCreditCardPage().fillAddressForCreditCard();
        pages.addCreditCardPage().clickSubmit();
        Assert.assertEquals(pages.addCreditCardPage().getConfirmMessage(), Constants.Messages.CONFIRM_MSG_ADDED_CC.toString());
    }

    @Test(priority = 4, description = "the user searches for default sku and navigates to PDP page")
    public void theUserSearchForSku() {
        pages.headerFooter().searchFor(Constants.Sku.defaultSku);

        Assert.assertTrue(pages.productDetailsPage().isAt(), "Product details page is not displayed.");
    }

    @Test(priority = 5, description = "the user clicks on 'Add to Cart' button and navigates to Cart page")
    public void theUserClicksOnAddToCartButton() {
        pages.productDetailsPage().clickAddToCartButton();

        Util.popupHandler(pages, driver);

        Assert.assertTrue(pages.cartPage().isAt(), "Cart page is not displayed.");
    }

    @Test(priority = 6, description = "the user clicks on 'Checkout' button and navigates to checkout page")
    public void theUserClickOnCheckout() {
        pages.cartPage().clickCheckoutButton();

        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed.");
    }

    @Test(priority = 7, description = "the user enters the CID for the credit card and clicks Submit Order button")
    public void theUserEntersCIDAndSubmitsOrder() {
        driver.navigate().refresh();
        pages.checkoutPage().fillInCIDNumber("visa");
        pages.checkoutPage().clickSubmitOrderButton();
        Assert.assertTrue(pages.verifiedByVisaPage().isAt(), "Verified by Visa page is not displayed");
    }

    @Test(priority = 8, description = "the user enters the CID and clicks Submit in the Verifies By Visa page")
    public void theUserEntersCIDInVBVPage() {
        pages.verifiedByVisaPage().fillInCIDNumber();
        pages.verifiedByVisaPage().clickSubmit();
        Assert.assertTrue(pages.checkoutPage().isAt(), "Checkout page is not displayed");
    }

    @Test(priority = 9, description = "the user verifies order confirmation after clicking Submit Order button")
    public void theUserSubmitTheOrder() {
        Assert.assertTrue(pages.orderConfirmationPage().isOrderSuccessfullyMade(), "Order is not submitted");
    }
}