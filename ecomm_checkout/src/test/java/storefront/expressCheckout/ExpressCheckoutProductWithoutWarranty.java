package storefront.expressCheckout;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;
/**
 * OC-1827
 *
 * Created by sucho on 10/5/2017.
 */
public class ExpressCheckoutProductWithoutWarranty extends BaseTest {
    private DesktopPageFactory pages;

    public ExpressCheckoutProductWithoutWarranty(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    @Parameters("testItem")
    public void theUserSearchesForItem(@Optional("10175595") final String testItem){
        pages.headerFooter().searchFor(testItem);

        Assert.assertTrue(!pages.productDetailsPage().isAt());
    }

    @Test(priority = 3)
    public void theUserAddItemToCart(){
        pages.productDetailsPage().clickAddToCartButton();
        pages.warrantyPopup().clickViewCartLink();

        Assert.assertFalse(pages.warrantyPopup().isExpressCheckoutDisplayed());
        Assert.assertTrue(pages.cartPage().isAt());
    }
}