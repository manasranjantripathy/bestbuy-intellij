package storefront.expressCheckout;

import org.testng.Assert;
import org.testng.annotations.*;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;

/**
 * Created by sucho on 10/5/2017.
 */
public class ExpressCheckoutIsOffProductWithWarranty extends BaseTest {
    private DesktopPageFactory pages;

    public ExpressCheckoutIsOffProductWithWarranty(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {
        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    @Parameters("testItem")
    public void theUserSearchesForItem(@Optional("10204251") final String testItem){
        pages.headerFooter().searchFor(testItem);

        Assert.assertTrue(pages.productDetailsPage().isAt());
    }

    @Test(priority = 3)
    public void theUserAddItemToCartWithWarranty_NoAcceptTerms(){
        pages.homePage().turnOffExpressCheckout();
        pages.productDetailsPage().clickAddToCartButton();

        Assert.assertFalse(pages.warrantyPopup().isExpressCheckoutDisplayed());
        Assert.assertTrue(pages.warrantyPopup().isWarrantyDisplayed());
    }

    @AfterClass
    public void revertExpressCheckout(){
        pages.homePage().turnOnExpressCheckout();
    }
}