package storefront.expressCheckout;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.DesktopPageFactory;
import utils.BaseTest;
import utils.Constants.TestEnvironment;
import utils.Constants.TestLanguage;
import utils.Constants.TestPlatform;


/**
 *
 * OC-1827
 *
 * Created by sucho on 10/5/2017.
 */
public class ExpressCheckoutProductWithWarranty extends BaseTest {
    private DesktopPageFactory pages;

    public ExpressCheckoutProductWithWarranty(){
        super.TEST_PLATFORM = TestPlatform.STOREFRONT.toString();
        super.TEST_ENVIRONMENT = TestEnvironment.STOREFRONT_TEST03.toString();
        super.TEST_LANGUAGE = TestLanguage.EN.toString();
    }

    @BeforeClass
    public void initPageFactory() {
        pages = new DesktopPageFactory(super.driver);
    }

    @Test(priority = 1)
    public void theUserIsInHomePageAndTheCartIsEmpty() {

        pages.homePage().openPage();

        Assert.assertTrue(pages.headerFooter().isCartEmpty(), "Cart is not empty");
    }

    @Test(priority = 2)
    @Parameters("testItem")
    public void theUserSearchesForItem(@Optional("10204251") final String testItem){
        pages.headerFooter().searchFor(testItem);

        Assert.assertTrue(pages.productDetailsPage().isAt());
    }

    @Test(priority = 3)
    public void theUserAddItemToCartWithWarranty_NoAcceptTerms(){
        pages.productDetailsPage().clickAddToCartButton();
        pages.warrantyPopup().clickAddWarrantyButton();

        Assert.assertTrue(pages.warrantyPopup().isErrorMessageDisplayed());
    }

    @Test(priority = 4)
    public void selectDifferentWarrantyOption(){

        pages.warrantyPopup().selectWarranty("10146735");
        Assert.assertTrue(pages.warrantyPopup().getSelectedWarrantyName().equals("2 Year iPad Geek Squad Protection Service Plan (InStore) for $79.99"));

    }

    @Test(priority = 5)
    public void theUserAddItemToCartWithWarranty_AcceptTerms(){
        pages.warrantyPopup().checkWarrantyAgreement();
        Assert.assertTrue(pages.warrantyPopup().isExpressCheckoutDisplayed());

        pages.warrantyPopup().clickAddWarrantyButton();
        Assert.assertTrue(pages.warrantyPopup().isWarrantyConfirmationMsgDisplayed());
    }

    @Test(priority = 6)
    public void theUserProceedCheckout(){
        pages.warrantyPopup().clickCheckout();

        Assert.assertTrue(pages.secureCheckoutPage().isAt());
    }

}